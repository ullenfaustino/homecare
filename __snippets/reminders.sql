select `services_reminders`.*,
`vehicles`.`odometer`,
MOD(vehicles.odometer, services_reminders.meter_interval) as mod_odometer,
`services_reminders`.`time_interval`,
DATEDIFF(NOW(), services_reminders.created_at) as date_diff,
TIMESTAMPDIFF(DAY, services_reminders.created_at, NOW()) as day_diff,
TIMESTAMPDIFF(WEEK, services_reminders.created_at, NOW()) as week_diff,
TIMESTAMPDIFF(MONTH, services_reminders.created_at, NOW()) as moth_diff,
TIMESTAMPDIFF(YEAR, services_reminders.created_at, NOW()) as year_diff,
MOD(TIMESTAMPDIFF(DAY, services_reminders.created_at, NOW()), services_reminders.time_interval) as mod_day
from `services_reminders`
inner join `vehicles`
on `vehicles`.`id` = `vehicle_id`
where not exists (select * from `reminder_logs` where reminder_logs.time_interval = TIMESTAMPDIFF(DAY, services_reminders.created_at, NOW()) and reminder_logs.time_interval_type = 'day' and reminder_logs.time_interval_type is not null)
#and MOD(TIMESTAMPDIFF(DAY, services_reminders.created_at, NOW()), services_reminders.time_interval) = 0
and (MOD(TIMESTAMPDIFF(DAY, services_reminders.created_at, NOW()), services_reminders.time_interval) = 0 OR MOD(TIMESTAMPDIFF(DAY, services_reminders.created_at, NOW()), services_reminders.time_interval) < services_reminders.time_interval)
and `services_reminders`.`time_interval` > 0
and `services_reminders`.`deleted_at` is null
and `services_reminders`.`deleted_at` is null



select `services_reminders`.*, `vehicles`.`odometer`, MOD(vehicles.odometer, services_reminders.meter_interval) as mod_odometer, TIMESTAMPDIFF(DAY, services_reminders.created_at, NOW()) as day_diff from `services_reminders` inner join `vehicles` on `vehicles`.`id` = `vehicle_id` where not exists (select * from `reminder_logs` where CASE services_reminders.time_interval_type WHEN "day" THEN reminder_logs.time_interval = TIMESTAMPDIFF(day, services_reminders.created_at, NOW()) AND reminder_logs.time_interval_type = "day" END) and MOD(TIMESTAMPDIFF(DAY, services_reminders.created_at, NOW()), services_reminders.time_interval) = 0 and `services_reminders`.`time_interval` > 0 and `services_reminders`.`deleted_at` is null and `services_reminders`.`deleted_at` is null
