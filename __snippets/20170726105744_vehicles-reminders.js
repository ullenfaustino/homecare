exports.up = function (knex, Promise) {
  const vechilesRenewalsTypesMigration = () => {
    return knex.schema.createTable('vehicle_renewal_types', (t) => {
      t.increments('id')

      t.string('name', 255)
      t.text('description')

      t.timestamps()
      t.string('created_by', 150)
      t.string('updated_by', 150)

      // soft delete
      t.specificType('is_deleted', 'tinyint').unsigned().defaultsTo(0)
      t.dateTime('deleted_at')
      t.string('deleted_by', 150)
    }).then(() => {
      const servicesTaskList = ['Emission Test', 'Inspection', 'Insurance', 'Registration']
      return new Promise.map(servicesTaskList, (servicesTask) => {
        return knex('vehicle_renewal_types').insert({
          name: servicesTask
        })
      })
    })
  }

  const vehiclesRenewalsMigration = () => {
    return knex.schema.createTable('vehicles_renewals', (t) => {
      t.increments('id')

      t.integer('vehicle_id').unsigned()
      t.integer('vehicle_renewal_type_id').unsigned()

      t.integer('time_interval').unsigned().defaultsTo(0)
      t.enu('time_interval_type', ['day', 'week', 'month', 'year'])
      t.specificType('is_email_notif', 'tinyint').unsigned().defaultsTo(0)

      t.timestamps()
      t.string('created_by', 150)
      t.string('updated_by', 150)

      // soft delete
      t.specificType('is_deleted', 'tinyint').unsigned().defaultsTo(0)
      t.dateTime('deleted_at')
      t.string('deleted_by', 150)
    })
  }

  // aside from subsribers, we will notify driver and current head of fleet
  const vehiclesRenewalsSubsribersMigration = () => {
    return knex.schema.createTable('vehicles_renewals_subscribers', (t) => {
      t.increments('id')

      t.integer('service_reminder_id').unsigned()
      t.string('subsriber_id', 150)

      t.timestamps()
      t.string('created_by', 150)
      t.string('updated_by', 150)

      // soft delete
      t.specificType('is_deleted', 'tinyint').unsigned().defaultsTo(0)
      t.dateTime('deleted_at')
      t.string('deleted_by', 150)
    })
  }

  return Promise.all([
    vechilesRenewalsTypesMigration(),
    vehiclesRenewalsMigration(),
    vehiclesRenewalsSubsribersMigration()
  ])
}

exports.down = function (knex, Promise) {
  return Promise.all([
    knex.schema.dropTable('vehicles_renewals'),
    knex.schema.dropTable('vehicle_renewal_types'),
    knex.schema.dropTable('vehicles_renewals_subscribers')
  ])
}
