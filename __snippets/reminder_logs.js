return knex.schema.createTable('reminder_logs', (t) => {
  t.increments('id')

  t.integer('vehicle_id').unsigned()
  t.integer('service_reminder_id').unsigned() // not so important
  t.integer('vehicle_renewal_id').unsigned() // not so importan

  t.integer('meter_interval').unsigned().defaultsTo(0)
  t.integer('time_interval').unsigned().defaultsTo(0)
  t.enu('time_interval_type', ['day', 'week', 'month', 'year'])
  t.specificType('is_email_notif', 'tinyint').unsigned().defaultsTo(0)

  t.timestamps()
  t.string('created_by', 150)
  t.string('updated_by', 150)

  // soft delete
  t.specificType('is_deleted', 'tinyint').unsigned().defaultsTo(0)
  t.dateTime('deleted_at')
  t.string('deleted_by', 150)
})
