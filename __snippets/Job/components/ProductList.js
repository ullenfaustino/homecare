import React, { Component, PropTypes } from 'react'
import { Button } from 'react-bootstrap'
import { Link } from 'react-router'

import * as Api from '../modules/api'

export default class JobList extends Component {
  constructor () {
    super()
    this.state = {
      jobs: []
    }
  }

  componentDidMount () {
    this.__fetchJobs()
  }

  __fetchJobs () {
    this.props.fetchJobs()
  }

  handleDeleteJob (jobId) {
    this.props.destroyJob(jobId).then(() => {
      this.__fetchJobs()
    })
  }

  render () {
    const { jobs } = this.props
    return (
      <div>
        <div className='row wrapper border-bottom white-bg page-heading'>
          <div className='col-md-8'>
            <h2>Job List</h2>
          </div>
          <div className='col-md-4'>
            <div className='pull-right' style={{ padding: 10 }}>
              {/* <Button bsStyle='default'>Some Actions</Button> */}
              { ' ' }
              <Link to={`/jobs/new`}><Button bsStyle='primary'>Add Job</Button></Link>
            </div>
          </div>
        </div>
        <div className='wrapper wrapper-content'>
          <div className='row'>
            <div className='col-md-12'>
              <div className='ibox float-e-margins'>
                <div className='ibox-title'>
                  <h5>Jobs </h5>
                </div>
                <div className='ibox-content'>

                  <table className='table table-striped'>
                    <thead>
                      <tr>
                        <th>#</th>
                        <th>Job Name</th>
                        <th>Model</th>
                        <th>Price</th>
                        <th>Quantity</th>
                        <th>Status</th>
                        <th>--</th>
                      </tr>
                    </thead>
                    <tbody>
                      {
                        jobs.map((job) => {
                          return (
                            <tr>
                              <td>{job.id}</td>
                              <td><Link to={`jobs/${job.id}/view`}>{job.name}</Link></td>
                              <td>{job.model}</td>
                              <td className='text-right'>{job.price}</td>
                              <td className='text-right'><span className='label label-primary'>{job.quantity}</span></td>
                              <td>Enabled</td>
                              <td>
                                <Link to={`jobs/${job.id}/edit`} className='btn btn-default'>
                                  edit
                                </Link>
                                {' '}
                                <Button bsStyle='danger' onClick={() => {
                                  this.handleDeleteJob(job.id)
                                }}>delete</Button>
                              </td>
                            </tr>
                          )
                        })
                      }
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    )
  }
}

JobList.propTypes = {}
