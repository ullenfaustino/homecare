import React, { Component, PropTypes } from 'react'
import { browserHistory } from 'react-router'

import JobForm from './JobForm'

export default class JobNew extends Component {
  constructor (props) {
    super(props)

    this.state = {
      currentJob: {},
      isJobSaving: false,
      isEdit: false
    }
  }

  componentDidMount () {
    if (this.props.params.id) {
      this.setState({
        isEdit: true
      }, () => {
        this.props.fetchJobById(this.props.params.id)
      })
    }
  }

  componentWillReceiveProps (nextProps) {
    if (this.state.isEdit && nextProps.params.id) {
      if (this.state.currentJob != nextProps.currentJob) {
        if (!this.state.isJobSaving) {
          this.setState({
            currentJob: nextProps.currentJob
          })
        }
      }
    } else {
      this.setState({
        isEdit: false,
        currentJob: {}
      })
    }
  }

  render () {
    const { fetchByIdLoading } = this.props
    const { currentJob, isEdit } = this.state

    if (isEdit) {
      if (!currentJob || fetchByIdLoading) {
        return <div style={{ padding: 25 }}><h3>Loading...</h3></div>
      }
    }

    const tagsJobKeyList = ['jobType', 'modelType', 'makeType', 'bodyType', 'bodySubType', 'engine_aspiration']
    const formState = _.chain(currentJob)
      .mapValues(function (value, key) {
        if (tagsJobKeyList.indexOf(key) > -1) {
          if (value && value.id) {
            return {
              value: Object.assign({}, value, {
                value: value.id,
                label: value.tag
              })
            }
          }

          return { value: {} }
        }
        return { value: value || '' }
      }).value()
    return (
      <div>
        <div className='wrapper wrapper-content'>

          <JobForm
            formRef={formRef => this.formRef = formRef}
            formState={formState}
            isSaving={this.state.isJobSaving}
            onFieldsChange={(fields) => {
              // console.log('onFieldsChange', fields)
              // this.setState({
              //   currentJob: { ...this.state.currentJob, ...fields }
              // })
            }}
            onSubmit={(values) => {
              this.setState({
                isJobSaving: true,
                isEdit: false,
                currentJob: undefined
              }, () => {
                if (isEdit) {
                  this.props.updateJob(values).then((job) => {
                    browserHistory.push(`/jobs/${job.id}/view`)
                  })
                } else {
                  this.props.createJob(values).then((job) => {
                    browserHistory.push(`/jobs/${job.id}/view`)
                  })
                }
              })
            }} />

        </div>
      </div>
    )
  }
}

JobNew.propTypes = {}
