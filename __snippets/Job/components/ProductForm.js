import React, { Component, PropTypes } from 'react'
import { Button, Checkbox, Col, Form, FormControl, FormGroup, ControlLabel, HelpBlock, Tab, Pane, FieldGroup, Row, Nav, NavItem, Label } from 'react-bootstrap'
import { createForm } from 'rc-form'
import _ from 'lodash'
import { Link } from 'react-router'

class JobForm extends Component {
  constructor (props) {
    super(props)
  }

  submit = () => {
    this.props.form.validateFields((error, values) => {
      if (!error) {
        if (this.props.onSubmit) {
          this.props.onSubmit(values)
        }
      }
    })
  }

  render () {
    let errors
    const {
      isSaving,
      formRef
    } = this.props

    const { getFieldDecorator, getFieldProps, getFieldError, getFieldsError, isFieldTouched, getFieldValue, setFieldsValue } = this.props.form

    function getValidationState (field) {
      if (isFieldTouched(field)) {
        if (getFieldError(field)) {
          return 'error'
        } else {
          return 'success'
        }
      } else {
        return null
      }
    }

    function toNumber (v) {
      if (v === undefined) {
        return v
      }
      if (v === '') {
        return undefined
      }
      return Number(v)
    }

    return (
      <Form ref={formRef} horizontal onSubmit={(ev) => {
        ev.preventDefault()
        this.submit()
      }}>
        <div className='row'>
          <div className='col-md-12'>
            <div className='ibox float-e-margins'>
              <div className='ibox-content'>
                <h3>Job Details</h3>
                <hr />
                <h3>Identification</h3>
                <input type='hidden' {...getFieldProps('id')} />
                <FormGroup controlId='name' validationState={getFieldError('name') ? 'error' : getValidationState('name')}>
                  <Col componentClass={ControlLabel} sm={2}>Vehicle Name <span className='text-danger'>*</span></Col>
                  <Col sm={8}>
                    {
                      getFieldDecorator('name', {
                        initialValue: '',
                        rules: [{
                          required: true,
                          message: 'This is a required field.'
                        }]
                      })(<input className='form-control' />)
                    }
                    {(errors = getFieldError('name')) ? <HelpBlock>{errors.join(',')}</HelpBlock> : null}
                  </Col>
                </FormGroup>

                <FormGroup>
                  <Col componentClass={ControlLabel} sm={2} />
                  <Col sm={8}>
                    <Link to={`/jobs`} className='btn btn-default'>
                      cancel
                    </Link>
                    { ' ' }
                    <Button type='submit' bsStyle='primary' disabled={isSaving}>{isSaving ? 'Submit...' : 'Save'}</Button>
                  </Col>
                </FormGroup>
              </div>
            </div>
          </div>
        </div>
      </Form>
    )
  }
}

JobForm.propTypes = {}

export default createForm({
  mapPropsToFields (props) {
    return props.formState
  },
  onFieldsChange (props, fields) {
    if (props.onFieldsChange) {
      props.onFieldsChange(_.mapValues(fields, function (o, key) {
        return o.value || undefined
      }))
    }
  }
})(JobForm)
