import { injectReducer } from '../../store/reducers'
import { injectSagas } from '../../store/sagas'

export default (store) => ({
  path : 'jobs',
  /*  Async getComponent is only invoked when route matches   */
  getComponent (nextState, cb) {
    /*  Webpack - use 'require.ensure' to create a split point
        and embed an async module loader (jsonp) when bundling   */
    require.ensure([], (require) => {
      /*  Webpack - use require callback to define
          dependencies for bundling   */
      const Job = require('./containers/JobContainer').default
      const reducer = require('./modules/job').default
      // const sagas = require('./modules/category').sagas
      //
      // /*  Add the reducer to the store on key 'counter'  */
      injectReducer(store, { key: 'job', reducer })
      // injectSagas(store, { key: 'category', sagas })

      /*  Return getComponent   */
      cb(null, Job)

    /* Webpack named bundle   */
    }, 'jobs')
  },
  getChildRoutes (location, next) {
    require.ensure([], (require) => {
      next(null, [
        {
          path : 'new',
          getComponent (nextState, next) {
            require.ensure(['./containers/JobNewContainer'], (require) => {
              const Container = require('./containers/JobNewContainer').default
              next(null, Container)
            })
          }
        },
        {
          path : ':id/edit',
          getComponent (nextState, next) {
            require.ensure(['./containers/JobNewContainer'], (require) => {
              const Container = require('./containers/JobNewContainer').default
              next(null, Container)
            })
          }
        },
        {
          path : ':id/view',
          getComponent (nextState, next) {
            require.ensure(['./containers/JobViewContainer'], (require) => {
              const Container = require('./containers/JobViewContainer').default
              next(null, Container)
            })
          }
        }
      ])
    })
  }
})
