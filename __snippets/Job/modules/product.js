import { takeLatest } from 'redux-saga'
import { take, put, select, call, fork } from 'redux-saga/effects'
import * as Api from './api'
import _ from 'lodash'
import { browserHistory } from 'react-router'

// ------------------------------------
// Constants
// ------------------------------------
const PAGINATION_DEF = {
  page: 1,
  pageSize: 10,
  searchText: '',
  sortName: undefined,
  sortOrder: undefined
}

export const JOB_FETCH_REQUESTED = 'JOB_FETCH_REQUESTED'
export const JOB_FETCH_SUCCEEDED = 'JOB_FETCH_SUCCEEDED'
export const JOB_FETCH_FAILED = 'JOB_FETCH_FAILED'

export const JOB_CREATE_REQUESTED = 'JOB_CREATE_REQUESTED'
export const JOB_CREATE_SUCCEEDED = 'JOB_CREATE_SUCCEEDED'
export const JOB_CREATE_FAILED = 'JOB_CREATE_FAILED'

export const JOB_UPDATE_REQUESTED = 'JOB_UPDATE_REQUESTED'
export const JOB_UPDATE_SUCCEEDED = 'JOB_UPDATE_SUCCEEDED'
export const JOB_UPDATE_FAILED = 'JOB_UPDATE_FAILED'

export const JOB_FETCH_BY_ID_REQUESTED = 'JOB_FETCH_BY_ID_REQUESTED'
export const JOB_FETCH_BY_ID_SUCCEEDED = 'JOB_FETCH_BY_ID_SUCCEEDED'
export const JOB_FETCH_BY_ID_FAILED = 'JOB_FETCH_BY_ID_FAILED'

// ------------------------------------
// Actions
// ------------------------------------
export function fetchJobs (data) {
  return (dispatch) => {
    dispatch({
      type: JOB_FETCH_REQUESTED
    })
    return Api.fetchJobs(data).then((payload) => {
      const { data: jobs, recordsTotal, recordsFiltered } = payload
      const params = Object.assign({}, data || {}, ...PAGINATION_DEF)

      dispatch({
        type: JOB_FETCH_SUCCEEDED,
        params,
        jobs
        // recordsTotal,
        // recordsFiltered
      })
      return payload
    })
  }
}

export function createJob (data) {
  return (dispatch) => {
    dispatch({ type: JOB_CREATE_REQUESTED, data })
    return Api.createJob(data).then((payload) => {
      dispatch({ type: JOB_CREATE_SUCCEEDED, data: payload })
      return payload
    })
  }
}

export function updateJob (data) {
  return (dispatch) => {
    dispatch({ type: JOB_UPDATE_REQUESTED, data })
    return Api.updateJob(data.id, data).then((payload) => {
      dispatch({
        type: JOB_UPDATE_SUCCEEDED,
        job: payload
      })
      return payload
    })
  }
}

export function destroyJob (jobId) {
  return (dispatch) => {
    // dispatch({ type: JOB_FETCH_BY_ID_REQUESTED, id: jobId })
    return Api.destroyJob(jobId).then((payload) => {
      // dispatch({
      //   type: JOB_FETCH_BY_ID_SUCCEEDED,
      //   job: payload
      // })
      return payload
    })
  }
}

export function fetchJobById (jobId) {
  return (dispatch) => {
    dispatch({ type: JOB_FETCH_BY_ID_REQUESTED, id: jobId })
    return Api.fetchJobById(jobId).then((payload) => {
      dispatch({
        type: JOB_FETCH_BY_ID_SUCCEEDED,
        job: payload
      })
      return payload
    })
  }
}

export const actions = {
  fetchJobs,
  createJob,
  updateJob,
  destroyJob,
  fetchJobById
}

// ------------------------------------
// Action Handlers
// ------------------------------------
const ACTION_HANDLERS = {
  [JOB_FETCH_REQUESTED] : (state, action) => Object.assign({}, state, {
    isFetchLoading: true
  }),
  [JOB_FETCH_SUCCEEDED] : (state, action) => Object.assign({}, state, {
    jobs: action.jobs,
    isFetchLoading: false,
    page: action.params.page,
    pageSize: action.params.pageSize,
    searchText: action.params.searchText,
    sortName: action.params.sortName,
    sortOrder: action.params.sortOrder
  }),
  [JOB_FETCH_BY_ID_REQUESTED] : (state, action) => Object.assign({}, state, {
    fetchByIdLoading: true
  }),
  [JOB_FETCH_BY_ID_SUCCEEDED] : (state, action) => Object.assign({}, state, {
    currentJob: action.job,
    fetchByIdLoading: false
  })
}

// ------------------------------------
// Reducer
// ------------------------------------
const initialState = {
  jobs: [],
  isFetchLoading: false,

  currentJob: undefined,
  fetchByIdLoading: false,

  page: 1,
  pageSize: 10,
  searchText: '',
  sortName: undefined,
  sortOrder: undefined
}

export default function jobReducer (state = initialState, action) {
  const handler = ACTION_HANDLERS[action.type]
  return handler ? handler(state, action) : state
}

// ------------------------------------
// Sagas
// ------------------------------------

export const sagas = []
