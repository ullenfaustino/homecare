import axios from 'axios'

import { URL } from 'consts/apiConsts'

function parseJSON (response) {
  return response.data.data || response.data
}

export function fetchJobs (params) {
  return axios.get(`${URL}/jobs`, {
    params: params
  }).then((response) => response.data)
}

export function createJob (data) {
  return axios.post(`${URL}/jobs`, data).then(parseJSON)
}

export function updateJob (jobId, data) {
  return axios.put(`${URL}/jobs/${jobId}`, data).then(parseJSON)
}

export function destroyJob (jobId) {
  return axios.delete(`${URL}/jobs/${jobId}`).then(parseJSON)
}

export function fetchJobById (jobId) {
  return axios.get(`${URL}/jobs/${jobId}`).then(parseJSON)
}
