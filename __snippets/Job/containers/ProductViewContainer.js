import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Link } from 'react-router'

import {
  actions
} from '../modules/job'

import JobDetails from '../components/JobDetails'

class JobViewContainer extends Component {

  render () {
    return this.props.children || <JobDetails {...this.props} />
  }
}

const mapDispatchToProps = { ...actions }

const mapStateToProps = (state) => ({
  ...state.job
})

export default connect(mapStateToProps, mapDispatchToProps)(JobViewContainer)
