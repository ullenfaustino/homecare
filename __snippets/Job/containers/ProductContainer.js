import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Link } from 'react-router'

import {
  actions
} from '../modules/job'

import JobList from '../components/JobList'

class JobContainer extends Component {

  render () {
    return this.props.children || <JobList {...this.props} />
  }
}

const mapDispatchToProps = {
  ...actions
}

const mapStateToProps = (state) => ({
  jobs : state.job.jobs
})

export default connect(mapStateToProps, mapDispatchToProps)(JobContainer)
