import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Link } from 'react-router'

import {
  actions
} from '../modules/job'

import JobNew from '../components/JobNew'

class JobNewContainer extends Component {

  render () {
    return this.props.children || <JobNew {...this.props} />
  }
}

const mapDispatchToProps = { ...actions }

const mapStateToProps = (state) => ({
  ...state.job
})

export default connect(mapStateToProps, mapDispatchToProps)(JobNewContainer)
