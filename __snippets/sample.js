exports.assignDriver = (req, res, next) => {
  const { id } = req.params
  const { driver_ids = [] } = req.body

  dataProvider.Vehicle.where('id', id)
  .fetch({ require: true })
  .then((vehicle) => {
    return dataProvider.Drivers.query((qb) => {
      qb.whereIn('id', driver_ids)
      qb.whereNotExists(function () {
        this.select('*')
        .from('vehicle_drivers')
        .where('vehicle_id', vehicle.get('id'))
        .whereRaw('vehicle_drivers.driver_id = drivers.id')
      })
    })
    .fetchAll()
    .then((drivers) => drivers.toJSON())
    .then((drivers) => {
      const driverIdList = drivers.map((driver) => driver.id)

      return new Promise.mapSeries(driverIdList, (driverId) => {
        return dataProvider.VehicleDrivers.forge()
        .save({
          vehicle_id: vehicle.get('id'),
          driver_id: driverId
        })
        .then((nuDriver) => nuDriver.toJSON())
      }).then((nuDrivers) => {
        res.send(ApiUtils.apiResponse(
          nuDrivers
        ))
      })
    })
  })
  .catch(dataProvider.Vehicle.NotFoundError, () => {
    return next(new errors.NotFoundError('Vehicle not found.'))
  })
  .catch((error) => { return next(error) })
}
