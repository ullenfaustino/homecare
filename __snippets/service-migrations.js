const servicesTask = [
  {
    'id': 1,
    'name': 'A/C Compressor Replacement'
  },
  {
    'id': 2,
    'name': 'A/C Condenser Replacement'
  },
  {
    'id': 3,
    'name': 'A/C Diagnosis'
  },
  {
    'id': 4,
    'name': 'A/C Evaporator Replacement'
  },
  {
    'id': 5,
    'name': 'A/C Recharge'
  },
  {
    'id': 6,
    'name': 'Air Filter Replacement'
  },
  {
    'id': 7,
    'name': 'Alternator Replacement'
  },
  {
    'id': 8,
    'name': 'Ball Joint Replacement'
  },
  {
    'id': 9,
    'name': 'Battery Inspection'
  },
  {
    'id': 10,
    'name': 'Battery Replacement'
  },
  {
    'id': 11,
    'name': 'Brake Caliper Replacement'
  },
  {
    'id': 12,
    'name': 'Brake Inspection'
  },
  {
    'id': 13,
    'name': 'Brake Pad(s) Replacement'
  },
  {
    'id': 14,
    'name': 'Car Wash'
  },
  {
    'id': 15,
    'name': 'Catalytic Converter Replacement'
  },
  {
    'id': 16,
    'name': 'Charging System Diagnosis'
  },
  {
    'id': 17,
    'name': 'Door Window Motor/Regulator Replacement'
  },
  {
    'id': 18,
    'name': 'Engine Belt Inspection'
  },
  {
    'id': 19,
    'name': 'Engine Coolant Replacement'
  },
  {
    'id': 20,
    'name': 'Engine/Drive Belt(s) Replacement'
  },
  {
    'id': 21,
    'name': 'Exhaust Manifold Replacement'
  },
  {
    'id': 22,
    'name': 'Factory Scheduled Maintenance'
  },
  {
    'id': 23,
    'name': 'Fuel Injector Replacement'
  },
  {
    'id': 24,
    'name': 'Fuel Pump Replacement'
  },
  {
    'id': 25,
    'name': 'Head Gasket(s) Replacement'
  },
  {
    'id': 26,
    'name': 'Heater Blower Motor Replacement'
  },
  {
    'id': 27,
    'name': 'Heater Control Valve Replacement'
  },
  {
    'id': 28,
    'name': 'Heater Core Replacement'
  },
  {
    'id': 29,
    'name': 'Heater Hose Replacement'
  },
  {
    'id': 30,
    'name': 'Ignition Coil Replacement'
  },
  {
    'id': 31,
    'name': 'Intake Manifold Gasket Replacement'
  },
  {
    'id': 32,
    'name': 'Oil Change'
  },
  {
    'id': 33,
    'name': 'Oil Pump Replacement'
  },
  {
    'id': 34,
    'name': 'Other Maintenance'
  },
  {
    'id': 35,
    'name': 'Oxygen Sensor Replacement'
  },
  {
    'id': 36,
    'name': 'Power Steering Hose Replacement'
  },
  {
    'id': 37,
    'name': 'Power Steering Pump Replacement'
  },
  {
    'id': 38,
    'name': 'Radiator Repair'
  },
  {
    'id': 39,
    'name': 'Resurface Rotors'
  },
  {
    'id': 40,
    'name': 'Rotate Tires'
  },
  {
    'id': 41,
    'name': 'Rotor Replacement'
  },
  {
    'id': 42,
    'name': 'Spark Plug Replacement'
  },
  {
    'id': 43,
    'name': 'Starter Replacement'
  },
  {
    'id': 44,
    'name': 'Thermostat Replacement'
  },
  {
    'id': 45,
    'name': 'Tie Rod End Replacement'
  },
  {
    'id': 46,
    'name': 'Tire Replacement'
  },
  {
    'id': 47,
    'name': 'Tire Service'
  },
  {
    'id': 48,
    'name': 'Transmission Filter Replacement'
  },
  {
    'id': 49,
    'name': 'Transmission Fluid Replacement'
  },
  {
    'id': 50,
    'name': 'Transmission Replacement'
  }
]
