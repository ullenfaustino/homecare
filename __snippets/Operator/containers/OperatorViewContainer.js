import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Link } from 'react-router'

import {
  actions
} from '../modules/operator'

import OperatorDetails from '../components/OperatorDetails'

class OperatorViewContainer extends Component {

  render () {
    return this.props.children || <OperatorDetails {...this.props} />
  }
}

const mapDispatchToProps = { ...actions }

const mapStateToProps = (state) => ({
  ...state.operator
})

export default connect(mapStateToProps, mapDispatchToProps)(OperatorViewContainer)
