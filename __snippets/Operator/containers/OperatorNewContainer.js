import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Link } from 'react-router'

import {
  actions
} from '../modules/operator'

import OperatorNew from '../components/OperatorNew'

class OperatorNewContainer extends Component {

  render () {
    return this.props.children || <OperatorNew {...this.props} />
  }
}

const mapDispatchToProps = { ...actions }

const mapStateToProps = (state) => ({
  ...state.operator
})

export default connect(mapStateToProps, mapDispatchToProps)(OperatorNewContainer)
