import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Link } from 'react-router'

import {
  actions
} from '../modules/operator'

import OperatorList from '../components/OperatorList'

class OperatorContainer extends Component {

  render () {
    return this.props.children || <OperatorList {...this.props} />
  }
}

const mapDispatchToProps = {
  ...actions
}

const mapStateToProps = (state) => ({ ...state.operator })

export default connect(mapStateToProps, mapDispatchToProps)(OperatorContainer)
