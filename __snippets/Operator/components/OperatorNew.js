import React, { Component, PropTypes } from 'react'
import { browserHistory } from 'react-router'

import OperatorForm from './OperatorForm'

export default class OperatorNew extends Component {
  constructor (props) {
    super(props)

    this.state = {
      currentOperator: {},
      isOperatorSaving: false,
      isEdit: false
    }
  }

  componentDidMount () {
    if (this.props.params.id) {
      this.setState({
        isEdit: true
      }, () => {
        this.props.fetchOperatorById(this.props.params.id)
      })
    }
  }

  componentWillReceiveProps (nextProps) {
    if (this.state.isEdit && nextProps.params.id) {
      if (this.state.currentOperator != nextProps.currentOperator) {
        if (!this.state.isOperatorSaving) {
          this.setState({
            currentOperator: nextProps.currentOperator
          })
        }
      }
    } else {
      this.setState({
        isEdit: false,
        currentOperator: {}
      })
    }
  }

  render () {
    const { fetchByIdLoading } = this.props
    const { currentOperator = {}, isEdit } = this.state

    if (isEdit) {
      if (!currentOperator || fetchByIdLoading) {
        return <div style={{ padding: 25 }}><h3>Loading...</h3></div>
      }
    }

    const formState = _.chain(currentOperator)
      .mapValues(function (value, key) {
        return { value: value || '' }
      }).value()
    return (
      <div>
        <div className='wrapper wrapper-content'>

          <OperatorForm
            formRef={formRef => this.formRef = formRef}
            formState={formState}
            isSaving={this.state.isOperatorSaving}
            onFieldsChange={(fields) => {
              // console.log('onFieldsChange', fields)
              // this.setState({
              //   currentOperator: { ...this.state.currentOperator, ...fields }
              // })
            }}
            onSubmit={(values) => {
              this.setState({
                isOperatorSaving: true,
                isEdit: false,
                currentOperator: undefined
              }, () => {
                if (isEdit) {
                  this.props.updateOperator(values).then((operator) => {
                    browserHistory.push(`/operators/${operator.id}/view`)
                  })
                } else {
                  this.props.createOperator(values).then((operator) => {
                    browserHistory.push(`/operators/${operator.id}/view`)
                  })
                }
              })
            }} />

        </div>
      </div>
    )
  }
}

OperatorNew.propTypes = {}
