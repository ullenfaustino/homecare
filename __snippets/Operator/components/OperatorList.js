import React, { Component, PropTypes } from 'react'
import { Button } from 'react-bootstrap'
import { Link } from 'react-router'
import { BootstrapTable, TableHeaderColumn } from 'react-bootstrap-table'

import * as Api from '../modules/api'

export default class OperatorList extends Component {
  constructor () {
    super()
    this.state = {
      operators: []
    }
  }

  componentDidMount () {
    this.__fetchOperators()
  }

  __fetchOperators (params = {}) {
    this.props.fetchOperators(params)
  }

  handleDeleteOperator (operatorId) {
    this.props.destroyOperator(operatorId).then(() => {
      this.__fetchOperators()
    })
  }

  onPageChange = (page, sizePerPage) => {
    this.__fetchOperators({
      pageSize: sizePerPage,
      page
    })
  }

  onSearchChange = (searchText, colInfos, multiColumnSearch) => {
    const text = searchText.trim()
    this.__fetchOperators({
      page:1,
      searchText: text
    })
  }

  onFilterChange = (filterObj) => {
    const { value } = filterObj.status !== undefined ? filterObj.status : filterObj.type !== undefined ? filterObj.type : ''
    this.__fetchOperators({
      page:1,
      searchText: value
    })
  }

  onSizePerPageList = (sizePerPage) => {
    this.__fetchOperators({
      pageSize: sizePerPage,
      page: 1
    })
  }

  onSortChange = (sortName, sortOrder) => {
    this.__fetchOperators({
      page: 1,
      sortName,
      sortOrder
    })
  }

  dataActionFormat = (operator) => {
    return (
      <div>
        <Link to={`operators/${operator.id}/edit`} className='btn btn-default'>
          edit
        </Link>
        {' '}
        <Button bsStyle='danger' onClick={() => {
          this.handleDeleteOperator(operator.id)
        }}>delete</Button>
      </div>
    )
  }

  render () {
    const { operators = [], pagination = {} } = this.props
    const { pageSize } = pagination
    const options = {
      // insertBtn: this.createCustomInsertButton,
      onPageChange: this.onPageChange,
      onSearchChange: this.onSearchChange,
      onSortChange: this.onSortChange,
      page: pagination.page,
      sizePerPage: pageSize,
      clearSearch: true,
      onFilterChange: this.onFilterChange,
      toolBar: () => null
    }
    const fetchInfo = { dataTotalSize: pagination.rowCount }

    return (
      <div>
        <div className='row wrapper border-bottom white-bg page-heading'>
          <div className='col-md-8'>
            <h2>Operator List</h2>
          </div>
          <div className='col-md-4'>
            <div className='pull-right' style={{ padding: 10 }}>
              {/* <Button bsStyle='default'>Some Actions</Button> */}
              { ' ' }
              <Link to={`/operators/new`}><Button bsStyle='primary'>Add Operator</Button></Link>
            </div>
          </div>
        </div>
        <div className='wrapper wrapper-content'>
          <div className='row'>
            <div className='col-md-12'>
              <div className='ibox float-e-margins'>
                <div className='ibox-title'>
                  <h5>Operators </h5>
                </div>
                <div className='ibox-content'>
                  <BootstrapTable data={operators} options={options} fetchInfo={fetchInfo} striped search pagination remote insertRow>
                    <TableHeaderColumn isKey dataField='id' hidden>ID</TableHeaderColumn>
                    <TableHeaderColumn dataField='name' dataSort
                      dataFormat={(cell, operator) => {
                        return <Link to={`operators/${operator.id}/view`}>{operator.name}</Link>
                      }}>Name</TableHeaderColumn>
                    <TableHeaderColumn dataField='email' dataSort>Email</TableHeaderColumn>
                    <TableHeaderColumn dataField='telephone' dataSort>Telephone</TableHeaderColumn>
                    <TableHeaderColumn dataField='fax' dataSort>Fax</TableHeaderColumn>
                    <TableHeaderColumn dataField='status' dataSort>Status</TableHeaderColumn>
                    <TableHeaderColumn dataField='id' width={150}
                      dataFormat={(cell, row) => this.dataActionFormat(row)}>--</TableHeaderColumn>
                  </BootstrapTable>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    )
  }
}

OperatorList.propTypes = {}
