import { injectReducer } from '../../store/reducers'
import { injectSagas } from '../../store/sagas'

export default (store) => ({
  path : 'operators',
  /*  Async getComponent is only invoked when route matches   */
  getComponent (nextState, cb) {
    /*  Webpack - use 'require.ensure' to create a split point
        and embed an async module loader (jsonp) when bundling   */
    require.ensure([], (require) => {
      /*  Webpack - use require callback to define
          dependencies for bundling   */
      const Operator = require('./containers/OperatorContainer').default
      const reducer = require('./modules/operator').default
      // const sagas = require('./modules/category').sagas
      //
      // /*  Add the reducer to the store on key 'counter'  */
      injectReducer(store, { key: 'operator', reducer })
      // injectSagas(store, { key: 'category', sagas })

      /*  Return getComponent   */
      cb(null, Operator)

    /* Webpack named bundle   */
    }, 'operators')
  },
  getChildRoutes (location, next) {
    require.ensure([], (require) => {
      next(null, [
        {
          path : 'new',
          getComponent (nextState, next) {
            require.ensure(['./containers/OperatorNewContainer'], (require) => {
              const Container = require('./containers/OperatorNewContainer').default
              next(null, Container)
            })
          }
        },
        {
          path : ':id/edit',
          getComponent (nextState, next) {
            require.ensure(['./containers/OperatorNewContainer'], (require) => {
              const Container = require('./containers/OperatorNewContainer').default
              next(null, Container)
            })
          }
        },
        {
          path : ':id/view',
          getComponent (nextState, next) {
            require.ensure(['./containers/OperatorViewContainer'], (require) => {
              const Container = require('./containers/OperatorViewContainer').default
              next(null, Container)
            })
          }
        }
      ])
    })
  }
})
