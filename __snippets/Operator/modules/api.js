import axios from 'axios'

import { URL } from 'consts/apiConsts'

function parseJSON (response) {
  return response.data.data || response.data
}

export function fetchOperators (params) {
  return axios.get(`${URL}/operators`, {
    params: params
  }).then((response) => response.data)
}

export function createOperator (data) {
  return axios.post(`${URL}/operators`, data).then(parseJSON)
}

export function updateOperator (operatorId, data) {
  return axios.put(`${URL}/operators/${operatorId}`, data).then(parseJSON)
}

export function destroyOperator (operatorId) {
  return axios.delete(`${URL}/operators/${operatorId}`).then(parseJSON)
}

export function fetchOperatorById (operatorId) {
  return axios.get(`${URL}/operators/${operatorId}`).then(parseJSON)
}
