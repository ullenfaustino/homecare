import { takeLatest } from 'redux-saga'
import { take, put, select, call, fork } from 'redux-saga/effects'
import * as Api from './api'
import _ from 'lodash'
import { browserHistory } from 'react-router'

// ------------------------------------
// Constants
// ------------------------------------
const PAGINATION_DEF = {
  page: 1,
  pageSize: 10,
  searchText: '',
  sortName: undefined,
  sortOrder: undefined
}

export const OPERATOR_FETCH_REQUESTED = 'OPERATOR_FETCH_REQUESTED'
export const OPERATOR_FETCH_SUCCEEDED = 'OPERATOR_FETCH_SUCCEEDED'
export const OPERATOR_FETCH_FAILED = 'OPERATOR_FETCH_FAILED'

export const OPERATOR_CREATE_REQUESTED = 'OPERATOR_CREATE_REQUESTED'
export const OPERATOR_CREATE_SUCCEEDED = 'OPERATOR_CREATE_SUCCEEDED'
export const OPERATOR_CREATE_FAILED = 'OPERATOR_CREATE_FAILED'

export const OPERATOR_UPDATE_REQUESTED = 'OPERATOR_UPDATE_REQUESTED'
export const OPERATOR_UPDATE_SUCCEEDED = 'OPERATOR_UPDATE_SUCCEEDED'
export const OPERATOR_UPDATE_FAILED = 'OPERATOR_UPDATE_FAILED'

export const OPERATOR_FETCH_BY_ID_REQUESTED = 'OPERATOR_FETCH_BY_ID_REQUESTED'
export const OPERATOR_FETCH_BY_ID_SUCCEEDED = 'OPERATOR_FETCH_BY_ID_SUCCEEDED'
export const OPERATOR_FETCH_BY_ID_FAILED = 'OPERATOR_FETCH_BY_ID_FAILED'

// ------------------------------------
// Actions
// ------------------------------------
export function fetchOperators (data) {
  return (dispatch, getState) => {
    dispatch({
      type: OPERATOR_FETCH_REQUESTED
    })
    const { operator } = getState()
    const params = Object.assign({}, operator.pagination, data)

    return Api.fetchOperators(params).then((payload) => {
      const { data: operators, pagination } = payload
      const __pagination = Object.assign({}, params, pagination)

      dispatch({
        type: OPERATOR_FETCH_SUCCEEDED,
        pagination: __pagination,
        operators
      })
      return payload
    })
  }
}

export function createOperator (data) {
  return (dispatch) => {
    dispatch({ type: OPERATOR_CREATE_REQUESTED, data })
    return Api.createOperator(data).then((payload) => {
      dispatch({ type: OPERATOR_CREATE_SUCCEEDED, data: payload })
      return payload
    })
  }
}

export function updateOperator (data) {
  return (dispatch) => {
    dispatch({ type: OPERATOR_UPDATE_REQUESTED, data })
    return Api.updateOperator(data.id, data).then((payload) => {
      dispatch({
        type: OPERATOR_UPDATE_SUCCEEDED,
        operator: payload
      })
      return payload
    })
  }
}

export function destroyOperator (operatorId) {
  return (dispatch) => {
    // dispatch({ type: OPERATOR_FETCH_BY_ID_REQUESTED, id: operatorId })
    return Api.destroyOperator(operatorId).then((payload) => {
      // dispatch({
      //   type: OPERATOR_FETCH_BY_ID_SUCCEEDED,
      //   operator: payload
      // })
      return payload
    })
  }
}

export function fetchOperatorById (operatorId) {
  return (dispatch) => {
    dispatch({ type: OPERATOR_FETCH_BY_ID_REQUESTED, id: operatorId })
    return Api.fetchOperatorById(operatorId).then((payload) => {
      dispatch({
        type: OPERATOR_FETCH_BY_ID_SUCCEEDED,
        operator: payload
      })
      return payload
    })
  }
}

export const actions = {
  fetchOperators,
  createOperator,
  updateOperator,
  destroyOperator,
  fetchOperatorById
}

// ------------------------------------
// Action Handlers
// ------------------------------------
const ACTION_HANDLERS = {
  [OPERATOR_FETCH_REQUESTED] : (state, action) => Object.assign({}, state, {
    isFetchLoading: true
  }),
  [OPERATOR_FETCH_SUCCEEDED] : (state, action) => Object.assign({}, state, {
    operators: action.operators,
    isFetchLoading: false,
    pagination: { ...state.pagination, ...action.pagination }
  }),
  [OPERATOR_FETCH_BY_ID_REQUESTED] : (state, action) => Object.assign({}, state, {
    fetchByIdLoading: true
  }),
  [OPERATOR_FETCH_BY_ID_SUCCEEDED] : (state, action) => Object.assign({}, state, {
    currentOperator: action.operator,
    fetchByIdLoading: false
  })
}

// ------------------------------------
// Reducer
// ------------------------------------
const initialState = {
  operators: [],
  isFetchLoading: false,
  pagination: PAGINATION_DEF,

  currentOperator: undefined,
  fetchByIdLoading: false
}

export default function operatorReducer (state = initialState, action) {
  const handler = ACTION_HANDLERS[action.type]
  return handler ? handler(state, action) : state
}

// ------------------------------------
// Sagas
// ------------------------------------

export const sagas = []
