import axios from 'axios'

import { URL } from 'consts/apiConsts'

function parseJSON (response) {
  return response.data.data || response.data
}

export function fetchCustomers (params) {
  return axios.get(`${URL}/customers`, {
    params: params
  }).then((response) => response.data)
}

export function createCustomer (data) {
  return axios.post(`${URL}/customers`, data).then(parseJSON)
}

export function updateCustomer (customerId, data) {
  return axios.put(`${URL}/customers/${customerId}`, data).then(parseJSON)
}

export function destroyCustomer (customerId) {
  return axios.delete(`${URL}/customers/${customerId}`).then(parseJSON)
}

export function fetchCustomerById (customerId) {
  return axios.get(`${URL}/customers/${customerId}`).then(parseJSON)
}
