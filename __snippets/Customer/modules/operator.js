import { takeLatest } from 'redux-saga'
import { take, put, select, call, fork } from 'redux-saga/effects'
import * as Api from './api'
import _ from 'lodash'
import { browserHistory } from 'react-router'

// ------------------------------------
// Constants
// ------------------------------------
const PAGINATION_DEF = {
  page: 1,
  pageSize: 10,
  searchText: '',
  sortName: undefined,
  sortOrder: undefined
}

export const CUSTOMER_FETCH_REQUESTED = 'CUSTOMER_FETCH_REQUESTED'
export const CUSTOMER_FETCH_SUCCEEDED = 'CUSTOMER_FETCH_SUCCEEDED'
export const CUSTOMER_FETCH_FAILED = 'CUSTOMER_FETCH_FAILED'

export const CUSTOMER_CREATE_REQUESTED = 'CUSTOMER_CREATE_REQUESTED'
export const CUSTOMER_CREATE_SUCCEEDED = 'CUSTOMER_CREATE_SUCCEEDED'
export const CUSTOMER_CREATE_FAILED = 'CUSTOMER_CREATE_FAILED'

export const CUSTOMER_UPDATE_REQUESTED = 'CUSTOMER_UPDATE_REQUESTED'
export const CUSTOMER_UPDATE_SUCCEEDED = 'CUSTOMER_UPDATE_SUCCEEDED'
export const CUSTOMER_UPDATE_FAILED = 'CUSTOMER_UPDATE_FAILED'

export const CUSTOMER_FETCH_BY_ID_REQUESTED = 'CUSTOMER_FETCH_BY_ID_REQUESTED'
export const CUSTOMER_FETCH_BY_ID_SUCCEEDED = 'CUSTOMER_FETCH_BY_ID_SUCCEEDED'
export const CUSTOMER_FETCH_BY_ID_FAILED = 'CUSTOMER_FETCH_BY_ID_FAILED'

// ------------------------------------
// Actions
// ------------------------------------
export function fetchCustomers (data) {
  return (dispatch, getState) => {
    dispatch({
      type: CUSTOMER_FETCH_REQUESTED
    })
    const { customer } = getState()
    const params = Object.assign({}, customer.pagination, data)

    return Api.fetchCustomers(params).then((payload) => {
      const { data: customers, pagination } = payload
      const __pagination = Object.assign({}, params, pagination)

      dispatch({
        type: CUSTOMER_FETCH_SUCCEEDED,
        pagination: __pagination,
        customers
      })
      return payload
    })
  }
}

export function createCustomer (data) {
  return (dispatch) => {
    dispatch({ type: CUSTOMER_CREATE_REQUESTED, data })
    return Api.createCustomer(data).then((payload) => {
      dispatch({ type: CUSTOMER_CREATE_SUCCEEDED, data: payload })
      return payload
    })
  }
}

export function updateCustomer (data) {
  return (dispatch) => {
    dispatch({ type: CUSTOMER_UPDATE_REQUESTED, data })
    return Api.updateCustomer(data.id, data).then((payload) => {
      dispatch({
        type: CUSTOMER_UPDATE_SUCCEEDED,
        customer: payload
      })
      return payload
    })
  }
}

export function destroyCustomer (customerId) {
  return (dispatch) => {
    // dispatch({ type: CUSTOMER_FETCH_BY_ID_REQUESTED, id: customerId })
    return Api.destroyCustomer(customerId).then((payload) => {
      // dispatch({
      //   type: CUSTOMER_FETCH_BY_ID_SUCCEEDED,
      //   customer: payload
      // })
      return payload
    })
  }
}

export function fetchCustomerById (customerId) {
  return (dispatch) => {
    dispatch({ type: CUSTOMER_FETCH_BY_ID_REQUESTED, id: customerId })
    return Api.fetchCustomerById(customerId).then((payload) => {
      dispatch({
        type: CUSTOMER_FETCH_BY_ID_SUCCEEDED,
        customer: payload
      })
      return payload
    })
  }
}

export const actions = {
  fetchCustomers,
  createCustomer,
  updateCustomer,
  destroyCustomer,
  fetchCustomerById
}

// ------------------------------------
// Action Handlers
// ------------------------------------
const ACTION_HANDLERS = {
  [CUSTOMER_FETCH_REQUESTED] : (state, action) => Object.assign({}, state, {
    isFetchLoading: true
  }),
  [CUSTOMER_FETCH_SUCCEEDED] : (state, action) => Object.assign({}, state, {
    customers: action.customers,
    isFetchLoading: false,
    pagination: { ...state.pagination, ...action.pagination }
  }),
  [CUSTOMER_FETCH_BY_ID_REQUESTED] : (state, action) => Object.assign({}, state, {
    fetchByIdLoading: true
  }),
  [CUSTOMER_FETCH_BY_ID_SUCCEEDED] : (state, action) => Object.assign({}, state, {
    currentCustomer: action.customer,
    fetchByIdLoading: false
  })
}

// ------------------------------------
// Reducer
// ------------------------------------
const initialState = {
  customers: [],
  isFetchLoading: false,
  pagination: PAGINATION_DEF,

  currentCustomer: undefined,
  fetchByIdLoading: false
}

export default function customerReducer (state = initialState, action) {
  const handler = ACTION_HANDLERS[action.type]
  return handler ? handler(state, action) : state
}

// ------------------------------------
// Sagas
// ------------------------------------

export const sagas = []
