import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Link } from 'react-router'

import {
  actions
} from '../modules/customer'

import CustomerList from '../components/CustomerList'

class CustomerContainer extends Component {

  render () {
    return this.props.children || <CustomerList {...this.props} />
  }
}

const mapDispatchToProps = {
  ...actions
}

const mapStateToProps = (state) => ({ ...state.customer })

export default connect(mapStateToProps, mapDispatchToProps)(CustomerContainer)
