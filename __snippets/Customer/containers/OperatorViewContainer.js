import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Link } from 'react-router'

import {
  actions
} from '../modules/customer'

import CustomerDetails from '../components/CustomerDetails'

class CustomerViewContainer extends Component {

  render () {
    return this.props.children || <CustomerDetails {...this.props} />
  }
}

const mapDispatchToProps = { ...actions }

const mapStateToProps = (state) => ({
  ...state.customer
})

export default connect(mapStateToProps, mapDispatchToProps)(CustomerViewContainer)
