import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Link } from 'react-router'

import {
  actions
} from '../modules/customer'

import CustomerNew from '../components/CustomerNew'

class CustomerNewContainer extends Component {

  render () {
    return this.props.children || <CustomerNew {...this.props} />
  }
}

const mapDispatchToProps = { ...actions }

const mapStateToProps = (state) => ({
  ...state.customer
})

export default connect(mapStateToProps, mapDispatchToProps)(CustomerNewContainer)
