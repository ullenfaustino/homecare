import React, { Component, PropTypes } from 'react'
import { Button } from 'react-bootstrap'
import { Link } from 'react-router'
import { BootstrapTable, TableHeaderColumn } from 'react-bootstrap-table'

import * as Api from '../modules/api'

export default class CustomerList extends Component {
  constructor () {
    super()
    this.state = {
      customers: []
    }
  }

  componentDidMount () {
    this.__fetchCustomers()
  }

  __fetchCustomers (params = {}) {
    this.props.fetchCustomers(params)
  }

  handleDeleteCustomer (customerId) {
    this.props.destroyCustomer(customerId).then(() => {
      this.__fetchCustomers()
    })
  }

  onPageChange = (page, sizePerPage) => {
    this.__fetchCustomers({
      pageSize: sizePerPage,
      page
    })
  }

  onSearchChange = (searchText, colInfos, multiColumnSearch) => {
    const text = searchText.trim()
    this.__fetchCustomers({
      page:1,
      searchText: text
    })
  }

  onFilterChange = (filterObj) => {
    const { value } = filterObj.status !== undefined ? filterObj.status : filterObj.type !== undefined ? filterObj.type : ''
    this.__fetchCustomers({
      page:1,
      searchText: value
    })
  }

  onSizePerPageList = (sizePerPage) => {
    this.__fetchCustomers({
      pageSize: sizePerPage,
      page: 1
    })
  }

  onSortChange = (sortName, sortOrder) => {
    this.__fetchCustomers({
      page: 1,
      sortName,
      sortOrder
    })
  }

  dataActionFormat = (customer) => {
    return (
      <div>
        <Link to={`customers/${customer.id}/edit`} className='btn btn-default'>
          edit
        </Link>
        {' '}
        <Button bsStyle='danger' onClick={() => {
          this.handleDeleteCustomer(customer.id)
        }}>delete</Button>
      </div>
    )
  }

  render () {
    const { customers = [], pagination = {} } = this.props
    const { pageSize } = pagination
    const options = {
      // insertBtn: this.createCustomInsertButton,
      onPageChange: this.onPageChange,
      onSearchChange: this.onSearchChange,
      onSortChange: this.onSortChange,
      page: pagination.page,
      sizePerPage: pageSize,
      clearSearch: true,
      onFilterChange: this.onFilterChange,
      toolBar: () => null
    }
    const fetchInfo = { dataTotalSize: pagination.rowCount }

    return (
      <div>
        <div className='row wrapper border-bottom white-bg page-heading'>
          <div className='col-md-8'>
            <h2>Customer List</h2>
          </div>
          <div className='col-md-4'>
            <div className='pull-right' style={{ padding: 10 }}>
              {/* <Button bsStyle='default'>Some Actions</Button> */}
              { ' ' }
              <Link to={`/customers/new`}><Button bsStyle='primary'>Add Customer</Button></Link>
            </div>
          </div>
        </div>
        <div className='wrapper wrapper-content'>
          <div className='row'>
            <div className='col-md-12'>
              <div className='ibox float-e-margins'>
                <div className='ibox-title'>
                  <h5>Customers </h5>
                </div>
                <div className='ibox-content'>
                  <BootstrapTable data={customers} options={options} fetchInfo={fetchInfo} striped search pagination remote insertRow>
                    <TableHeaderColumn isKey dataField='id' hidden>ID</TableHeaderColumn>
                    <TableHeaderColumn dataField='name' dataSort
                      dataFormat={(cell, customer) => {
                        return <Link to={`customers/${customer.id}/view`}>{customer.name}</Link>
                      }}>Name</TableHeaderColumn>
                    <TableHeaderColumn dataField='email' dataSort>Email</TableHeaderColumn>
                    <TableHeaderColumn dataField='telephone' dataSort>Telephone</TableHeaderColumn>
                    <TableHeaderColumn dataField='fax' dataSort>Fax</TableHeaderColumn>
                    <TableHeaderColumn dataField='status' dataSort>Status</TableHeaderColumn>
                    <TableHeaderColumn dataField='id' width={150}
                      dataFormat={(cell, row) => this.dataActionFormat(row)}>--</TableHeaderColumn>
                  </BootstrapTable>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    )
  }
}

CustomerList.propTypes = {}
