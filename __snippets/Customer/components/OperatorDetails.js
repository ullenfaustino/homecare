import React, { Component, PropTypes } from 'react'
import { Link } from 'react-router'

export default class CustomerDetails extends Component {
  constructor (props) {
    super(props)
  }

  componentDidMount () {
    this.props.fetchCustomerById(this.props.params.id)
  }

  render () {
    const { currentCustomer } = this.props
    if (!currentCustomer || this.props.fetchByIdLoading) {
      return <div>Loading...</div>
    }
    const fields = [
      'id',
      'name'
    ]
    return (
      <div>
        <div className='row wrapper border-bottom white-bg page-heading'>
          <div className='col-lg-8'>
            <h2>Customer Details</h2>
            <ol className='breadcrumb'>
              <li>
                <Link to={'/customers'}>Customer List</Link>
              </li>
              <li className='active'>
                <strong>Customer Details</strong>
              </li>
            </ol>
          </div>
          <div className='col-lg-4' />
        </div>
        <div className='wrapper wrapper-content'>
          <table className='table table-striped'>
            <tbody>
              {
                fields.map((f) => {
                  return (
                    <tr>
                      <th width={150}>{f}</th>
                      <td>{currentCustomer[f]}</td>
                    </tr>
                  )
                })
              }
            </tbody>
          </table>
        </div>
      </div>
    )
  }
}

CustomerDetails.propTypes = {}
