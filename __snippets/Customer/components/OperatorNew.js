import React, { Component, PropTypes } from 'react'
import { browserHistory } from 'react-router'

import CustomerForm from './CustomerForm'

export default class CustomerNew extends Component {
  constructor (props) {
    super(props)

    this.state = {
      currentCustomer: {},
      isCustomerSaving: false,
      isEdit: false
    }
  }

  componentDidMount () {
    if (this.props.params.id) {
      this.setState({
        isEdit: true
      }, () => {
        this.props.fetchCustomerById(this.props.params.id)
      })
    }
  }

  componentWillReceiveProps (nextProps) {
    if (this.state.isEdit && nextProps.params.id) {
      if (this.state.currentCustomer != nextProps.currentCustomer) {
        if (!this.state.isCustomerSaving) {
          this.setState({
            currentCustomer: nextProps.currentCustomer
          })
        }
      }
    } else {
      this.setState({
        isEdit: false,
        currentCustomer: {}
      })
    }
  }

  render () {
    const { fetchByIdLoading } = this.props
    const { currentCustomer = {}, isEdit } = this.state

    if (isEdit) {
      if (!currentCustomer || fetchByIdLoading) {
        return <div style={{ padding: 25 }}><h3>Loading...</h3></div>
      }
    }

    const formState = _.chain(currentCustomer)
      .mapValues(function (value, key) {
        return { value: value || '' }
      }).value()
    return (
      <div>
        <div className='wrapper wrapper-content'>

          <CustomerForm
            formRef={formRef => this.formRef = formRef}
            formState={formState}
            isSaving={this.state.isCustomerSaving}
            onFieldsChange={(fields) => {
              // console.log('onFieldsChange', fields)
              // this.setState({
              //   currentCustomer: { ...this.state.currentCustomer, ...fields }
              // })
            }}
            onSubmit={(values) => {
              this.setState({
                isCustomerSaving: true,
                isEdit: false,
                currentCustomer: undefined
              }, () => {
                if (isEdit) {
                  this.props.updateCustomer(values).then((customer) => {
                    browserHistory.push(`/customers/${customer.id}/view`)
                  })
                } else {
                  this.props.createCustomer(values).then((customer) => {
                    browserHistory.push(`/customers/${customer.id}/view`)
                  })
                }
              })
            }} />

        </div>
      </div>
    )
  }
}

CustomerNew.propTypes = {}
