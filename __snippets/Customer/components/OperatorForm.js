import React, { Component, PropTypes } from 'react'
import { Button, Checkbox, Col, Form, FormControl, FormGroup, ControlLabel, HelpBlock, Tab, Pane, FieldGroup, Row, Nav, NavItem, Label } from 'react-bootstrap'
import { createForm } from 'rc-form'
import _ from 'lodash'
import { Link } from 'react-router'

class CustomerForm extends Component {
  constructor (props) {
    super(props)
  }

  submit = () => {
    this.props.form.validateFields((error, values) => {
      if (!error) {
        if (this.props.onSubmit) {
          this.props.onSubmit(values)
        }
      }
    })
  }

  render () {
    let errors
    const {
      isSaving,
      formRef
    } = this.props

    const { getFieldDecorator, getFieldProps, getFieldError, getFieldsError, isFieldTouched, getFieldValue, setFieldsValue } = this.props.form

    function getValidationState (field) {
      if (isFieldTouched(field)) {
        if (getFieldError(field)) {
          return 'error'
        } else {
          return 'success'
        }
      } else {
        return null
      }
    }

    function toNumber (v) {
      if (v === undefined) {
        return v
      }
      if (v === '') {
        return undefined
      }
      return Number(v)
    }

    return (
      <Form ref={formRef} horizontal onSubmit={(ev) => {
        ev.preventDefault()
        this.submit()
      }}>
        <div className='row'>
          <div className='col-md-12'>
            <div className='ibox float-e-margins'>
              <div className='ibox-content'>
                <h3>Customer Details</h3>
                <hr />
                <h3>Identification</h3>
                <input type='hidden' {...getFieldProps('id')} />
                <FormGroup controlId='first_name' validationState={getFieldError('first_name') ? 'error' : getValidationState('first_name')}>
                  <Col componentClass={ControlLabel} sm={2}>First Name <span className='text-danger'>*</span></Col>
                  <Col sm={8}>
                    {
                      getFieldDecorator('first_name', {
                        initialValue: '',
                        rules: [{
                          required: true,
                          message: 'This is a required field.'
                        }]
                      })(<input className='form-control' />)
                    }
                    {(errors = getFieldError('first_name')) ? <HelpBlock>{errors.join(',')}</HelpBlock> : null}
                  </Col>
                </FormGroup>

                <FormGroup controlId='last_name' validationState={getFieldError('last_name') ? 'error' : getValidationState('last_name')}>
                  <Col componentClass={ControlLabel} sm={2}>Last Name <span className='text-danger'>*</span></Col>
                  <Col sm={8}>
                    {
                      getFieldDecorator('last_name', {
                        initialValue: '',
                        rules: [{
                          required: true,
                          message: 'This is a required field.'
                        }]
                      })(<input className='form-control' />)
                    }
                    {(errors = getFieldError('last_name')) ? <HelpBlock>{errors.join(',')}</HelpBlock> : null}
                  </Col>
                </FormGroup>

                <FormGroup controlId='email' validationState={getFieldError('email') ? 'error' : getValidationState('email')}>
                  <Col componentClass={ControlLabel} sm={2}>E-Mail <span className='text-danger'>*</span></Col>
                  <Col sm={6}>
                    {
                      getFieldDecorator('email', {
                        initialValue: '',
                        rules: [{
                          required: true,
                          message: 'This is a required field.'
                        }, {
                          type: 'email',
                          message: 'Invalid E-email.'
                        }]
                      })(<input className='form-control' />)
                    }
                    {(errors = getFieldError('email')) ? <HelpBlock>{errors.join(',')}</HelpBlock> : null}
                  </Col>
                </FormGroup>

                <FormGroup controlId='telephone' validationState={getFieldError('telephone') ? 'error' : getValidationState('telephone')}>
                  <Col componentClass={ControlLabel} sm={2}>Telephone</Col>
                  <Col sm={4}>
                    {
                      getFieldDecorator('telephone', {
                        initialValue: '',
                        rules: []
                      })(<input className='form-control' />)
                    }
                    {(errors = getFieldError('telephone')) ? <HelpBlock>{errors.join(',')}</HelpBlock> : null}
                  </Col>
                </FormGroup>

                <FormGroup controlId='mobile' validationState={getFieldError('mobile') ? 'error' : getValidationState('mobile')}>
                  <Col componentClass={ControlLabel} sm={2}>Mobile</Col>
                  <Col sm={4}>
                    {
                      getFieldDecorator('mobile', {
                        initialValue: '',
                        rules: []
                      })(<input className='form-control' />)
                    }
                    {(errors = getFieldError('mobile')) ? <HelpBlock>{errors.join(',')}</HelpBlock> : null}
                  </Col>
                </FormGroup>

                <hr />

                <FormGroup>
                  <Col componentClass={ControlLabel} sm={2} />
                  <Col sm={8}>
                    <Link to={`/customers`} className='btn btn-default'>
                      Cancel
                    </Link>
                    { ' ' }
                    <Button type='submit' bsStyle='primary' disabled={isSaving}>{isSaving ? 'Submit...' : 'Save'}</Button>
                  </Col>
                </FormGroup>
              </div>
            </div>
          </div>
        </div>
      </Form>
    )
  }
}

CustomerForm.propTypes = {}

export default createForm({
  mapPropsToFields (props) {
    return props.formState
  },
  onFieldsChange (props, fields) {
    if (props.onFieldsChange) {
      props.onFieldsChange(_.mapValues(fields, function (o, key) {
        return o.value || undefined
      }))
    }
  }
})(CustomerForm)
