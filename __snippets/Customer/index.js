import { injectReducer } from '../../store/reducers'
import { injectSagas } from '../../store/sagas'

export default (store) => ({
  path : 'customers',
  /*  Async getComponent is only invoked when route matches   */
  getComponent (nextState, cb) {
    /*  Webpack - use 'require.ensure' to create a split point
        and embed an async module loader (jsonp) when bundling   */
    require.ensure([], (require) => {
      /*  Webpack - use require callback to define
          dependencies for bundling   */
      const Customer = require('./containers/CustomerContainer').default
      const reducer = require('./modules/customer').default
      // const sagas = require('./modules/category').sagas
      //
      // /*  Add the reducer to the store on key 'counter'  */
      injectReducer(store, { key: 'customer', reducer })
      // injectSagas(store, { key: 'category', sagas })

      /*  Return getComponent   */
      cb(null, Customer)

    /* Webpack named bundle   */
    }, 'customers')
  },
  getChildRoutes (location, next) {
    require.ensure([], (require) => {
      next(null, [
        {
          path : 'new',
          getComponent (nextState, next) {
            require.ensure(['./containers/CustomerNewContainer'], (require) => {
              const Container = require('./containers/CustomerNewContainer').default
              next(null, Container)
            })
          }
        },
        {
          path : ':id/edit',
          getComponent (nextState, next) {
            require.ensure(['./containers/CustomerNewContainer'], (require) => {
              const Container = require('./containers/CustomerNewContainer').default
              next(null, Container)
            })
          }
        },
        {
          path : ':id/view',
          getComponent (nextState, next) {
            require.ensure(['./containers/CustomerViewContainer'], (require) => {
              const Container = require('./containers/CustomerViewContainer').default
              next(null, Container)
            })
          }
        }
      ])
    })
  }
})
