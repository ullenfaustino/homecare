const Queue = require('bull')
// var globe = require('globe-connect');
const nodemailer = require('nodemailer')
const Promise = require('bluebird')
const _ = require('lodash')
const debug = require('debug')('app:queue')

const serverUtils = require('./utils')
const constants = require('./constants')
const SmartHR = require('./api/smarthr')
const dataProvider = require('./models')
// const io = require('./io')
const helpers = require('./helpers')
const db = require('./db')

const composeSubject = (status, item) => {
  if (status === 'ds') {
    return `Item ready for O.R. - ${item.id}`
  } else if (status === 'on-hold') {
    if (item.type === 'check') {
      return `Check for verification #${item.id}`
    } else {
      return `Item  for verification #${item.id}`
    }
  }
  return `This is subject for status: ${status} #${item.id}`
}

const formatEmailTemplateData = (item) => {
  return {
    isCheck: item.status === 'ds'
  }
}

const setupQueue = (app) => {
  debug('Bull Started -> Setup Queue')

  // helper function for generating email template
  const generateEmailTemplate = (viewPath, data) => {
    const __data = {
      layout: 'email',
      baseUrl: process.env.URL || 'http://ar.vibalgroup.com'
    }
    return new Promise(function (resolve, reject) {
      app.render(viewPath, _.merge(__data, data), (err, html) => {
        if (err) {
          reject(err)
        }
        resolve(html)
      })
    })
  }

  const newCollectionQueue = Queue(constants.QUEUE.NOTIFY_COLLECTION_NEW)
  const updateStatusQueue = Queue(constants.QUEUE.NOTIFY_COLLECTION_UPDATE_STATUS)
  const bounceCheckQueue = Queue(constants.QUEUE.NOTIFY_COLLECTION_BOUNCE_CHECK)

  // getUsersByRole
  newCollectionQueue.process(function process (job, done) {
    const { data = {} } = job
    const { collection = {}, item = {}, status } = data
    done()
  })

  updateStatusQueue.process(function process (job, done) {
    const { data = {} } = job
    const { collection = {}, item = {}, user:userAction = {}, status } = data
    const operationTrackList = [
      'new',
      'on-hold', // this is explicitly triggered, when items is check
      'verify', // TODO:: verfy action not yet implemented
      'ds'
    ]

    const isNeedToNotify = operationTrackList.indexOf(status) > -1

    if (!isNeedToNotify) {
      debug(`No need to notify for status operation: ${status}`)
      return done()
    }

    debug(`Start process queue with status operation: ${status}`)
    // TODO:: only filter things to notify when status is change
    const roleToNotifyBaseOnOperationStatus = {
      new: 'cashier',
      set_check_type: 'cashier',
      'on-hold': 'exec',
      verify: 'exec', // only if type===check
      ds: 'ar'
    }
    const role = roleToNotifyBaseOnOperationStatus[status]

    debug(`Start notify user with role ${role}`)
    try {
      SmartHR.getUsersByRole(role, undefined, db.get('smarthr').value())
      .then((users = []) => {
        debug(`Found ${users.length} to notify`)
        return generateEmailTemplate(
          'email/update',
          _.merge({
            status,
            item
          }, formatEmailTemplateData(item))
        ).then((html) => {
          return new Promise.map(users, (user) => {
            return dataProvider.Notification.forge().save({
              user_id: user.id,
              target_id: item.id,
              type: 'status_update',
              action: status,
              action_by: userAction.id
            })
            .then((notification) => notification.toJSON())
            .then((notification) => {
              return helpers.formatNotificationItem(
                notification,
                item,
                db.get('smarthr').value()
              )
            })
            .then((notification) => {
              const notificationPayload = Object.assign({}, notification, {
                item: item,
                collection: collection
              }
              )
              io.to(user.id).emit('notif', notificationPayload)
              serverUtils.sendMail({
                to: user.emailaddress,
                subject: composeSubject(status, item, collection),
                html: html
              })
              serverUtils.sendSMS({
                numbers: user.contact_number,
                message: composeSubject(status, item, collection),
                sendername: null  // to be replaced by 'ARTracker' ones the Sender Name is verified by semaphore
              }).then(() => {
                debug('SMS sent')
              })
              return null // return nothing?
            })
          })
        })
      })
      .catch((err) => console.log(err))
      .finally(() => {
        debug(`Queue process job done`)
        done()
      })
    } catch (e) {
      console.log('e', e)
    }
  })

  bounceCheckQueue.process((job, done) => {
    const { data = {} } = job
    const { collection = {}, item = {}, status } = data
    const user = item.__created_by

    console.log('bounce cid', collection.id)
    console.log('bounce iid', item.id)
    console.log('bounce created_by', item.created_by)

    debug('Bounce check start job...')
    // notify who created the collection

    if (user) {
      return new Promise(function (resolve, reject) {
        return generateEmailTemplate('email/bounce', {
          item: item
        }).then((html) => {
          serverUtils.sendSMS({
            numbers: user.contact_number,
            message: composeSubject(status, item, collection),
            sendername: null // to be replaced by 'ARTracker' ones the Sender Name is verified by semaphore
          })
          serverUtils.sendMail({
            to: user.emailaddress,
            subject: `Bounce Check Notification - #${item.check_number}`,
            html: html
          })
        })
      }).finally(() => {
        console.log('Bounce check job done...')
        debug('Bounce check job done...')
        done()
      })
    }
    return new Promise.resolve()
  })
}

module.exports = setupQueue
module.exports.updateStatusQueue = Queue(constants.QUEUE.NOTIFY_COLLECTION_UPDATE_STATUS)
