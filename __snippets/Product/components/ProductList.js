import React, { Component, PropTypes } from 'react'
import { Button } from 'react-bootstrap'
import { Link } from 'react-router'

import * as Api from '../modules/api'

export default class ProductList extends Component {
  constructor () {
    super()
    this.state = {
      products: []
    }
  }

  componentDidMount () {
    this.__fetchProducts()
  }

  __fetchProducts () {
    this.props.fetchProducts()
  }

  handleDeleteProduct (productId) {
    this.props.destroyProduct(productId).then(() => {
      this.__fetchProducts()
    })
  }

  render () {
    const { products } = this.props
    return (
      <div>
        <div className='row wrapper border-bottom white-bg page-heading'>
          <div className='col-md-8'>
            <h2>Product List</h2>
          </div>
          <div className='col-md-4'>
            <div className='pull-right' style={{ padding: 10 }}>
              {/* <Button bsStyle='default'>Some Actions</Button> */}
              { ' ' }
              <Link to={`/products/new`}><Button bsStyle='primary'>Add Product</Button></Link>
            </div>
          </div>
        </div>
        <div className='wrapper wrapper-content'>
          <div className='row'>
            <div className='col-md-12'>
              <div className='ibox float-e-margins'>
                <div className='ibox-title'>
                  <h5>Products </h5>
                </div>
                <div className='ibox-content'>

                  <table className='table table-striped'>
                    <thead>
                      <tr>
                        <th>#</th>
                        <th>Product Name</th>
                        <th>Model</th>
                        <th>Price</th>
                        <th>Quantity</th>
                        <th>Status</th>
                        <th>--</th>
                      </tr>
                    </thead>
                    <tbody>
                      {
                        products.map((product) => {
                          return (
                            <tr>
                              <td>{product.id}</td>
                              <td><Link to={`products/${product.id}/view`}>{product.name}</Link></td>
                              <td>{product.model}</td>
                              <td className='text-right'>{product.price}</td>
                              <td className='text-right'><span className='label label-primary'>{product.quantity}</span></td>
                              <td>Enabled</td>
                              <td>
                                <Link to={`products/${product.id}/edit`} className='btn btn-default'>
                                  edit
                                </Link>
                                {' '}
                                <Button bsStyle='danger' onClick={() => {
                                  this.handleDeleteProduct(product.id)
                                }}>delete</Button>
                              </td>
                            </tr>
                          )
                        })
                      }
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    )
  }
}

ProductList.propTypes = {}
