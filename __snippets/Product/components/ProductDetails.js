import React, { Component, PropTypes } from 'react'
import { Link } from 'react-router'

export default class ProductDetails extends Component {
  constructor (props) {
    super(props)
  }

  componentDidMount () {
    this.props.fetchProductById(this.props.params.id)
  }

  render () {
    return (
      <div>
        <div className='row wrapper border-bottom white-bg page-heading'>
          <div className='col-lg-8'>
            <h2>Product Details</h2>
            <ol className='breadcrumb'>
              <li>
                <Link to={'/products'}>Product List</Link>
              </li>
              <li className='active'>
                <strong>Product Details</strong>
              </li>
            </ol>
          </div>
          <div className='col-lg-4' />
        </div>
        <div className='wrapper wrapper-content'>
          MyComponent: {JSON.stringify(this.props.currentProduct)}
        </div>
      </div>
    )
  }
}

ProductDetails.propTypes = {}
