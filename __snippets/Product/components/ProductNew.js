import React, { Component, PropTypes } from 'react'
import { browserHistory } from 'react-router'

import ProductForm from './ProductForm'

export default class ProductNew extends Component {
  constructor (props) {
    super(props)

    this.state = {
      currentProduct: {},
      isProductSaving: false,
      isEdit: false
    }
  }

  componentDidMount () {
    if (this.props.params.id) {
      this.setState({
        isEdit: true
      }, () => {
        this.props.fetchProductById(this.props.params.id)
      })
    }
  }

  componentWillReceiveProps (nextProps) {
    if (this.state.isEdit && nextProps.params.id) {
      if (this.state.currentProduct != nextProps.currentProduct) {
        if (!this.state.isProductSaving) {
          this.setState({
            currentProduct: nextProps.currentProduct
          })
        }
      }
    } else {
      this.setState({
        isEdit: false,
        currentProduct: {}
      })
    }
  }

  render () {
    const { fetchByIdLoading } = this.props
    const { currentProduct, isEdit } = this.state

    if (isEdit) {
      if (!currentProduct || fetchByIdLoading) {
        return <div style={{ padding: 25 }}><h3>Loading...</h3></div>
      }
    }

    const tagsProductKeyList = ['productType', 'modelType', 'makeType', 'bodyType', 'bodySubType', 'engine_aspiration']
    const formState = _.chain(currentProduct)
      .mapValues(function (value, key) {
        if (tagsProductKeyList.indexOf(key) > -1) {
          if (value && value.id) {
            return {
              value: Object.assign({}, value, {
                value: value.id,
                label: value.tag
              })
            }
          }

          return { value: {} }
        }
        return { value: value || '' }
      }).value()
    return (
      <div>
        <div className='wrapper wrapper-content'>

          <ProductForm
            formRef={formRef => this.formRef = formRef}
            formState={formState}
            isSaving={this.state.isProductSaving}
            onFieldsChange={(fields) => {
              // console.log('onFieldsChange', fields)
              // this.setState({
              //   currentProduct: { ...this.state.currentProduct, ...fields }
              // })
            }}
            onSubmit={(values) => {
              this.setState({
                isProductSaving: true,
                isEdit: false,
                currentProduct: undefined
              }, () => {
                if (isEdit) {
                  this.props.updateProduct(values).then((product) => {
                    browserHistory.push(`/products/${product.id}/view`)
                  })
                } else {
                  this.props.createProduct(values).then((product) => {
                    browserHistory.push(`/products/${product.id}/view`)
                  })
                }
              })
            }} />

        </div>
      </div>
    )
  }
}

ProductNew.propTypes = {}
