import { takeLatest } from 'redux-saga'
import { take, put, select, call, fork } from 'redux-saga/effects'
import * as Api from './api'
import _ from 'lodash'
import { browserHistory } from 'react-router'

// ------------------------------------
// Constants
// ------------------------------------
const PAGINATION_DEF = {
  page: 1,
  pageSize: 10,
  searchText: '',
  sortName: undefined,
  sortOrder: undefined
}

export const PRODUCT_FETCH_REQUESTED = 'PRODUCT_FETCH_REQUESTED'
export const PRODUCT_FETCH_SUCCEEDED = 'PRODUCT_FETCH_SUCCEEDED'
export const PRODUCT_FETCH_FAILED = 'PRODUCT_FETCH_FAILED'

export const PRODUCT_CREATE_REQUESTED = 'PRODUCT_CREATE_REQUESTED'
export const PRODUCT_CREATE_SUCCEEDED = 'PRODUCT_CREATE_SUCCEEDED'
export const PRODUCT_CREATE_FAILED = 'PRODUCT_CREATE_FAILED'

export const PRODUCT_UPDATE_REQUESTED = 'PRODUCT_UPDATE_REQUESTED'
export const PRODUCT_UPDATE_SUCCEEDED = 'PRODUCT_UPDATE_SUCCEEDED'
export const PRODUCT_UPDATE_FAILED = 'PRODUCT_UPDATE_FAILED'

export const PRODUCT_FETCH_BY_ID_REQUESTED = 'PRODUCT_FETCH_BY_ID_REQUESTED'
export const PRODUCT_FETCH_BY_ID_SUCCEEDED = 'PRODUCT_FETCH_BY_ID_SUCCEEDED'
export const PRODUCT_FETCH_BY_ID_FAILED = 'PRODUCT_FETCH_BY_ID_FAILED'

// ------------------------------------
// Actions
// ------------------------------------
export function fetchProducts (data) {
  return (dispatch) => {
    dispatch({
      type: PRODUCT_FETCH_REQUESTED
    })
    return Api.fetchProducts(data).then((payload) => {
      const { data: products, recordsTotal, recordsFiltered } = payload
      const params = Object.assign({}, data || {}, ...PAGINATION_DEF)

      dispatch({
        type: PRODUCT_FETCH_SUCCEEDED,
        params,
        products
        // recordsTotal,
        // recordsFiltered
      })
      return payload
    })
  }
}

export function createProduct (data) {
  return (dispatch) => {
    dispatch({ type: PRODUCT_CREATE_REQUESTED, data })
    return Api.createProduct(data).then((payload) => {
      dispatch({ type: PRODUCT_CREATE_SUCCEEDED, data: payload })
      return payload
    })
  }
}

export function updateProduct (data) {
  return (dispatch) => {
    dispatch({ type: PRODUCT_UPDATE_REQUESTED, data })
    return Api.updateProduct(data.id, data).then((payload) => {
      dispatch({
        type: PRODUCT_UPDATE_SUCCEEDED,
        product: payload
      })
      return payload
    })
  }
}

export function destroyProduct (productId) {
  return (dispatch) => {
    // dispatch({ type: PRODUCT_FETCH_BY_ID_REQUESTED, id: productId })
    return Api.destroyProduct(productId).then((payload) => {
      // dispatch({
      //   type: PRODUCT_FETCH_BY_ID_SUCCEEDED,
      //   product: payload
      // })
      return payload
    })
  }
}

export function fetchProductById (productId) {
  return (dispatch) => {
    dispatch({ type: PRODUCT_FETCH_BY_ID_REQUESTED, id: productId })
    return Api.fetchProductById(productId).then((payload) => {
      dispatch({
        type: PRODUCT_FETCH_BY_ID_SUCCEEDED,
        product: payload
      })
      return payload
    })
  }
}

export const actions = {
  fetchProducts,
  createProduct,
  updateProduct,
  destroyProduct,
  fetchProductById
}

// ------------------------------------
// Action Handlers
// ------------------------------------
const ACTION_HANDLERS = {
  [PRODUCT_FETCH_REQUESTED] : (state, action) => Object.assign({}, state, {
    isFetchLoading: true
  }),
  [PRODUCT_FETCH_SUCCEEDED] : (state, action) => Object.assign({}, state, {
    products: action.products,
    isFetchLoading: false,
    page: action.params.page,
    pageSize: action.params.pageSize,
    searchText: action.params.searchText,
    sortName: action.params.sortName,
    sortOrder: action.params.sortOrder
  }),
  [PRODUCT_FETCH_BY_ID_REQUESTED] : (state, action) => Object.assign({}, state, {
    fetchByIdLoading: true
  }),
  [PRODUCT_FETCH_BY_ID_SUCCEEDED] : (state, action) => Object.assign({}, state, {
    currentProduct: action.product,
    fetchByIdLoading: false
  })
}

// ------------------------------------
// Reducer
// ------------------------------------
const initialState = {
  products: [],
  isFetchLoading: false,

  currentProduct: undefined,
  fetchByIdLoading: false,

  page: 1,
  pageSize: 10,
  searchText: '',
  sortName: undefined,
  sortOrder: undefined
}

export default function productReducer (state = initialState, action) {
  const handler = ACTION_HANDLERS[action.type]
  return handler ? handler(state, action) : state
}

// ------------------------------------
// Sagas
// ------------------------------------

export const sagas = []
