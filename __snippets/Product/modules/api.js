import axios from 'axios'

import { URL } from 'consts/apiConsts'

function parseJSON (response) {
  return response.data.data || response.data
}

export function fetchProducts (params) {
  return axios.get(`${URL}/products`, {
    params: params
  }).then((response) => response.data)
}

export function createProduct (data) {
  return axios.post(`${URL}/products`, data).then(parseJSON)
}

export function updateProduct (productId, data) {
  return axios.put(`${URL}/products/${productId}`, data).then(parseJSON)
}

export function destroyProduct (productId) {
  return axios.delete(`${URL}/products/${productId}`).then(parseJSON)
}

export function fetchProductById (productId) {
  return axios.get(`${URL}/products/${productId}`).then(parseJSON)
}
