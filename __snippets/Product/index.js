import { injectReducer } from '../../store/reducers'
import { injectSagas } from '../../store/sagas'

export default (store) => ({
  path : 'products',
  /*  Async getComponent is only invoked when route matches   */
  getComponent (nextState, cb) {
    /*  Webpack - use 'require.ensure' to create a split point
        and embed an async module loader (jsonp) when bundling   */
    require.ensure([], (require) => {
      /*  Webpack - use require callback to define
          dependencies for bundling   */
      const Product = require('./containers/ProductContainer').default
      const reducer = require('./modules/product').default
      // const sagas = require('./modules/category').sagas
      //
      // /*  Add the reducer to the store on key 'counter'  */
      injectReducer(store, { key: 'product', reducer })
      // injectSagas(store, { key: 'category', sagas })

      /*  Return getComponent   */
      cb(null, Product)

    /* Webpack named bundle   */
    }, 'products')
  },
  getChildRoutes (location, next) {
    require.ensure([], (require) => {
      next(null, [
        {
          path : 'new',
          getComponent (nextState, next) {
            require.ensure(['./containers/ProductNewContainer'], (require) => {
              const Container = require('./containers/ProductNewContainer').default
              next(null, Container)
            })
          }
        },
        {
          path : ':id/edit',
          getComponent (nextState, next) {
            require.ensure(['./containers/ProductNewContainer'], (require) => {
              const Container = require('./containers/ProductNewContainer').default
              next(null, Container)
            })
          }
        },
        {
          path : ':id/view',
          getComponent (nextState, next) {
            require.ensure(['./containers/ProductViewContainer'], (require) => {
              const Container = require('./containers/ProductViewContainer').default
              next(null, Container)
            })
          }
        }
      ])
    })
  }
})
