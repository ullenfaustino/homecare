import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Link } from 'react-router'

import {
  actions
} from '../modules/product'

import ProductList from '../components/ProductList'

class ProductContainer extends Component {

  render () {
    return this.props.children || <ProductList {...this.props} />
  }
}

const mapDispatchToProps = {
  ...actions
}

const mapStateToProps = (state) => ({
  products : state.product.products
})

export default connect(mapStateToProps, mapDispatchToProps)(ProductContainer)
