import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Link } from 'react-router'

import {
  actions
} from '../modules/product'

import ProductNew from '../components/ProductNew'

class ProductNewContainer extends Component {

  render () {
    return this.props.children || <ProductNew {...this.props} />
  }
}

const mapDispatchToProps = { ...actions }

const mapStateToProps = (state) => ({
  ...state.product
})

export default connect(mapStateToProps, mapDispatchToProps)(ProductNewContainer)
