import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Link } from 'react-router'

import {
  actions
} from '../modules/product'

import ProductDetails from '../components/ProductDetails'

class ProductViewContainer extends Component {

  render () {
    return this.props.children || <ProductDetails {...this.props} />
  }
}

const mapDispatchToProps = { ...actions }

const mapStateToProps = (state) => ({
  ...state.product
})

export default connect(mapStateToProps, mapDispatchToProps)(ProductViewContainer)
