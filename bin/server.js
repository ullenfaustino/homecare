const config = require('../config')
const server = require('../server/main')
// const io = require('../server/io')
const debug = require('debug')('app:bin:server')
const port = config.server_port

const _server = server.listen(port)
debug(`Server is now running at http://localhost:${port}.`)

// io.attach(_server)
