import axios from 'axios'

import { URL } from 'consts/apiConsts'
/**
 * Parses the JSON returned by a network request
 *
 * @param  {object} response A response from a network request
 *
 * @return {object}          The parsed JSON from the request
 */
function parseJSON (response) {
  return response.data.data || response.data
}

export function login (username, password) {
  return axios.post(`${URL}/auth/login`, {
    username,
    password
  }).then(parseJSON).then((payload) => {
    localStorage.token = payload.token
    return Promise.resolve(payload)
  })
}

export function findByUid (uid) {
  return axios.get(`${URL}/user/${uid}`).then(parseJSON)
}

export function updateUserActivation (data) {
  return axios.post(`${URL}/activate/account`, data).then(parseJSON)
}

export function sendMessage (data) {
  return axios.post(`${URL}/chat/sendMessage`, data).then(parseJSON)
}

export function getMessage (data) {
  return axios.post(`${URL}/chat/getMessage`, data).then(parseJSON)
}

export function getMessages (params) {
  return axios.get(`${URL}/chat/getMessages`, {
    params: params
  })
  // .then(parseJSON)
  .then((response) => response.data)
}

export function markAsRead (chatroom_id) {
  return axios.get(`${URL}/chat/markAsRead/${chatroom_id}`).then(parseJSON)
}

export function createAccount (params) {
  return axios.post(`${URL}/users/createAccount`, params).then(parseJSON)
}

export function getRecipients (uid) {
  return axios.get(`${URL}/user/${uid}/recipients`).then(parseJSON)
}

export function addRecipient (data) {
  return axios.post(`${URL}/seeker/add/recipient`, data).then(parseJSON)
}

export function findRecipient (recipientId) {
  return axios.get(`${URL}/find/recipient/${recipientId}`).then(parseJSON)
}

export function findUserById (id) {
  return axios.get(`${URL}/user/byId/${id}`).then(parseJSON)
}

export function hireByManualMatch (data) {
  return axios.post(`${URL}/provider/hire/manual-matching`, data).then(parseJSON)
}

export function fetchRecipients () {
  return axios.get(`${URL}/find/recipients`).then(parseJSON)
}
