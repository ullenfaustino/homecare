import { takeLatest, takeEvery, delay } from 'redux-saga'
import { take, call, put, fork, race } from 'redux-saga/effects'
import { browserHistory } from 'react-router'

import * as Api from './api'
import * as _Api from 'modules/api'

// ------------------------------------
// Constants
// ------------------------------------
export const APP_INIT_REQUESTED = 'APP_INIT_REQUESTED'
export const APP_INIT_SUCCESS = 'APP_INIT_SUCCESS'
export const APP_INIT_FAILED = 'APP_INIT_FAILED'

export const APP_TOOGLE_HEADER_SEARCHBAR = 'APP_TOOGLE_HEADER_SEARCHBAR'
export const APP_SHOW_SLIDER_PANE = 'APP_SHOW_SLIDER_PANE'

export const FETCH_MESSAGES = 'FETCH_MESSAGES'
export const IS_FETCHING_MESSAGES = 'IS_FETCHING_MESSAGES'

// ------------------------------------
// Actions
// ------------------------------------

/**
 * Tells the app we want to log out a user
 */
export function initApp () {
  return { type: APP_INIT_REQUESTED }
}

export function toogleHeaderSearchbar (isShow = true) {
  return { type: APP_TOOGLE_HEADER_SEARCHBAR, isShow: isShow }
}

export function fetchMessages (params) {
  return (dispatch, getState) => {
    const { auth } = getState()
    const { user } = auth
    dispatch({
      type: IS_FETCHING_MESSAGES
    })
    params = Object.assign({}, params, { user_id: user.id })
    return _Api.getMessages(params).then((response) => {
      dispatch({
        type: FETCH_MESSAGES,
        response
      })
      return response
    })
  }
}

export const actions = {
  initApp,
  fetchMessages
}

// ------------------
// Slider Pane
// --------------
export function showSliderPane (isShow = true, operation) {
  return { type: APP_SHOW_SLIDER_PANE, isShow: isShow, operation }
}

// ------------------------------------
// Action Handlers
// ------------------------------------
const ACTION_HANDLERS = {
  [APP_INIT_SUCCESS] : (state, action) => Object.assign({}, state, {
    isInit: true,
    ...action
  }),
  [APP_TOOGLE_HEADER_SEARCHBAR] : (state, action) => Object.assign({}, state, {
    isShowMainContent: action.isShow
  }),
  [APP_SHOW_SLIDER_PANE] : (state, action) => Object.assign({}, state, {
    isShowSliderPane: action.isShow,
    activeOperation: action.operation
  }),
  'LOCATION_CHANGE' : (state, action) => Object.assign({}, state, {
    isShowMainContent: true
  }),
  [IS_FETCHING_MESSAGES] : (state, action) => Object.assign({}, state, {
    isFetchingMessages: true
  }),
  [FETCH_MESSAGES] : (state, action) => Object.assign({}, state, {
    isFetchingMessages: false,
    messages: action.response.data,
    messages_pagination: action.response.pagination
  })
}

// ------------------------------------
// Reducer
// ------------------------------------
// The initial application state
const initialState = {
  isInit: false,
  isShowMainContent: true,
  isShowSliderPane: false,
  isFetchingMessages: false,
  messages: [],
  messages_pagination: undefined
}
export default function appReducer (state = initialState, action) {
  const handler = ACTION_HANDLERS[action.type]

  return handler ? handler(state, action) : state
}

// ------------------------------------
// Sagas
// ------------------------------------

function* doInitApp (action) {
  try {
    const [settings] = yield [
      // yield call(Api.getMe, action.data),
      yield call(Api.getSettings, action.data)
    ]
    yield put({ type: APP_INIT_SUCCESS, settings })
  } catch (e) {
    yield put({ type: APP_INIT_FAILED, message: e.message })
  }
}

export function* watchInitApp () {
  yield takeLatest(APP_INIT_REQUESTED, doInitApp)
}

export const sagas = [
  watchInitApp
]

// Little helper function to abstract going to different pages
function forwardTo (location) {
  browserHistory.push(location)
}
