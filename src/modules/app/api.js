import axios from 'axios'

import * as Utils from 'lib/utils'
import { URL } from 'consts/apiConsts'

// export function getMe (params) {
//   return axios.get(`${URL}/users/me`, {
//     params: params
//   }).then(Utils.parseJSON)
// }

export function getSettings (params) {
  return axios.get(`${URL}/app/init`, {
    params: params
  }).then(Utils.parseJSON)
}

export function findByUid (uid) {
  return axios.get(`${URL}/user/${uid}`).then(Utils.parseJSON)
}
