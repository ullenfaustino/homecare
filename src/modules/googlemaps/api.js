import axios from 'axios'

import * as Utils from 'lib/utils'
import { URL } from 'consts/apiConsts'

export function autoComplete (params) {
  return axios.get(`${URL}/google-maps/autocomplete`, {
    params: params
  })
  .then(Utils.parseJSON)
  .then((json = []) => {
    return {
      options: json.map((item) => {
        return Object.assign({}, item, {
          value: item.place_id,
          label: item.description
        })
      })
    }
  })
}
