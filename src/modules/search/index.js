import { takeLatest } from 'redux-saga'
import { take, put, select, call, fork } from 'redux-saga/effects'
import * as Api from './api'

// ------------------------------------
// Constants
// ------------------------------------
export const GLOBAL_FETCH_REQUESTED = 'GLOBAL_FETCH_REQUESTED'
export const GLOBAL_FETCH_SUCCEEDED = 'GLOBAL_FETCH_SUCCEEDED'
export const GLOBAL_FETCH_FAILED = 'GLOBAL_FETCH_FAILED'

// ------------------------------------
// Actions
// ------------------------------------
/**
 * Tells the app we want to log in a user
 * @param  {object} data  The data we're sending for log in
 */
export function searchRequest (data) {
  return { type: GLOBAL_FETCH_REQUESTED, data }
}

export const actions = {
  searchRequest
}

// ------------------------------------
// Action Handlers
// ------------------------------------
const ACTION_HANDLERS = {
  [GLOBAL_FETCH_REQUESTED] : (state, action) => Object.assign({}, state, {
    isLoading: true
  }),
  [GLOBAL_FETCH_SUCCEEDED] : (state, action) => Object.assign({}, state, {
    isLoading: false,
    results: action.results,
    searchText: action.q
  }),
  [GLOBAL_FETCH_FAILED] : (state, action) => Object.assign({}, state, {
    isLoading: false
  }),
  'LOCATION_CHANGE' : (state, action) => Object.assign({}, state, {
    results: {},
    searchText: ''
  })
}

// ------------------------------------
// Reducer
// ------------------------------------
const initialState = {
  results: {},
  isLoading: false,

  page: 1,
  pageSize: 10,
  searchText: '',
  sortName: undefined,
  sortOrder: undefined
}

export default function searchReducer (state = initialState, action) {
  const handler = ACTION_HANDLERS[action.type]
  return handler ? handler(state, action) : state
}

// ------------------------------------
// Sagas
// ------------------------------------
// worker Saga: will be fired on GLOBAL_FETCH_REQUESTED actions
function* search (action) {
  try {
    const { results = {} } = yield call(Api.search, action.data)
    yield put({
      type: GLOBAL_FETCH_SUCCEEDED,
      results
    })
  } catch (e) {
    console.log('error', e)
    yield put({ type: GLOBAL_FETCH_FAILED, message: e.message })
  }
}

/*
  Alternatively you may use takeLatest.

  Does not allow concurrent fetches of user. If "GLOBAL_FETCH_REQUESTED" gets
  dispatched while a fetch is already pending, that pending fetch is cancelled
  and only the latest one will be run.
*/
export function* watchSearch () {
  yield takeLatest(GLOBAL_FETCH_REQUESTED, search)
}

export const sagas = [
  watchSearch
]
