import axios from 'axios'

import * as Utils from 'lib/utils'
import { URL } from 'consts/apiConsts'

export function search (params) {
  return axios.get(`${URL}/search`, {
    params: params
  }).then(Utils.parseJSON)
}
