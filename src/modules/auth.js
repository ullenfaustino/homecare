import { take, call, put, fork, race } from 'redux-saga/effects'
import { hashSync } from 'bcryptjs'
import genSalt from '../auth/salt'
import { browserHistory } from 'react-router'
import auth from '../auth'
import * as authApi from './api'
// ------------------------------------
// Constants
// ------------------------------------
export const COUNTER_INCREMENT = 'COUNTER_INCREMENT'
export const COUNTER_DOUBLE = 'COUNTER_DOUBLE'

export const CHANGE_FORM = 'CHANGE_FORM'
export const SET_AUTH = 'SET_AUTH'
export const SENDING_REQUEST = 'SENDING_REQUEST'
export const LOGIN_REQUEST = 'LOGIN_REQUEST'
export const REGISTER_REQUEST = 'REGISTER_REQUEST'
export const LOGOUT = 'LOGOUT'
export const REQUEST_ERROR = 'REQUEST_ERROR'
export const CLEAR_ERROR = 'CLEAR_ERROR'

export const USER_REFRESHED = 'USER_REFRESHED'
export const USER_REFRESH_REQUEST = 'USER_REFRESH_REQUEST'

// ------------------------------------
// Actions
// ------------------------------------

/**
 * Tells the app we want to log in a user
 * @param  {object} data          The data we're sending for log in
 * @param  {string} data.username The username of the user to log in
 * @param  {string} data.password The password of the user to log in
 */
export function loginRequest (data) {
  console.log('action loginRequest')
  return { type: LOGIN_REQUEST, data }
}

/**
 * Tells the app we want to log out a user
 */
export function logoutRequest () {
  console.log('logoutRequest')
  return { type: LOGOUT }
}

export function refreshUser () {
  return (dispatch, getState) => {
    dispatch({
      type: USER_REFRESH_REQUEST
    })
    const { auth } = getState()
    return authApi.findByUid(auth.user.uid).then((payload) => {
      dispatch({
        type: USER_REFRESHED,
        payload
      })
      return payload
    })
  }
}

export const actions = {
  loginRequest,
  refreshUser
}

// ------------------------------------
// Action Handlers
// ------------------------------------
const ACTION_HANDLERS = {
  [COUNTER_INCREMENT] : (state, action) => state + action.payload,
  [SET_AUTH] : (state, action) => {
    const payload = action.payload ? action.payload : {}
    return Object.assign({}, state, {
      loggedIn: action.newAuthState,
      user: payload.user,
      token: payload.token
    })
  },
  [SENDING_REQUEST] : (state, action) => Object.assign({}, state, {
    currentlySending: action.sending
  }),
  [USER_REFRESHED] : (state, action) => {
    return Object.assign({}, state, {
      user: action.payload,
      currentlySending: false
    })
  },
  [USER_REFRESH_REQUEST] : (state, action) => Object.assign({}, state, {
    currentlySending: true
  })
}

// ------------------------------------
// Reducer
// ------------------------------------
// The initial application state
const initialState = {
  formState: {
    username: '',
    password: ''
  },
  error: '',
  currentlySending: false
  // loggedIn: auth.loggedIn()
}
export default function counterReducer (state = initialState, action) {
  const handler = ACTION_HANDLERS[action.type]

  return handler ? handler(state, action) : state
}

// ------------------------------------
// Sagas
// ------------------------------------
/**
 * Effect to handle authorization
 * @param  {string} username               The username of the user
 * @param  {string} password               The password of the user
 * @param  {object} options                Options
 * @param  {boolean} options.isRegistering Is this a register request?
 */
export function * authorize ({ username, password, operator, isRegistering }) {
  // We send an action that tells Redux we're sending a request
  yield put({ type: SENDING_REQUEST, sending: true })

  // We then try to register or log in the user, depending on the request
  try {
    let salt = genSalt(username)
    let hash = hashSync(password, salt)
    let response

    // For either log in or registering, we call the proper function in the `auth`
    // module, which is asynchronous. Because we're using generators, we can work
    // as if it's synchronous because we pause execution until the call is done
    // with `yield`!
    if (isRegistering) {
      response = yield call(auth.register, username, hash)
    } else {
      response = yield call(authApi.login, username, password, operator)
    }
    return response
  } catch (error) {
    let __message
    try {
      __message = error.response.data.message
    } catch (e) {
      __message = 'Invalid username or password.'
    }
    // If we get an error we send Redux the appropiate action and return
    yield put({ type: REQUEST_ERROR, error: __message })

    return false
  } finally {
    // When done, we tell Redux we're not in the middle of a request any more
    yield put({ type: SENDING_REQUEST, sending: false })
  }
}

/**
 * Effect to handle logging out
 */
export function * logout () {
  // We tell Redux we're in the middle of a request
  yield put({ type: SENDING_REQUEST, sending: true })

  // Similar to above, we try to log out by calling the `logout` function in the
  // `auth` module. If we get an error, we send an appropiate action. If we don't,
  // we return the response.
  try {
    let response = yield call(auth.logout)
    yield put({ type: SENDING_REQUEST, sending: false })

    return response
  } catch (error) {
    yield put({ type: REQUEST_ERROR, error: error.message })
  }
}

/**
 * Log in saga
 */
export function * loginFlow () {
  // Because sagas are generators, doing `while (true)` doesn't block our program
  // Basically here we say "this saga is always listening for actions"
  while (true) {
    // And we're listening for `LOGIN_REQUEST` actions and destructuring its payload
    let request = yield take(LOGIN_REQUEST)
    let { username, password } = request.data

    // A `LOGOUT` action may happen while the `authorize` effect is going on, which may
    // lead to a race condition. This is unlikely, but just in case, we call `race` which
    // returns the "winner", i.e. the one that finished first
    let winner = yield race({
      auth: call(authorize, { username, password, isRegistering: false }),
      logout: take(LOGOUT)
    })

    // If `authorize` was the winner...
    if (winner.auth && winner.auth.token) {
      // ...we send Redux appropiate actions
      yield put({ type: SET_AUTH, newAuthState: true, payload: winner.auth }) // User is logged in (authorized)
      yield put({ type: CHANGE_FORM, newFormState: { username: '', password: '' } }) // Clear form

      const { query = {} } = browserHistory.getCurrentLocation()
      if (query && query.return_to) {
        forwardTo(query.return_to)
      } else {
        forwardTo('/') // Go to dashboard page
      }

      // If `logout` won...
    } else if (winner.logout) {
      console.log('logout winner')
      // ...we send Redux appropiate action
      yield put({ type: SET_AUTH, newAuthState: false }) // User is not logged in (not authorized)
      yield call(logout) // Call `logout` effect
      forwardTo('/login') // Go to root page
    }
  }
}

/**
 * Log out saga
 * This is basically the same as the `if (winner.logout)` of above, just written
 * as a saga that is always listening to `LOGOUT` actions
 */
export function * logoutFlow () {
  while (true) {
    yield take(LOGOUT)
    yield put({ type: SET_AUTH, newAuthState: false })

    yield call(logout)
    forwardTo('/login')
  }
}

export const sagas = [
  loginFlow,
  logoutFlow
]

// Little helper function to abstract going to different pages
function forwardTo (location) {
  browserHistory.push(location)
}
