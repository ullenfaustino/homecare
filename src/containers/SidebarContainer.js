import { connect } from 'react-redux'

import { logoutRequest } from 'modules/auth'
import Sidebar from '../components/Sidebar'

const mapDispatchToProps = {
  logout: () => logoutRequest()
}

const mapStateToProps = (state) => ({
  auth : state.auth
})

// NOTE:: this made pure false to make the active route working properly
export default connect(mapStateToProps, mapDispatchToProps, null, {
  pure: false
})(Sidebar)
