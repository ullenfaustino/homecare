import { connect } from 'react-redux'

import { logoutRequest } from 'modules/auth'
import { searchRequest } from 'modules/search'
import { toogleHeaderSearchbar, showSliderPane } from 'modules/app'
import Header from '../components/Header'

const mapDispatchToProps = {
  logout: () => logoutRequest(),
  searchRequest,
  toogleHeaderSearchbar,
  showSliderPane
}

const mapStateToProps = (state) => ({
  auth : state.auth,
  searchText: state.search.searchText
})

export default connect(mapStateToProps, mapDispatchToProps, null, {
  pure: false
})(Header)
