import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { DateTimePicker } from 'react-widgets'
import moment from 'moment'
import _ from 'lodash'

export default class DateTimePickerHOC extends React.Component {
  constructor (props) {
    super(props)

    const value = this.props.value || new Date()
    this.state = {
      value: value
    }
  }
  componentWillReceiveProps (nextProps) {
    // Should be a controlled component.
    if ('value' in nextProps) {
      const value = nextProps.value
      this.setState({
        value: value
      })
    }
  }
  render () {
    return (
      <DateTimePicker
        {...this.props.pickerProps}

        value={(this.state.value) ? this.state.value : null}
        // onSelect={(name, value) => {
        //   const onChange = this.props.onChange
        //   if (onChange) {
        //     onChange(value)
        //   }
        // }}

        onChange={(value) => {
          if (_.has(this.props.pickerProps, 'minCurrent')) {
            if (moment(value).toDate() < this.props.pickerProps.minCurrent) {
              value = this.props.pickerProps.minCurrent
            }
          }

          const onChange = this.props.onChange
          if (onChange) {
            onChange(value)
          }
        }}
      />
    )
  }
}

// Specifies the default values for props:
DateTimePickerHOC.defaultProps = {
  pickerProps: PropTypes.object
  // value: new Date()
}
