import React, { Component, PropTypes } from 'react'
import { IndexLink, Link } from 'react-router'
import { connect } from 'react-redux'

import ReactImageFallback from 'react-image-fallback'
import ProfileModal from './../ProfileModal'
import ChatRoom from 'components/ChatRoom'

import * as _Api from 'modules/api'
import { fetchMessages } from 'modules/app'

class Header extends Component {
  constructor (props) {
    super(props)
    this.state = {
      modal_visibility: false,
      chat_visibility: false,
      msg_recipient: undefined
    }
  }

  componentDidMount () {
    const params = {
      page: 1,
      pageSize: 10
    }
    this.props.fetchMessages(params)
  }

  handleProfileModalView = () => {
    return (
      <ProfileModal
        {...this.props}
        visibility={this.state.modal_visibility}
        setVisibility={(visibility) => {
          this.setState({
            modal_visibility: visibility
          })
        }}
      />
    )
  }

  renderChatRoom = () => {
    const { chat_visibility, msg_recipient } = this.state
    return (
      <ChatRoom
        recipient={msg_recipient}
        visibility={chat_visibility}
        setVisibility={(visibility) => {
          this.setState({ chat_visibility: visibility })
        }}
      />
    )
  }

  render () {
    const { user } = this.props.auth
    const { chat_visibility } = this.state
    const { messages, messages_pagination } = this.props.app

    const unreadChat = messages.filter((message) => (message.is_read == 1 && message.last_sender_id != user.id))

    return (
      <div className='topbar'>
        <div className='topbar-left'>
          <div className='text-center'>
            <a href='/' className='logo logo-admin'>
              <img src='/logos/logo_main.png' className='logo' style={{ width: 140, height: 75 }} />
            </a>
          </div>
        </div>
        <div className='navbar navbar-default' role='navigation'>
          <div className='container'>
            <div className=''>
              <div className='pull-left'>
                {/* <button type='button' className='button-menu-mobile open-left waves-effect waves-light'> <i className='ion-navicon' /> </button> <span className='clearfix' /> */}
                <h3 style={{
                  'color': 'white',
                  'marginTop': '15px',
                  'marginLeft' : '15px',
                  'textDecoration' : 'underline'
                }}>Welcome to Care Network { user.first_name }!</h3>
              </div>
              {/* <form className='navbar-form pull-left' role='search'>
                <div className='form-group'>
                  <input type='text' className='form-control search-bar' placeholder='Search...' />
                </div>
                <button type='submit' className='btn btn-search'><i className='fa fa-search' /></button>
              </form> */}
              <ul className='nav navbar-nav navbar-right pull-right'>
                <li className='dropdown hidden-xs'>
                  <a href='#' data-target='#' className='dropdown-toggle waves-effect waves-light notification-icon-box' data-toggle='dropdown' aria-expanded='true'>
                    <i className='fa fa-envelope-o' />
                    {
                      (unreadChat.length > 0) &&
                      <span className='badge badge-xs badge-danger' />
                    }
                  </a>
                  <ul className='dropdown-menu dropdown-menu-lg'>
                    <li className='text-center notifi-title'>Messages
                      {
                        (unreadChat.length > 0) &&
                        <span className='badge badge-xs badge-success'>{unreadChat.length}</span>
                      }
                    </li>
                    {
                      (messages.length > 0)
                      ? <li className='list-group'>
                        {
                          messages.map((message) => {
                            const unreadMessages = message.messages.filter((unreadMessage) => (unreadMessage.is_read == 1 && unreadMessage.sender_id != user.id))
                            const _msg = (unreadMessages.length > 0) ? _.last(unreadMessages) : _.last(message.messages)
                            const _from = (message.sender_id == user.id) ? message.recipient : message.sender
                            return (
                              <a key={message.id} href='javascript:void(0);' className='list-group-item' onClick={() => {
                                this.setState({ chat_visibility: true, msg_recipient: _from }, () => {
                                  if (message.is_read == 1 && message.last_sender_id != user.id) {
                                    _Api.markAsRead(message.id)
                                    .then((response) => {
                                      const params = {
                                        page: 1,
                                        pageSize: 10
                                      }
                                      this.props.fetchMessages(params)
                                    })
                                    .catch((err) => {
                                      console.log('err', err)
                                    })
                                  }
                                })
                              }}>
                                <div className='row'>
                                  <div className='col-sm-2'>
                                    <ReactImageFallback
                                      src={_from.avatar || '/user-avatar/user.svg'}
                                      fallbackImage='/user-avatar/user.svg'
                                      alt={_from.first_name + ' ' + _from.last_name}
                                      className='img-circle'
                                      style={{ width: 35, height: 35 }}
                                  />
                                  </div>
                                  <div className='col-sm-10'>
                                    <div className='media'>
                                      <div className='media-body clearfix'>
                                        <div className='media-heading'>{(message.is_read == 1 && message.last_sender_id != user.id) ? `New Message received from ${_from.first_name + ' ' + _from.last_name}` : _from.first_name + ' ' + _from.last_name}</div>
                                        <p className='m-0'> <small>{_msg.message}</small></p>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </a>
                            )
                          })
                        }
                        {
                          (messages_pagination.rowCount > 10) &&
                          <a href='javascript:void(0);' className='list-group-item'> <small className='text-primary'>See all Messages</small> </a>
                        }
                      </li>
                      : <div className='text-center'>
                        <h4>No Messages</h4>
                      </div>
                    }
                  </ul>
                </li>
                {/* <li className='dropdown hidden-xs'>
                  <a href='#' data-target='#' className='dropdown-toggle waves-effect waves-light notification-icon-box' data-toggle='dropdown' aria-expanded='true'> <i className='fa fa-bell' /> <span className='badge badge-xs badge-danger' /> </a>
                  <ul className='dropdown-menu dropdown-menu-lg'>
                    <li className='text-center notifi-title'>Notification <span className='badge badge-xs badge-success'>3</span></li>
                    <li className='list-group'>
                      <a href='javascript:void(0);' className='list-group-item'>
                        <div className='media'>
                          <div className='media-heading'>Your order is placed</div>
                          <p className='m-0'> <small>Dummy text of the printing and typesetting industry.</small></p>
                        </div>
                      </a>
                      <a href='javascript:void(0);' className='list-group-item'>
                        <div className='media'>
                          <div className='media-body clearfix'>
                            <div className='media-heading'>New Message received</div>
                            <p className='m-0'> <small>You have 87 unread messages</small></p>
                          </div>
                        </div>
                      </a>
                      <a href='javascript:void(0);' className='list-group-item'>
                        <div className='media'>
                          <div className='media-body clearfix'>
                            <div className='media-heading'>Your item is shipped.</div>
                            <p className='m-0'> <small>It is a long established fact that a reader will</small></p>
                          </div>
                        </div>
                      </a>
                      <a href='javascript:void(0);' className='list-group-item'> <small className='text-primary'>See all notifications</small> </a>
                    </li>
                  </ul>
                </li> */}
                <li className='dropdown'>
                  <a href='#' className='dropdown-toggle profile waves-effect waves-light' data-toggle='dropdown' aria-expanded='true'>
                    <ReactImageFallback
                      src={user.avatar || '/user-avatar/user.svg'}
                      fallbackImage='/user-avatar/user.svg'
                      alt={user.first_name}
                      className='img-circle'
                      style={{ width: 35, height: 35 }}
                    />
                    {/* <span className='profile-username'> {user.first_name + ' ' + user.last_name} <br /> <small>{ (user.user_type == 0) ? 'Admin' : ((user.user_type == 1) ? 'Care Provider' : 'Care Seeker') }</small> </span> */}
                  </a>
                  <ul className='dropdown-menu'>
                    <li><a href='javascript:void(0)' onClick={(ev) => {
                      ev.preventDefault()
                      this.setState({ modal_visibility: true })
                    }}> Profile</a></li>
                    {/* <li><a href='javascript:void(0)'><span className='badge badge-success pull-right'>5</span> Settings </a></li>
                    <li><a href='javascript:void(0)'> Lock screen</a></li> */}
                    <li className='divider' />
                    <li>
                      <a href='javascript:void(0)' onClick={(ev) => {
                        ev.preventDefault()
                        this.props.logout()
                      }}> Logout</a>
                    </li>
                  </ul>
                </li>
              </ul>
            </div>
          </div>
          {
            (this.state.modal_visibility) && this.handleProfileModalView()
          }
          {
            chat_visibility && this.renderChatRoom()
          }
        </div>
      </div>
    )
  }
}

Header.propTypes = {
}

const mapDispatchToProps = {
  fetchMessages
}
const mapStateToProps = (state) => ({
  ...state
})

export default connect(mapStateToProps, mapDispatchToProps)(Header)
