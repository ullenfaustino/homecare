import React, { Component } from 'react'
import { Link } from 'react-router'

class ActiveLiTag extends Component {
  render () {
    let isActive = this.context.router.isActive(this.props.to, true)
    let className = isActive ? 'active' : ''

    return (
      <li className={className}>{this.props.children}</li>
    )
  }
}

ActiveLiTag.contextTypes = {
  router: React.PropTypes.object
}

export default ActiveLiTag
