import React, { Component, PropTypes } from 'react'
import { connect } from 'react-redux'
import { Upload, Icon, Modal, Button, Spin, message } from 'antd'
import { Link, browserHistory } from 'react-router'
import moment from 'moment'
import _ from 'lodash'
import * as _Api from 'modules/api'
import * as Helper from 'consts/helper'
import { URL } from 'consts/apiConsts'
import { refreshUser } from 'modules/auth'
import ReactImageFallback from 'react-image-fallback'
import ReactStars from 'react-stars'
import TotalRatings from './TotalRatings'

class ProfileModal extends Component {

  constructor (props) {
    super(props)
    this.state = {
      loading: false
    }
  }

  handleChange = (info) => {
    const { auth } = this.props
    if (info.file.status === 'uploading') {
      this.setState({ loading: true })
      return
    }
    if (info.file.status === 'done') {
      // Get this url from response in real world.
      getBase64(info.file.originFileObj, imageUrl => this.setState({
        imageUrl,
        loading: false
      }, () => {
        this.props.refreshUser()
      }))
    }
  }

  render () {
    const { visibility } = this.props
    const { user } = this.props.auth
    let currentUser = user

    const uploadButton = (
      <div>
        <Icon type={this.state.loading ? 'loading' : 'plus'} />
        <div className='ant-upload-text'>Upload</div>
      </div>
    )
    const imageUrl = this.state.imageUrl
    const actionURL = `${URL}/uploads/avatar?userId=${this.props.auth.user.id}`

    return (
      <Modal
        destroyOnClose
        title='My Profile'
        width={950}
        visible={visibility}
        footer={null}
        onOk={() => {
          this.props.setVisibility(false)
        }}
        onCancel={() => {
          this.props.setVisibility(false)
        }}
        >
        <div className='row'>
          <div className='col-sm-12'>
            <div className='panel panel-primary'>
              <div className='panel-body'>
                <form>
                  <legend>Profile Avatar</legend>
                  <div className='form-group'>
                    <Upload
                      name='avatar'
                      listType='picture-card'
                      className='avatar-uploader'
                      showUploadList={false}
                      action={actionURL}
                      beforeUpload={beforeUpload}
                      onChange={this.handleChange}
                      >
                      {(imageUrl || currentUser.avatar) ? <img width={120} height='auto' src={imageUrl || currentUser.avatar} alt='' /> : uploadButton}
                    </Upload>
                  </div>
                  <legend>Personal Information</legend>
                  <div className='form-group'>
                    <span className='col-md-2'>Full Name</span>
                    <div className='col-md-10'>
                      <label className='control-label'>{ currentUser.first_name + ' ' + currentUser.last_name }</label>
                    </div>
                  </div>
                  {
                    (currentUser.user_type == 1) &&
                    <div className='form-group'>
                      <span className='col-md-2'>Address</span>
                      <div className='col-md-10'>
                        <label className='control-label'>{ currentUser.location.label }</label>
                      </div>
                    </div>
                  }
                  <div className='form-group'>
                    <span className='col-md-2'>Birthday</span>
                    <div className='col-md-10'>
                      <label className='control-label'>{ (currentUser.birthday) ? moment(currentUser.birthday).format('LL') : <small>not set</small> }</label>
                    </div>
                  </div>
                  {
                    (currentUser.birthday) &&
                    <div className='form-group'>
                      <span className='col-md-2'>Age</span>
                      <div className='col-md-10'>
                        <label className='control-label'>{ Helper.getAge(currentUser.birthday) }</label>
                      </div>
                    </div>
                  }
                  <div className='form-group'>
                    <span className='col-md-2'>Gender</span>
                    <div className='col-md-10'>
                      <label className='control-label'>{ (currentUser.gender == 0) ? 'Female' : 'Male' }</label>
                    </div>
                  </div>
                  <div className='form-group'>
                    <span className='col-md-2'>Zip Code</span>
                    <div className='col-md-10'>
                      <label className='control-label'>{ currentUser.zip_code }</label>
                    </div>
                  </div>
                  <div className='form-group'>
                    <span className='col-md-2'>Contact Number</span>
                    <div className='col-md-10'>
                      <label className='control-label'>{ currentUser.contact_no }</label>
                    </div>
                  </div>
                  <legend>Account Details</legend>
                  <div className='form-group'>
                    <span className='col-md-2'>Email Address</span>
                    <div className='col-md-10'>
                      <label className='control-label'>{ currentUser.email }</label>
                    </div>
                  </div>
                  <div className='form-group'>
                    <span className='col-md-2'>Username</span>
                    <div className='col-md-10'>
                      <label className='control-label'>{ currentUser.username }</label>
                    </div>
                  </div>
                </form>
              </div>
            </div>
          </div>
          {
            (currentUser.user_type == 1 && currentUser.ratings.length > 0) &&
            <div className='col-sm-12'>
              <div className='panel panel-primary'>
                <div className='panel-body'>
                  <legend>Ratings and Feedbacks</legend>
                  <TotalRatings
                    ratings={currentUser.ratings}
                  />
                </div>
              </div>
            </div>
          }
        </div>
      </Modal>
    )
  }
}

function getBase64 (img, callback) {
  const reader = new FileReader()
  reader.addEventListener('load', () => callback(reader.result))
  reader.readAsDataURL(img)
}

function beforeUpload (file) {
  const isJPG = file.type === 'image/jpeg'
  if (!isJPG) {
    message.error('You can only upload JPG file!')
  }
  const isLt2M = file.size / 1024 / 1024 < 2
  if (!isLt2M) {
    message.error('Image must smaller than 2MB!')
  }
  return isJPG && isLt2M
}

// ProfileModal.propTypes = {
// }

const mapDispatchToProps = { refreshUser }

const mapStateToProps = (state) => ({ ...state })

export default connect(mapStateToProps, mapDispatchToProps)(ProfileModal)
