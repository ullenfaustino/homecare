import React, { Component, PropTypes } from 'react'

export default class PrevNext extends Component {
  constructor (props) {
    super(props)
  }

  render () {
    const { page, pageCount, pageSize, rowCount, total, recordLength } = this.props
    const maxRowCount = page * pageSize
    const start = page === 1 ? 1 : maxRowCount > rowCount ? maxRowCount - pageSize : maxRowCount - length
    const end = page === 1 ? length : maxRowCount > rowCount ? rowCount : maxRowCount
    const isPrevDisabled = page === 1
    const isNextDisabled = maxRowCount > rowCount
    return (<div style={{ float: 'left' }}>
      <div className='btn-group results-count' style={{
        color: '#4f5c61',
        paddingTop: 8,
        marginRight: 4
      }}>{start}-{end} of {rowCount}</div>
      <div className='btn-group'>
        <button className='btn btn-default' disabled={isPrevDisabled} onClick={this.props.onPrevClick}>Prev</button>
        <button className='btn btn-default' disabled={isNextDisabled} onClick={this.props.onNextClick}>Next</button>
      </div>
    </div>)
  }
}

PrevNext.propTypes = {}
