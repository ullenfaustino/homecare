import React, { Component, PropTypes } from 'react'

export default class Ibox extends Component {
  constructor (props) {
    super(props)
  }

  render () {
    const { children, isStatic, title } = this.props
    const style = {
      cursor: isStatic ? '' : 'pointer'
    }
    return (
      <div className='ibox' style={style}>
        {
          title
          ? <div className='ibox-title'>{title}</div>
          : ' '
           /* <div className='ibox-title'>
            <h5>Your daily feed</h5>
            <div className='ibox-tools'>
              <span className='label label-warning-light pull-right'>10 Messages</span>
            </div>
          </div> */
        }
        <div className='ibox-content'>{children}</div>
      </div>
    )
  }
}

Ibox.propTypes = {}
