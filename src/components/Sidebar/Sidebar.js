import React, { Component, PropTypes } from 'react'
import { IndexLink, Link } from 'react-router'
import { Button } from 'antd'
import { LinkContainer, IndexLinkContainer } from 'react-router-bootstrap'
import { Navbar, Nav, NavItem, NavDropdown, MenuItem, DropdownButton } from 'react-bootstrap'
import ReactImageFallback from 'react-image-fallback'

import ProfileModal from './../ProfileModal'

export default class Sidebar extends Component {
  constructor (props) {
    super(props)

    this.state = {
      modal_visibility: false
    }
  }

  handleSidebarStatus = (pathName) => {
    const { routes } = this.props

    if (routes[routes.length - 1].path === pathName) {
      return 'waves-effect active'
    } else {
      return 'waves-effect'
    }
  }

  handleProfileModalView = () => {
    return (
      <ProfileModal
        {...this.props}
        visibility={this.state.modal_visibility}
        setVisibility={(visibility) => {
          this.setState({
            modal_visibility: visibility
          })
        }}
      />
    )
  }

  handleSideBarMenus = () => {
    const { user } = this.props.auth
    if (user.user_type == 0) { // ADMIN
      return (
        <ul>
          <li><a href='/admin' className={this.handleSidebarStatus('/admin')}><i className='mdi mdi-home' /><span> Dashboard</span></a></li>
          <li><a href='/admin/users' className={this.handleSidebarStatus('/admin/users')}><i className='mdi mdi-account-multiple' /><span> Users</span></a></li>
          <li><a href='/admin/recipients' className={this.handleSidebarStatus('/admin/recipients')}><i className='mdi mdi-account-multiple' /><span> Care Recipients</span></a></li>
          <li><a href='/admin/applications' className={this.handleSidebarStatus('/admin/applications')}><i className='mdi mdi-account-check' /><span> Applications</span></a></li>
          <li><a href='/admin/manual-assign' className={this.handleSidebarStatus('/admin/manual-assign')}><i className='mdi mdi-account-multiple-plus' /><span> Manual Match</span></a></li>
          <li><a href='/admin/settings' className={this.handleSidebarStatus('/admin/settings')}><i className='mdi mdi-settings' /><span> Settings</span></a></li>
        </ul>
      )
    } else if (user.user_type == 1) { // Provider
      return (
        <ul>
          <li><a href='/provider' className={this.handleSidebarStatus('/provider')}><i className='mdi mdi-home' /><span> Dashboard</span></a></li>
          <li><a href='/provider/preferences' className={this.handleSidebarStatus('/provider/preferences')}><i className='mdi mdi-account-card-details' /><span> Care Preferences</span></a></li>
          <li><a href='/provider/application-status' className={this.handleSidebarStatus('/provider/application-status')}><i className='mdi mdi-av-timer' /><span> My Application</span></a></li>
          <li style={{ display: (user.application_status == 2) ? '' : 'none' }}><a href='/provider/care-seekers' className={this.handleSidebarStatus('/provider/care-seekers')}><i className='mdi mdi-account-location' /><span> Find Care Seeker</span></a></li>
          <li style={{ display: (user.application_status == 2) ? '' : 'none' }}><a href='/provider/clients' className={this.handleSidebarStatus('/provider/clients')}><i className='mdi mdi-sitemap' /><span> My Clients</span></a></li>
          {/* <li><a href='#' className={this.handleSidebarStatus('/#')}><i className='mdi mdi-wechat' /><span> Community</span></a></li> */}
        </ul>
      )
    } else if (user.user_type == 2) { // Seeker
      return (
        <ul>
          <li><a href='/seeker' className={this.handleSidebarStatus('/seeker')}><i className='mdi mdi-home' /><span> Dashboard</span></a></li>
          <li><a href='/seeker/preferences' className={this.handleSidebarStatus('/seeker/preferences')}><i className='mdi mdi-account-card-details' /><span> Preferences</span></a></li>
          <li><a href='/seeker/recipients' className={this.handleSidebarStatus('/seeker/recipients')}><i className='mdi mdi-account-multiple-plus' /><span> Care Recipients</span></a></li>
          <li><a href='/seeker/find-provider' className={this.handleSidebarStatus('/seeker/find-provider')}><i className='mdi mdi-account-location' /><span> Find Care Provider</span></a></li>
          <li><a href='/seeker/care-providers' className={this.handleSidebarStatus('/seeker/care-providers')}><i className='mdi mdi-account-multiple' /><span> My Care Providers</span></a></li>
          <li><a href='/seeker/post-job' className={this.handleSidebarStatus('/seeker/post-job')}><i className='mdi mdi-clipboard-text' /><span> Job Post</span></a></li>
          {/* <li><a href='#' className={this.handleSidebarStatus('/#')}><i className='mdi mdi-wechat' /><span> Community</span></a></li> */}
        </ul>
      )
    }
  }

  render () {
    const { user } = this.props.auth

    return (
      <div className='left side-menu'>
        <div className='sidebar-inner slimscrollleft'>
          <div className='user-details'>
            <div className='text-center'>
              <ReactImageFallback
                src={user.avatar}
                fallbackImage='/user-avatar/user.svg'
                alt={user.first_name}
                className='img-circle'
                style={{ width: 65, height: 65 }}
              />
            </div>
            <div className='user-info'>
              <div className='dropdown'> <a href='#' className='dropdown-toggle' data-toggle='dropdown' aria-expanded='false'>{ user.first_name + ' ' + user.last_name}</a>
                <ul className='dropdown-menu'>
                  <li><a href='javascript:void(0)' onClick={(ev) => {
                    ev.preventDefault()
                    this.setState({ modal_visibility: true })
                  }}> Profile</a></li>
                  <li className='divider' />
                  <li>
                    <a href='javascript:void(0)' onClick={(ev) => {
                      ev.preventDefault()
                      this.props.logout()
                    }}> Logout</a>
                  </li>
                </ul>
              </div>
              <p className='text-muted m-0'>{ (user.user_type == 0) ? 'Admin' : ((user.user_type == 1) ? 'Care Provider' : 'Care Seeker') }</p>
            </div>
          </div>
          <div id='sidebar-menu'>
            { this.handleSideBarMenus() }
          </div>
          <div className='clearfix' />

          {
            (this.state.modal_visibility) && this.handleProfileModalView()
          }
        </div>
      </div>
    )
  }
}

Sidebar.propTypes = {
}
