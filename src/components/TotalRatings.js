import React, { Component, PropTypes } from 'react'
import ReactStars from 'react-stars'
import ReactImageFallback from 'react-image-fallback'
import * as Helper from 'consts/helper'

export default class TotalRatings extends Component {
  constructor (props) {
    super(props)
  }

  render () {
    const { ratings } = this.props

    const fiveStars = ratings.filter((rating) => (rating.ratings == 5))
    const fourStars = ratings.filter((rating) => (rating.ratings == 4))
    const threeStars = ratings.filter((rating) => (rating.ratings == 3))
    const twoStars = ratings.filter((rating) => (rating.ratings == 2))
    const oneStar = ratings.filter((rating) => (rating.ratings == 1))

    const ave_fiveStars = (fiveStars.length / ratings.length) * 100
    const ave_fourStars = (fourStars.length / ratings.length) * 100
    const ave_threeStars = (threeStars.length / ratings.length) * 100
    const ave_twoStars = (twoStars.length / ratings.length) * 100
    const ave_oneStar = (oneStar.length / ratings.length) * 100

    return (
      <div>
        <div className='row' style={{ marginBottom: '35px' }}>
          <div className='col-sm-3 text-center'>
            <span style={{ fontSize: '45px' }}>{Helper.getAverageRating(ratings)}</span>
            <ReactStars
              count={5}
              half={false}
              edit={false}
              value={Helper.getAverageRating(ratings)}
              size={35}
              color2={'#ffd700'} />
            <span><i className='mdi mdi-account' /> {ratings.length} total</span>
          </div>
          <div className='col-sm-2'>
            <ReactStars
              count={5}
              edit={false}
              size={20}
              value={5}
              color2={'#ffd700'} />
            <ReactStars
              count={4}
              edit={false}
              size={20}
              value={4}
              color2={'#ffd700'} />
            <ReactStars
              count={3}
              edit={false}
              size={20}
              value={3}
              color2={'#ffd700'} />
            <ReactStars
              count={2}
              edit={false}
              size={20}
              value={2}
              color2={'#ffd700'} />
            <ReactStars
              count={1}
              edit={false}
              size={20}
              value={1}
              color2={'#ffd700'} />
          </div>
          <div className='col-sm-5'>
            <div className='progress' style={{ marginTop: '13px' }}>
              <div className={`progress-bar progress-bar-${(ave_fiveStars < 60) ? `warning` : `success`}`} role='progressbar' aria-valuenow='60' aria-valuemin='0' aria-valuemax='100' style={{ width: `${ave_fiveStars}%` }}>
                <span className='sr-only'>{`${ave_fiveStars}%`} Complete</span>
              </div>
            </div>
            <div className='progress' style={{ marginTop: '19px' }}>
              <div className={`progress-bar progress-bar-${(ave_fourStars < 60) ? `warning` : `success`}`} role='progressbar' aria-valuenow='60' aria-valuemin='0' aria-valuemax='100' style={{ width: `${ave_fourStars}%` }}>
                <span className='sr-only'>{`${ave_fourStars}%`} Complete</span>
              </div>
            </div>
            <div className='progress' style={{ marginTop: '19px' }}>
              <div className={`progress-bar progress-bar-${(ave_fiveStars < 60) ? `warning` : `success`}`} role='progressbar' aria-valuenow='60' aria-valuemin='0' aria-valuemax='100' style={{ width: `${ave_threeStars}%` }}>
                <span className='sr-only'>{`${ave_threeStars}%`} Complete</span>
              </div>
            </div>
            <div className='progress' style={{ marginTop: '19px' }}>
              <div className={`progress-bar progress-bar-${(ave_twoStars < 60) ? `warning` : `success`}`} role='progressbar' aria-valuenow='60' aria-valuemin='0' aria-valuemax='100' style={{ width: `${ave_twoStars}%` }}>
                <span className='sr-only'>{`${ave_twoStars}%`} Complete</span>
              </div>
            </div>
            <div className='progress' style={{ marginTop: '19px' }}>
              <div className={`progress-bar progress-bar-${(ave_oneStar < 60) ? `warning` : `success`}`} role='progressbar' aria-valuenow='60' aria-valuemin='0' aria-valuemax='100' style={{ width: `${ave_oneStar}%` }}>
                <span className='sr-only'>{`${ave_oneStar}%`} Complete</span>
              </div>
            </div>
          </div>
        </div>
        {
          ratings.map((rating) => {
            return (
              <div className='row'>
                <div className='col-sm-2'>
                  <ReactImageFallback
                    src={rating.recipient.avatar}
                    fallbackImage='/user-avatar/user.svg'
                    alt={rating.recipient.name}
                    className='img-circle pull-right'
                    style={{ width: 50, height: 50 }}
                  />
                </div>
                <div className='col-sm-10'>
                  <b>{rating.recipient.name}</b>
                  <ReactStars
                    count={5}
                    edit={false}
                    value={rating.ratings}
                    size={20}
                    color2={'#ffd700'} />
                  <p>{rating.comment}</p>
                </div>
              </div>
            )
          })
        }
      </div>
    )
  }
}

TotalRatings.propTypes = {}
