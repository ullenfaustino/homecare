import React, { Component, PropTypes } from 'react'
import Select from 'react-select'

export default class SelectAsync extends React.Component {
  constructor (props) {
    super(props)

    const __def = this.props.multi ? [] : {}
    const value = this.props.value || __def
    this.state = {
      value: value
    }
  }
  componentWillReceiveProps (nextProps) {
    if ('value' in nextProps) {
      const value = nextProps.value
      this.setState({
        value: value
      })
    }
  }
  render () {
    return (
      <Select.Async
        {...this.props}
        value={this.state.value}
        onBlur={(event) => {
          const onBlur = this.props.onBlur
          if (onBlur) {
            onBlur(event)
          }
        }}
      />
    )
  }
}
