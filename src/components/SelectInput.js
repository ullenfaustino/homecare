import React, { Component, PropTypes } from 'react'
import { AsyncCreatable } from 'react-select'

export default class SelectInput extends React.Component {
  constructor (props) {
    super(props)

    const value = this.props.value || {}
    this.state = {
      value: value
    }
  }
  componentWillReceiveProps (nextProps) {
    // Should be a controlled component.
    if ('value' in nextProps) {
      const value = nextProps.value
      this.setState({
        value: value
      })
    }
  }
  render () {
    return (
      <AsyncCreatable
        ignoreCase={false}
        allowCreate
        value={this.state.value}
        // onNewOptionClick={(option) => {
        //   const onChange = this.props.onChange
        //   if (onChange) {
        //     onChange({
        //       label: option.label,
        //       value: option.value
        //     })
        //   }
        // }}
        // promptTextCreator={(label) => {
        //   return label
        // }}
        onBlur={(event) => {
          const onBlur = this.props.onBlur
          if (onBlur) {
            onBlur(event)
          }
        }}
        onChange={(newValue = {}) => {
          const onChange = this.props.onChange
          if (onChange) {
            onChange(newValue)
          }
        }}
        loadOptions={(inputText) => {
          return this.props.autoComplete({
            searchText: inputText
          })
        }}
      />
    )
  }
}
