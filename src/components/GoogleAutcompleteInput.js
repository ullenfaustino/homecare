import React, { Component, PropTypes } from 'react'
import { Async } from 'react-select'

export default class GoogleAutcompleteInput extends React.Component {
  constructor (props) {
    super(props)

    const value = this.props.value || {}
    this.state = {
      value: value
    }
  }
  componentWillReceiveProps (nextProps) {
    // Should be a controlled component.
    if ('value' in nextProps) {
      const value = nextProps.value
      this.setState({
        value: value
      })
    }
  }
  render () {
    // console.log('[props] GoogleAutcompleteInput', this.props)
    // console.log('[state] GoogleAutcompleteInput', this.state)
    return (
      <Async
        value={this.state.value}
        onBlur={(event) => {
          const onBlur = this.props.onBlur
          if (onBlur) {
            onBlur(event)
          }
        }}
        onChange={(newValue = {}) => {
          const onChange = this.props.onChange
          if (onChange) {
            onChange(newValue)
          }
        }}
        loadOptions={(inputText) => {
          return this.props.autoComplete(inputText)
        }}
      />
    )
  }
}
