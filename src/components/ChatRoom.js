import React, { Component, PropTypes } from 'react'
import { connect } from 'react-redux'
import { Modal, Button, message, Input } from 'antd'
import { Link, browserHistory } from 'react-router'
import { ChatFeed, Message, ChatInput } from 'react-chat-ui'
import ReactImageFallback from 'react-image-fallback'
import moment from 'moment'
import _ from 'lodash'
import * as _Api from 'modules/api'
import * as Helper from 'consts/helper'
import { refreshUser } from 'modules/auth'
import { fetchMessages } from 'modules/app'

class ChatRoom extends Component {

  constructor (props) {
    super(props)
    this.state = {
      loading: false,
      is_typing: false,
      chatroom_id: undefined,
      chatroom_ref_id: undefined,
      currentMessage: '',
      messages: []
      // messages: [
      //   new Message({
      //     id: 1,
      //     // senderName: 'ullen',
      //     message: "I'm the recipient! (The person you're talking to)"
      //   }), // Gray bubble
      //   new Message({ id: 0, message: "I'm you -- the blue bubble!" })
      // ]
    }
  }

  componentDidMount () {
    this.fetchMessages()
  }

  fetchMessages = () => {
    const { recipient } = this.props
    const { user } = this.props.auth
    const params = {
      sender_id: user.id,
      recipient_id: recipient.id
    }
    _Api.getMessage(params)
    .then((chatRoom) => {
      this.setState({ chatroom_id: chatRoom.id, chatroom_ref_id: chatRoom.ref_id, currentMessage: '', loading: false }, () => {
        const formattedMessages = chatRoom.messages.map((message) => {
          return new Message({
            id: (message.sender.id == user.id) ? 0 : 1,
            message: message.message
          })
        })
        this.setState({ messages: formattedMessages })
      })
    })
    .catch((err) => {
      console.log('err', err)
    })
  }

  handleMessageSubmit = () => {
    const { user } = this.props.auth
    const { loading, messages, is_typing, chatroom_id, chatroom_ref_id, currentMessage } = this.state
    const { visibility, recipient } = this.props
    if (currentMessage.length > 0) {
      const params = {
        sender_id: user.id,
        recipient_id: recipient.id,
        chatroom_id: chatroom_id || -1,
        message: currentMessage
      }
      this.setState({ loading: true }, () => {
        _Api.sendMessage(params).then((response) => {
          this.fetchMessages()
          this.props.fetchMessages({
            page: 1,
            pageSize: 10
          })
        })
        .catch((err) => {
          console.log('error', err)
          this.setState({ loading: false })
        })
      })
    } else {
      message.error('Please enter a message!')
    }
  }

  render () {
    const { loading, messages, is_typing, chatroom_id, chatroom_ref_id, currentMessage } = this.state
    const { visibility, recipient } = this.props
    const { user } = this.props.auth

    return (
      <Modal
        destroyOnClose
        title={(!chatroom_id) ? 'New Message' : 'Chat Room #: ' + (chatroom_ref_id || '')}
        width={950}
        visible={visibility}
        footer={null}
        onCancel={() => {
          this.props.setVisibility(false)
        }}
        >
        <div className='row'>
          <div className='col-sm-12'>
            <div className='panel panel-primary'>
              <div className='panel-body'>
                <div className='row'>
                  <div className='col-sm-10'>
                    <h4>{recipient.first_name} {recipient.last_name}</h4>
                    <p>{recipient.location.label}</p>
                    <p>{Helper.getAge(recipient.birthday)} years old</p>
                  </div>
                  <div className='col-sm-2 text-center'>
                    <ReactImageFallback
                      src={recipient.avatar}
                      fallbackImage='/user-avatar/user.svg'
                      alt={recipient.first_name}
                      className='img-circle'
                      style={{ width: 120, height: 120 }}
                    />
                    <small>{(recipient.user_type == 1) ? 'Care Provider' : 'Care Seeker'}</small>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className='col-sm-12'>
            <div className='panel panel-primary'>
              <div className='panel-body'>
                {
                  (messages.length > 0)
                  ? <ChatFeed
                    messages={messages} // Boolean: list of message objects
                    isTyping={is_typing} // Boolean: is the recipient typing
                    hasInputField={false} // Boolean: use our input, or use your own
                    showSenderName // show the name of the user who sent the message
                    bubblesCentered={false} // Boolean should the bubbles be centered in the feed?
                    maxHeight={550}
                    bubbleStyles={{
                      text: {
                        fontSize: 30
                      },
                      chatbubble: {
                        borderRadius: 70,
                        padding: 40
                      }
                    }}
                  />
                  : <div className='text-center'>
                    <h2>No Message Found</h2>
                  </div>
                }
              </div>
            </div>
          </div>
          <div className='col-sm-12'>
            <div className='row'>
              <div className='col-sm-10'>
                <Input value={currentMessage} onPressEnter={this.handleMessageSubmit} onChange={(e) => {
                  this.setState({ currentMessage: e.target.value })
                }} />
              </div>
              <div className='col-sm-2'>
                <Button type='primary' className='btn-block' loading={loading} onClick={this.handleMessageSubmit}>Send</Button>
              </div>
            </div>
          </div>
        </div>
      </Modal>
    )
  }
}

// ChatRoom.propTypes = {
// }

const mapDispatchToProps = { refreshUser, fetchMessages }

const mapStateToProps = (state) => ({ ...state })

export default connect(mapStateToProps, mapDispatchToProps)(ChatRoom)
