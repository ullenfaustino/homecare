export const extractError = (error) => {
  if (error.response && error.response.data) {
    return error.response.data
  }

  return error
}

/**
 * Parses the JSON returned by a network request
 *
 * @param  {object} response A response from a network request
 *
 * @return {object}          The parsed JSON from the request
 */
export const parseJSON = (response) => {
  return response.data.data || response.data
}

export const fixParams = (params = {}) => {
  let { sortOrder } = params

  if (sortOrder === 'ascend') {
    sortOrder = 'asc'
  }

  if (sortOrder === 'descend') {
    sortOrder = 'desc'
  }

  return Object.assign(params, {
    sortOrder
  })
}

export const fixModUrl = (url) => {
  return '/' + url
}
