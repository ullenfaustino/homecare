import RegistrationContainer from './containers/RegistrationContainer'
// import AccountList from './components/AccountList'

export default (store) => ({
  path : 'register',
  component : RegistrationContainer,
  childRoutes: [
    // {
    //   path : ':id/accounts',
    //   component: AccountList
    // }
  ]
})
