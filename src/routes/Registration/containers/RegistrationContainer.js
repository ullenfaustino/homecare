import React, { Component } from 'react'
import { connect } from 'react-redux'
import { loginRequest } from 'modules/auth'

import RegistrationForm from '../components/RegistrationForm'

class RegistrationContainer extends Component {

  render () {
    let { children, params, location } = this.props

    let content
    if (children) {
      content = this.props.children
    } else {
      content = <RegistrationForm {...this.props} />
    }

    return (
      <div>
        {content}
      </div>
    )
  }
}

const mapDispatchToProps = {
  loginRequest
}

const mapStateToProps = (state) => ({ ...state })

export default connect(mapStateToProps, mapDispatchToProps)(RegistrationContainer)
