import React, { Component, PropTypes } from 'react'
import { Link, browserHistory } from 'react-router'
import moment from 'moment'
import { DatePicker, message } from 'antd'

import * as Api from '../modules/api'
import LogoSvg from 'assets/homecare_logo.png'
import LogoMain from 'static/logos/logo_main.png'

export default class RegistrationForm extends Component {
  constructor () {
    super()
    this.state = {
      isSaving: false,
      showButtonPanel: true,

      first_name: undefined,
      last_name: undefined,
      middle_name: undefined,
      zip_code: undefined,
      contact_no: undefined,
      email: undefined,
      username: undefined,
      password: undefined,
      user_type: 0,
      birthday: moment(),
      gender: -1
    }
  }

  clearFields = () => {
    this.setState({
      isSaving: false,
      showButtonPanel: true,

      first_name: undefined,
      last_name: undefined,
      middle_name: undefined,
      zip_code: undefined,
      contact_no: undefined,
      email: undefined,
      username: undefined,
      password: undefined,
      user_type: 0,
      birthday: moment(),
      gender: -1
    })
  }

  componentDidMount () {
    const { query } = this.props.router.getCurrentLocation()
    if (query && query.userType) {
      this.setState({ showButtonPanel: false, user_type: query.userType })
    } else {
      this.setState({ showButtonPanel: true })
    }
  }

  handleOnSubmit = () => {
    if (this.state.user_type == 0) {
      message.error('Please select your account type.')
      return
    }

    if (this.state.gender == -1) {
      message.error('Please select your gender.')
      return
    }

    this.setState({
      isSaving: true
    }, this.handleOnCreateAccount())
  }

  handleOnCreateAccount = () => {
    const { username, password } = this.state

    const params = {
      first_name: this.state.first_name,
      last_name: this.state.last_name,
      middle_name: this.state.middle_name,
      zip_code: this.state.zip_code,
      contact_no: this.state.contact_no,
      email: this.state.email,
      username: this.state.username,
      password: this.state.password,
      user_type: this.state.user_type,
      birthday: this.state.birthday,
      gender: this.state.gender
    }

    Api.createAccount(params)
    .then((response) => {
      this.setState({
        isSaving: false
      }, () => {
        this.clearFields()
        this.props.loginRequest({ username, password })
      })
    })
    .catch((err) => {
      console.log('err ', err)
      this.setState({
        isSaving: false
      }, () => {
        message.error('Failed to save account, please contact the system administrator.')
        this.clearFields()
      })
    })
  }

  render () {
    if (this.state.showButtonPanel) {
      return (
        <div>
          <div className='container' style={{ 'marginTop' : '10%', 'width' : '70%' }}>
            <div className='row'>
              <div className='col-sm-12'>
                <div className='panel panel-primary'>
                  <div className='panel-body'>
                    <div className='row'>
                      <div className='col-md-12 text-center'>
                        <a href='/'>
                          <img src={LogoMain} className='logo' style={{ widht: 120, height: 120 }} />
                        </a>
                        <h2>Register in Care Network System.</h2>
                      </div>
                    </div>
                    <br />
                    <legend />
                    <br />
                    <div className='col-md-12 text-center'>
                      <div className='row'>
                        <div className='col-md-6'>
                          <a style={{ width: '100%', height:'175px', fontSize: '2.5em', lineHeight: '4em' }} className='text-center btn btn-primary waves-effect waves-light' href='/register?userType=1'>I am Care Provider</a>
                        </div>
                        <div className='col-md-6'>
                          <a style={{ width: '100%', height:'175px', fontSize: '2.5em', lineHeight: '4em' }} className='text-center btn btn-warning waves-effect waves-light' href='/register?userType=2'>I am Care Seeker</a>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      )
    }

    return (
      <div>
        <div className='container' style={{ 'marginTop' : '3%', 'width' : '70%' }}>
          <div className='row'>
            <div className='col-sm-12'>
              <div className='panel panel-primary'>
                <div className='panel-body'>
                  <img src={LogoMain} className='logo' style={{ widht: 120, height: 120 }} />
                  <h2>Create Account {(this.state.user_type == 1) ? ' as Care Provider' : ' as Care Seeker'}</h2>
                  <small>Please Fill out in all fields</small>
                  <hr />
                  <form className='form-horizontal m-t-20' onSubmit={(ev) => {
                    this.handleOnSubmit()
                    ev.preventDefault()
                  }}>
                    <div className='form-group'>
                      <label className='col-md-2 control-label'>First Name</label>
                      <div className='col-md-10'>
                        <input type='text' className='form-control' required placeholder='First Name' value={this.state.first_name} onChange={(e) => {
                          this.setState({
                            first_name: e.target.value
                          })
                        }} />
                      </div>
                    </div>
                    <div className='form-group'>
                      <label className='col-md-2 control-label'>Last Name</label>
                      <div className='col-md-10'>
                        <input type='text' className='form-control' required placeholder='Last Name' value={this.state.last_name} onChange={(e) => {
                          this.setState({
                            last_name: e.target.value
                          })
                        }} />
                      </div>
                    </div>
                    <div className='form-group'>
                      <label className='col-md-2 control-label'>Middle Name</label>
                      <div className='col-md-10'>
                        <input type='text' className='form-control' required placeholder='Middle Name' value={this.state.middle_name} onChange={(e) => {
                          this.setState({
                            middle_name: e.target.value
                          })
                        }} />
                      </div>
                    </div>
                    <div className='form-group'>
                      <label className='col-md-2 control-label'>Gender</label>
                      <div className='col-md-10'>
                        <select className='form-control' required value={this.state.gender} onChange={(e) => {
                          this.setState({
                            gender: parseInt(e.target.value)
                          })
                        }}>
                          <option value={-1}>- Select Gender -</option>
                          <option value={0}>Female</option>
                          <option value={1}>Male</option>
                        </select>
                      </div>
                    </div>
                    <div className='form-group'>
                      <label className='col-md-2 control-label'>Birthday</label>
                      <div className='col-md-10'>
                        <DatePicker value={this.state.birthday} onChange={(date, dateString) => {
                          this.setState({
                            birthday: date
                          })
                        }} />
                      </div>
                    </div>
                    <div className='form-group'>
                      <label className='col-md-2 control-label'>Zip Code</label>
                      <div className='col-md-10'>
                        <input type='text' className='form-control' required placeholder='Zip Code' value={this.state.zip_code} onChange={(e) => {
                          this.setState({
                            zip_code: e.target.value
                          })
                        }} />
                      </div>
                    </div>
                    <div className='form-group'>
                      <label className='col-md-2 control-label'>Contact Number</label>
                      <div className='col-md-10'>
                        <input type='text' className='form-control' required placeholder='Contact Number' value={this.state.contact_no} onChange={(e) => {
                          this.setState({
                            contact_no: e.target.value
                          })
                        }} />
                      </div>
                    </div>

                    <hr />

                    <div className='form-group'>
                      <label className='col-md-2 control-label'>Email</label>
                      <div className='col-md-10'>
                        <input type='email' className='form-control' required placeholder='Email' value={this.state.email} onChange={(e) => {
                          this.setState({
                            email: e.target.value
                          })
                        }} />
                      </div>
                    </div>
                    <div className='form-group'>
                      <label className='col-md-2 control-label'>Username</label>
                      <div className='col-md-10'>
                        <input type='text' className='form-control' required placeholder='Username' value={this.state.username} onChange={(e) => {
                          this.setState({
                            username: e.target.value
                          })
                        }} />
                      </div>
                    </div>
                    <div className='form-group'>
                      <label className='col-md-2 control-label'>Password</label>
                      <div className='col-md-10'>
                        <input type='password' className='form-control' required placeholder='Password' value={this.state.password} onChange={(e) => {
                          this.setState({
                            password: e.target.value
                          })
                        }} />
                      </div>
                    </div>

                    <div className='form-group text-center m-t-40'>
                      <div className='col-xs-12'>
                        <a href='/register' className='btn btn-warning waves-effect waves-light' style={{ marginRight: '10px' }}>Back <i className='mdi mdi-keyboard-backspace' aria-hidden='true' /></a>
                        <button className='btn btn-primary waves-effect waves-light' type='submit' disabled={this.state.isSaving}>
                          { this.state.isSaving ? 'Saving...' : 'Create' } <i className='fa fa-floppy-o' aria-hidden='true' />
                        </button>
                      </div>
                    </div>
                    <div className='form-group m-t-30 m-b-0'>
                      <div className='col-sm-12 text-center'> <a href='/' className='text-muted'>Already have account?</a></div>
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    )
  }
}

RegistrationForm.propTypes = {
}
