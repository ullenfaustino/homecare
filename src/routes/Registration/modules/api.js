import axios from 'axios'

import { URL } from '../../../consts/apiConsts'

function parseJSON (response) {
  return response.data.data || response.data
}

export function createAccount (params) {
  return axios.post(`${URL}/users/createAccount`, params).then(parseJSON)
}
