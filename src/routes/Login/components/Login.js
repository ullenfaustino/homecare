import React, { Component } from 'react'
import {
  Button,
  Row,
  Col,
  Checkbox,
  ControlLabel,
  Form,
  FormControl,
  FormGroup
} from 'react-bootstrap'

import LogoSvg from 'assets/homecare_logo.png'
import LogoMain from 'static/logos/logo_main.png'

class Login extends Component {

  constructor (props) {
    super(props)

    this.state = {
      username: '',
      password: ''
    }

    this._login = this._login.bind(this)
    this.handleUsernameChange = this.handleUsernameChange.bind(this)
    this.handlePasswordChange = this.handlePasswordChange.bind(this)
  }

  handleUsernameChange (e) {
    this.setState({
      username: e.target.value
    })
  }

  handlePasswordChange (e) {
    this.setState({
      password: e.target.value
    })
  }

  _login () {
    const { username, password } = this.state
    this.props.loginRequest({ username, password })
  }

  render () {
    const { currentlySending, error } = this.props

    return (
      <div>
        <div className='accountbg' />
        <div className='wrapper-page'>
          <div className='panel panel-color panel-primary panel-pages'>
            <div className='panel-body'>
              <div className='text-center m-t-0 m-b-15'>
                <a href='/' className='logo logo-admin'>
                  <img src={LogoMain} className='logo' style={{ widht: 120, height: 120 }} />
                </a>
              </div>
              <h3 className='text-center m-t-0 m-b-15' />
              <h4 className='text-muted text-center m-t-0'><b>Sign In to your account</b></h4>
              <Form className='form-horizontal m-t-20' onSubmit={(ev) => {
                this._login()
                ev.preventDefault()
              }}>
                <FormGroup controlId='formHorizontalEmail' className='login-form-input'>
                  <Col componentClass={ControlLabel} />
                  <FormControl className='form-input'
                    type='text'
                    required
                    placeholder='username'
                    value={this.state.username}
                    onChange={this.handleUsernameChange} />
                </FormGroup>
                <FormGroup controlId='formHorizontalPassword' className='login-form-input'>
                  <Col componentClass={ControlLabel} />
                  <FormControl
                    type='password'
                    className='form-input'
                    placeholder='password'
                    required
                    value={this.state.password}
                    onChange={this.handlePasswordChange} />
                </FormGroup>
                <div className='text-center'>
                  <p>{error}</p>
                </div>

                <div className='form-group text-center m-t-40'>
                  <div className='col-xs-12' />
                  <Button className='btn btn-primary btn-block btn-lg waves-effect waves-light' type='submit'
                    disabled={currentlySending}
                     // onClick={this._login}
                   >
                    <i className='fa fa-sign-in' aria-hidden='true'> { currentlySending ? 'Sign in...' : 'Sign in'}</i>
                  </Button>
                </div>

                <div className='form-group m-t-30 m-b-0'>
                  <div className='col-sm-7'>
                    <a href='#' className='text-muted'><i className='fa fa-lock m-r-5' /> Forgot your password?</a>
                  </div>
                  <div className='col-sm-5 text-right'>
                    <a href='/register' className='text-muted'>Create an account</a>
                  </div>
                </div>

                <div className='form-group m-t-30 m-b-0'>
                  <p className='text-center m-t'> <small>HomeCare © 2017</small> </p>
                </div>
              </Form>
            </div>
          </div>
        </div>
      </div>
    )
  }
}

Login.propTypes = {
  loginRequest   : React.PropTypes.func.isRequired
}

export default Login
