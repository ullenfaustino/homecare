export const SENDING_REQUEST = 'SENDING_REQUEST'
export const REQUEST_ERROR = 'REQUEST_ERROR'

// ------------------------------------
// Action Handlers
// ------------------------------------
const ACTION_HANDLERS = {
  [SENDING_REQUEST] : (state, action) => Object.assign({}, state, {
    currentlySending: action.sending,
    error: action.sending ? '' : state.error
  }),
  [REQUEST_ERROR] : (state, action) => Object.assign({}, state, {
    error: action.error
  })
}

// ------------------------------------
// Reducer
// ------------------------------------
// The initial application state
const initialState = {
  error: '',
  currentlySending: false
}
export default function loginReducer (state = initialState, action) {
  const handler = ACTION_HANDLERS[action.type]

  return handler ? handler(state, action) : state
}
