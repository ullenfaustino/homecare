import { takeLatest } from 'redux-saga'
import { take, put, select, call, fork } from 'redux-saga/effects'
import * as Api from './api'
import * as mainApi from 'modules/api'
import _ from 'lodash'

// ------------------------------------
// Constants
// ------------------------------------
// export const USER_REFRESHED = 'USER_REFRESHED'

// export function refreshUser (uid) {
//   return (dispatch, getState) => {
//     const { job } = getState()
//     const params = Object.assign({}, job.pagination, data)
//
//     return mainApi.findByUid(uid).then((payload) => {
//       dispatch({
//         type: USER_REFRESHED,
//         payload
//       })
//       return payload
//     })
//   }
// }

export const actions = {
  // refreshUser
}

// ------------------------------------
// Action Handlers
// ------------------------------------
const ACTION_HANDLERS = {
  // [USER_REFRESHED] : (state, action) => {
  //   console.log('state', state)
  //   console.log('action', action)
  // }
}

// ------------------------------------
// Reducer
// ------------------------------------
const initialState = {
}
export default function Reducer (state = initialState, action) {
  const handler = ACTION_HANDLERS[action.type]
  return handler ? handler(state, action) : state
}

// ------------------------------------
// Sagas
// ------------------------------------
export const sagas = []
