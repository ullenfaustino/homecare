import axios from 'axios'

import { URL } from 'consts/apiConsts'

function parseJSON (response) {
  return response.data.data || response.data
}

// export function fetchVehicles (params) {
//   return axios.get(`${URL}/user`, {
//     params: params
//   }).then((response) => response.data)
// }

export function findByUid (uid) {
  return axios.get(`${URL}/user/${uid}`).then(parseJSON)
}

export function addPreferences (data) {
  return axios.post(`${URL}/seeker/add/preferences`, data).then(parseJSON)
}

export function addRecipient (data) {
  return axios.post(`${URL}/seeker/add/recipient`, data).then(parseJSON)
}

export function getRecipients (uid) {
  return axios.get(`${URL}/user/${uid}/recipients`).then(parseJSON)
}

export function findAvailableProvider (data) {
  return axios.post(`${URL}/provider/find-available`, data).then(parseJSON)
}

export function hireProvider (data) {
  return axios.post(`${URL}/provider/hire`, data).then(parseJSON)
}

export function postJob (data) {
  return axios.post(`${URL}/seeker/post-job`, data).then(parseJSON)
}

export function hireApplicant (data) {
  return axios.post(`${URL}/seeker/hire-applicant`, data).then(parseJSON)
}

export function finishService (data) {
  return axios.post(`${URL}/provider/finish-service`, data).then(parseJSON)
}
