import React, { Component, PropTypes } from 'react'
import { Link, browserHistory } from 'react-router'

import * as VehicleApi from '../modules/api'

import moment from 'moment'
import ReactImageFallback from 'react-image-fallback'

export default class Dashboard extends Component {
  constructor (props) {
    super(props)
  }

  render () {
    return (
      <div className='content'>
        <div className=''>
          <div className='page-header-title'>
            <h4 className='page-title'>Dashboard</h4>
          </div>
          <div className='page-content-wrapper '>
            <div className='container'>
              <div className='row'>
                <div className='col-md-4'>
                  <div className='panel'>
                    <div className='panel-body'>
                      <h4 className='m-b-30 m-t-0'>Lorem ipsum dolor soat</h4>
                      <p className='font-600 m-b-5'>Lorem ipsum dolor soat <span className='text-primary pull-right'><b>80%</b></span></p>
                      <div className='progress m-b-20'>
                        <div className='progress-bar progress-bar-primary ' role='progressbar' aria-valuenow='80' aria-valuemin='0' aria-valuemax='100' style={{ width: '80%' }} />
                      </div>
                      <p className='font-600 m-b-5'>Lorem ipsum dolor soat <span className='text-primary pull-right'><b>50%</b></span></p>
                      <div className='progress m-b-20'>
                        <div className='progress-bar progress-bar-primary ' role='progressbar' aria-valuenow='50' aria-valuemin='0' aria-valuemax='100' style={{ width: '80%' }} />
                      </div>
                      <p className='font-600 m-b-5'>Lorem ipsum dolor soat <span className='text-primary pull-right'><b>70%</b></span></p>
                      <div className='progress m-b-20'>
                        <div className='progress-bar progress-bar-primary ' role='progressbar' aria-valuenow='70' aria-valuemin='0' aria-valuemax='100' style={{ width: '80%' }} />
                      </div>
                      <p className='font-600 m-b-5'>Lorem ipsum dolor soat <span className='text-primary pull-right'><b>65%</b></span></p>
                      <div className='progress m-b-20'>
                        <div className='progress-bar progress-bar-primary ' role='progressbar' aria-valuenow='65' aria-valuemin='0' aria-valuemax='100' style={{ width: '80%' }} />
                      </div>
                      <p className='font-600 m-b-5'>Lorem ipsum dolor soat <span className='text-primary pull-right'><b>25%</b></span></p>
                      <div className='progress m-b-20'>
                        <div className='progress-bar progress-bar-primary ' role='progressbar' aria-valuenow='25' aria-valuemin='0' aria-valuemax='100' style={{ width: '80%' }} />
                      </div>
                      <p className='font-600 m-b-5'> Lorem ipsum dolor soat<span className='text-primary pull-right'><b>40%</b></span></p>
                      <div className='progress m-b-0'>
                        <div className='progress-bar progress-bar-primary ' role='progressbar' aria-valuenow='40' aria-valuemin='0' aria-valuemax='100' style={{ width: '80%' }} />
                      </div>
                    </div>
                  </div>
                </div>
                <div className='col-md-8'>
                  <div className='panel'>
                    <div className='panel-body'>
                      <h4 className='m-b-30 m-t-0'>Lorem ipsum dolor soat</h4>
                      <div className='row'>
                        <div className='col-xs-12'>
                          <div className='table-responsive'>
                            <table className='table table-hover m-b-0'>
                              <thead>
                                <tr>
                                  <th>Name</th>
                                  <th>Position</th>
                                  <th>Office</th>
                                  <th>Age</th>
                                  <th>Start date</th>
                                  <th>Amount</th>
                                </tr>
                              </thead>
                              <tbody>
                                <tr>
                                  <td>Lorem ipsum dolor soat</td>
                                  <td>Lorem ipsum dolor soat</td>
                                  <td>Lorem ipsum dolor soat</td>
                                  <td>61</td>
                                  <td>2011/04/25</td>
                                  <td>$320,800</td>
                                </tr>
                                <tr>
                                  <td>Lorem ipsum dolor soat</td>
                                  <td>Lorem ipsum dolor soat</td>
                                  <td>Lorem ipsum dolor soat</td>
                                  <td>61</td>
                                  <td>2011/04/25</td>
                                  <td>$320,800</td>
                                </tr>
                                <tr>
                                  <td>Lorem ipsum dolor soat</td>
                                  <td>Lorem ipsum dolor soat</td>
                                  <td>Lorem ipsum dolor soat</td>
                                  <td>61</td>
                                  <td>2011/04/25</td>
                                  <td>$320,800</td>
                                </tr>
                                <tr>
                                  <td>Lorem ipsum dolor soat</td>
                                  <td>Lorem ipsum dolor soat</td>
                                  <td>Lorem ipsum dolor soat</td>
                                  <td>61</td>
                                  <td>2011/04/25</td>
                                  <td>$320,800</td>
                                </tr>
                                <tr>
                                  <td>Lorem ipsum dolor soat</td>
                                  <td>Lorem ipsum dolor soat</td>
                                  <td>Lorem ipsum dolor soat</td>
                                  <td>61</td>
                                  <td>2011/04/25</td>
                                  <td>$320,800</td>
                                </tr>
                                <tr>
                                  <td>Lorem ipsum dolor soat</td>
                                  <td>Lorem ipsum dolor soat</td>
                                  <td>Lorem ipsum dolor soat</td>
                                  <td>61</td>
                                  <td>2011/04/25</td>
                                  <td>$320,800</td>
                                </tr>
                                <tr>
                                  <td>Lorem ipsum dolor soat</td>
                                  <td>Lorem ipsum dolor soat</td>
                                  <td>Lorem ipsum dolor soat</td>
                                  <td>61</td>
                                  <td>2011/04/25</td>
                                  <td>$320,800</td>
                                </tr>
                              </tbody>
                            </table>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    )
  }
}

Dashboard.propTypes = {
}
