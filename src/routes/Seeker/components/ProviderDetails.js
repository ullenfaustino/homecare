import React, { Component, PropTypes } from 'react'
import { Link, browserHistory } from 'react-router'
import { Select, Button, TimePicker, message, DatePicker, Modal, Spin, Icon, Upload, Slider, InputNumber, Row, Col } from 'antd'
import moment from 'moment'
import * as Helper from 'consts/helper'
import ReactImageFallback from 'react-image-fallback'
import TotalRatings from 'components/TotalRatings'

export default class ProviderDetails extends Component {

  constructor (props) {
    super(props)
  }

  handleTagsView = (tagItems) => {
    const items = tagItems.map((item) => {
      return item.value.name
    })
    return <b>{items.join(', ')}</b>
  }

  handleAvailability = (tagItems) => {
    const items = tagItems.map((item) => {
      return item.day + ' ' + item.start_time + ' - ' + item.end_time
    })
    return <b>{items.join(', ')}</b>
  }

  handleFilesView = (fileType) => {
    const { provider } = this.props
    const { files } = provider

    const _filtered_defaultFiles = files.filter((file) => file.file.file_type === fileType)
    const _defaultFiles = _filtered_defaultFiles.map((file) => {
      if (file.file.file_type === fileType) {
        return {
          uid: file.file.id,
          name: file.file.name,
          status: 'done',
          url: file.file.url
        }
      }
    })
    const formattedFiles = _defaultFiles.map((file, i) => {
      return (<Link key={i} target='_blank' to={file.url}> {file.name}{ ((i + 1) == _defaultFiles.length) ? '' : ',' }</Link>)
    })
    return <p>{formattedFiles}</p>
  }

  render () {
    const { provider, visibility } = this.props

    return (
      <Modal
        destroyOnClose
        title={'Details'}
        width={1000}
        visible={visibility}
        footer={null}
        onCancel={() => {
          this.props.setVisibility(false)
        }}
        >
        <div className='row'>
          <div className='col-sm-12'>
            <div className='panel panel-primary'>
              <div className='panel-body'>
                <ReactImageFallback
                  src={provider.avatar}
                  fallbackImage='/user-avatar/user.svg'
                  alt={provider.first_name}
                  className='img-circle pull-right'
                  style={{ width: 110, height: 110 }}
                />
                <h3>{provider.first_name + ' ' + provider.last_name}</h3>
                <span>{provider.location.label}</span>
                <br />
                <span>{Helper.getAge(provider.birthday)} years old</span>
                <br />
                <span>Rate per hour: ${provider.salary_rate.hourly_rate}</span>
                <br />
                <br />
                <p>
                  Experienced service: {this.handleTagsView(provider.services_experience)}
                </p>
                <p>
                  Services performed: {this.handleTagsView(provider.services_perform)}
                </p>
                <p>
                  Language(s): {this.handleTagsView(provider.languages)}
                </p>
                <p>
                  Availability: {this.handleAvailability(provider.availability)}
                </p>
                <p>
                  Means of transportation: {this.handleTagsView(provider.transportations)}
                </p>
                <p>
                  Work Schedules: {this.handleTagsView(provider.work_schedules)}
                </p>
                <p>
                  Work Restrictions: {this.handleTagsView(provider.work_restrictions)}
                </p>
                <p>
                  Preferred Health Condition(s): {this.handleTagsView(provider.health_conditions)}
                </p>
                <p>
                  Client Preferences: {this.handleTagsView(provider.client_preferences)}
                </p>
              </div>
            </div>
          </div>
          <div className='col-sm-12'>
            <div className='panel panel-primary'>
              <div className='panel-body'>
                <legend>UPLOADED DOCUMENTS</legend>
                <div className='form-group'>
                  <span className='col-md-4'>Resume</span>
                  <div className='col-md-8'>
                    <label className='control-label'>{ this.handleFilesView('cv_resume') }</label>
                  </div>
                </div>
                <div className='form-group'>
                  <span className='col-md-4'>Driver's License</span>
                  <div className='col-md-8'>
                    <label className='control-label'>{ this.handleFilesView('drivers_license') }</label>
                  </div>
                </div>
                <div className='form-group'>
                  <span className='col-md-4'>ID by the DMV</span>
                  <div className='col-md-8'>
                    <label className='control-label'>{ this.handleFilesView('dmv_id') }</label>
                  </div>
                </div>
                <div className='form-group'>
                  <span className='col-md-4'>Alien Registration Card</span>
                  <div className='col-md-8'>
                    <label className='control-label'>{ this.handleFilesView('alien_registration_card') }</label>
                  </div>
                </div>
                <div className='form-group'>
                  <span className='col-md-4'>Numbered ID <smal>(If living outside CA)</smal></span>
                  <div className='col-md-8'>
                    <label className='control-label'>{ this.handleFilesView('numbered_id') }</label>
                  </div>
                </div>
                <div className='form-group'>
                  <span className='col-md-4'>Criminal Record Clearance</span>
                  <div className='col-md-8'>
                    <label className='control-label'>{ this.handleFilesView('criminal_record_clearance') }</label>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className='col-sm-12'>
            <div className='panel panel-primary'>
              <div className='panel-body'>
                <legend>TRAININGS AND CERTIFICATIONS</legend>
                <div className='form-group'>
                  <span className='col-md-4'>Trainings and Certifications</span>
                  <div className='col-md-8'>
                    <label className='control-label'>{ this.handleFilesView('certifications') }</label>
                  </div>
                </div>
              </div>
            </div>
          </div>
          {
            (provider.user_type == 1 && provider.ratings.length > 0) &&
            <div className='col-sm-12'>
              <div className='panel panel-primary'>
                <div className='panel-body'>
                  <legend>Ratings and Feedbacks</legend>
                  <TotalRatings
                    ratings={provider.ratings}
                  />
                </div>
              </div>
            </div>
          }
        </div>
      </Modal>
    )
  }
}

ProviderDetails.propTypes = {
}
