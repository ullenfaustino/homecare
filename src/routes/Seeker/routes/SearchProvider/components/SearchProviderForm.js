import React, { Component, PropTypes } from 'react'
import { Link, browserHistory } from 'react-router'
import { Select, Button, TimePicker, message, DatePicker, Modal, Spin, Icon, Upload, Slider, InputNumber, Row, Col } from 'antd'
import GoogleAutcompleteInput from 'components/GoogleAutcompleteInput'
import moment from 'moment'
import * as GoogleApi from 'modules/googlemaps/api'
import * as _Api from './../../../modules/api'
import { URL } from 'consts/apiConsts'
import * as Helper from 'consts/helper'

export default class SearchProviderForm extends Component {

  constructor (props) {
    super(props)
    this.state = {
      loading: false,
      selectedRecipient: undefined,
      distanceRange: 10,
      searchParams: {
        health_conditions: [],
        work_restrictions: [],
        work_schedules: [],
        client_preferences: []
      }
    }
  }

  findAvailableCareProvider = () => {
    const { selectedRecipient, distanceRange, searchParams } = this.state
    const params = {
      recipient: Object.assign({}, selectedRecipient, { distanceRange }),
      searchParams
    }
    // console.log('[params]', params)

    _Api.findAvailableProvider(params)
    .then((providers) => {
      this.setState({ loading: false }, () => {
        this.props.searchedProviders(providers)
        this.props.selectedRecipient(selectedRecipient)
        this.props.setVisibility(false)
      })
    })
    .catch((err) => {
      console.log('err', err)
      this.setState({ loading: false }, () => {
        message.error('Error searching providers...')
      })
    })
  }

  handleTagsView = (tagItems) => {
    const items = tagItems.map((item) => {
      return item.value.name
    })
    return items.join(', ')
  }

  handleChangeSliderDistance = (value) => {
    this.setState({
      distanceRange: value
    })
  }

  render () {
    const { visibility, settings, currentUser } = this.props
    const { recipients, postedJobs } = currentUser
    const { selectedRecipient, loading, distanceRange } = this.state
    const { health_conditions, work_restrictions, work_schedules, client_preferences } = settings

    const filteredRecipients = recipients.filter((recipient) => {
      const index = _.findIndex(postedJobs, { id: recipient.id })
      const isExist = index > -1
      if (isExist) {
        const postedJob = postedJobs[index]
        return (postedJob.status > 1)
      } else {
        return true
      }
    })

    return (
      <Modal
        destroyOnClose
        title={'Find Care Provider'}
        width={1000}
        visible={visibility}
        okText='Search Provider'
        footer={null}
        onCancel={() => {
          this.props.setVisibility(false)
        }}
        >
        <div className='row'>
          <div className='col-sm-12'>
            <div className='panel panel-primary'>
              <div className='panel-body'>
                <legend>Care Recipient’s Information</legend>
                <form className='form-horizontal'>
                  <div className='form-group'>
                    <span className='col-md-2 control-label'>Who needs care?</span>
                    <div className='col-md-5'>
                      <Select
                        mode='dropdown'
                        style={{ width: '100%' }}
                        placeholder='Select Recipient'
                        // defaultValue={this.state.gender}
                        onChange={(values) => {
                          const selRecipients = _.find(filteredRecipients, function (o) { return o.id == values })
                          this.setState({ selectedRecipient : selRecipients })
                        }}
                        >
                        {
                          filteredRecipients.map((item) => {
                            return (
                              <Select.Option key={item.id} value={item.id}>{item.name}</Select.Option>
                            )
                          })
                        }
                      </Select>
                    </div>
                    <div className='col-md-5'>
                      <Button type='primary' style={{ marginTop: '4px' }} disabled={!selectedRecipient} loading={loading} onClick={() => {
                        this.setState({ loading: true }, () => {
                          this.findAvailableCareProvider()
                        })
                      }}><i className='mdi mdi-account-search' /> Search Care Provider</Button>
                    </div>
                  </div>
                  <div className='form-group'>
                    <span className='col-md-2 control-label'>Location:</span>
                    <div className='col-md-10'>
                      <label className='control-label'>{(_.has(selectedRecipient, 'location')) ? selectedRecipient.location.label : ''}</label>
                    </div>
                  </div>
                  <div className='form-group'>
                    <span className='col-md-2 control-label'>Gender:</span>
                    <div className='col-md-10'>
                      <label className='control-label'>{(_.has(selectedRecipient, 'gender')) ? ((selectedRecipient.gender == 0) ? 'Female' : 'Male') : ''}</label>
                    </div>
                  </div>
                  <div className='form-group'>
                    <span className='col-md-2 control-label'>Age:</span>
                    <div className='col-md-10'>
                      <label className='control-label'>{(_.has(selectedRecipient, 'birthday')) ? Helper.getAge(selectedRecipient.birthday) : ''}</label>
                    </div>
                  </div>
                  <div className='form-group'>
                    <span className='col-md-2 control-label'>General Status:</span>
                    <div className='col-md-10'>
                      <label className='control-label'>{(_.has(selectedRecipient, 'general_status')) ? this.handleTagsView(selectedRecipient.general_status) : ''}</label>
                    </div>
                  </div>
                  <div className='form-group'>
                    <span className='col-md-2 control-label'>Medical Condition:</span>
                    <div className='col-md-10'>
                      <label className='control-label'>{(_.has(selectedRecipient, 'medical_conditions')) ? this.handleTagsView(selectedRecipient.medical_conditions) : ''}</label>
                    </div>
                  </div>
                  <div className='form-group'>
                    <span className='col-md-2 control-label'>Range (km):</span>
                    <div className='col-md-10'>
                      <Row>
                        <Col span={12}>
                          <Slider min={1} max={100} onChange={this.handleChangeSliderDistance} value={distanceRange} />
                        </Col>
                        <Col span={4}>
                          <InputNumber
                            min={1}
                            max={100}
                            style={{ marginLeft: 16 }}
                            value={distanceRange}
                            onChange={this.handleChangeSliderDistance}
                          />
                        </Col>
                      </Row>
                    </div>
                  </div>
                  <div className='panel-group' id='accordion-main'>
                    <div className='panel panel-info panel-color'>
                      <div className='panel-heading'>
                        <h4 className='panel-title'><a aria-expanded='false' className='collapsed' data-parent='#accordion-main' data-toggle='collapse' href='#collapse-main'>Advance Search for Providers</a></h4>
                      </div>
                      <div className='panel-collapse collapse' id='collapse-main'>
                        <div className='row'>
                          <div className='col-lg-6 col-md-6 col-sm-12'>
                            <div className='panel-body'>
                              <div className='panel-group' id='accordion-health-condition'>
                                <div className='panel panel-info panel-color'>
                                  <div className='panel-heading'>
                                    <h4 className='panel-title'><a aria-expanded='false' className='collapsed' data-parent='#accordion-health-condition' data-toggle='collapse' href='#collapse-health-condition'>Health Conditions</a></h4>
                                  </div>
                                  <div className='panel-collapse collapse' id='collapse-health-condition'>
                                    <div className='panel-body'>
                                      {
                                        health_conditions.map((health_condition, i) => {
                                          return (
                                            <div className='checkbox checkbox-success' key={health_condition.id + '_hc'}>
                                              <input id={health_condition.id + '_hc'} type='checkbox' value={health_condition.id} checked={this.state[`health_condition_checked_${health_condition.id}`]} onChange={(e) => {
                                                const isExist = _.find(this.state.searchParams.health_conditions, (o) => (o == e.target.value))
                                                if (this.state[`health_condition_checked_${health_condition.id}`]) {
                                                  if (isExist) _.remove(this.state.searchParams.health_conditions, (o) => (o == e.target.value))
                                                  this.state[`health_condition_checked_${health_condition.id}`] = false
                                                } else {
                                                  if (!isExist) this.state.searchParams.health_conditions.push(e.target.value)
                                                  this.state[`health_condition_checked_${health_condition.id}`] = true
                                                }
                                              }} />
                                              <label htmlFor={health_condition.id + '_hc'}> {health_condition.name} </label>
                                            </div>
                                          )
                                        })
                                      }
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                          <div className='col-lg-6 col-md-6 col-sm-12'>
                            <div className='panel-body'>
                              <div className='panel-group' id='accordion-work-restriction'>
                                <div className='panel panel-info panel-color'>
                                  <div className='panel-heading'>
                                    <h4 className='panel-title'><a aria-expanded='false' className='collapsed' data-parent='#accordion-work-restriction' data-toggle='collapse' href='#collapse-work-restriction'>Work Restrictions</a></h4>
                                  </div>
                                  <div className='panel-collapse collapse' id='collapse-work-restriction'>
                                    <div className='panel-body'>
                                      {
                                        work_restrictions.map((work_restriction, i) => {
                                          return (
                                            <div className='checkbox checkbox-success' key={work_restriction.id + '_wr'}>
                                              <input id={work_restriction.id + '_wr'} type='checkbox' value={work_restriction.id} checked={this.state[`work_restriction_checked_${work_restriction.id}`]} onChange={(e) => {
                                                const isExist = _.find(this.state.searchParams.work_restrictions, (o) => (o == e.target.value))
                                                if (this.state[`work_restriction_checked_${work_restriction.id}`]) {
                                                  if (isExist) _.remove(this.state.searchParams.work_restrictions, (o) => (o == e.target.value))
                                                  this.state[`work_restriction_checked_${work_restriction.id}`] = false
                                                } else {
                                                  if (!isExist) this.state.searchParams.work_restrictions.push(e.target.value)
                                                  this.state[`work_restriction_checked_${work_restriction.id}`] = true
                                                }
                                              }} />
                                              <label htmlFor={work_restriction.id + '_wr'}> {work_restriction.name} </label>
                                            </div>
                                          )
                                        })
                                      }
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                          <div className='col-lg-6 col-md-6 col-sm-12'>
                            <div className='panel-body'>
                              <div className='panel-group' id='accordion-work-schedule'>
                                <div className='panel panel-info panel-color'>
                                  <div className='panel-heading'>
                                    <h4 className='panel-title'><a aria-expanded='false' className='collapsed' data-parent='#accordion-work-schedule' data-toggle='collapse' href='#collapse-work-schedule'>Work Schedules</a></h4>
                                  </div>
                                  <div className='panel-collapse collapse' id='collapse-work-schedule'>
                                    <div className='panel-body'>
                                      {
                                        work_schedules.map((work_schedule, i) => {
                                          return (
                                            <div className='checkbox checkbox-success' key={work_schedule.id + '_ws'}>
                                              <input id={work_schedule.id + '_ws'} type='checkbox' value={work_schedule.id} checked={this.state[`work_restriction_checked_${work_schedule.id}`]} onChange={(e) => {
                                                const isExist = _.find(this.state.searchParams.work_schedules, (o) => (o == e.target.value))
                                                if (this.state[`work_restriction_checked_${work_schedule.id}`]) {
                                                  if (isExist) _.remove(this.state.searchParams.work_schedules, (o) => (o == e.target.value))
                                                  this.state[`work_restriction_checked_${work_schedule.id}`] = false
                                                } else {
                                                  if (!isExist) this.state.searchParams.work_schedules.push(e.target.value)
                                                  this.state[`work_restriction_checked_${work_schedule.id}`] = true
                                                }
                                              }} />
                                              <label htmlFor={work_schedule.id + '_ws'}> {work_schedule.name} </label>
                                            </div>
                                          )
                                        })
                                      }
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                          <div className='col-lg-6 col-md-6 col-sm-12'>
                            <div className='panel-body'>
                              <div className='panel-group' id='accordion-client-preferences'>
                                <div className='panel panel-info panel-color'>
                                  <div className='panel-heading'>
                                    <h4 className='panel-title'><a aria-expanded='false' className='collapsed' data-parent='#accordion-client-preferences' data-toggle='collapse' href='#collapse-client-preferences'>Client Preferences</a></h4>
                                  </div>
                                  <div className='panel-collapse collapse' id='collapse-client-preferences'>
                                    <div className='panel-body'>
                                      {
                                        client_preferences.map((client_preference, i) => {
                                          return (
                                            <div className='checkbox checkbox-success' key={client_preference.id + '_cf'}>
                                              <input id={client_preference.id + '_cf'} type='checkbox' value={client_preference.id} checked={this.state[`client_preference_checked_${client_preference.id}`]} onChange={(e) => {
                                                const isExist = _.find(this.state.searchParams.client_preferences, (o) => (o == e.target.value))
                                                if (this.state[`client_preference_checked_${client_preference.id}`]) {
                                                  if (isExist) _.remove(this.state.searchParams.client_preferences, (o) => (o == e.target.value))
                                                  this.state[`client_preference_checked_${client_preference.id}`] = false
                                                } else {
                                                  if (!isExist) this.state.searchParams.client_preferences.push(e.target.value)
                                                  this.state[`client_preference_checked_${client_preference.id}`] = true
                                                }
                                              }} />
                                              <label htmlFor={client_preference.id + '_cf'}> {client_preference.name} </label>
                                            </div>
                                          )
                                        })
                                      }
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <legend>Schedule</legend>
                  <div className='form-group'>
                    <span className='col-md-2 control-label'>Days:</span>
                    <div className='col-md-10'>
                      <label className='control-label'>{(_.has(selectedRecipient, 'days')) ? selectedRecipient.days.map((day) => {
                        return day.day
                      }).join(', ') : ''}
                      </label>
                    </div>
                  </div>
                  <div className='form-group'>
                    <span className='col-md-2 control-label'>Hours per day:</span>
                    <div className='col-md-10'>
                      <label className='control-label'>{(_.has(selectedRecipient, 'hours_per_day')) ? selectedRecipient.hours_per_day : ''}</label>
                    </div>
                  </div>
                  <div className='form-group'>
                    <span className='col-md-2 control-label'>Start of Duty:</span>
                    <div className='col-md-10'>
                      <label className='control-label'>{(_.has(selectedRecipient, 'start_duty')) ? moment(selectedRecipient.start_duty).format('LL') : ''}</label>
                    </div>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </Modal>
    )
  }
}

SearchProviderForm.propTypes = {
}
