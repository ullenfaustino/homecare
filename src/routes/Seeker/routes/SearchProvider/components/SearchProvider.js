import React, { Component, PropTypes } from 'react'
import { Select, Button, TimePicker, Table, Icon, Divider, Spin, message, DatePicker, Popconfirm } from 'antd'
import GoogleAutcompleteInput from 'components/GoogleAutcompleteInput'
import moment from 'moment'
import ReactStars from 'react-stars'
import ReactImageFallback from 'react-image-fallback'
import * as GoogleApi from 'modules/googlemaps/api'
import * as _Api from './../../../modules/api'
import * as Helper from 'consts/helper'

import SearchProviderForm from './SearchProviderForm'
import ProviderDetails from './../../../components/ProviderDetails'
import ChatRoom from 'components/ChatRoom'

export default class SearchProvider extends Component {

  constructor (props) {
    super(props)
    this.state = {
      loading: false,
      isFetchingUserData: true,
      didSearch: false,
      SearchProviderFormVisibility: false,
      chat_visibility: false,
      availableProviders: [],
      recipient: undefined,
      msg_recipient: undefined
    }
  }

  componentWillMount () {
    this.props.refreshUser()
  }

  handleTagsView = (tagItems) => {
    const items = tagItems.map((item) => {
      return item.value.name
    })
    return items.join(', ')
  }

  renderSearchProviderFormView = () => {
    const { SearchProviderFormVisibility } = this.state
    const { settings } = this.props.app
    const { user } = this.props.auth
    const currentUser = user
    return (
      <SearchProviderForm
        currentUser={currentUser}
        settings={settings}
        visibility={SearchProviderFormVisibility}
        setVisibility={(visibility) => {
          this.setState({ SearchProviderFormVisibility: visibility })
        }}
        searchedProviders={(available_providers) => {
          // console.log('[available_providers]', available_providers)
          this.setState({ availableProviders: available_providers, didSearch: true }, () => {
            if (available_providers.length == 0) message.error('Provider not found.')
          })
        }}
        selectedRecipient={(recipient) => {
          this.setState({ recipient })
        }}
      />
    )
  }

  renderChatRoom = () => {
    const { chat_visibility, msg_recipient } = this.state
    return (
      <ChatRoom
        recipient={msg_recipient}
        visibility={chat_visibility}
        setVisibility={(visibility) => {
          this.setState({ chat_visibility: visibility })
        }}
      />
    )
  }

  render () {
    const { SearchProviderFormVisibility, chat_visibility, availableProviders, didSearch, recipient } = this.state
    const { user } = this.props.auth

    return (
      <div className='row'>
        <div className='col-sm-12'>
          <div className='panel panel-primary'>
            <div className='panel-body'>
              <Button type='primary' onClick={() => {
                this.setState({ SearchProviderFormVisibility: true })
              }}><i className='mdi mdi-search' /><i className='ion-android-search' /> Search Provider</Button>
              {
                (!didSearch) &&
                <h1>Search for a Care Provider</h1>
              }
              {
                (availableProviders.length == 0 && didSearch) &&
                <h1>No Data Found</h1>
              }
              {
                (availableProviders.length > 0) &&
                <h1>Yey! We found {availableProviders.length} care provider{(availableProviders.length > 1) ? 's.' : '.'}</h1>
              }
              {
                SearchProviderFormVisibility && this.renderSearchProviderFormView()
              }
              {
                chat_visibility && this.renderChatRoom()
              }
            </div>
          </div>
        </div>
        {
          availableProviders.map((provider) => {
            return (
              <div className='col-sm-4' key={provider.id}>
                <div className='panel'>
                  <div className='panel-body user-card'>
                    <div className='media-main'>
                      <a className='pull-left' href='#'>
                        <ReactImageFallback
                          src={provider.avatar}
                          fallbackImage='/user-avatar/user.svg'
                          alt={provider.first_name}
                          className='img-circle'
                          style={{ width: 80, height: 80 }}
                        />
                      </a>
                      <div className='info'>
                        <h4>{ provider.first_name + ' ' + provider.middle_name + ' ' + provider.last_name }</h4>
                        <ReactStars
                          count={5}
                          // onChange={ratingChanged}
                          edit={false}
                          value={Helper.getAverageRating(provider.ratings)}
                          size={18}
                          color2={'#ffd700'} />
                        <p className='text-muted'>{Helper.getAge(provider.birthday)} years old</p>
                      </div>
                    </div>
                    <div className='clearfix' />
                    <p className='text-muted info-text'> Rate per hour: <strong>${provider.salary_rate.hourly_rate}</strong></p>
                    <p className='text-muted info-text'>{this.handleTagsView(provider.services_experience)}</p>
                    <hr />
                    <ul className='social-links list-inline m-b-0'>
                      <li>
                        <a title='View' data-placement='top' data-toggle='tooltip' className='tooltips' href='#' data-original-title='View' onClick={() => {
                          this.setState({ [`is_details_shown_${provider.id}`] : true })
                        }}><i className='ion-eye' /></a>
                        {
                          (this.state[`is_details_shown_${provider.id}`] == true) &&
                          <ProviderDetails
                            provider={provider}
                            visibility={this.state[`is_details_shown_${provider.id}`]}
                            setVisibility={(visibility) => {
                              this.setState({ [`is_details_shown_${provider.id}`] : visibility })
                            }}
                          />
                        }
                      </li>
                      <li>
                        <a title='Message' data-placement='top' data-toggle='tooltip' className='tooltips' href='#' data-original-title='Message' onClick={() => {
                          this.setState({ chat_visibility: true, msg_recipient: provider })
                        }}><i className='ion-chatbox-working' /></a>
                      </li>
                      <li>
                        {
                          ((_.findIndex(user.hired, { id: provider.id }) > -1) && user.hired[_.findIndex(user.hired, { id: provider.id })].hiring_status < 2)
                          ? <small style={{ color: 'red' }}>{ (user.hired[_.findIndex(user.hired, { id: provider.id })].hiring_status == 0) ? 'Awaiting Reply' : 'Hired' }</small>
                          : <Popconfirm title={`Are you sure do you want to hire ${provider.first_name} ${provider.last_name}？`} okText='Send Job Offer' cancelText='No' onConfirm={() => {
                            _Api.hireProvider({
                              seeker_id: user.id,
                              provider_id: provider.id,
                              recipient_id: recipient.id
                            }).then((response) => {
                              this.props.refreshUser()
                            })
                          }}>
                            <a title='Hire' data-placement='top' data-toggle='tooltip' className='tooltips' href='#' data-original-title='Hire'><i className='ion-ios7-personadd' /></a>
                          </Popconfirm>
                        }
                      </li>
                    </ul>
                  </div>
                </div>
              </div>
            )
          })
        }
      </div>
    )
  }
}

SearchProvider.propTypes = {
}
