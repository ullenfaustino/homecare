import SearchProviderContainer from './containers/SearchProviderContainer'

export default (store) => ({
  path: '/seeker/find-provider',
  component : SearchProviderContainer
})
