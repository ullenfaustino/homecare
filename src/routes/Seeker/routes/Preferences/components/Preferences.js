import React, { Component, PropTypes } from 'react'
import { Select, Button, TimePicker, Table, Icon, Divider, Spin, message, DatePicker } from 'antd'
import GoogleAutcompleteInput from 'components/GoogleAutcompleteInput'
import moment from 'moment'
import * as GoogleApi from 'modules/googlemaps/api'
import * as _Api from './../../../modules/api'

import {
  GENDER
} from 'consts/apiConsts'

import {
  days
} from 'consts/provider'

const { MonthPicker, RangePicker, WeekPicker } = DatePicker

export default class Preferences extends Component {

  constructor (props) {
    super(props)
    this.state = {
      loading: false,
      isFetchingUserData: true,

      gender: 0,
      type_of_care:[],
      require_car: 0,
      days: [],
      start_duty: moment(),
      rate: '',
      rate_amount: 0,
      hours_per_day: 0
    }
  }

  componentWillMount () {
    console.log('[this-props]', this.props)
    _Api.findByUid(this.props.auth.user.uid)
    .then((mUser) => {
      this.setState({
        isFetchingUserData: false
      }, this.parseUserData(mUser))
    }).catch((err) => {
      console.log('err', err)
      this.setState({
        isFetchingUserData: false
      })
    })
  }

  parseUserData = (user) => {
    console.log('[CurrentUser]', user)

    const formattedDays = user.seekers_days_pref.map((day) => {
      return day.day
    })

    const formattedTypeCare = user.seekers_type_care.map((type_care) => {
      return type_care.lkup_type_care_id.toString()
    })

    this.setState({
      gender: user.gender,
      require_car: user.is_require_car.toString(),
      days: formattedDays,
      type_of_care: formattedTypeCare,
      start_duty: moment(user.seekers_search_preferences.start_duty),
      hours_per_day: user.seekers_search_preferences.hours_per_day,
      rate: user.seekers_search_preferences.rate,
      rate_amount: user.seekers_search_preferences.rate_amount
    })
  }

  handleOnSubmitPreferences = () => {
    const params = {
      user: {
        uid: this.props.auth.user.uid,
        id: this.props.auth.user.id
      },
      gender: this.state.gender,
      type_of_care: this.state.type_of_care,
      require_car: this.state.require_car,
      days: this.state.days,
      start_duty: this.state.start_duty,
      rate: this.state.rate,
      rate_amount: this.state.rate_amount,
      hours_per_day: this.state.hours_per_day
    }
    console.log('[post-params]', params)
    _Api.addPreferences(params).then((response) => {
      console.log(response)
      this.setState({
        loading: false
      }, message.success('Successfully saved'))
    }).catch((err) => {
      console.log(err)
      this.setState({
        loading: false
      }, message.error('Error saving preferences'))
    })
  }

  render () {
    const { type_care } = this.props.app.settings

    if (this.state.isFetchingUserData) {
      return (
        <Spin />
      )
    }

    return (
      <div className='row'>
        <div className='col-sm-12'>
          <div className='panel panel-primary'>
            <div className='panel-body'>
              {/* <h4 className='m-t-0 m-b-30'>My Information <br /><small>Preferences</small></h4> */}
              <legend>Care Provider's Qualification Preferences<span className='pull-right' style={{ fontSize: '12px', color: 'red', marginTop: '10px' }}>(required)</span></legend>
              <form className='form-horizontal' role='form' onSubmit={(ev) => {
                ev.preventDefault()
                this.setState({
                  loading: true
                }, this.handleOnSubmitPreferences())
              }}>
                <div className='form-group'>
                  <label className='col-md-2 control-label' htmlFor='example-email'>Gender</label>
                  <div className='col-md-2'>
                    <Select
                      mode='dropdown'
                      style={{ width: '100%' }}
                      placeholder='Select Gender'
                      defaultValue={this.state.gender}
                      onChange={(values) => {
                        this.setState({
                          gender : values
                        })
                      }}
                      >
                      {
                        GENDER.map((item) => {
                          return (
                            <Select.Option key={item.id} value={item.id}>{item.value}</Select.Option>
                          )
                        })
                      }
                    </Select>
                  </div>
                </div>
                <div className='form-group'>
                  <label className='col-md-2 control-label' htmlFor='example-email'>Type of Care</label>
                  <div className='col-md-6'>
                    <Select
                      mode='multiple'
                      style={{ width: '100%' }}
                      placeholder='Select Type of Care'
                      defaultValue={this.state.type_of_care}
                      onChange={(values) => {
                        this.setState({
                          type_of_care : values
                        })
                      }}
                      >
                      {
                        type_care.map((item) => {
                          return (
                            <Select.Option key={item.id}>{item.name}</Select.Option>
                          )
                        })
                      }
                    </Select>
                    <small>(you may select more than one)</small>
                  </div>
                </div>
                <div className='form-group'>
                  <label className='col-md-2 control-label' htmlFor='example-email'>Require Car?</label>
                  <div className='col-md-2'>
                    <Select
                      mode='dropdown'
                      style={{ width: '100%' }}
                      placeholder='Yes or No'
                      defaultValue={this.state.require_car}
                      onChange={(values) => {
                        this.setState({
                          require_car : values
                        })
                      }}
                      >
                      <Select.Option key='1' value='1'>Yes</Select.Option>
                      <Select.Option key='0' value='0'>No</Select.Option>
                    </Select>
                  </div>
                </div>
                <legend>Schedule<span className='pull-right' style={{ fontSize: '12px', color: 'red', marginTop: '10px' }}>(required)</span></legend>
                <div className='form-group'>
                  <label className='col-md-2 control-label' htmlFor='example-email'>Day(s)</label>
                  <div className='col-md-6'>
                    <Select
                      showSearch
                      mode='multiple'
                      placeholder='Select Day'
                      defaultValue={this.state.days}
                      optionFilterProp='children'
                      onChange={(value) => {
                        this.setState({
                          days: value
                        })
                      }}
                      filterOption={(input, option) => option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
                    >
                      {
                          days.map((item) => {
                            return (
                              <Select.Option key={item.id} value={item.value}>{item.value}</Select.Option>
                            )
                          })
                        }
                    </Select>
                    <small>(you may select more than one)</small>
                  </div>
                </div>
                <div className='form-group'>
                  <label className='col-md-2 control-label' htmlFor='example-email'>Hours per Day</label>
                  <div className='col-md-2'>
                    <input type='number' className='form-control' required placeholder='Hours per day' value={this.state.hours_per_day} onChange={(ev) => {
                      this.setState({
                        hours_per_day: ev.target.value
                      })
                    }} />
                  </div>
                </div>
                <div className='form-group'>
                  <label className='col-md-2 control-label' htmlFor='example-email'>Preferred start of duty</label>
                  <div className='col-md-10'>
                    <DatePicker value={this.state.start_duty} onChange={(date, dateString) => {
                      this.setState({
                        start_duty: date
                      })
                    }} />
                  </div>
                </div>
                <legend>Preferred Rate<span className='pull-right' style={{ fontSize: '12px', color: 'black', marginTop: '10px' }}>(Optional)</span></legend>
                <div className='form-group'>
                  <label className='col-md-2 control-label' htmlFor='example-email'>Rate</label>
                  <div className='col-md-2'>
                    <Select
                      mode='dropdown'
                      style={{ width: '100%' }}
                      placeholder='Select Type of Rate'
                      defaultValue={this.state.rate}
                      onChange={(values) => {
                        this.setState({
                          rate : values
                        })
                      }}
                      >
                      <Select.Option key='1' value='hourly_rate'>Hourly Rate</Select.Option>
                      <Select.Option key='2' value='daily_rate'>Daily Rate</Select.Option>
                      <Select.Option key='3' value='overnight_rate'>Overnight Rate</Select.Option>
                    </Select>
                  </div>
                </div>
                <div className='form-group'>
                  <label className='col-md-2 control-label' htmlFor='example-email'>Rate Amount</label>
                  <div className='col-md-2'>
                    <input type='number' className='form-control' required placeholder='rate amount' value={this.state.rate_amount} onChange={(ev) => {
                      this.setState({
                        rate_amount: ev.target.value
                      })
                    }} />
                  </div>
                </div>
                <hr />
                <div className='form-group'>
                  <label className='col-md-2 control-label' htmlFor='example-email' />
                  <div className='col-md-10' >
                    <Button type='primary' className='pull-right' id={321} loading={this.state.loading} htmlType='submit'>
                      <i className='mdi mdi-content-save' /> Save Information
                    </Button>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    )
  }
}

Preferences.propTypes = {
}
