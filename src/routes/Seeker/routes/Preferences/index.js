import PreferenceContainer from './containers/PreferenceContainer'

export default (store) => ({
  path: '/seeker/preferences',
  component : PreferenceContainer
})
