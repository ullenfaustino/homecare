import JobPostContainer from './containers/JobPostContainer'

export default (store) => ({
  path: '/seeker/post-job',
  component : JobPostContainer
})
