import React, { Component, PropTypes } from 'react'
import { Link, browserHistory } from 'react-router'
import { Select, Button, TimePicker, message, DatePicker, Modal, Spin, Icon, Divider, Table, Popconfirm } from 'antd'
import ReactImageFallback from 'react-image-fallback'
import moment from 'moment'
import * as _Api from './../../../modules/api'
import { URL } from 'consts/apiConsts'
import * as Helper from 'consts/helper'

export default class Applicants extends Component {

  constructor (props) {
    super(props)
    this.state = {
      loading: false,
      provider: undefined
    }
  }

  render () {
    const { visibility, applicants, recipient } = this.props

    const data = applicants.map((applicant, i) => {
      return {
        key: applicant.id,
        applicant_name: applicant.provider.first_name,
        age: Helper.getAge(applicant.provider.birthday),
        gender_name: (applicant.provider.gender == 0) ? 'Female' : 'Male',
        address: applicant.provider.location.label,
        ...applicant
      }
    })

    const columns = [{
      title: 'Applicant Name',
      dataIndex: 'applicant_name',
      key: 'applicant_name',
      render: (text, record) => {
        return (
          <a href='#' onClick={(ev) => {
            ev.preventDefault()
            this.props.setSelectedProvider(record.provider)
          }}>
            <ReactImageFallback
              src={record.provider.avatar}
              fallbackImage='/user-avatar/user.svg'
              alt={record.provider.name}
              className='img-circle'
              style={{ width: 40, height: 40, marginRight: '10px' }}
            />
            {text}
          </a>
        )
      }
    }, {
      title: 'Age',
      dataIndex: 'age',
      key: 'age'
    }, {
      title: 'Gender',
      dataIndex: 'gender_name',
      key: 'gender'
    }, {
      title: 'Address',
      dataIndex: 'address',
      key: 'address'
    }, {
      title: 'Action',
      key: 'action',
      render: (text, record) => {
        const { provider } = record
        return (
          <span>
            <Popconfirm title={`Are you sure do you want to hire ${provider.first_name} ${provider.last_name}？`} okText='Yes, I want to hire' cancelText='No' onConfirm={() => {
              const params = {
                provider_id: provider.id,
                job_post_id: record.job_post_id,
                application_id: record.id,
                recipient_id: recipient.id,
                seeker_id: recipient.user_id
              }
              _Api.hireApplicant(params).then((response) => {
                message.success('Provider successfully hired.')
                this.props.refresh()
                this.props.setVisibility(false)
              })
              .catch((err) => {
                message.error('[Error] unable to hire this provider')
                console.log('[err]', err)
              })
            }}>
              <a href='#'>Hire</a>
            </Popconfirm>
            {/* <Divider type='vertical' />
            <a href='#'>Delete</a> */}
          </span>
        )
      }
    }]

    return (
      <Modal
        destroyOnClose
        title='Applicants'
        width={1200}
        visible={visibility}
        footer={null}
        onCancel={() => {
          this.props.setVisibility(false)
        }}
        >
        <div className='row'>
          <div className='col-sm-12'>
            <div className='panel panel-primary'>
              <div className='panel-body'>
                <Table columns={columns} dataSource={data} />
              </div>
            </div>
          </div>
        </div>
      </Modal>
    )
  }
}

Applicants.propTypes = {
}
