import React, { Component, PropTypes } from 'react'
import { Link, browserHistory } from 'react-router'
import { Select, Button, TimePicker, Table, Icon, Divider, Spin, message, DatePicker, Popconfirm } from 'antd'
import GoogleAutcompleteInput from 'components/GoogleAutcompleteInput'
import moment from 'moment'
import ReactStars from 'react-stars'
import ReactImageFallback from 'react-image-fallback'
import * as _Api from './../../../modules/api'
import * as Helper from 'consts/helper'

import JobPostForm from './JobPostForm'
import Applicants from './Applicants'
import CareRecipientDetails from './../../../components/CareRecipientDetails'
import ProviderDetails from './../../../components/ProviderDetails'

export default class JobPost extends Component {

  constructor (props) {
    super(props)
    this.state = {
      loading: false,
      JobPostFormVisibility: false,
      recipient_view_visibility: false,
      applicants_visibility: false,
      provider_details_visibility: false,
      recipient: undefined,
      applicants: undefined,
      selectedProvider: undefined
    }
  }

  componentWillMount () {
    this.props.refreshUser()
  }

  handleTagsView = (tagItems) => {
    const items = tagItems.map((item) => {
      return item.value.name
    })
    return items.join(', ')
  }

  handleShowJobPostFormModal = () => {
    const { JobPostFormVisibility } = this.state
    const { settings } = this.props.app
    const { user } = this.props.auth

    const filteredRecipients = user.recipients.filter((recipient) => {
      if (user.hired.length > 0) {
        const filteredHired = user.hired.filter((hire) => {
          const index = _.findIndex(hire.client, { id: recipient.id })
          const isExists = index > -1
          if (isExists) {
            const client = hire.client[index]
            return (client && hire.hiring_status > 1)
          } else {
            return true
          }
        })
        return (filteredHired.length > 0)
      } else {
        return true
      }
    })

    return (
      <JobPostForm
        recipients={filteredRecipients}
        settings={settings}
        visibility={JobPostFormVisibility}
        setVisibility={(visibility) => {
          this.setState({ JobPostFormVisibility: visibility })
        }}
        refresh={() => {
          this.props.refreshUser()
        }}
      />
    )
  }

  renderRecipientDetailsModal = () => {
    return (
      <CareRecipientDetails
        recipient={this.state.recipient}
        visibility={this.state.recipient_view_visibility}
        setVisibility={(visibility) => {
          this.setState({ recipient_view_visibility: visibility })
        }}
      />
    )
  }

  renderApplicantsModal = () => {
    return (
      <Applicants
        applicants={this.state.applicants}
        recipient={this.state.recipient}
        visibility={this.state.applicants_visibility}
        setVisibility={(visibility) => {
          this.setState({ applicants_visibility: visibility })
        }}
        setSelectedProvider={(provider) => {
          this.setState({ setSelectedProvider: provider, provider_details_visibility: true })
        }}
        refresh={() => {
          this.props.refreshUser().then(() => {
            browserHistory.push('/seeker/care-providers')
          })
        }}
      />
    )
  }

  renderProviderDetailsModal = () => {
    return (
      <ProviderDetails
        provider={this.state.setSelectedProvider}
        visibility={this.state.provider_details_visibility}
        setVisibility={(visibility) => {
          this.setState({ provider_details_visibility: visibility })
        }}
      />
    )
  }

  render () {
    const { JobPostFormVisibility, recipient_view_visibility, applicants_visibility, provider_details_visibility } = this.state
    const { user, currentlySending } = this.props.auth

    if (currentlySending) {
      return (
        <Spin />
      )
    }

    const data = user.postedJobs.map((job, i) => {
      return {
        key: job.id,
        care_recipient_name: job.recipient.name,
        age: Helper.getAge(job.recipient.birthday),
        gender_name: (job.recipient.gender == 0) ? 'Female' : 'Male',
        address: job.recipient.location.label,
        post_date_formatted: moment(job.post_date).format('LL'),
        application_date_formatted: moment(job.application_date).isValid() ? moment(job.application_date).format('LL') : '',
        hired_date_formatted: moment(job.hired_date).isValid() ? moment(job.hired_date).format('LL') : '',
        status_name: (job.status == 0) ? 'Posted'
                      : (job.status == 1) ? 'Closed'
                      : (job.status == 2) ? 'Completed' : 'Canceled',
        applicants_count: job.applicants.length,
        ...job
      }
    })

    const columns = [{
      title: 'Care Recipient',
      dataIndex: 'care_recipient_name',
      key: 'care_recipient_name',
      render: (text, record) => {
        return (
          <a href='#' onClick={(ev) => {
            ev.preventDefault()
            this.setState({ recipient_view_visibility: true, recipient: record.recipient })
          }}>
            <ReactImageFallback
              src={record.recipient.avatar}
              fallbackImage='/user-avatar/user.svg'
              alt={record.recipient.name}
              className='img-circle'
              style={{ width: 40, height: 40, marginRight: '10px' }}
            />
            {text}
          </a>
        )
      }
    }, {
      title: 'Applicants',
      dataIndex: 'applicants_count',
      key: 'applicants_count',
      render: (text, record) => {
        if (record.status > 0) {
          return 'Hired'
        } else {
          return (
            <a href='#' onClick={(ev) => {
              ev.preventDefault()
              this.setState({ applicants_visibility: true, applicants: record.applicants, recipient: record.recipient })
            }}>
              <strong>{text}</strong><span>{(record.applicants.length > 1) ? ' applicants' : ' applicant'}</span>
            </a>
          )
        }
      }
    }, {
      title: 'Age',
      dataIndex: 'age',
      key: 'age'
    }, {
      title: 'Gender',
      dataIndex: 'gender_name',
      key: 'gender'
    }, {
      title: 'Address',
      dataIndex: 'address',
      key: 'address'
    }, {
      title: 'Post Date',
      dataIndex: 'post_date_formatted',
      key: 'post_date_formatted'
    }, {
      title: 'Provider Application Date',
      dataIndex: 'application_date_formatted',
      key: 'application_date_formatted'
    }, {
      title: 'Provider Hired Date',
      dataIndex: 'hired_date_formatted',
      key: 'hired_date_formatted'
    }, {
      title: 'Status',
      dataIndex: 'status_name',
      key: 'status_name'
    }]

    return (
      <div className='row'>
        <div className='col-sm-12'>
          <div className='panel panel-primary'>
            <div className='panel-body'>
              <Button type='primary' onClick={() => {
                this.setState({ JobPostFormVisibility: true })
              }}><i className='mdi mdi-clipboard-text' /> Post Job</Button>
              <Table columns={columns} dataSource={data} />
              {
                JobPostFormVisibility && this.handleShowJobPostFormModal()
              }
              {
                recipient_view_visibility && this.renderRecipientDetailsModal()
              }
              {
                applicants_visibility && this.renderApplicantsModal()
              }
              {
                provider_details_visibility && this.renderProviderDetailsModal()
              }
            </div>
          </div>
        </div>
      </div>
    )
  }
}

JobPost.propTypes = {
}
