import React, { Component, PropTypes } from 'react'
import { Link, browserHistory } from 'react-router'
import { Select, Button, TimePicker, message, DatePicker, Modal, Spin, Icon, Upload, Slider, InputNumber, Row, Col } from 'antd'
import GoogleAutcompleteInput from 'components/GoogleAutcompleteInput'
import moment from 'moment'
import * as _Api from './../../../modules/api'
import { URL } from 'consts/apiConsts'
import * as Helper from 'consts/helper'

export default class JobPostForm extends Component {

  constructor (props) {
    super(props)
    this.state = {
      loading: false,
      selectedRecipient: undefined
    }
  }

  handleTagsView = (tagItems) => {
    const items = tagItems.map((item) => {
      return item.value.name
    })
    return items.join(', ')
  }

  handleSubmitForm = () => {
    const { selectedRecipient } = this.state
    const params = {
      seeker_id: selectedRecipient.user_id,
      recipient_id: selectedRecipient.id
    }
    console.log(selectedRecipient, params)
    _Api.postJob(params).then((response) => {
      console.log('[response]', response)
      this.setState({ loading: false }, () => {
        message.success('Job successfully posted')
        this.props.refresh()
        this.props.setVisibility(false)
      })
    }).catch((err) => {
      console.log('[err]', err)
      message.error('[ERROR]: unable to submit request.')
      this.setState({ loading: false, selectedRecipient: undefined }, () => {
        this.props.setVisibility(false)
      })
    })
  }

  render () {
    const { visibility, settings, recipients } = this.props
    const { selectedRecipient, loading } = this.state
    const { health_conditions, work_restrictions, work_schedules, client_preferences } = settings

    return (
      <Modal
        destroyOnClose
        title='Post Job'
        width={1000}
        visible={visibility}
        footer={null}
        onCancel={() => {
          this.props.setVisibility(false)
        }}
        >
        <div className='row'>
          <div className='col-sm-12'>
            <div className='panel panel-primary'>
              <div className='panel-body'>
                <legend>Care Recipient’s Information</legend>
                <form className='form-horizontal'>
                  <div className='form-group'>
                    <span className='col-md-2 control-label'>Who needs care?</span>
                    <div className='col-md-5'>
                      <Select
                        mode='dropdown'
                        style={{ width: '100%' }}
                        placeholder='Select Recipient'
                        onChange={(values) => {
                          const selRecipients = _.find(recipients, function (o) { return o.id == values })
                          this.setState({ selectedRecipient : selRecipients })
                        }}
                        >
                        {
                          recipients.map((item) => {
                            return (
                              <Select.Option key={item.id} value={item.id}>{item.name}</Select.Option>
                            )
                          })
                        }
                      </Select>
                    </div>
                    <div className='col-md-5'>
                      <Button type='primary' style={{ marginTop: '4px' }} disabled={!selectedRecipient} loading={loading} onClick={() => {
                        this.setState({ loading: true }, () => {
                          this.handleSubmitForm()
                        })
                      }}><i className='mdi mdi-account-search' /> Post Now</Button>
                    </div>
                  </div>
                  <div className='form-group'>
                    <span className='col-md-2 control-label'>Location:</span>
                    <div className='col-md-10'>
                      <label className='control-label'>{(_.has(selectedRecipient, 'location')) ? selectedRecipient.location.label : ''}</label>
                    </div>
                  </div>
                  <div className='form-group'>
                    <span className='col-md-2 control-label'>Gender:</span>
                    <div className='col-md-10'>
                      <label className='control-label'>{(_.has(selectedRecipient, 'gender')) ? ((selectedRecipient.gender == 0) ? 'Female' : 'Male') : ''}</label>
                    </div>
                  </div>
                  <div className='form-group'>
                    <span className='col-md-2 control-label'>Age:</span>
                    <div className='col-md-10'>
                      <label className='control-label'>{(_.has(selectedRecipient, 'birthday')) ? Helper.getAge(selectedRecipient.birthday) : ''}</label>
                    </div>
                  </div>
                  <div className='form-group'>
                    <span className='col-md-2 control-label'>General Status:</span>
                    <div className='col-md-10'>
                      <label className='control-label'>{(_.has(selectedRecipient, 'general_status')) ? this.handleTagsView(selectedRecipient.general_status) : ''}</label>
                    </div>
                  </div>
                  <div className='form-group'>
                    <span className='col-md-2 control-label'>Medical Condition:</span>
                    <div className='col-md-10'>
                      <label className='control-label'>{(_.has(selectedRecipient, 'medical_conditions')) ? this.handleTagsView(selectedRecipient.medical_conditions) : ''}</label>
                    </div>
                  </div>
                  <legend>Schedule</legend>
                  <div className='form-group'>
                    <span className='col-md-2 control-label'>Days:</span>
                    <div className='col-md-10'>
                      <label className='control-label'>{(_.has(selectedRecipient, 'days')) ? selectedRecipient.days.map((day) => {
                        return day.day
                      }).join(', ') : ''}
                      </label>
                    </div>
                  </div>
                  <div className='form-group'>
                    <span className='col-md-2 control-label'>Hours per day:</span>
                    <div className='col-md-10'>
                      <label className='control-label'>{(_.has(selectedRecipient, 'hours_per_day')) ? selectedRecipient.hours_per_day : ''}</label>
                    </div>
                  </div>
                  <div className='form-group'>
                    <span className='col-md-2 control-label'>Start of Duty:</span>
                    <div className='col-md-10'>
                      <label className='control-label'>{(_.has(selectedRecipient, 'start_duty')) ? moment(selectedRecipient.start_duty).format('LL') : ''}</label>
                    </div>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </Modal>
    )
  }
}

JobPostForm.propTypes = {
}
