import CareRecipientsContainer from './containers/CareRecipientsContainer'

export default (store) => ({
  path: '/seeker/recipients',
  component : CareRecipientsContainer
})
