import React, { Component, PropTypes } from 'react'
import { Link, browserHistory } from 'react-router'
import { Select, Button, TimePicker, message, DatePicker, Modal, Spin, Icon, Upload } from 'antd'
import GoogleAutcompleteInput from 'components/GoogleAutcompleteInput'
import moment from 'moment'
import * as GoogleApi from 'modules/googlemaps/api'
import * as _Api from './../../../modules/api'
import { URL } from 'consts/apiConsts'

import {
  GENDER
} from 'consts/apiConsts'

import {
  days
} from 'consts/provider'

const { MonthPicker, RangePicker, WeekPicker } = DatePicker

export default class CareRecipientsForm extends Component {

  constructor (props) {
    super(props)
    this.state = {
      loading: false,

      gender: 0,
      general_status:[],
      medical_conditions:[],
      days: [],
      start_duty: moment(),
      birthday: moment(),
      hours_per_day: 0,
      recipient_name: '',
      selectedLocation: {},
      editableContentReady: false
    }
  }

  componentDidMount () {
    if (this.props.isEdit) {
      this.renderEditableValues()
    }
  }

  renderEditableValues = () => {
    // console.log('[componentDidMount][props]', this.props)
    const { recipient } = this.props
    const formattedSelectedLocation = {
      place_id: recipient.location.place_id,
      id: recipient.location.google_place_id,
      description: recipient.location.description,
      label: recipient.location.label,
      reference: recipient.location.reference
    }
    const formattedGeneralStatus = recipient.general_status.map((_general_status) => {
      return _general_status.lkup_general_status_id.toString()
    })
    const formattedMedicalConditions = recipient.medical_conditions.map((medical_condition) => {
      return medical_condition.lkup_medical_conditions_id.toString()
    })
    const formattedDays = recipient.days.map((day) => {
      return day.day
    })

    this.setState({
      recipient_name: recipient.name,
      gender: recipient.gender,
      start_duty: moment(recipient.start_duty),
      birthday: moment(recipient.birthday),
      hours_per_day: recipient.hours_per_day,
      general_status: formattedGeneralStatus,
      medical_conditions: formattedMedicalConditions,
      days: formattedDays,
      selectedLocation: formattedSelectedLocation,
      editableContentReady: true
    })
  }

  handleOnSubmitPreferences = () => {
    const { currentUser, recipient } = this.props
    const params = {
      user: {
        uid: currentUser.uid,
        id: currentUser.id,
        recipient_id: recipient.id || -1
      },
      location: {
        place_id: this.state.selectedLocation.place_id,
        google_place_id: this.state.selectedLocation.id,
        description: this.state.selectedLocation.description,
        label: this.state.selectedLocation.label,
        reference: this.state.selectedLocation.reference
      },
      gender: this.state.gender,
      birthday: this.state.birthday,
      general_status: this.state.general_status,
      medical_conditions: this.state.medical_conditions,
      days: this.state.days,
      start_duty: this.state.start_duty,
      hours_per_day: this.state.hours_per_day,
      recipient_name: this.state.recipient_name
    }
    // console.log('[post-params]', params)
    _Api.addRecipient(params).then((response) => {
      // console.log(response)
      this.setState({
        loading: false
      }, () => {
        message.success('Successfully saved')
        this.props.setVisibility(false)
        this.props.setRefreshValues()
      })
    }).catch((err) => {
      console.log(err)
      this.setState({
        loading: false
      }, () => {
        message.error('Error saving recipient preferences')
        this.props.setVisibility(false)
      })
    })
  }

  handleChange = (info) => {
    if (info.file.status === 'uploading') {
      this.setState({ loading: true })
      return
    }
    if (info.file.status === 'done') {
      // Get this url from response in real world.
      getBase64(info.file.originFileObj, imageUrl => this.setState({
        imageUrl,
        loading: false
      }, () => {
        this.props.setRefreshValues()
      }))
    }
  }

  render () {
    const { visibility, settings, isEdit, recipient } = this.props
    const { general_status, medical_conditions } = settings

    if (isEdit && !this.state.editableContentReady) {
      return (
        <Spin />
      )
    }

    const uploadButton = (
      <div>
        <Icon type={this.state.loading ? 'loading' : 'plus'} />
        <div className='ant-upload-text'>Upload</div>
      </div>
    )
    const imageUrl = this.state.imageUrl
    const actionURL = `${URL}/uploads/avatar/recipient?recipientId=${recipient.id}`

    return (
      <Modal
        destroyOnClose
        title={(isEdit) ? 'Update Recipient Preferences' : 'New Care Recipient'}
        width={1000}
        visible={visibility}
        onOk={() => {
          this.setState({
            loading: true
          }, this.handleOnSubmitPreferences())
        }}
        onCancel={() => {
          this.props.setVisibility(false)
        }}
        >
        <div className='row'>
          <div className='col-sm-12'>
            <div className='panel panel-primary'>
              <div className='panel-body'>
                {
                  (isEdit) &&
                  <div>
                    <legend>Avatar</legend>
                    <div className='form-group'>
                      <Upload
                        name='avatar'
                        listType='picture-card'
                        className='avatar-uploader'
                        showUploadList={false}
                        action={actionURL}
                        beforeUpload={beforeUpload}
                        onChange={this.handleChange}
                        >
                        {(imageUrl || recipient.avatar) ? <img width={120} height='auto' src={imageUrl || recipient.avatar} alt='' /> : uploadButton}
                      </Upload>
                    </div>
                  </div>
                }
                <legend>Care Recipient's Information<span className='pull-right' style={{ fontSize: '12px', color: 'red', marginTop: '10px' }}>(required)</span></legend>
                <form className='form-horizontal'>
                  <div className='form-group'>
                    <label className='col-md-2 control-label' htmlFor='example-email'>Who needs care?</label>
                    <div className='col-md-10'>
                      <input type='text' className='form-control' placeholder='name of care recipient' value={this.state.recipient_name} onChange={(ev) => {
                        this.setState({ recipient_name: ev.target.value })
                      }} />
                    </div>
                  </div>
                  <div className='form-group'>
                    <label className='col-md-2 control-label'>Location</label>
                    <div className='col-md-10'>
                      <GoogleAutcompleteInput autoComplete={(inputText) => {
                        return GoogleApi.autoComplete({
                          q: inputText
                        })
                      }} onChange={(value) => {
                        this.setState({
                          selectedLocation: value
                        })
                      }} value={this.state.selectedLocation} />
                    </div>
                  </div>
                  <div className='form-group'>
                    <label className='col-md-2 control-label' htmlFor='example-email'>Gender</label>
                    <div className='col-md-2'>
                      <Select
                        mode='dropdown'
                        style={{ width: '100%' }}
                        placeholder='Select Gender'
                        defaultValue={this.state.gender}
                        onChange={(values) => {
                          this.setState({
                            gender : values
                          })
                        }}
                        >
                        {
                          GENDER.map((item) => {
                            return (
                              <Select.Option key={item.id} value={item.id}>{item.value}</Select.Option>
                            )
                          })
                        }
                      </Select>
                    </div>
                  </div>
                  <div className='form-group'>
                    <label className='col-md-2 control-label' htmlFor='example-email'>Birthday</label>
                    <div className='col-md-10'>
                      <DatePicker value={this.state.birthday} onChange={(date, dateString) => {
                        this.setState({
                          birthday: date
                        })
                      }} />
                    </div>
                  </div>
                  <div className='form-group'>
                    <label className='col-md-2 control-label' htmlFor='example-email'>General Status</label>
                    <div className='col-md-10'>
                      <Select
                        mode='multiple'
                        style={{ width: '100%' }}
                        placeholder='Select General Status'
                        defaultValue={this.state.general_status}
                        onChange={(values) => {
                          this.setState({
                            general_status : values
                          })
                        }}
                        >
                        {
                          general_status.map((item) => {
                            return (
                              <Select.Option key={item.id}>{item.name}</Select.Option>
                            )
                          })
                        }
                      </Select>
                      <small>(you may select more than one)</small>
                    </div>
                  </div>
                  <div className='form-group'>
                    <label className='col-md-2 control-label' htmlFor='example-email'>Medical Conditions</label>
                    <div className='col-md-10'>
                      <Select
                        mode='multiple'
                        style={{ width: '100%' }}
                        placeholder='Select Medical Conditions'
                        defaultValue={this.state.medical_conditions}
                        onChange={(values) => {
                          this.setState({
                            medical_conditions : values
                          })
                        }}
                        >
                        {
                          medical_conditions.map((item) => {
                            return (
                              <Select.Option key={item.id}>{item.name}</Select.Option>
                            )
                          })
                        }
                      </Select>
                      <small>(you may select more than one)</small>
                    </div>
                  </div>
                  <legend>Care Schedule<span className='pull-right' style={{ fontSize: '12px', color: 'red', marginTop: '10px' }}>(required)</span></legend>
                  <div className='form-group'>
                    <label className='col-md-2 control-label' htmlFor='example-email'>Day(s)</label>
                    <div className='col-md-10'>
                      <Select
                        showSearch
                        mode='multiple'
                        placeholder='Select Day'
                        defaultValue={this.state.days}
                        optionFilterProp='children'
                        onChange={(value) => {
                          this.setState({
                            days: value
                          })
                        }}
                        filterOption={(input, option) => option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
                      >
                        {
                            days.map((item) => {
                              return (
                                <Select.Option key={item.id} value={item.value}>{item.value}</Select.Option>
                              )
                            })
                          }
                      </Select>
                      <small>(you may select more than one)</small>
                    </div>
                  </div>
                  <div className='form-group'>
                    <label className='col-md-2 control-label' htmlFor='example-email'>Hours per Day</label>
                    <div className='col-md-2'>
                      <input type='number' className='form-control' required placeholder='Hours per day' value={this.state.hours_per_day} onChange={(ev) => {
                        this.setState({
                          hours_per_day: ev.target.value
                        })
                      }} />
                    </div>
                  </div>
                  <div className='form-group'>
                    <label className='col-md-2 control-label' htmlFor='example-email'>Preferred start of duty</label>
                    <div className='col-md-10'>
                      <DatePicker value={this.state.start_duty} onChange={(date, dateString) => {
                        this.setState({
                          start_duty: date
                        })
                      }} />
                    </div>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </Modal>
    )
  }
}

function getBase64 (img, callback) {
  const reader = new FileReader()
  reader.addEventListener('load', () => callback(reader.result))
  reader.readAsDataURL(img)
}

function beforeUpload (file) {
  const isJPG = file.type === 'image/jpeg'
  if (!isJPG) {
    message.error('You can only upload JPG file!')
  }
  const isLt2M = file.size / 1024 / 1024 < 2
  if (!isLt2M) {
    message.error('Image must smaller than 2MB!')
  }
  return isJPG && isLt2M
}

CareRecipientsForm.propTypes = {
}
