import React, { Component, PropTypes } from 'react'
import { Select, Button, TimePicker, Table, Icon, Divider, Spin, message, DatePicker } from 'antd'
import GoogleAutcompleteInput from 'components/GoogleAutcompleteInput'
import moment from 'moment'
import ReactImageFallback from 'react-image-fallback'
import * as GoogleApi from 'modules/googlemaps/api'
import * as _Api from './../../../modules/api'
import CareRecipientDetails from './../../../components/CareRecipientDetails'

import {
  GENDER
} from 'consts/apiConsts'

import * as Helper from 'consts/helper'

import {
  days
} from 'consts/provider'

import CareRecipientsForm from './CareRecipientsForm'

const { MonthPicker, RangePicker, WeekPicker } = DatePicker

export default class CareRecipients extends Component {

  constructor (props) {
    super(props)
    this.state = {
      loading: false,
      isFetchingUserData: true,
      currentUser: {},
      recipient: {},
      new_form_visibility: false,
      modal_visibility: false,
      isEdit: false
    }
  }

  componentWillMount () {
    this.fetchCurrentUser()
  }

  fetchCurrentUser = () => {
    _Api.findByUid(this.props.auth.user.uid)
    .then((mUser) => {
      this.setState({
        isFetchingUserData: false,
        currentUser: mUser
      })
    }).catch((err) => {
      console.log('err', err)
      this.setState({
        isFetchingUserData: false
      })
    })
  }

  renderFormView = () => {
    const { currentUser } = this.state
    const { settings } = this.props.app
    return (
      <CareRecipientsForm
        isEdit={this.state.isEdit}
        recipient={this.state.recipient}
        settings={settings}
        currentUser={currentUser}
        visibility={this.state.new_form_visibility}
        setVisibility={(visibility) => {
          this.setState({ new_form_visibility: visibility })
        }}
        setRefreshValues={() => {
          this.fetchCurrentUser()
        }}
      />
    )
  }

  renderRecipientDetailsModal = () => {
    return (
      <CareRecipientDetails
        recipient={this.state.recipient}
        visibility={this.state.modal_visibility}
        setVisibility={(visibility) => {
          this.setState({ modal_visibility:visibility })
        }}
      />
    )
  }

  render () {
    if (this.state.isFetchingUserData) {
      return (
        <Spin />
      )
    }

    const { currentUser } = this.state
    const { settings } = this.props.app
    const { recipients } = currentUser

    const data_recipients = recipients.map((recipient, i) => {
      return {
        key: recipient.id,
        name: recipient.name,
        age: Helper.getAge(recipient.birthday),
        gender_name: (recipient.gender == 0) ? 'Female' : 'Male',
        address: recipient.location.label,
        ...recipient
      }
    })

    const columns = [{
      title: 'Name',
      dataIndex: 'name',
      key: 'name',
      render: (text, record) => {
        return (
          <a href='#' onClick={(ev) => {
            ev.preventDefault()
            this.setState({ modal_visibility: true, recipient: record })
          }}>
            <ReactImageFallback
              src={record.avatar}
              fallbackImage='/user-avatar/user.svg'
              alt={record.first_name}
              className='img-circle'
              style={{ width: 40, height: 40, marginRight: '10px' }}
            />
            {text}
          </a>
        )
      }
    }, {
      title: 'Age',
      dataIndex: 'age',
      key: 'age'
    }, {
      title: 'Gender',
      dataIndex: 'gender_name',
      key: 'gender'
    }, {
      title: 'Address',
      dataIndex: 'address',
      key: 'address'
    }, {
      title: 'Action',
      key: 'action',
      render: (text, record) => {
        return (
          <span>
            <a href='#' onClick={(ev) => {
              ev.preventDefault()
              this.setState({ new_form_visibility: true, isEdit: true, recipient: record })
            }}>Edit</a>
            <Divider type='vertical' />
            <a href='#'>Delete</a>
          </span>
        )
      }
    }]

    return (
      <div className='row'>
        <div className='col-sm-12'>
          <div className='panel panel-primary'>
            <div className='panel-body'>
              <Button type='primary' onClick={() => {
                this.setState({ new_form_visibility: true, isEdit: false })
              }}><i className='mdi mdi-plus' /> Add Care Recipients</Button>
              <Table columns={columns} dataSource={data_recipients} />

              {
                this.state.new_form_visibility && this.renderFormView()
              }
              {
                this.state.modal_visibility && this.renderRecipientDetailsModal()
              }
            </div>
          </div>
        </div>
      </div>
    )
  }
}

CareRecipients.propTypes = {
}
