import React, { Component, PropTypes } from 'react'
import { Select, Button, TimePicker, Table, Icon, Divider, Spin, message, DatePicker, Popconfirm } from 'antd'
import GoogleAutcompleteInput from 'components/GoogleAutcompleteInput'
import moment from 'moment'
import ReactStars from 'react-stars'
import ReactImageFallback from 'react-image-fallback'
import * as GoogleApi from 'modules/googlemaps/api'
import * as _Api from './../../../modules/api'
import * as Helper from 'consts/helper'

import ProviderDetails from './../../../components/ProviderDetails'
import CareRecipientDetails from './CareRecipientDetails'
import FinishServiceForm from './FinishServiceForm'

export default class CareProviders extends Component {

  constructor (props) {
    super(props)
    this.state = {
      loading: false,
      modal_visibility: false,
      finish_form_visibility: false,
      recipientdetails_modal_visibility: false,
      provider: undefined,
      recipient: undefined,
      reviews: undefined
    }
  }

  componentDidMount () {
    this.props.refreshUser()
  }

  renderProviderDetailsModal = () => {
    return (
      <ProviderDetails
        provider={this.state.provider}
        visibility={this.state.modal_visibility}
        setVisibility={(visibility) => {
          this.setState({ modal_visibility: visibility })
        }}
      />
    )
  }

  renderRecipientDetailsModal = () => {
    return (
      <CareRecipientDetails
        recipient={this.state.recipient}
        visibility={this.state.recipientdetails_modal_visibility}
        setVisibility={(visibility) => {
          this.setState({ recipientdetails_modal_visibility: visibility })
        }}
      />
    )
  }

  renderFinishFormModal = () => {
    const { user } = this.props.auth
    return (
      <FinishServiceForm
        provider={this.state.provider}
        visibility={this.state.finish_form_visibility}
        setVisibility={(visibility) => {
          this.setState({ finish_form_visibility: visibility })
        }}
        setRatingsAndReviews={(reviews) => {
          const params = {
            hiring_id: this.state.provider.hiring_id,
            ratings: reviews.ratings,
            comment: reviews.review
          }
          console.log('params', params)
          _Api.finishService(params).then((response) => {
            this.props.refreshUser()
          })
          .catch((err) => {
            console.log('[err]', err)
          })
        }}
      />
    )
  }

  render () {
    const { user } = this.props.auth

    const data_recipients = user.hired.map((hire, i) => {
      return {
        key: hire.id,
        name: hire.first_name + ' ' + hire.last_name,
        care_recipient_name: hire.recipient.name,
        age: Helper.getAge(hire.birthday),
        gender_name: (hire.gender == 0) ? 'Female' : 'Male',
        address: hire.location.label,
        job_offer_date_formatted: moment(hire.job_offer_date).format('LL'),
        hired_date_formatted: moment(hire.hired_date).format('LL'),
        status: (hire.hiring_status == 0) ? 'Awaiting Reply' : (hire.hiring_status == 1) ? 'Hired' : (hire.hiring_status == 2) ? 'Finished' : 'Canceled',
        ...hire
      }
    })

    const columns = [{
      title: 'Care Recipient',
      dataIndex: 'care_recipient_name',
      key: 'care_recipient_name',
      render: (text, record) => {
        return (
          <a href='#' onClick={(ev) => {
            ev.preventDefault()
            this.setState({ recipientdetails_modal_visibility: true, recipient: record.recipient })
          }}>
            <ReactImageFallback
              src={record.recipient.avatar}
              fallbackImage='/user-avatar/user.svg'
              alt={record.recipient.name}
              className='img-circle'
              style={{ width: 40, height: 40, marginRight: '10px' }}
            />
            {text}
          </a>
        )
      }
    }, {
      title: 'Provider Name',
      dataIndex: 'name',
      key: 'name',
      render: (text, record) => {
        return (
          <a href='#' onClick={(ev) => {
            ev.preventDefault()
            this.setState({ modal_visibility: true, provider: record })
          }}>
            <ReactImageFallback
              src={record.avatar}
              fallbackImage='/user-avatar/user.svg'
              alt={record.first_name}
              className='img-circle'
              style={{ width: 40, height: 40, marginRight: '10px' }}
            />
            {text}
          </a>
        )
      }
    }, {
      title: 'Age',
      dataIndex: 'age',
      key: 'age'
    }, {
      title: 'Gender',
      dataIndex: 'gender_name',
      key: 'gender'
    }, {
      title: 'Address',
      dataIndex: 'address',
      key: 'address'
    }, {
      title: 'Job Offer Date',
      dataIndex: 'job_offer_date_formatted',
      key: 'job_offer_date_formatted'
    }, {
      title: 'Hired Date',
      dataIndex: 'hired_date_formatted',
      key: 'hired_date_formatted'
    }, {
      title: 'Status',
      dataIndex: 'status',
      key: 'status',
      render: (text, record) => {
        console.log('provider', record)
        return (
          <div>
            <span>{text}</span>
            { (record.ratings.length == 0 && record.hiring_status == 1) &&
              <Popconfirm title={`Are you sure do you want to finish it now?`} okText='Yes, I want to finish' cancelText='No' onConfirm={() => {
                this.setState({ finish_form_visibility: true, provider: record, recipient: record.recipient })
              }}>
                <Divider type='vertical' />
                <a href='#'>Finish</a>
              </Popconfirm>
            }
          </div>
        )
      }
    }]

    return (
      <div className='row'>
        <div className='col-sm-12'>
          <div className='panel panel-primary'>
            <div className='panel-body'>
              <Table columns={columns} dataSource={data_recipients} />
              {
                this.state.modal_visibility && this.renderProviderDetailsModal()
              }
              {
                this.state.recipientdetails_modal_visibility && this.renderRecipientDetailsModal()
              }
              {
                this.state.finish_form_visibility && this.renderFinishFormModal()
              }
            </div>
          </div>
        </div>
      </div>
    )
  }
}

CareProviders.propTypes = {
}
