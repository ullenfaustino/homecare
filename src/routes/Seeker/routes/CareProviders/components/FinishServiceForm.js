import React, { Component, PropTypes } from 'react'
import { Link, browserHistory } from 'react-router'
import { Button, Table, Icon, Divider, Spin, message, Modal } from 'antd'
import moment from 'moment'
import ReactStars from 'react-stars'
import ReactImageFallback from 'react-image-fallback'

import * as _Api from './../../../modules/api'
import * as Helper from 'consts/helper'

export default class FinishServiceForm extends Component {

  constructor (props) {
    super(props)
    this.state = {
      loading: false,
      currRate: 0,
      comment: ''
    }
  }

  render () {
    const { visibility, provider } = this.props
    let currentUser = provider
    return (
      <Modal
        destroyOnClose
        title='Finish Provider Service'
        width={1100}
        visible={visibility}
        // footer={null}
        onOk={() => {
          this.setState({ loading: true }, () => {
            if (this.state.comment.trim().length > 0) {
              this.props.setRatingsAndReviews({
                ratings: this.state.currRate,
                review: this.state.comment
              })
              this.props.setVisibility(false)
            } else {
              this.setState({ loading: false }, () => {
                message.error('Please write any comment or review for this provider.')
              })
            }
          })
        }}
        onCancel={() => {
          this.props.setVisibility(false)
        }}
        >
        <div className='row'>
          <div className='col-sm-12'>
            <div className='panel panel-primary'>
              <div className='panel-body'>
                <div className='row'>
                  <div className='col-sm-2'>
                    <ReactImageFallback
                      src={currentUser.avatar}
                      fallbackImage='/user-avatar/user.svg'
                      alt={currentUser.first_name}
                      className='img-circle'
                      style={{ width: 120, height: 120, marginBottom: '20px' }}
                    />
                  </div>
                  <div className='col-sm-10'>
                    <legend>Provider Preferences</legend>
                    <div className='form-group'>
                      <span className='col-md-2'>Name</span>
                      <div className='col-md-10'>
                        <label className='control-label'>{currentUser.name}</label>
                      </div>
                    </div>
                    <div className='form-group'>
                      <span className='col-md-2'>Gender</span>
                      <div className='col-md-10'>
                        <label className='control-label'>{(currentUser.gender == 0) ? 'Female' : 'Male'}</label>
                      </div>
                    </div>
                    <div className='form-group'>
                      <span className='col-md-2'>Age</span>
                      <div className='col-md-10'>
                        <label className='control-label'>{ Helper.getAge(currentUser.birthday) }</label>
                      </div>
                    </div>
                    <div className='form-group'>
                      <span className='col-md-2'>Location</span>
                      <div className='col-md-10'>
                        <label className='control-label'>{currentUser.location.label}</label>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className='col-sm-12'>
            <legend>Ratings And Feedback</legend>
            <div className='row'>
              <div className='col-sm-3'>
                <ReactStars
                  count={5}
                  onChange={(val) => {
                    this.setState({ currRate: val })
                  }}
                  size={45}
                  half={false}
                  value={this.state.currRate}
                  color2={'#ffd700'} />
                <h2>{this.state.currRate} out of 5</h2>
              </div>
              <div className='col-sm-9'>
                <label>Write a review/comment</label>
                <textarea className='form-control' maxLength={2000} rows={6} cols={50} onChange={(e) => {
                  this.setState({ comment: e.target.value })
                }} />
                <small style={{ color: 'red', fontStyle: 'italic' }}>this field is required</small>
              </div>
            </div>
          </div>
        </div>
      </Modal>
    )
  }
}

FinishServiceForm.propTypes = {
}
