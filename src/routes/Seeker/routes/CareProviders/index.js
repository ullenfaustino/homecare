import CareProvidersContainer from './containers/CareProvidersContainer'

export default (store) => ({
  path: '/seeker/care-providers',
  component : CareProvidersContainer
})
