import React, { Component } from 'react'
import { connect } from 'react-redux'
import {
  actions
} from '../../../modules/seeker'
import { refreshUser } from 'modules/auth'
import CareProviders from '../components/CareProviders'

class CareProvidersContainer extends Component {

  render () {
    let { children, params, location } = this.props

    if (children) {
      return this.props.children
    } else {
      // content =
      return (
        <div className='content'>
          <div className=''>
            <div className='page-header-title'>
              <h4 className='page-title'><i className='mdi mdi-account-multiple' /> Recent Job Offer(s)</h4>
            </div>
          </div>
          <div className='page-content-wrapper '>
            <div className='container'>
              <CareProviders {...this.props} />
            </div>
          </div>
        </div>
      )
    }
  }
}

const mapDispatchToProps = { ...actions, refreshUser }

const mapStateToProps = (state) => ({ ...state })

export default connect(mapStateToProps, mapDispatchToProps)(CareProvidersContainer)
