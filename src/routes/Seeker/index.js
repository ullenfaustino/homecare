import { injectReducer } from '../../store/reducers'
import { injectSagas } from '../../store/sagas'

import SeekerContainer from './containers/SeekerContainer'

export default (store) => ({
  path : '/seeker',
  /*  Async getComponent is only invoked when route matches   */
  getComponent (nextState, cb) {
    /*  Webpack - use 'require.ensure' to create a split point
        and embed an async module loader (jsonp) when bundling   */
    require.ensure([], (require) => {
      /*  Webpack - use require callback to define
          dependencies for bundling   */
      const Seeker = require('./containers/SeekerContainer').default
      const Breadcrumb = require('./components/Breadcrumb').default
      const reducer = require('./modules/seeker').default
      // const sagas = require('./modules/category').sagas
      //
      // /*  Add the reducer to the store on key 'counter'  */
      injectReducer(store, { key: 'seeker', reducer })
      // injectSagas(store, { key: 'vehicle', sagas })
      /*  Return getComponent   */
      cb(null, Seeker)

    /* Webpack named bundle   */
    }, 'seeker')
  },
  onEnter : (nextState, replace) => checkAuth(nextState, replace, store),
  onChange : (prevState, nextState, replace) => checkAuth(nextState, replace, store),
  getChildRoutes (location, next) {
    require.ensure([], (require) => {
      next(null, [
        // {
        //   path: ':id/view',
        //   component: VehicleViewContainer
        // },
        require('./routes/Preferences').default(store),
        require('./routes/CareRecipients').default(store),
        require('./routes/SearchProvider').default(store),
        require('./routes/CareProviders').default(store),
        require('./routes/JobPost').default(store)
        // require('./routes/VehicleNew/edit').default(store),
        // require('./routes/VehicleRegistration').default(store)
      ])
    })
  }
})

function checkAuth (nextState, replace, store) {
  let { auth, location } = store.getState()
  let { loggedIn, user } = auth

  const pName = nextState.location.pathname.split('/')

  if (user.user_type == 0) {
    if (pName[1] !== 'admin') {
      replace('/admin')
    }
  } else if (user.user_type == 1) {
    if (pName[1] !== 'provider') {
      replace('/provider')
    }
  } else if (user.user_type == 2) {
    if (pName[1] !== 'seeker') {
      replace('/seeker')
    }
  }
}
