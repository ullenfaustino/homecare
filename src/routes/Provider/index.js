import { injectReducer } from '../../store/reducers'
import { injectSagas } from '../../store/sagas'

import ProviderContainer from './containers/ProviderContainer'

export default (store) => ({
  path : '/provider',
  /*  Async getComponent is only invoked when route matches   */
  getComponent (nextState, cb) {
    /*  Webpack - use 'require.ensure' to create a split point
        and embed an async module loader (jsonp) when bundling   */
    require.ensure([], (require) => {
      /*  Webpack - use require callback to define
          dependencies for bundling   */
      const Provider = require('./containers/ProviderContainer').default
      const Breadcrumb = require('./components/Breadcrumb').default
      const reducer = require('./modules/provider').default
      // const sagas = require('./modules/category').sagas
      //
      // /*  Add the reducer to the store on key 'counter'  */
      injectReducer(store, { key: 'provider', reducer })
      // injectSagas(store, { key: 'vehicle', sagas })
      /*  Return getComponent   */
      cb(null, Provider)

    /* Webpack named bundle   */
    }, 'provider')
  },
  onEnter : (nextState, replace) => checkAuth(nextState, replace, store),
  onChange : (prevState, nextState, replace) => checkAuth(nextState, replace, store),
  getChildRoutes (location, next) {
    require.ensure([], (require) => {
      next(null, [
        // {
        //   path: ':id/view',
        //   component: VehicleViewContainer
        // },
        require('./routes/Preferences').default(store),
        require('./routes/ApplicationStatus').default(store),
        require('./routes/Clients').default(store),
        require('./routes/CareSeekers').default(store)
      ])
    })
  }
})

function checkAuth (nextState, replace, store) {
  let { auth, location } = store.getState()
  let { loggedIn, user } = auth

  const pName = nextState.location.pathname.split('/')

  if (user.user_type == 0) {
    if (pName[1] !== 'admin') {
      replace('/admin')
    }
  } else if (user.user_type == 1) {
    if (pName[1] !== 'provider') {
      replace('/provider')
    }
  } else if (user.user_type == 2) {
    if (pName[1] !== 'seeker') {
      replace('/seeker')
    }
  }
}
