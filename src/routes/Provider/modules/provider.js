import { takeLatest } from 'redux-saga'
import { take, put, select, call, fork } from 'redux-saga/effects'
import * as Api from './api'
import _ from 'lodash'

// ------------------------------------
// Constants
// ------------------------------------
const PAGINATION_DEF = {
  page: 1,
  pageSize: 10,
  searchText: '',
  sortName: undefined,
  sortOrder: undefined
}

export const VEHICLE_FETCH_SUCCEEDED = 'VEHICLE_FETCH_SUCCEEDED'

// export function fetchVehicles (data) {
//   return (dispatch, getState) => {
//     const { vehicle } = getState()
//     const params = Object.assign({}, vehicle.pagination, data)
//     return Api.fetchVehicles(params).then((payload) => {
//       const { data: vehicles, pagination } = payload
//       const __pagination = Object.assign({}, params, pagination)
//       dispatch({
//         type: VEHICLE_FETCH_SUCCEEDED,
//         pagination: __pagination,
//         vehicles
//         // recordsTotal,
//         // recordsFiltered
//       })
//       return payload
//     })
//   }
// }

export const actions = {
  // fetchVehicles
}

// ------------------------------------
// Action Handlers
// ------------------------------------
const ACTION_HANDLERS = {
  [VEHICLE_FETCH_SUCCEEDED] : (state, action) => Object.assign({}, state, {
    vehicles: action.vehicles,
    pagination: { ...state.pagination, ...action.pagination }
  })
}

// ------------------------------------
// Reducer
// ------------------------------------
const initialState = {
  pagination: PAGINATION_DEF,

  page: 1,
  pageSize: 10,
  searchText: '',
  sortName: undefined,
  sortOrder: undefined
}

export default function Reducer (state = initialState, action) {
  const handler = ACTION_HANDLERS[action.type]
  return handler ? handler(state, action) : state
}

// ------------------------------------
// Sagas
// ------------------------------------
export const sagas = []
