import axios from 'axios'

import { URL } from 'consts/apiConsts'

function parseJSON (response) {
  return response.data.data || response.data
}

// export function fetchVehicles (params) {
//   return axios.get(`${URL}/user`, {
//     params: params
//   }).then((response) => response.data)
// }

export function findByUid (uid) {
  return axios.get(`${URL}/user/${uid}`).then(parseJSON)
}

export function addPreferences (data) {
  return axios.post(`${URL}/provider/add/preferences`, data).then(parseJSON)
}

export function addBasicInfo (data) {
  return axios.post(`${URL}/provider/add/basicInfo`, data).then(parseJSON)
}

export function addReferencesExperience (data) {
  return axios.post(`${URL}/provider/add/referenceExperience`, data).then(parseJSON)
}

export function removeFile (data) {
  return axios.post(`${URL}/uploads/delete/file`, data).then(parseJSON)
}

export function updateStatusForApproval (data) {
  return axios.post(`${URL}/provider/status/for-approval`, data).then(parseJSON)
}

export function acceptJobOffer (data) {
  return axios.post(`${URL}/provider/accept-job-offer`, data).then(parseJSON)
}

export function findNearJob (data) {
  return axios.post(`${URL}/provider/find-near-job`, data).then(parseJSON)
}

export function applyOnJobPost (data) {
  return axios.post(`${URL}/provider/apply-job-post`, data).then(parseJSON)
}
