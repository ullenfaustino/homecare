import React, { Component, PropTypes } from 'react'
import { Link } from 'react-router'
import { Button, ButtonToolbar, Nav, NavItem, NavDropdown, MenuItem } from 'react-bootstrap'

export default class Breadcrumb extends Component {
  render () {
    return (
      <div className='row wrapper border-bottom white-bg page-heading'>
        <div className='col-md-8'>
          <h2>Vehicle List</h2>
        </div>
        <div className='col-md-4'>
          <div className='pull-right' style={{ padding: 10 }}>
            <Button bsStyle='default'>Some Actions</Button>
            { ' ' }
            <Link to={`/vehicles/new`}><Button bsStyle='primary'>Add Vehicle</Button></Link>
          </div>
        </div>
      </div>
    )
  }
}

Breadcrumb.propTypes = {}
