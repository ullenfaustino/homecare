import React, { Component, PropTypes } from 'react'
import { Input, Select, Button, TimePicker, Table, Icon, Divider, Spin, message } from 'antd'
import * as _Api from './../../../modules/api'

export default class BasicInfo extends Component {

  constructor (props) {
    super(props)
    this.state = {
      loading: false,
      isFetchingUserData: true,

      transportations: [],
      authorized_to_work: false,
      registered_homecare: false,
      languages: [],
      registry_id: undefined,
      salary_rate: {
        hourly: undefined,
        overnight: undefined,
        daily: undefined
      }
    }
  }

  componentWillMount () {
    this.fetchData()
  }

  fetchData = () => {
    // console.log('[componentWillMount][this-props]', this.props)
    _Api.findByUid(this.props.auth.user.uid)
    .then((mUser) => {
      // console.log('[CurrentUser]', mUser)
      this.setState({
        isFetchingUserData: false
      }, this.parseUserData(mUser))
    }).catch((err) => {
      console.log('err', err)
      this.setState({
        isFetchingUserData: false
      })
    })
  }

  parseUserData = (user) => {
    const formattedTransportations = user.transportations.map((transportation) => {
      return transportation.lkup_transportations_id.toString()
    })
    const formattedLanguages = user.languages.map((language) => {
      return language.lkup_languages_id.toString()
    })

    this.setState({
      authorized_to_work: user.is_authorized_to_work.toString(),
      registered_homecare: user.is_registered_homecare.toString(),
      registry_id: user.registry_id,
      salary_rate: {
        hourly: user.salary_rate.hourly_rate,
        overnight: user.salary_rate.overnight_rate,
        daily: user.salary_rate.daily_rate
      },
      transportations: formattedTransportations,
      languages: formattedLanguages
    }, () => {
      const isValid = ((formattedTransportations.length > 0) &&
                      (formattedLanguages.length > 0))
      this.props.updateStatus(isValid)
      this.props.status(!isValid)
    })
  }

  handleOnSubmit = () => {
    // console.log('[handleOnSubmit][this-state]', this.state)

    const params = {
      user: {
        uid: this.props.auth.user.uid,
        id: this.props.auth.user.id
      },
      salary_rate: {
        hourly_rate: this.state.salary_rate.hourly,
        overnight_rate: this.state.salary_rate.overnight,
        daily_rate: this.state.salary_rate.daily
      },
      transportations: this.state.transportations,
      authorized_to_work: this.state.authorized_to_work,
      registered_homecare: this.state.registered_homecare,
      registry_id: this.state.registry_id,
      languages: this.state.languages
    }

    // console.log('[post-params]', params)
    _Api.addBasicInfo(params).then((response) => {
      // console.log(response)
      this.setState({
        loading: false
      }, () => {
        this.fetchData()
        message.success('Successfully saved')
      })
    }).catch((err) => {
      console.log(err)
      this.setState({
        loading: false
      }, message.error('Error saving basic info'))
    })
  }

  render () {
    // console.log('[render][this-state]', this.state)

    const { languages, transportations } = this.props.app.settings

    if (this.state.isFetchingUserData) {
      return (
        <Spin />
      )
    }

    return (
      <div className='row'>
        <div className='col-sm-12'>
          <div className='panel panel-primary'>
            <div className='panel-body'>
              {/* <h4 className='m-t-0 m-b-30'>My Qualifications <br /><small>Basic Information</small></h4> */}
              <legend>Basic Information <span className='pull-right' style={{ fontSize: '12px', color: 'red', marginTop: '10px' }}>(required)</span></legend>
              <form className='form-horizontal' role='form' onSubmit={(ev) => {
                ev.preventDefault()
                this.setState({
                  loading: true
                }, this.handleOnSubmit())
              }}>
                <div className='form-group'>
                  <label className='col-md-2 control-label' htmlFor='example-email'>Means of transportation</label>
                  <div className='col-md-10'>
                    <Select
                      mode='multiple'
                      style={{ width: '100%' }}
                      placeholder='Select means of transportation'
                      defaultValue={this.state.transportations}
                      optionFilterProp='children'
                      filterOption={(input, option) => option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
                      onChange={(values) => {
                        this.setState({
                          transportations : values
                        })
                      }}
                    >
                      {
                        transportations.map((item) => {
                          return (
                            <Select.Option key={item.id}>{item.name}</Select.Option>
                          )
                        })
                      }
                    </Select>
                    <small>(you may select more than one)</small>
                  </div>
                </div>
                <div className='form-group'>
                  <label className='col-md-2 control-label' htmlFor='example-email'>Salary Rate</label>
                  <div className='col-md-3'>
                    <input type='number' className='form-control' required placeholder='Hourly Rate' value={this.state.salary_rate.hourly} onChange={(ev) => {
                      this.setState({
                        salary_rate: Object.assign({}, this.state.salary_rate, {
                          hourly: ev.target.value
                        })
                      })
                    }} />
                  </div>
                  <div className='col-md-3'>
                    <input type='number' className='form-control' required placeholder='Overnight Rate' value={this.state.salary_rate.overnight} onChange={(ev) => {
                      this.setState({
                        salary_rate: Object.assign({}, this.state.salary_rate, {
                          overnight: ev.target.value
                        })
                      })
                    }} />
                  </div>
                  <div className='col-md-3'>
                    <input type='number' className='form-control' required placeholder='Daily Rate' value={this.state.salary_rate.daily} onChange={(ev) => {
                      this.setState({
                        salary_rate: Object.assign({}, this.state.salary_rate, {
                          daily: ev.target.value
                        })
                      })
                    }} />
                  </div>
                </div>
                <div className='form-group'>
                  <label className='col-md-3 control-label' htmlFor='example-email'>Are you legally authorized to work in the US?</label>
                  <div className='col-md-3'>
                    <Select
                      mode='dropdown'
                      style={{ width: '100%' }}
                      placeholder='Yes or No'
                      defaultValue={this.state.authorized_to_work}
                      onChange={(values) => {
                        this.setState({
                          authorized_to_work : values
                        })
                      }}
                    >
                      <Select.Option key={1}>Yes</Select.Option>
                      <Select.Option key={0}>No</Select.Option>
                    </Select>
                  </div>
                </div>
                <div className='form-group'>
                  <label className='col-md-3 control-label' htmlFor='example-email'>Are you registered on the Home Care Aide Registry?</label>
                  <div className='col-md-3'>
                    <Select
                      mode='dropdown'
                      style={{ width: '100%' }}
                      placeholder='Yes or No'
                      defaultValue={this.state.registered_homecare}
                      onChange={(values) => {
                        this.setState({
                          registered_homecare : values
                        })
                      }}
                    >
                      <Select.Option key={1}>Yes</Select.Option>
                      <Select.Option key={0}>No</Select.Option>
                    </Select>
                  </div>
                </div>
                <div className='form-group'>
                  <label className='col-md-3 control-label' htmlFor='example-email'>Registry ID Number</label>
                  <div className='col-md-4'>
                    <input type='text' className='form-control' placeholder='registry number' value={this.state.registry_id} onChange={(ev) => {
                      this.setState({
                        registry_id : ev.target.value
                      })
                    }} />
                    <small>(you can leave this blank)</small>
                  </div>
                </div>
                <div className='form-group'>
                  <label className='col-md-3 control-label' htmlFor='example-email'>Other than English, what other language(s) do you speak?</label>
                  <div className='col-md-9'>
                    <Select
                      showSearch
                      mode='multiple'
                      style={{ width: '100%' }}
                      placeholder='Select language'
                      defaultValue={this.state.languages}
                      optionFilterProp='children'
                      filterOption={(input, option) => option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
                      onChange={(values) => {
                        this.setState({
                          languages : values
                        })
                      }}
                    >
                      {
                        languages.map((item) => {
                          return (
                            <Select.Option key={item.id} >{item.name}</Select.Option>
                          )
                        })
                      }
                    </Select>
                  </div>
                </div>
                <hr />
                <div className='form-group'>
                  <label className='col-md-2 control-label' htmlFor='example-email' />
                  <div className='col-md-10' >
                    <Button type='primary' className='pull-right' id={321} loading={this.state.loading} htmlType='submit'>
                      <i className='mdi mdi-content-save' /> Save Information
                    </Button>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    )
  }
}

BasicInfo.propTypes = {
}
