import React, { Component, PropTypes } from 'react'
import { Input, Select, Button, Icon, Spin, Upload, message } from 'antd'
import * as _Api from './../../../modules/api'
import { URL } from 'consts/apiConsts'

export default class Requirements extends Component {

  constructor (props) {
    super(props)
    this.state = {
      loading: false,
      isFetchingUserData: true,
      currentUser: {}
    }
  }

  componentWillMount () {
    // console.log('[componentWillMount][this-props]', this.props)
    _Api.findByUid(this.props.auth.user.uid)
    .then((mUser) => {
      // console.log('[CurrentUser]', mUser)
      this.setState({
        isFetchingUserData: false,
        currentUser: mUser
      }, this.parseUserData(mUser))
    }).catch((err) => {
      console.log('err', err)
      this.setState({
        isFetchingUserData: false
      })
    })
  }

  parseUserData = (user) => {
    // console.log('[parseUserData][user]', user)
  }

  uploadHandler = (fileType) => {
    const { currentUser } = this.state

    const _filtered_defaultFiles = currentUser.files.filter((file) => file.file.file_type === fileType)
    const _defaultFiles = _filtered_defaultFiles.map((file) => {
      if (file.file.file_type === fileType) {
        return {
          uid: file.file.id,
          name: file.file.name,
          status: 'done',
          url: file.file.url
        }
      }
    })

    const actionURL = `${URL}/uploads/file?userId=${this.props.auth.user.id}&fileType=${fileType}`
    // console.log('actionURL', actionURL)
    return {
      name: 'file',
      action: actionURL,
      supportServerRender: true,
      defaultFileList: _defaultFiles,
      onChange (info) {
        console.log('info', info)
        if (info.file.status !== 'uploading') {
          console.log(info.file, info.fileList)
        }
        if (info.file.status === 'done') {
          message.success(`${info.file.name} file uploaded successfully`)
        } else if (info.file.status === 'error') {
          message.error(`${info.file.name} file upload failed.`)
        }
      },
      onRemove: (file) => {
        this.handleRemoveFile(file)
      }
    }
  }

  handleRemoveFile = (file) => {
    const params = {
      user: {
        uid: this.props.auth.user.uid,
        id: this.props.auth.user.id
      },
      name: file.name
    }
    _Api.removeFile(params).then((response) => {
      console.log(response)
      message.success('File successfully removed')
    }).catch((err) => {
      console.log(err)
      message.error('Error deleting file')
    })
  }

  render () {
    if (this.state.isFetchingUserData) {
      return (
        <Spin />
      )
    }

    return (
      <div className='row'>
        <div className='col-sm-12'>
          <div className='panel panel-primary'>
            <div className='panel-body'>
              <legend>Requirements <span className='pull-right' style={{ fontSize: '12px', color: 'red', marginTop: '10px' }}>(required)</span></legend>
              <div className='form-group col-md-12'>
                <label className='col-md-4 control-label' htmlFor='example-email'>Valid Driver's License</label>
                <div className='col-md-8'>
                  <Upload {...this.uploadHandler('drivers_license')}>
                    <Button>
                      <Icon type='upload' /> Click to Upload
                    </Button>
                  </Upload>
                </div>
              </div>
              <div className='form-group col-md-12'>
                <label className='col-md-4 control-label' htmlFor='example-email'>Valid ID by the DMV</label>
                <div className='col-md-8'>
                  <Upload {...this.uploadHandler('dmv_id')}>
                    <Button>
                      <Icon type='upload' /> Click to Upload
                    </Button>
                  </Upload>
                </div>
              </div>
              <div className='form-group col-md-12'>
                <label className='col-md-4 control-label' htmlFor='example-email'>Valid Alien Registration Card</label>
                <div className='col-md-8'>
                  <Upload {...this.uploadHandler('alien_registration_card')}>
                    <Button>
                      <Icon type='upload' /> Click to Upload
                    </Button>
                  </Upload>
                </div>
              </div>
              <div className='form-group col-md-12'>
                <label className='col-md-4 control-label' htmlFor='example-email'>Valid Numbered ID</label>
                <div className='col-md-8'>
                  <Upload {...this.uploadHandler('numbered_id')}>
                    <Button>
                      <Icon type='upload' /> Click to Upload
                    </Button>
                  </Upload>
                  <small>(if living outside CA)</small>
                </div>
              </div>
              <div className='form-group col-md-12'>
                <label className='col-md-4 control-label' htmlFor='example-email'>Criminal Record Clearance</label>
                <div className='col-md-8'>
                  <Upload {...this.uploadHandler('criminal_record_clearance')}>
                    <Button>
                      <Icon type='upload' /> Click to Upload
                    </Button>
                  </Upload>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    )
  }
}

Requirements.propTypes = {
}
