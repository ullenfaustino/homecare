import React, { Component, PropTypes } from 'react'
import { Select, Button, TimePicker, Table, Icon, Divider, Spin, message } from 'antd'
import GoogleAutcompleteInput from 'components/GoogleAutcompleteInput'
import * as GoogleApi from 'modules/googlemaps/api'
import * as _Api from './../../../modules/api'

import {
  // default_WorkRestrictions,
  // default_SchedulePreferences,
  // default_ClientPreferences
  days,
  availability_columns
} from 'consts/provider'

export default class Preferences extends Component {

  constructor (props) {
    super(props)
    this.state = {
      loading: false,
      isFetchingUserData: true,

      selectedLocation: {},
      client_preferences : [],
      work_restrictions: [],
      work_schedules: [],
      availability: [],
      currSelectedSchedule: {
        day: undefined,
        start_time: undefined,
        end_time: undefined
      }
    }
  }

  componentWillMount () {
    this.fetchData()
  }

  fetchData = () => {
    // console.log('[this-props]', this.props)
    _Api.findByUid(this.props.auth.user.uid)
    .then((mUser) => {
      // console.log('[CurrentUser]', mUser)
      this.setState({
        isFetchingUserData: false
      }, this.parseUserData(mUser))
    }).catch((err) => {
      console.log('err', err)
      this.setState({
        isFetchingUserData: false
      })
    })
  }

  parseUserData = (user) => {
    const formattedSelectedLocation = {
      place_id: user.location.place_id,
      id: user.location.google_place_id,
      description: user.location.description,
      label: user.location.label,
      reference: user.location.reference
    }
    const formattedClientPreferences = user.client_preferences.map((client_preference) => {
      return client_preference.lkup_client_preferences_id.toString()
    })
    const formattedWorkRestrictions = user.work_restrictions.map((work_restriction) => {
      return work_restriction.lkup_work_restrictions_id.toString()
    })
    const formattedWorkSchedules = user.work_schedules.map((work_schedule) => {
      return work_schedule.lkup_work_schedules_id.toString()
    })
    const formattedAvailability = user.availability.map((_availability) => {
      return _.pick(_availability, ['day', 'start_time', 'end_time'])
    })

    this.setState({
      selectedLocation: formattedSelectedLocation,
      client_preferences: formattedClientPreferences,
      work_restrictions: formattedWorkRestrictions,
      work_schedules: formattedWorkSchedules,
      availability: formattedAvailability
    }, () => {
      const isValid = (_.has(formattedSelectedLocation, 'id') &&
                      (formattedClientPreferences.length > 0) &&
                      (formattedWorkRestrictions.length > 0) &&
                      (formattedWorkSchedules.length > 0) &&
                      (formattedAvailability.length > 0))
      this.props.updateStatus(isValid)
      this.props.status(!isValid)
    })
  }

  handleOnSubmitPreferences = () => {
    // console.log('[this-state]', this.state)
    const params = {
      user: {
        uid: this.props.auth.user.uid,
        id: this.props.auth.user.id
      },
      location: {
        place_id: this.state.selectedLocation.place_id,
        google_place_id: this.state.selectedLocation.id,
        description: this.state.selectedLocation.description,
        label: this.state.selectedLocation.label,
        reference: this.state.selectedLocation.reference
      },
      client_preferences: this.state.client_preferences,
      work_restrictions: this.state.work_restrictions,
      work_schedules: this.state.work_schedules,
      availability: this.state.availability
    }
    console.log('[post-params]', params)
    _Api.addPreferences(params).then((response) => {
      console.log(response)
      this.setState({
        loading: false
      }, () => {
        this.fetchData()
        message.success('Successfully saved')
      })
    }).catch((err) => {
      console.log(err)
      this.setState({
        loading: false
      }, message.error('Error saving preferences'))
    })
  }

  render () {
    // console.log('[this-state]', this.state)
    const { client_preferences, work_restrictions, work_schedules } = this.props.app.settings

    if (this.state.isFetchingUserData) {
      return (
        <Spin />
      )
    }

    return (
      <div className='row'>
        <div className='col-sm-12'>
          <div className='panel panel-primary'>
            <div className='panel-body'>
              {/* <h4 className='m-t-0 m-b-30'>My Information <br /><small>Preferences</small></h4> */}
              <legend>Preferences <span className='pull-right' style={{ fontSize: '12px', color: 'red', marginTop: '10px' }}>(required)</span></legend>
              <form className='form-horizontal' role='form' onSubmit={(ev) => {
                ev.preventDefault()
                this.setState({
                  loading: true
                }, this.handleOnSubmitPreferences())
              }}>
                <div className='form-group'>
                  <label className='col-md-2 control-label'>Location</label>
                  <div className='col-md-10'>
                    <GoogleAutcompleteInput autoComplete={(inputText) => {
                      return GoogleApi.autoComplete({
                        q: inputText
                      })
                    }} onChange={(value) => {
                      this.setState({
                        selectedLocation: value
                      })
                    }} value={this.state.selectedLocation} />
                  </div>
                </div>
                <div className='form-group'>
                  <label className='col-md-2 control-label' htmlFor='example-email'>Client Preferences</label>
                  <div className='col-md-10'>
                    <Select
                      mode='multiple'
                      style={{ width: '100%' }}
                      placeholder='Select Client Preferences'
                      defaultValue={this.state.client_preferences}
                      onChange={(values) => {
                        this.setState({
                          client_preferences : values
                        })
                      }}
                      >
                      {
                        client_preferences.map((item) => {
                          return (
                            <Select.Option key={item.id}>{item.name}</Select.Option>
                          )
                        })
                      }
                    </Select>
                    <small>(you may select more than one)</small>
                  </div>
                </div>
                <div className='form-group'>
                  <label className='col-md-2 control-label' htmlFor='example-email'>Work Restrictions</label>
                  <div className='col-md-10'>
                    <Select
                      mode='tags'
                      style={{ width: '100%' }}
                      placeholder='Select Work Restrictions'
                      defaultValue={this.state.work_restrictions}
                      onChange={(values) => {
                        this.setState({
                          work_restrictions : values
                        })
                      }}
                      >
                      {
                        work_restrictions.map((item) => {
                          return (
                            <Select.Option key={item.id}>{item.name}</Select.Option>
                          )
                        })
                      }
                    </Select>
                    <small>(you may select more than one)</small>
                  </div>
                </div>
                <div className='form-group'>
                  <label className='col-md-2 control-label' htmlFor='example-email'>Work Schedule Preferences</label>
                  <div className='col-md-10'>
                    <Select
                      mode='tags'
                      style={{ width: '100%' }}
                      placeholder='Select Work Preferences'
                      defaultValue={this.state.work_schedules}
                      onChange={(values) => {
                        this.setState({
                          work_schedules : values
                        })
                      }}
                      >
                      {
                        work_schedules.map((item) => {
                          return (
                            <Select.Option key={item.id}>{item.name}</Select.Option>
                          )
                        })
                      }
                    </Select>
                    <small>(you may select more than one)</small>
                  </div>
                </div>
                <div className='form-group'>
                  <label className='col-md-2 control-label' htmlFor='example-email'>Availability</label>
                  <div className='col-md-4'>
                    <Select
                      showSearch
                      placeholder='Select Day'
                      optionFilterProp='children'
                      onChange={(value) => {
                        this.setState({
                          currSelectedSchedule: Object.assign({}, this.state.currSelectedSchedule, {
                            day: value
                          })
                        })
                      }}
                      filterOption={(input, option) => option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
                    >
                      {
                          days.map((item) => {
                            return (
                              <Select.Option key={item.id} value={item.value}>{item.value}</Select.Option>
                            )
                          })
                        }
                    </Select>
                  </div>
                  <div className='col-md-2' >
                    <TimePicker
                      use12Hours
                      style={{ width: '100%', marginTop: '4px' }}
                      format='h:mm a'
                      placeholder='Start Time'
                      onChange={(time, timeString) => {
                        this.setState({
                          currSelectedSchedule: Object.assign({}, this.state.currSelectedSchedule, {
                            start_time: timeString
                          })
                        })
                      }}
                    />
                  </div>
                  <div className='col-md-2' >
                    <TimePicker
                      use12Hours
                      style={{ width: '100%', marginTop: '4px' }}
                      format='h:mm a'
                      placeholder='End Time'
                      onChange={(time, timeString) => {
                        this.setState({
                          currSelectedSchedule: Object.assign({}, this.state.currSelectedSchedule, {
                            end_time: timeString
                          })
                        })
                      }}
                    />
                  </div>
                  <div className='col-md-2' >
                    <Button type='primary' id={123} className='btn-block' style={{ marginTop: '4px' }} onClick={(e) => {
                      if (this.state.currSelectedSchedule.day !== undefined && this.state.currSelectedSchedule.start_time !== undefined && this.state.currSelectedSchedule.end_time !== undefined) {
                        this.state.availability.push(this.state.currSelectedSchedule)
                        this.setState({
                          availability: this.state.availability
                        })
                      }
                    }}>
                      <i className='mdi mdi-playlist-plus' /> Add
                    </Button>
                  </div>
                </div>
                <div className='form-group'>
                  <label className='col-md-2 control-label' htmlFor='example-email' />
                  <div className='col-md-10' >
                    <Table columns={availability_columns} dataSource={this.state.availability} />
                  </div>
                </div>
                <hr />
                <div className='form-group'>
                  <label className='col-md-2 control-label' htmlFor='example-email' />
                  <div className='col-md-10' >
                    <Button type='primary' className='pull-right' id={321} loading={this.state.loading} htmlType='submit'>
                      <i className='mdi mdi-content-save' /> Save Information
                    </Button>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    )
  }
}

Preferences.propTypes = {
}
