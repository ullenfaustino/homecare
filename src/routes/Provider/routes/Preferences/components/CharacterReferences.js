import React, { Component, PropTypes } from 'react'
import { Input, Select, Button, TimePicker, Table, Icon, Divider, Spin, message } from 'antd'
import * as _Api from './../../../modules/api'

export default class CharacterReferences extends Component {

  constructor (props) {
    super(props)
    this.state = {
      loading: false,
      isFetchingUserData: true,

      character_references: {
        reference_name_1: undefined,
        reference_contact_1: undefined,
        reference_name_2: undefined,
        reference_contact_2: undefined,
        reference_name_3: undefined,
        reference_contact_3: undefined
      },

      services_performing: [],
      services_experience: [],
      health_conditions: []
    }
  }

  componentWillMount () {
    // console.log('[componentWillMount][this-props]', this.props)
    _Api.findByUid(this.props.auth.user.uid)
    .then((mUser) => {
      // console.log('[CurrentUser]', mUser)
      this.setState({
        isFetchingUserData: false
      }, this.parseUserData(mUser))
    }).catch((err) => {
      console.log('err', err)
      this.setState({
        isFetchingUserData: false
      })
    })
  }

  parseUserData = (user) => {
    const formattedServicesPerform = user.services_perform.map((item) => {
      return item.lkup_services_perform_id.toString()
    })
    const formattedServicesExperience = user.services_experience.map((item) => {
      return item.lkup_services_experience_id.toString()
    })
    const formattedHealthCondition = user.health_conditions.map((item) => {
      return item.lkup_health_conditions_id.toString()
    })

    this.setState({
      character_references: {
        reference_name_1: user.character_references.reference_name_1,
        reference_contact_1: user.character_references.reference_contact_1,
        reference_name_2: user.character_references.reference_name_2,
        reference_contact_2: user.character_references.reference_contact_2,
        reference_name_3: user.character_references.reference_name_3,
        reference_contact_3: user.character_references.reference_contact_3
      },
      services_performing: formattedServicesPerform,
      services_experience: formattedServicesExperience,
      health_conditions: formattedHealthCondition
    })
  }

  handleOnSubmit = () => {
    // console.log('[handleOnSubmit][this-state]', this.state)

    const params = {
      user: {
        uid: this.props.auth.user.uid,
        id: this.props.auth.user.id
      },
      character_references: this.state.character_references,
      services_perform: this.state.services_performing,
      services_experience: this.state.services_experience,
      health_conditions: this.state.health_conditions
    }

    console.log('[post-params]', params)
    _Api.addReferencesExperience(params).then((response) => {
      // console.log(response)
      this.setState({
        loading: false
      }, message.success('Successfully saved'))
    }).catch((err) => {
      console.log(err)
      this.setState({
        loading: false
      }, message.error('Error saving data'))
    })
  }

  render () {
    // console.log('[render][this-state]', this.state)

    const { health_conditions, services_experience, services_perform } = this.props.app.settings

    if (this.state.isFetchingUserData) {
      return (
        <Spin />
      )
    }

    return (
      <div className='row'>
        <div className='col-sm-12'>
          <div className='panel panel-primary'>
            <div className='panel-body'>
              {/* <h4 className='m-t-0 m-b-30'>My character references</h4> */}
              <legend>My character references <span className='pull-right' style={{ fontSize: '12px', color: 'red', marginTop: '10px' }}>(required)</span></legend>
              <form className='form-horizontal' role='form' onSubmit={(ev) => {
                ev.preventDefault()
                this.setState({
                  loading: true
                }, this.handleOnSubmit())
              }}>
                <div className='form-group'>
                  <label className='col-md-2 control-label' htmlFor='example-email'>Reference #1</label>
                  <div className='col-md-5'>
                    <input type='text' className='form-control' placeholder='name' value={this.state.character_references.reference_name_1} required onChange={(ev) => {
                      this.setState({
                        character_references: Object.assign({}, this.state.character_references, {
                          reference_name_1: ev.target.value
                        })
                      })
                    }} />
                  </div>
                  <div className='col-md-5'>
                    <input type='text' className='form-control' placeholder='contact' value={this.state.character_references.reference_contact_1} required onChange={(ev) => {
                      this.setState({
                        character_references: Object.assign({}, this.state.character_references, {
                          reference_contact_1: ev.target.value
                        })
                      })
                    }} />
                  </div>
                </div>
                <div className='form-group'>
                  <label className='col-md-2 control-label' htmlFor='example-email'>Reference #2</label>
                  <div className='col-md-5'>
                    <input type='text' className='form-control' placeholder='name' value={this.state.character_references.reference_name_2} required onChange={(ev) => {
                      this.setState({
                        character_references: Object.assign({}, this.state.character_references, {
                          reference_name_2: ev.target.value
                        })
                      })
                    }} />
                  </div>
                  <div className='col-md-5'>
                    <input type='text' className='form-control' placeholder='contact' value={this.state.character_references.reference_contact_2} required onChange={(ev) => {
                      this.setState({
                        character_references: Object.assign({}, this.state.character_references, {
                          reference_contact_2: ev.target.value
                        })
                      })
                    }} />
                  </div>
                </div>
                <div className='form-group'>
                  <label className='col-md-2 control-label' htmlFor='example-email'>Reference #3</label>
                  <div className='col-md-5'>
                    <input type='text' className='form-control' placeholder='name' value={this.state.character_references.reference_name_3} required onChange={(ev) => {
                      this.setState({
                        character_references: Object.assign({}, this.state.character_references, {
                          reference_name_3: ev.target.value
                        })
                      })
                    }} />
                  </div>
                  <div className='col-md-5'>
                    <input type='text' className='form-control' placeholder='contact' value={this.state.character_references.reference_contact_3} required onChange={(ev) => {
                      this.setState({
                        character_references: Object.assign({}, this.state.character_references, {
                          reference_contact_3: ev.target.value
                        })
                      })
                    }} />
                  </div>
                </div>

                <legend>My experiences <span className='pull-right' style={{ fontSize: '12px', color: 'gray', marginTop: '10px' }}>(optional)</span></legend>

                <div className='form-group'>
                  <label className='col-md-4 control-label' htmlFor='example-email'>Select the services you are performing</label>
                  <div className='col-md-8'>
                    <Select
                      mode='multiple'
                      style={{ width: '100%' }}
                      placeholder='Select services performing'
                      defaultValue={this.state.services_performing}
                      optionFilterProp='children'
                      filterOption={(input, option) => option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
                      onChange={(values) => {
                        this.setState({
                          services_performing : values
                        })
                      }}
                    >
                      {
                        services_perform.map((item) => {
                          return (
                            <Select.Option key={item.id}>{item.name}</Select.Option>
                          )
                        })
                      }
                    </Select>
                    <small>(you may select more than one)</small>
                  </div>
                </div>
                <div className='form-group'>
                  <label className='col-md-4 control-label' htmlFor='example-email'>Experience on other special services</label>
                  <div className='col-md-8'>
                    <Select
                      mode='multiple'
                      style={{ width: '100%' }}
                      placeholder='Select experience(s)'
                      defaultValue={this.state.services_experience}
                      optionFilterProp='children'
                      filterOption={(input, option) => option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
                      onChange={(values) => {
                        this.setState({
                          services_experience : values
                        })
                      }}
                    >
                      {
                        services_experience.map((item) => {
                          return (
                            <Select.Option key={item.id}>{item.name}</Select.Option>
                          )
                        })
                      }
                    </Select>
                    <small>(you may select more than one)</small>
                  </div>
                </div>
                <div className='form-group'>
                  <label className='col-md-4 control-label' htmlFor='example-email'>Health conditions you have cared for</label>
                  <div className='col-md-8'>
                    <Select
                      mode='multiple'
                      style={{ width: '100%' }}
                      placeholder='Select health condition(s)'
                      defaultValue={this.state.health_conditions}
                      optionFilterProp='children'
                      filterOption={(input, option) => option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
                      onChange={(values) => {
                        this.setState({
                          health_conditions : values
                        })
                      }}
                    >
                      {
                        health_conditions.map((item) => {
                          return (
                            <Select.Option key={item.id}>{item.name}</Select.Option>
                          )
                        })
                      }
                    </Select>
                    <small>(you may select more than one)</small>
                  </div>
                </div>

                <hr />
                <div className='form-group'>
                  <label className='col-md-2 control-label' htmlFor='example-email' />
                  <div className='col-md-10' >
                    <Button type='primary' className='pull-right' id={321} loading={this.state.loading} htmlType='submit'>
                      <i className='mdi mdi-content-save' /> Save Information
                    </Button>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    )
  }
}

CharacterReferences.propTypes = {
}
