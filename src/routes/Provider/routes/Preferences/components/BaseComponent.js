import React, { Component, PropTypes } from 'react'
import { Input, Select, Button, TimePicker, Table, Icon, Divider, Spin, message, Steps } from 'antd'
import * as _Api from './../../../modules/api'

import Preferences from './Preferences'
import BasicInfo from './BasicInfo'
import Certifications from './Certifications'
import CharacterReferences from './CharacterReferences'
import Requirements from './Requirements'

const Step = Steps.Step

export default class BaseComponent extends Component {

  constructor (props) {
    super(props)
    this.state = {
      loading: false,
      isInvalid: false,
      isUpdated: false,
      current: 0
    }
  }

  next () {
    const current = this.state.current + 1
    this.setState({ current })
  }

  prev () {
    const current = this.state.current - 1
    this.setState({ current })
  }

  render () {
    const { current, isInvalid, isUpdated } = this.state
    const steps = [{
      title: 'Preferences',
      icon: <Icon type='user' />,
      content: <Preferences
        {...this.props}
        status={(status) => {
          this.setState({ isInvalid: status })
        }}
        updateStatus={(status) => {
          this.setState({ isUpdated: status })
        }}
        moveNext={() => {
          this.next()
        }}
      />
    }, {
      title: 'BasicInfo',
      icon: <Icon type='user' />,
      content: <BasicInfo
        {...this.props}
        status={(status) => {
          this.setState({ isInvalid: status })
        }}
        updateStatus={(status) => {
          this.setState({ isUpdated: status })
        }}
        moveNext={() => {
          this.next()
        }}
      />
    }, {
      title: 'Done',
      icon: <Icon type='user' />,
      content: 'Done Application setup'
    }]
    return (
      <div className='row'>
        <div className='col-sm-12'>
          <div className='panel panel-primary'>
            <div className='panel-body'>
              <div>
                <Steps current={current} status={(isInvalid) ? 'error' : ''}>
                  {steps.map(item => <Step key={item.title} title={item.title} icon={item.icon} />)}
                </Steps>
                <div className='steps-content'>{steps[this.state.current].content}</div>
                <div className='steps-action'>
                  {
                    (current < (steps.length - 1)) && isUpdated &&
                    <Button type='primary' onClick={() => this.next()}>Next</Button>
                  }
                  {/* {
                    (current < (steps.length - 1)) &&
                    <Button type='primary' onClick={this.handleDoMatching}>Done</Button>
                  } */}
                  {
                    current > 0 &&
                    <Button style={{ marginLeft: 8 }} onClick={() => this.prev()}>
                      Previous
                    </Button>
                  }
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    )
  }
}

BaseComponent.propTypes = {
}
