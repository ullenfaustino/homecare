import PreferenceContainer from './containers/PreferenceContainer'

export default (store) => ({
  path: '/provider/preferences',
  component : PreferenceContainer
})
