import CareSeekerContainer from './containers/CareSeekerContainer'

export default (store) => ({
  path: '/provider/care-seekers',
  component : CareSeekerContainer
})
