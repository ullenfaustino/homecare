import React, { Component, PropTypes } from 'react'
import { Select, Button, TimePicker, Table, Icon, Divider, Spin, message, DatePicker, Popconfirm, Slider, InputNumber, Row, Col } from 'antd'
import GoogleAutcompleteInput from 'components/GoogleAutcompleteInput'
import moment from 'moment'
import ReactStars from 'react-stars'
import ReactImageFallback from 'react-image-fallback'
import * as _Api from './../../../modules/api'
import * as Helper from 'consts/helper'

import CareRecipientDetails from './CareRecipientDetails'

export default class CareSeeker extends Component {

  constructor (props) {
    super(props)
    this.state = {
      loading: false,
      isFetchingData: false,
      modal_visibility: false,
      recipients: [],
      recipient: undefined,
      distanceRange: 10
    }
  }

  componentDidMount () {
    this.props.refreshUser()
    this.findNearJob()
  }

  handleChangeSliderDistance = (value) => {
    this.setState({
      distanceRange: value
    })
  }

  handleShowRecipientDetailsModal = () => {
    const { modal_visibility, recipient } = this.state
    return (
      <CareRecipientDetails
        recipient={recipient}
        visibility={modal_visibility}
        setVisibility={(visibility) => {
          this.setState({ modal_visibility: visibility })
        }}
      />
    )
  }

  findNearJob = () => {
    this.setState({ isFetchingData: true, loading: true }, () => {
      const { distanceRange } = this.state
      const { user } = this.props.auth
      const params = {
        distanceRange,
        fromLatLng: {
          lat: user.location.latitude,
          lng: user.location.longitude
        }
      }
      // console.log('[params]', params)
      _Api.findNearJob(params).then((recipients) => {
        console.log('[recipients]', recipients)
        this.setState({ recipients, isFetchingData: false, loading: false })
      }).catch((err) => {
        console.log('[err]', err)
        this.setState({ recipients: [], isFetchingData: false, loading: false })
      })
    })
  }

  handleApplyJob = () => {
    const { recipient } = this.state
    const { user } = this.props.auth

    const params = {
      recipient_id: recipient.id,
      seeker_id: recipient.user_id,
      provider_id: user.id
    }

    _Api.applyOnJobPost(params).then((response) => {
      this.props.refreshUser()
    }).catch((err) => {
      console.log('[err]', err)
    })
  }

  handleApplicationStatusLabel = (status) => {
    let statusName, statusClass
    switch (status) {
      case 1:
        statusName = 'Hired'
        statusClass = 'primary'
        break
      case 2:
        statusName = 'Completed'
        statusClass = 'success'
        break
      case 3:
        statusName = 'Canceled'
        statusClass = 'danger'
        break
      default:
        statusName = 'Waiting for reply'
        statusClass = 'warning'
        break
    }
    return (
      <span className={`label label-${statusClass}`}>{statusName}</span>
    )
  }

  render () {
    const { isFetchingData, recipients, distanceRange, modal_visibility } = this.state

    if (isFetchingData) {
      return (
        <Spin />
      )
    }

    const { user } = this.props.auth
    const data = recipients.map((item, i) => {
      return {
        key: item.id,
        age: Helper.getAge(item.birthday),
        gender_name: (item.gender == 0) ? 'Female' : 'Male',
        address: item.location.label,
        ...item
      }
    })

    const columns = [{
      title: 'Care Recipient',
      dataIndex: 'name',
      key: 'name',
      render: (text, record) => {
        return (
          <a href='#' onClick={(ev) => {
            ev.preventDefault()
            this.setState({ modal_visibility: true, recipient: record })
          }}>
            <ReactImageFallback
              src={record.avatar}
              fallbackImage='/user-avatar/user.svg'
              alt={record.first_name}
              className='img-circle'
              style={{ width: 40, height: 40, marginRight: '10px' }}
            />
            {text}
          </a>
        )
      }
    }, {
      title: 'Age',
      dataIndex: 'age',
      key: 'age'
    }, {
      title: 'Gender',
      dataIndex: 'gender_name',
      key: 'gender'
    }, {
      title: 'Address',
      dataIndex: 'address',
      key: 'address'
    }, {
      title: 'Action',
      key: 'action',
      render: (text, record) => {
        const { user } = this.props.auth
        const currApplication = _.find(user.job_applications, (o) => (o.job_post.recipient.id == record.id))
        return (
          <span>
            {/* <a href='#' onClick={(ev) => {
              ev.preventDefault()
              this.setState({ new_form_visibility: true, isEdit: true, recipient: record })
            }}>Edit</a>
            <Divider type='vertical' /> */}
            {
              (currApplication && (currApplication.status == 0))
              ? this.handleApplicationStatusLabel(currApplication.status)
              : <Popconfirm title='Are you sure do you want to apply in this job post？' okText='Yes, I want to apply now' cancelText='No' onConfirm={() => {
                this.setState({ recipient: record }, () => {
                  this.handleApplyJob()
                })
              }}>
                <a href='#'>Apply</a>
              </Popconfirm>
            }
          </span>
        )
      }
    }]

    return (
      <div className='row'>
        <div className='col-sm-12'>
          <div className='panel panel-primary'>
            <div className='panel-body'>
              <Row>
                <Col span={2}>
                  <Button type='primary' id={321} loading={this.state.loading} onClick={() => {
                    this.findNearJob()
                  }}>
                    <i className='mdi mdi-account-search' /> Search Job near you
                  </Button>
                </Col>
              </Row>
              <Row>
                <Col span={12}>
                  <Slider min={1} max={100} onChange={this.handleChangeSliderDistance} value={distanceRange} />
                </Col>
                <Col span={2}>
                  <InputNumber
                    min={1}
                    max={100}
                    style={{ marginLeft: 16, marginRight: '10px' }}
                    value={distanceRange}
                    onChange={this.handleChangeSliderDistance}
                  />
                </Col>
                <strong>Km(s)</strong>
              </Row>
              <br />
              <Table columns={columns} dataSource={data} />
              {
                modal_visibility && this.handleShowRecipientDetailsModal()
              }
            </div>
          </div>
        </div>
      </div>
    )
  }
}

CareSeeker.propTypes = {
}
