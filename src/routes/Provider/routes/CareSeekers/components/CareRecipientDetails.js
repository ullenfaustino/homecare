import React, { Component, PropTypes } from 'react'
import { Link, browserHistory } from 'react-router'
import { Button, Table, Icon, Divider, Spin, message, Modal } from 'antd'
import moment from 'moment'
import ReactImageFallback from 'react-image-fallback'

import * as _Api from './../../../modules/api'
import * as Helper from 'consts/helper'

import {
  availability_columns
} from 'consts/provider'

export default class CareRecipientDetails extends Component {

  constructor (props) {
    super(props)
    this.state = {
      loading: false
    }
  }

  handleTagsView = (tagItems) => {
    const items = tagItems.map((item) => {
      return item.value.name
    })
    return items.join(', ')
  }

  render () {
    const { visibility, recipient } = this.props
    const user = recipient

    if (!user) {
      return (
        <Spin />
      )
    }

    const currentUser = user

    const formattedDays = currentUser.days.map((day) => {
      return day.day
    })

    return (
      <Modal
        destroyOnClose
        title='Recipient Details'
        width={1100}
        visible={visibility}
        footer={null}
        // onOk={() => {
        //   this.props.setVisibility(false)
        // }}
        onCancel={() => {
          this.props.setVisibility(false)
        }}
        >
        <div className='row'>
          <div className='col-sm-12'>
            <div className='panel panel-primary'>
              <div className='panel-body'>
                <form>
                  <ReactImageFallback
                    src={currentUser.avatar}
                    fallbackImage='/user-avatar/user.svg'
                    alt={currentUser.first_name}
                    className='img-circle'
                    style={{ width: 120, height: 120, marginBottom: '20px' }}
                  />
                  <legend>Preferences</legend>
                  <div className='form-group'>
                    <span className='col-md-2'>Name</span>
                    <div className='col-md-10'>
                      <label className='control-label'>{currentUser.name}</label>
                    </div>
                  </div>
                  <div className='form-group'>
                    <span className='col-md-2'>Gender</span>
                    <div className='col-md-10'>
                      <label className='control-label'>{(currentUser.gender == 0) ? 'Female' : 'Male'}</label>
                    </div>
                  </div>
                  <div className='form-group'>
                    <span className='col-md-2'>Birthday</span>
                    <div className='col-md-10'>
                      <label className='control-label'>{ moment(currentUser.birthday).format('LL') }</label>
                    </div>
                  </div>
                  <div className='form-group'>
                    <span className='col-md-2'>Age</span>
                    <div className='col-md-10'>
                      <label className='control-label'>{ Helper.getAge(currentUser.birthday) }</label>
                    </div>
                  </div>
                  <div className='form-group'>
                    <span className='col-md-2'>Location</span>
                    <div className='col-md-10'>
                      <label className='control-label'>{currentUser.location.label}</label>
                    </div>
                  </div>
                  <div className='form-group'>
                    <span className='col-md-2'>General Status</span>
                    <div className='col-md-10'>
                      <label className='control-label'>{ this.handleTagsView(currentUser.general_status) }</label>
                    </div>
                  </div>
                  <div className='form-group'>
                    <span className='col-md-2'>Medical Conditions</span>
                    <div className='col-md-10'>
                      <label className='control-label'>{ this.handleTagsView(currentUser.medical_conditions) }</label>
                    </div>
                  </div>
                  <legend>Care Schedule</legend>
                  <div className='form-group'>
                    <span className='col-md-2'>Days</span>
                    <div className='col-md-10'>
                      <label className='control-label'>{ formattedDays.join(', ') }</label>
                    </div>
                  </div>
                  <div className='form-group'>
                    <span className='col-md-2'>Hours per day</span>
                    <div className='col-md-10'>
                      <label className='control-label'>{currentUser.hours_per_day}</label>
                    </div>
                  </div>
                  <div className='form-group'>
                    <span className='col-md-2'>Preferred Start of Duty</span>
                    <div className='col-md-10'>
                      <label className='control-label'>{ moment(currentUser.start_duty).format('LL') }</label>
                    </div>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </Modal>
    )
  }
}

CareRecipientDetails.propTypes = {
}
