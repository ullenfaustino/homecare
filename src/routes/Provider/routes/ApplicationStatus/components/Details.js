import React, { Component, PropTypes } from 'react'
import { Link, browserHistory } from 'react-router'
import { Input, Select, Button, TimePicker, Table, Icon, Divider, Spin, message } from 'antd'
import * as _Api from './../../../modules/api'

import {
  availability_columns
} from 'consts/provider'

export default class Details extends Component {

  constructor (props) {
    super(props)
    this.state = {
      loading: false,
      isFetchingUserData: true,
      currentUser: {}
    }
  }

  componentWillMount () {
    this.__fetchCurrentUser()
  }

  __fetchCurrentUser = () => {
    console.log('[componentWillMount][this-props]', this.props)
    _Api.findByUid(this.props.auth.user.uid)
    .then((mUser) => {
      // console.log('[CurrentUser]', mUser)
      this.setState({
        isFetchingUserData: false,
        currentUser: mUser
      })
    }).catch((err) => {
      console.log('err', err)
      this.setState({
        isFetchingUserData: false,
        currentUser: {}
      })
    })
  }

  handleTagsView = (tagItems) => {
    const items = tagItems.map((item) => {
      return item.value.name
    })
    return items.join(', ')
  }

  handleFilesView = (fileType) => {
    const { currentUser } = this.state

    const _filtered_defaultFiles = currentUser.files.filter((file) => file.file.file_type === fileType)
    const _defaultFiles = _filtered_defaultFiles.map((file) => {
      if (file.file.file_type === fileType) {
        return {
          uid: file.file.id,
          name: file.file.name,
          status: 'done',
          url: file.file.url
        }
      }
    })
    const formattedFiles = _defaultFiles.map((file, i) => {
      return (<Link key={i} target='_blank' to={file.url}> {file.name}{ ((i + 1) == _defaultFiles.length) ? '' : ',' }</Link>)
    })
    return <p>{formattedFiles}</p>
  }

  render () {
    const { isFetchingUserData, currentUser } = this.state

    if (isFetchingUserData) {
      return (
        <Spin />
      )
    }

    const formattedAvailability = currentUser.availability.map((_availability) => {
      return _.pick(_availability, ['day', 'start_time', 'end_time'])
    })

    return (
      <div className='row'>
        <div className='col-sm-12'>
          <div className='panel panel-primary'>
            <div className='panel-body'>
              {/* <h4 className='m-t-0 m-b-30'>My Qualifications <br /><small>Basic Information</small></h4> */}
              <form>
                <legend>Basic Information</legend>
                <div className='form-group'>
                  <span className='col-md-2'>Status</span>
                  <div className='col-md-10'>
                    <label className='control-label'>{(currentUser.application_status == 0) ? 'Pending' : ((currentUser.application_status == 1) ? 'For Approval' : ((currentUser.application_status == 2) ? 'Approved' : 'Denied'))}</label>
                  </div>
                </div>
                <div className='form-group'>
                  <span className='col-md-2'>Location</span>
                  <div className='col-md-10'>
                    <label className='control-label'>{currentUser.location.label}</label>
                  </div>
                </div>
                <div className='form-group'>
                  <span className='col-md-2'>Client Preferences</span>
                  <div className='col-md-10'>
                    <label className='control-label'>{ this.handleTagsView(currentUser.client_preferences) }</label>
                  </div>
                </div>
                <div className='form-group'>
                  <span className='col-md-2'>Work Restrictions</span>
                  <div className='col-md-10'>
                    <label className='control-label'>{ this.handleTagsView(currentUser.work_restrictions) }</label>
                  </div>
                </div>
                <div className='form-group'>
                  <span className='col-md-2'>Work Schedules</span>
                  <div className='col-md-10'>
                    <label className='control-label'>{ this.handleTagsView(currentUser.work_schedules) }</label>
                  </div>
                </div>
                <legend />
                <div className='form-group'>
                  <span className='col-md-2'>Work Availability</span>
                  <div className='col-md-10'>
                    <Table columns={availability_columns} dataSource={formattedAvailability} />
                  </div>
                </div>
                <legend />
                <div className='form-group'>
                  <span className='col-md-2'>Means of Transportation</span>
                  <div className='col-md-9'>
                    <label className='control-label'>{ this.handleTagsView(currentUser.transportations) }</label>
                  </div>
                </div>
                <div className='form-group'>
                  <span className='col-md-2'>Hourly Rate</span>
                  <div className='col-md-9'>
                    <label className='control-label'>${ currentUser.salary_rate.daily_rate }</label>
                  </div>
                </div>
                <div className='form-group'>
                  <span className='col-md-2'>Daily Rate</span>
                  <div className='col-md-9'>
                    <label className='control-label'>${ currentUser.salary_rate.overnight_rate }</label>
                  </div>
                </div>
                <div className='form-group'>
                  <span className='col-md-2'>Overnight Rate</span>
                  <div className='col-md-9'>
                    <label className='control-label'>${ currentUser.salary_rate.overnight_rate }</label>
                  </div>
                </div>
                <legend />
                <div className='form-group'>
                  <span className='col-md-4'>Legally authorized to work in the US?</span>
                  <div className='col-md-8'>
                    <label className='control-label'>{ (currentUser.is_authorized_to_work == 1) ? 'Yes' : 'No' }</label>
                  </div>
                </div>
                <div className='form-group'>
                  <span className='col-md-4'>Registered on the Home Care Aide Registry?</span>
                  <div className='col-md-8'>
                    <label className='control-label'>{ (currentUser.is_registered_homecare == 1) ? 'Yes' : 'No' }</label>
                  </div>
                </div>
                <div className='form-group'>
                  <span className='col-md-4'>Registry ID Number</span>
                  <div className='col-md-8'>
                    <label className='control-label'>{ currentUser.registry_id }</label>
                  </div>
                </div>
                <div className='form-group'>
                  <span className='col-md-4'>Language Spoken</span>
                  <div className='col-md-8'>
                    <label className='control-label'>{ this.handleTagsView(currentUser.languages) }</label>
                  </div>
                </div>
                <legend />
                <div className='form-group'>
                  <span className='col-md-3'>Character References</span>
                  <div className='col-md-9'>
                    <div className='row'>
                      <div className='col-md-4 text-center'>
                        <label className='control-label'>{ currentUser.character_references.reference_name_1 }</label>
                        <br />
                        <small>{ currentUser.character_references.reference_contact_1 }</small>
                      </div>
                      <div className='col-md-4 text-center'>
                        <label className='control-label'>{ currentUser.character_references.reference_name_2 }</label>
                        <br />
                        <small>{ currentUser.character_references.reference_contact_2 }</small>
                      </div>
                      <div className='col-md-4 text-center'>
                        <label className='control-label'>{ currentUser.character_references.reference_name_3 }</label>
                        <br />
                        <small>{ currentUser.character_references.reference_contact_3 }</small>
                      </div>
                    </div>
                  </div>
                </div>
                <legend />
                <div className='form-group'>
                  <span className='col-md-4'>Services you are performing</span>
                  <div className='col-md-8'>
                    <label className='control-label'>{ this.handleTagsView(currentUser.services_perform) }</label>
                  </div>
                </div>
                <div className='form-group'>
                  <span className='col-md-4'>Experience on other special services</span>
                  <div className='col-md-8'>
                    <label className='control-label'>{ this.handleTagsView(currentUser.services_experience) }</label>
                  </div>
                </div>
                <div className='form-group'>
                  <span className='col-md-4'>Health conditions you have cared for</span>
                  <div className='col-md-8'>
                    <label className='control-label'>{ this.handleTagsView(currentUser.health_conditions) }</label>
                  </div>
                </div>
                <legend />
                <div className='form-group'>
                  <span className='col-md-4'>Resume</span>
                  <div className='col-md-8'>
                    <label className='control-label'>{ this.handleFilesView('cv_resume') }</label>
                  </div>
                </div>
                <div className='form-group'>
                  <span className='col-md-4'>Trainings and Certifications</span>
                  <div className='col-md-8'>
                    <label className='control-label'>{ this.handleFilesView('certifications') }</label>
                  </div>
                </div>
                <div className='form-group'>
                  <span className='col-md-4'>Driver's License</span>
                  <div className='col-md-8'>
                    <label className='control-label'>{ this.handleFilesView('drivers_license') }</label>
                  </div>
                </div>
                <div className='form-group'>
                  <span className='col-md-4'>ID by the DMV</span>
                  <div className='col-md-8'>
                    <label className='control-label'>{ this.handleFilesView('dmv_id') }</label>
                  </div>
                </div>
                <div className='form-group'>
                  <span className='col-md-4'>Alien Registration Card</span>
                  <div className='col-md-8'>
                    <label className='control-label'>{ this.handleFilesView('alien_registration_card') }</label>
                  </div>
                </div>
                <div className='form-group'>
                  <span className='col-md-4'>Numbered ID <smal>(If living outside CA)</smal></span>
                  <div className='col-md-8'>
                    <label className='control-label'>{ this.handleFilesView('numbered_id') }</label>
                  </div>
                </div>
                <div className='form-group'>
                  <span className='col-md-4'>Criminal Record Clearance</span>
                  <div className='col-md-8'>
                    <label className='control-label'>{ this.handleFilesView('criminal_record_clearance') }</label>
                  </div>
                </div>
                {
                (currentUser.application_status == 0) && <div className='form-group'>
                  <div className='col-md-12 text-center' >
                    <Button type='primary' id={321} loading={this.state.loading} onClick={() => {
                      const params = {
                        user: {
                          uid: this.props.auth.user.uid,
                          id: this.props.auth.user.id
                        }
                      }
                      this.setState({
                        loading: true
                      })
                      _Api.updateStatusForApproval(params)
                      .then((response) => {
                        // console.log(response)
                        this.setState({
                          loading: false
                        }, () => {
                          this.props.auth.user.application_status = response.application_status
                          this.__fetchCurrentUser()
                          message.success('Application successfully submitted')
                        })
                      })
                      .catch((err) => {
                        // console.log(err)
                        this.setState({
                          loading: false
                        }, () => {
                          message.error('Application failed to submit')
                        })
                      })
                    }}>
                      <i className='mdi mdi-content-save' /> Submit Application
                    </Button>
                  </div>
                </div>
              }
              </form>
            </div>
          </div>
        </div>
      </div>
    )
  }
}

Details.propTypes = {
}
