import ApplicationStatusContainer from './containers/ApplicationStatusContainer'

export default (store) => ({
  path: '/provider/application-status',
  component : ApplicationStatusContainer
})
