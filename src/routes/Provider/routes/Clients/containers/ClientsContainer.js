import React, { Component } from 'react'
import { connect } from 'react-redux'
import {
  actions
} from '../../../modules/provider'
import { refreshUser } from 'modules/auth'
import Clients from '../components/Clients'

class ClientsContainer extends Component {

  render () {
    let { children, params, location } = this.props

    if (children) {
      return this.props.children
    } else {
      // content =
      return (
        <div className='content'>
          <div className=''>
            <div className='page-header-title'>
              <h4 className='page-title'><i className='mdi mdi-account-multiple' /> My Clients</h4>
            </div>
          </div>
          <div className='page-content-wrapper '>
            <div className='container'>
              <Clients {...this.props} />
            </div>
          </div>
        </div>
      )
    }
  }
}

const mapDispatchToProps = { ...actions, refreshUser }

const mapStateToProps = (state) => ({ ...state })

export default connect(mapStateToProps, mapDispatchToProps)(ClientsContainer)
