import React, { Component, PropTypes } from 'react'
import { Select, Button, TimePicker, Table, Icon, Divider, Spin, message, DatePicker, Popconfirm } from 'antd'
import GoogleAutcompleteInput from 'components/GoogleAutcompleteInput'
import moment from 'moment'
import ReactStars from 'react-stars'
import ReactImageFallback from 'react-image-fallback'
import * as GoogleApi from 'modules/googlemaps/api'
import * as _Api from './../../../modules/api'
import * as Helper from 'consts/helper'

import CareRecipientDetails from './CareRecipientDetails'

export default class Clients extends Component {

  constructor (props) {
    super(props)
    this.state = {
      loading: false,
      modal_visibility: false,
      recipient: {}
    }
  }

  componentDidMount () {
    this.props.refreshUser()
  }

  renderRecipientDetailsModal = () => {
    return (
      <CareRecipientDetails
        recipient={this.state.recipient}
        visibility={this.state.modal_visibility}
        setVisibility={(visibility) => {
          this.setState({ modal_visibility: visibility })
        }}
        acceptJobOffer={(hiredRecipient) => {
          this.handleAcceptJobOffer(hiredRecipient)
        }}
      />
    )
  }

  handleAcceptJobOffer = (hiredRecipient) => {
    _Api.acceptJobOffer({ hiring_id: hiredRecipient.hiring_id })
    .then((response) => {
      this.props.refreshUser()
      message.success('Job offer successfully accepted.')
    }).catch((err) => {
      console.log('[handleAcceptJobOffer][error]', err)
      message.error('Unable to process request')
    })
  }

  render () {
    const { user } = this.props.auth

    const data = user.client.map((item, i) => {
      return {
        key: item.id,
        age: Helper.getAge(item.birthday),
        gender_name: (item.gender == 0) ? 'Female' : 'Male',
        address: item.location.label,
        job_offer_date_formatted: moment(item.job_offer_date).format('LL'),
        hired_date_formatted: moment(item.hired_date).format('LL'),
        status: (item.hiring_status == 0) ? 'Awaiting Reply' : (item.hiring_status == 1) ? 'Hired' : (item.hiring_status == 2) ? 'Finished' : 'Canceled',
        ...item
      }
    })

    const columns = [{
      title: 'Name',
      dataIndex: 'name',
      key: 'name',
      render: (text, record) => {
        return (
          <a href='#' onClick={(ev) => {
            ev.preventDefault()
            this.setState({ modal_visibility: true, recipient: record })
          }}>
            <ReactImageFallback
              src={record.avatar}
              fallbackImage='/user-avatar/user.svg'
              alt={record.first_name}
              className='img-circle'
              style={{ width: 40, height: 40, marginRight: '10px' }}
            />
            {text}
          </a>
        )
      }
    }, {
      title: 'Age',
      dataIndex: 'age',
      key: 'age'
    }, {
      title: 'Gender',
      dataIndex: 'gender_name',
      key: 'gender'
    }, {
      title: 'Address',
      dataIndex: 'address',
      key: 'address'
    }, {
      title: 'Job Offer Date',
      dataIndex: 'job_offer_date_formatted',
      key: 'job_offer_date_formatted'
    }, {
      title: 'Hired Date',
      dataIndex: 'hired_date_formatted',
      key: 'hired_date_formatted'
    }, {
      title: 'Status',
      dataIndex: 'status',
      key: 'status'
    }]

    return (
      <div className='row'>
        <div className='col-sm-12'>
          <div className='panel panel-primary'>
            <div className='panel-body'>
              <Table columns={columns} dataSource={data} />
              {
                this.state.modal_visibility && this.renderRecipientDetailsModal()
              }
            </div>
          </div>
        </div>
      </div>
    )
  }
}

Clients.propTypes = {
}
