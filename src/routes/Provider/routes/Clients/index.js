import ClientsContainer from './containers/ClientsContainer'

export default (store) => ({
  path: '/provider/clients',
  component : ClientsContainer
})
