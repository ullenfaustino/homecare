import React from 'react'
import { Link } from 'react-router'

const NotFound = () => (
  <div>
    <br />
    <br />
    <br />
    <br />
    <div className='middle-box text-center animated fadeInRightBig'>
      <h3 className='font-bold'>Welcome to HomeCare Network</h3>
      <div className='error-desc'>
          HomeCare Network © 2017
        {/* <br /><Link to='/' className='btn btn-primary m-t'>Dashboard</Link> */}
        <br />
        <br />
        <h1>Page Not Found</h1>
      </div>
    </div>
  </div>
)

export default NotFound
