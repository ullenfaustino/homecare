import { injectSagas } from '../store/sagas'
// We only need to import the modules necessary for initial render
import CoreLayout from '../layouts/CoreLayout/CoreLayout'
import SiteLayout from '../layouts/SiteLayout/SiteLayout'

import Home from './Home'
import * as authModule from 'modules/auth'
import * as appModule from 'modules/app'
import * as searchModule from 'modules/search'

import _ from 'lodash'
// import CounterRoute from './Counter'
// import LoginRoute from './Login'

/**
* Checks authentication status on route change
* @param  {object}   nextState The state we want to change into when we change routes
* @param  {function} replace Function provided by React Router to replace the location
*/
function checkAuth (nextState, replace, store) {
  let { auth, location } = store.getState()
  let { loggedIn, user } = auth

  // If the user is already logged in, forward them to the homepage
  if (!loggedIn) {
    if (nextState.location.pathname === '/register') {
      // redirecting to registration
    } else if (nextState.location.pathname === '/phpmyadmin') {
      // redirecting to phpmyadmin
    } else {
      if (nextState.location.pathname !== '/login') {
        // replace('/login')
        replace({ pathname: '/login', query: { return_to: nextState.location.pathname } })
      }
    }
  }

  if (loggedIn) {
    // if (nextState.location.state && nextState.location.pathname) {

    if (user.is_activated == 0 && nextState.location.pathname !== '/deactivated') {
      replace('/deactivated')
    } else {
      if (nextState.location.pathname == '/login') {
        replace('/')
      }
    }

    // else if (user.role === 'operator_manager' &&
    //   _.find(nextState.routes, function (o) { return o.path === 'operators' }) &&
    //   nextState.location.pathname !== `/operators/${user._operator.id}/view`) {
    //   if (nextState.location.pathname === `/operators/${user._operator.id}/edit`) {
    //   } else {
    //     replace(`/notfound`)
    //   }
    // } else if (user.role === 'operator_manager' &&
    //   _.find(nextState.routes, function (o) { return o.path === 'settings' })) {
    //   replace(`/notfound`)
    // } else if (user.role === 'operator_manager' && nextState.location.pathname === '/') {
    //   replace(`/operators/${user._operator.id}/view`)
    // }
  }
}

/*  Note: Instead of using JSX, we recommend using react-router
    PlainRoute objects to build route definitions.   */

export const createRoutes = (store) => ({
  path        : '/',
  // component   : CoreLayout,
  getComponent: (nextState, cb) => {
    require.ensure([], (require) => {
      const authSagas = authModule.sagas

      injectSagas(store, { key: 'auth', sagas: authSagas })
      injectSagas(store, { key: 'app', sagas: appModule.sagas })
      injectSagas(store, { key: 'search', sagas: searchModule.sagas })

      const authState = store.getState().auth

      if (authState && authState.loggedIn) {
        // myaxios.defaults.headers.common['Authorization'] = 'Bearer ' + authState.token
        return cb(null, CoreLayout)
      }

      // myaxios.defaults.headers.common['Authorization'] = false
      cb(null, SiteLayout)
    })
  },
  indexRoute  : Home,
  // indexRoute: {
  //   path: '/home',
  //   onEnter: (nextState, replace) => {
  //     return replace('/contracts')
  //   }
  // },
  onEnter     : (nextState, replace) => checkAuth(nextState, replace, store),
  onChange: (prevState, nextState, replace) => checkAuth(nextState, replace, store),
  // childRoutes : [
  //   CounterRoute(store),
  //   LoginRoute(store)
  // ],
  getChildRoutes (location, cb) {
    require.ensure([], (require) => {
      cb(null, [
        // Remove imports!
        require('./Login').default(store),
        require('./Registration').default(store),
        require('./Admin').default(store),
        require('./Provider').default(store),
        require('./Seeker').default(store),
        require('./Deactivated').default(store),
        require('./NotFound').default(store)
      ])
    })
  }
})

export default createRoutes
