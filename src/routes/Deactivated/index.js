import Deactivated from './components/Deactivated'

export default (store) => ({
  path : '/deactivated',
  component : Deactivated
})
