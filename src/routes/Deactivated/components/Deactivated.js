import React, { Component, PropTypes } from 'react'
import { connect } from 'react-redux'
import { message } from 'antd'
import { logoutRequest } from 'modules/auth'
import { Link, browserHistory } from 'react-router'
import {
  Form,
  FormControl,
  FormGroup
} from 'react-bootstrap'

import * as Api from 'modules/api'

import LogoSvg from 'assets/homecare_logo.png'
import LogoMain from 'static/logos/logo_main.png'

class Deactivated extends Component {
  constructor (props) {
    super(props)

    this.state = {
      activation_code: ''
    }
  }

  clearFields = () => {
    this.setState({
      activation_code: ''
    })
  }

  componentDidMount () {
    const { query } = this.props.router.getCurrentLocation()
    if (query && query.activation_code) {
      this.setState({ activation_code: query.activation_code }, () => {
        this.handleActivateAccount()
      })
    }
  }

  handleActivateAccount = () => {
    const { auth } = this.props
    const { activation_code } = this.state

    if (auth.user.activation_code === activation_code) {
      Api.updateUserActivation({
        uid: auth.user.uid,
        activation_code: activation_code
      }).then((response) => {
        auth.user.is_activated = response.is_activated
        message.success('Your account was successfully activated!')
        browserHistory.push(`/`)
      })
    } else {
      this.clearFields()
      message.error('Invalid Activation Code!')
    }
  }

  render () {
    return (
      <div>
        <br />
        <br />
        <br />
        <br />
        <div className='middle-box text-center animated fadeInRightBig'>
          <div className='text-center m-t-0 m-b-15'>
            <a href='/' className='logo logo-admin'>
              <img src={LogoMain} className='logo' style={{ widht: 120, height: 120 }} />
            </a>
          </div>
          <h3 className='font-bold'>You have successfully registered to Care Network!</h3>
          <h4 className='font-bold'>Please check your email for the verification code</h4>
          <div className='error-desc'>
            <br />
            <h2>Enter your verification code here</h2>
            <Form className='form-horizontal m-t-20 text-center' onSubmit={(ev) => {
              ev.preventDefault()
              this.handleActivateAccount()
            }}>
              <div className='middle-box'>
                <input autoFocus required type='text' className='form-control text-center' placeholder='Activation code here...' value={this.state.activation_code} onChange={(e) => {
                  this.setState({
                    activation_code: e.target.value
                  })
                }} />
              </div>
            </Form>
            <small><i className='fa fa-envelope-o' aria-hidden='true' /> You will get your verification code from your email</small>
            <br />
            <br />
            HomeCare Network © 2017
            <br />
            <a href='javascript:void(0)' onClick={(ev) => {
              ev.preventDefault()
              this.props.logout()
            }}><i className='fa fa-sign-out' aria-hidden='true' /> Logout</a>
          </div>
        </div>
      </div>
    )
  }
}

Deactivated.propTypes = {
}

const mapDispatchToProps = {
  logout: () => logoutRequest()
}

const mapStateToProps = (state) => ({
  isInit: state.app.isInit,
  auth : state.auth
})

export default connect(mapStateToProps, mapDispatchToProps)(Deactivated)
