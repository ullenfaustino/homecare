import React, { Component, PropTypes } from 'react'
import { connect } from 'react-redux'
import { Link, browserHistory } from 'react-router'

class HomeView extends Component {
  constructor (props) {
    super(props)
  }

  componentDidMount () {
    console.log('HomeView', this.props)
    const { auth } = this.props
    const { user } = auth

    if (user.user_type == 0) {
      browserHistory.push(`/admin`)
    } else if (user.user_type == 1) {
      browserHistory.push(`/provider`)
    } else if (user.user_type == 2) {
      browserHistory.push(`/seeker`)
    }
  }

  render () {
    return (
      <h1>HomeView</h1>
    )
  }
}

HomeView.propTypes = {
}

const mapDispatchToProps = {}
const mapStateToProps = (state) => ({
  isInit: state.app.isInit,
  activeOperation: state.app.activeOperation,
  auth : state.auth
})

export default connect(mapStateToProps, mapDispatchToProps)(HomeView)
