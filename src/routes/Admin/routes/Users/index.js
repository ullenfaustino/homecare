import UsersContainer from './containers/UsersContainer'

export default (store) => ({
  path: '/admin/users',
  component : UsersContainer
})
