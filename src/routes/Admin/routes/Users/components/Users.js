import React, { Component, PropTypes } from 'react'
import { Select, Button, TimePicker, Table, Icon, Divider, Spin, message, DatePicker } from 'antd'
import moment from 'moment'
import ReactImageFallback from 'react-image-fallback'

import * as _Api from './../../../modules/api'
import UserDetails from './UserDetails'

export default class Users extends Component {

  constructor (props) {
    super(props)
    this.state = {
      loading: false,
      currentUsers : [],
      isFetchingUsersData: true,
      modal_visibility: false
    }
  }

  componentDidMount () {
    this.fetchUsers({
      user_type: 'users'
    })
  }

  fetchUsers = (params) => {
    _Api.fetchUsers(params)
    .then((mUsers) => {
      this.setState({
        isFetchingUsersData: false,
        currentUsers: mUsers
      })
    })
    .catch((err) => {
      console.log('[err]', err)
      this.setState({
        isFetchingUsersData: false
      })
    })
  }

  renderApplicationDetailsModal = () => {
    return (
      <UserDetails
        user={this.state.selectedUser}
        visibility={this.state.modal_visibility}
        setVisibility={(visibility) => {
          this.setState({ modal_visibility:visibility })
        }}
      />
    )
  }

  render () {
    if (this.state.isFetchingUsersData) {
      return (
        <Spin />
      )
    }

    const { currentUsers } = this.state
    // console.log('[currentUsers]', currentUsers)

    const table_data = currentUsers.map((user, i) => {
      return {
        key: user.id,
        gender_name: (user.gender == 0) ? 'Female' : 'Male',
        formatted_datecreated: moment(user.created_at).format('LLL'),
        account_type: (user.user_type == 0) ? 'Admin' : ((user.user_type == 1) ? 'Care Provider' : 'Care Seeker'),
        account_status: (user.is_activated == 1) ? 'Active' : 'Deactivated',
        application_status_label: (user.application_status == 1) ? 'For Approval'
                                  : (user.application_status == 2) ? 'Approved'
                                  : (user.application_status == 3) ? 'Denied' : 'Pending',
        ...user
      }
    })

    const columns = [{
      title: 'Username',
      dataIndex: 'username',
      key: 'username',
      render: (text, record) => {
        return (
          <a href='#' onClick={(ev) => {
            ev.preventDefault()
            this.setState({ modal_visibility: true, selectedUser: record })
          }}>
            <ReactImageFallback
              src={record.avatar}
              fallbackImage='/user-avatar/user.svg'
              alt={record.first_name}
              className='img-circle'
              style={{ width: 40, height: 40, marginRight: '10px' }}
            />
            {text}
          </a>
        )
      }
    }, {
      title: 'First Name',
      dataIndex: 'first_name',
      key: 'first_name'
    }, {
      title: 'Middle Name',
      dataIndex: 'middle_name',
      key: 'middle_name'
    }, {
      title: 'Last Name',
      dataIndex: 'last_name',
      key: 'last_name'
    }, {
      title: 'Gender',
      dataIndex: 'gender_name',
      key: 'gender_name'
    }, {
      title: 'Account Type',
      dataIndex: 'account_type',
      key: 'account_type',
      render: (text, record) => {
        return (record.user_type == 1) ? <span className='label label-info'>{text}</span> : <span className='label label-success'>{text}</span>
      }
    }, {
      title: 'Created At',
      dataIndex: 'formatted_datecreated',
      key: 'formatted_datecreated'
    }, {
      title: 'Account Status',
      dataIndex: 'account_status',
      key: 'account_status',
      render: (text, record) => {
        return (record.is_activated == 1) ? <span className='label label-success'>{text}</span> : <span className='label label-danger'>{text}</span>
      }
    }, {
      title: 'Application Status',
      dataIndex: 'application_status_label',
      key: 'application_status_label',
      render: (text, record) => {
        if (record.user_type == 2) {
          return <span className='label label-success'>Approved</span>
        } else {
          return (record.application_status == 2) ? <span className='label label-success'>{text}</span>
                 : (record.application_status == 1) ? <span className='label label-warning'>{text}</span>
                 : (record.application_status == 3) ? <span className='label label-danger'>{text}</span>
                 : <span className='label label-default'>{text}</span>
        }
      }
    }]

    return (
      <div className='row'>
        <div className='col-sm-12'>
          <div className='panel panel-primary'>
            <div className='panel-body'>
              <Table columns={columns} dataSource={table_data} />

              {
                this.state.modal_visibility && this.renderApplicationDetailsModal()
              }
            </div>
          </div>
        </div>
      </div>
    )
  }
}

Users.propTypes = {
}
