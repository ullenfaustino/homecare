import React, { Component } from 'react'
import { connect } from 'react-redux'
import {
  actions
} from '../../../modules/admin'
import Users from '../components/Users'

class UsersContainer extends Component {

  render () {
    let { children, params, location } = this.props

    if (children) {
      return this.props.children
    } else {
      // content =
      return (
        <div className='content'>
          <div className=''>
            <div className='page-header-title'>
              <h4 className='page-title'><i className='mdi mdi-account-check' /> Users</h4>
            </div>
          </div>
          <div className='page-content-wrapper '>
            <div className='container'>
              <Users {...this.props} />
            </div>
          </div>
        </div>
      )
    }
  }
}

const mapDispatchToProps = { ...actions }

const mapStateToProps = (state) => ({ ...state })

export default connect(mapStateToProps, mapDispatchToProps)(UsersContainer)
