import React, { Component, PropTypes } from 'react'
import { Select, Button, TimePicker, Table, Icon, Divider, Spin, message, DatePicker, Modal, Popconfirm } from 'antd'
import moment from 'moment'
import * as _Api from './../../../../modules/api'

export default class ServiceExperience extends Component {

  constructor (props) {
    super(props)
    this.state = {
      loading: false,
      form_visibility: false,
      isEdit: false,
      selectedItem: {},
      value: '',
      selectedItemId: 0
    }
  }

  handleSave = () => {
    _Api.setGenericSettings({ name: this.state.value, model: 'LkupServicesExperience' }, { id: this.state.isEdit ? this.state.selectedItem.id : -1 })
    .then((response) => {
      // console.log('response', response)
      this.setState({ form_visibility: false, isEdit: false, selectedItem: {}, value: '' }, () => {
        message.success('Successfully Saved.')
        this.props.initApp()
      })
    })
    .catch((err) => {
      console.log('err', err)
      message.error('unable to save data.')
    })
  }

  handleDelete = () => {
    _Api.deleteGenericSettings({ id: this.state.selectedItemId, model: 'LkupServicesExperience' })
    .then((response) => {
      this.setState({ selectedItemId: 0 }, () => {
        message.success('Successfully Saved.')
        this.props.initApp()
      })
    })
    .catch((err) => {
      console.log('err', err)
      message.error('unable to delete data.')
    })
  }

  renderModalForm = () => {
    return (
      <Modal
        destroyOnClose
        title='Services Experience'
        okText={this.state.isEdit ? 'Update' : 'Save'}
        width={700}
        visible={this.state.form_visibility}
        onOk={() => {
          this.handleSave()
        }}
        onCancel={() => {
          this.setState({ form_visibility: false, isEdit: false, value: '' })
        }}
        >
        <div className='row'>
          <div className='col-sm-12'>
            <form>
              <div className='form-group'>
                <label className='col-md-2 control-label' htmlFor='example-email'>Name</label>
                <div className='col-md-10'>
                  <input type='text' className='form-control' placeholder='add here...' value={this.state.value} onChange={(ev) => {
                    this.setState({ value: ev.target.value })
                  }} />
                </div>
              </div>
            </form>
          </div>
        </div>
      </Modal>
    )
  }

  render () {
    const { services_experience } = this.props.app.settings

    const table_data = services_experience.map((item, i) => {
      return Object.assign({}, item, {
        key: item.id,
        date_created_formatted: (item.updated_at) ? moment(item.updated_at).format('LLL') : moment().format('LLL')
      })
    })

    const columns = [{
      title: 'Name',
      dataIndex: 'name',
      key: 'name'
    }, {
      title: 'Date Created',
      dataIndex: 'date_created_formatted',
      key: 'date_created_formatted'
    }, {
      title: 'Action',
      key: 'action',
      render: (text, record) => {
        return (
          <span>
            <a href='#' onClick={() => {
              this.setState({ form_visibility: true, isEdit: true, selectedItem: record, value: record.name })
            }}>Edit</a>
            <Divider type='vertical' />
            {
              (record.providerSelected.length == 0)
              ? <Popconfirm title='Are you sure do you want to delete this item？' okText='Yes, I want to delete!' cancelText='No' onConfirm={() => {
                this.setState({ selectedItemId: record.id }, () => {
                  this.handleDelete()
                })
              }}>
                <a href='#' >Delete</a>
              </Popconfirm> : <small>In-Used</small>
            }
          </span>
        )
      }
    }]

    return (
      <div className='row'>
        <div className='col-sm-12'>
          <div className='panel panel-primary'>
            <div className='panel-body'>
              <legend>Services Experience</legend>
              <Button type='primary' onClick={() => {
                this.setState({ form_visibility: true, isEdit: false })
              }}><i className='mdi mdi-plus' /> Add New</Button>
              <Table
                columns={columns}
                dataSource={table_data}
              />

              {
                this.state.form_visibility && this.renderModalForm()
              }
            </div>
          </div>
        </div>
      </div>
    )
  }
}

ServiceExperience.propTypes = {
}
