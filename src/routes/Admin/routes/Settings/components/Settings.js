import React, { Component, PropTypes } from 'react'

import ClientPreferences from './SettingsComponent/ClientPreferences'
import GeneralStatus from './SettingsComponent/GeneralStatus'
import HealthConditions from './SettingsComponent/HealthConditions'
import MedicalConditions from './SettingsComponent/MedicalConditions'
import ServiceExperience from './SettingsComponent/ServiceExperience'
import ServicePerform from './SettingsComponent/ServicePerform'
import Transportation from './SettingsComponent/Transportation'
import TypeCare from './SettingsComponent/TypeCare'
import WorkRestrictions from './SettingsComponent/WorkRestrictions'
import WorkSchedule from './SettingsComponent/WorkSchedule'

export default class Settings extends Component {

  constructor (props) {
    super(props)

    console.log('[Settings][props]', props)
  }

  render () {
    return (
      <div className='row'>
        <div className='col-sm-12'>
          <div className='panel panel-primary'>
            <div className='panel-body'>
              <ClientPreferences {...this.props} />
              <GeneralStatus {...this.props} />
              <HealthConditions {...this.props} />
              <MedicalConditions {...this.props} />
              <ServiceExperience {...this.props} />
              <ServicePerform {...this.props} />
              <Transportation {...this.props} />
              <TypeCare {...this.props} />
              <WorkRestrictions {...this.props} />
              <WorkSchedule {...this.props} />
            </div>
          </div>
        </div>
      </div>
    )
  }
}

Settings.propTypes = {
}
