import React, { Component, PropTypes } from 'react'
import { Link, browserHistory } from 'react-router'
import { Button, Table, Icon, Divider, Spin, message, Modal } from 'antd'
import moment from 'moment'
import * as _Api from './../../../modules/api'
import * as Helper from 'consts/helper'

import {
  availability_columns
} from 'consts/provider'

export default class ApplicationDetails extends Component {

  constructor (props) {
    super(props)
    this.state = {
      loading: false
    }
  }

  componentDidMount () {
    console.log('[ApplicationDetails][componentDidMount][Props]', this.props)
  }

  handleTagsView = (tagItems) => {
    const items = tagItems.map((item) => {
      return item.value.name
    })
    return items.join(', ')
  }

  handleFilesView = (fileType) => {
    const { user } = this.props
    const currentUser = user

    const _filtered_defaultFiles = currentUser.files.filter((file) => file.file.file_type === fileType)
    const _defaultFiles = _filtered_defaultFiles.map((file) => {
      if (file.file.file_type === fileType) {
        return {
          uid: file.file.id,
          name: file.file.name,
          status: 'done',
          url: file.file.url
        }
      }
    })
    const formattedFiles = _defaultFiles.map((file, i) => {
      return (<Link key={i} target='_blank' to={file.url}> {file.name}{ ((i + 1) == _defaultFiles.length) ? '' : ',' }</Link>)
    })
    return <p>{formattedFiles}</p>
  }

  handleApproval = (user_id) => {
    _Api.updateStatusApproved({ user_id })
    .then((response) => {
      console.log(response)
      message.success('Provider successfully approved.')
      this.props.setVisibility(false)
      this.props.setRefreshData()
    })
    .catch((err) => {
      console.log('err', err)
      message.success('Cannot approve this provider.')
    })
  }

  render () {
    const { visibility, user } = this.props

    if (!user) {
      return (
        <Spin />
      )
    }

    const currentUser = user

    const formattedAvailability = currentUser.availability.map((_availability) => {
      return _.pick(_availability, ['day', 'start_time', 'end_time'])
    })

    return (
      <Modal
        destroyOnClose
        title='Application Details'
        okText='Yes, I want to approve'
        width={1200}
        visible={visibility}
        onOk={() => {
          // this.props.setVisibility(false)
          this.handleApproval(currentUser.id)
        }}
        onCancel={() => {
          this.props.setVisibility(false)
        }}
        >
        <div className='row'>
          <div className='col-sm-12'>
            <div className='panel panel-primary'>
              <div className='panel-body'>
                <form>
                  <legend>Profile</legend>
                  <div className='form-group'>
                    <span className='col-md-2'>Name</span>
                    <div className='col-md-10'>
                      <label className='control-label'>{currentUser.first_name + ' ' + currentUser.last_name}</label>
                    </div>
                  </div>
                  <div className='form-group'>
                    <span className='col-md-2'>Username</span>
                    <div className='col-md-10'>
                      <label className='control-label'>{currentUser.username}</label>
                    </div>
                  </div>
                  <div className='form-group'>
                    <span className='col-md-2'>Email</span>
                    <div className='col-md-10'>
                      <label className='control-label'>{currentUser.email}</label>
                    </div>
                  </div>
                  <div className='form-group'>
                    <span className='col-md-2'>Gender</span>
                    <div className='col-md-10'>
                      <label className='control-label'>{(currentUser.gender == 0) ? 'Female' : 'Male'}</label>
                    </div>
                  </div>
                  <div className='form-group'>
                    <span className='col-md-2'>Birthday</span>
                    <div className='col-md-10'>
                      <label className='control-label'>{ moment(currentUser.birthday).format('LL') }</label>
                    </div>
                  </div>
                  <div className='form-group'>
                    <span className='col-md-2'>Age</span>
                    <div className='col-md-10'>
                      <label className='control-label'>{ Helper.getAge(currentUser.birthday) }</label>
                    </div>
                  </div>
                  <div className='form-group'>
                    <span className='col-md-2'>Contact #:</span>
                    <div className='col-md-10'>
                      <label className='control-label'>{currentUser.contact_no}</label>
                    </div>
                  </div>
                  <div className='form-group'>
                    <span className='col-md-2'>Location</span>
                    <div className='col-md-10'>
                      <label className='control-label'>{currentUser.location.label}</label>
                    </div>
                  </div>
                  <div className='form-group'>
                    <span className='col-md-2'>Zip Code</span>
                    <div className='col-md-10'>
                      <label className='control-label'>{currentUser.zip_code}</label>
                    </div>
                  </div>
                  <legend>Provider Preferences</legend>
                  <div className='form-group'>
                    <span className='col-md-2'>Status</span>
                    <div className='col-md-10'>
                      <label className='control-label'>{(currentUser.application_status == 0) ? 'Pending' : ((currentUser.application_status == 1) ? 'For Approval' : ((currentUser.application_status == 2) ? 'Approved' : 'Denied'))}</label>
                    </div>
                  </div>
                  <div className='form-group'>
                    <span className='col-md-2'>Client Preferences</span>
                    <div className='col-md-10'>
                      <label className='control-label'>{ this.handleTagsView(currentUser.client_preferences) }</label>
                    </div>
                  </div>
                  <div className='form-group'>
                    <span className='col-md-2'>Work Restrictions</span>
                    <div className='col-md-10'>
                      <label className='control-label'>{ this.handleTagsView(currentUser.work_restrictions) }</label>
                    </div>
                  </div>
                  <div className='form-group'>
                    <span className='col-md-2'>Work Schedules</span>
                    <div className='col-md-10'>
                      <label className='control-label'>{ this.handleTagsView(currentUser.work_schedules) }</label>
                    </div>
                  </div>
                  <legend />
                  <div className='form-group'>
                    <span className='col-md-2'>Work Availability</span>
                    <div className='col-md-10'>
                      <Table columns={availability_columns} dataSource={formattedAvailability} />
                    </div>
                  </div>
                  <legend />
                  <div className='form-group'>
                    <span className='col-md-3'>Means of Transportation</span>
                    <div className='col-md-9'>
                      <label className='control-label'>{ this.handleTagsView(currentUser.transportations) }</label>
                    </div>
                  </div>
                  <div className='form-group'>
                    <span className='col-md-2'>Salary Rate</span>
                    <div className='col-md-10'>
                      <div className='row'>
                        <div className='col-md-4 text-center'>
                          <label className='control-label'>${ currentUser.salary_rate.hourly_rate }</label>
                          <br />
                          <small>Hourly Rate</small>
                        </div>
                        <div className='col-md-4 text-center'>
                          <label className='control-label'>${ currentUser.salary_rate.daily_rate }</label>
                          <br />
                          <small>Daily Rate</small>
                        </div>
                        <div className='col-md-4 text-center'>
                          <label className='control-label'>${ currentUser.salary_rate.overnight_rate }</label>
                          <br />
                          <small>Overnight Rate</small>
                        </div>
                      </div>
                    </div>
                  </div>
                  <legend />
                  <div className='form-group'>
                    <span className='col-md-5'>Legally authorized to work in the US?</span>
                    <div className='col-md-7'>
                      <label className='control-label'>{ (currentUser.is_authorized_to_work == 1) ? 'Yes' : 'No' }</label>
                    </div>
                  </div>
                  <div className='form-group'>
                    <span className='col-md-5'>Registered on the Home Care Aide Registry?</span>
                    <div className='col-md-7'>
                      <label className='control-label'>{ (currentUser.is_registered_homecare == 1) ? 'Yes' : 'No' }</label>
                    </div>
                  </div>
                  <div className='form-group'>
                    <span className='col-md-5'>Registry ID Number</span>
                    <div className='col-md-7'>
                      <label className='control-label'>{ currentUser.registry_id }</label>
                    </div>
                  </div>
                  <div className='form-group'>
                    <span className='col-md-5'>Language Spoken</span>
                    <div className='col-md-7'>
                      <label className='control-label'>{ this.handleTagsView(currentUser.languages) }</label>
                    </div>
                  </div>
                  <legend />
                  <div className='form-group'>
                    <span className='col-md-3'>Character References</span>
                    <div className='col-md-9'>
                      <div className='row'>
                        <div className='col-md-4 text-center'>
                          <label className='control-label'>{ currentUser.character_references.reference_name_1 }</label>
                          <br />
                          <small>{ currentUser.character_references.reference_contact_1 }</small>
                        </div>
                        <div className='col-md-4 text-center'>
                          <label className='control-label'>{ currentUser.character_references.reference_name_2 }</label>
                          <br />
                          <small>{ currentUser.character_references.reference_contact_2 }</small>
                        </div>
                        <div className='col-md-4 text-center'>
                          <label className='control-label'>{ currentUser.character_references.reference_name_3 }</label>
                          <br />
                          <small>{ currentUser.character_references.reference_contact_3 }</small>
                        </div>
                      </div>
                    </div>
                  </div>
                  <legend />
                  <div className='form-group'>
                    <span className='col-md-4'>Services you are performing</span>
                    <div className='col-md-8'>
                      <label className='control-label'>{ this.handleTagsView(currentUser.services_perform) }</label>
                    </div>
                  </div>
                  <div className='form-group'>
                    <span className='col-md-4'>Experience on other special services</span>
                    <div className='col-md-8'>
                      <label className='control-label'>{ this.handleTagsView(currentUser.services_experience) }</label>
                    </div>
                  </div>
                  <div className='form-group'>
                    <span className='col-md-4'>Health conditions you have cared for</span>
                    <div className='col-md-8'>
                      <label className='control-label'>{ this.handleTagsView(currentUser.health_conditions) }</label>
                    </div>
                  </div>
                  <legend />
                  <div className='form-group'>
                    <span className='col-md-4'>Resume</span>
                    <div className='col-md-8'>
                      <label className='control-label'>{ this.handleFilesView('cv_resume') }</label>
                    </div>
                  </div>
                  <div className='form-group'>
                    <span className='col-md-4'>Driver's License</span>
                    <div className='col-md-8'>
                      <label className='control-label'>{ this.handleFilesView('drivers_license') }</label>
                    </div>
                  </div>
                  <div className='form-group'>
                    <span className='col-md-4'>ID by the DMV</span>
                    <div className='col-md-8'>
                      <label className='control-label'>{ this.handleFilesView('dmv_id') }</label>
                    </div>
                  </div>
                  <div className='form-group'>
                    <span className='col-md-4'>Alien Registration Card</span>
                    <div className='col-md-8'>
                      <label className='control-label'>{ this.handleFilesView('alien_registration_card') }</label>
                    </div>
                  </div>
                  <div className='form-group'>
                    <span className='col-md-4'>Numbered ID <smal>(If living outside CA)</smal></span>
                    <div className='col-md-8'>
                      <label className='control-label'>{ this.handleFilesView('numbered_id') }</label>
                    </div>
                  </div>
                  <div className='form-group'>
                    <span className='col-md-4'>Criminal Record Clearance</span>
                    <div className='col-md-8'>
                      <label className='control-label'>{ this.handleFilesView('criminal_record_clearance') }</label>
                    </div>
                  </div>
                  {
                  (currentUser.application_status == 0) && <div className='form-group'>
                    <div className='col-md-12 text-center' >
                      <Button type='primary' id={321} loading={this.state.loading} onClick={() => {
                        const params = {
                          user: {
                            uid: this.props.auth.user.uid,
                            id: this.props.auth.user.id
                          }
                        }
                        this.setState({
                          loading: true
                        })
                        _Api.updateStatusForApproval(params)
                        .then((response) => {
                          // console.log(response)
                          this.setState({
                            loading: false
                          }, () => {
                            this.props.auth.user.application_status = response.application_status
                            this.__fetchCurrentUser()
                            message.success('Application successfully submitted')
                          })
                        })
                        .catch((err) => {
                          // console.log(err)
                          this.setState({
                            loading: false
                          }, () => {
                            message.error('Application failed to submit')
                          })
                        })
                      }}>
                        <i className='mdi mdi-content-save' /> Submit Application
                      </Button>
                    </div>
                  </div>
                }
                </form>
              </div>
            </div>
          </div>
        </div>
      </Modal>
    )
  }
}

ApplicationDetails.propTypes = {
}
