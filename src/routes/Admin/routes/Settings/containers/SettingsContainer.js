import React, { Component } from 'react'
import { connect } from 'react-redux'
import {
  actions
} from '../../../modules/admin'
import { initApp } from 'modules/app'
import Settings from '../components/Settings'

class SettingsContainer extends Component {

  render () {
    let { children, params, location } = this.props

    if (children) {
      return this.props.children
    } else {
      // content =
      return (
        <div className='content'>
          <div className=''>
            <div className='page-header-title'>
              <h4 className='page-title'><i className='mdi mdi-settings' /> Settings</h4>
            </div>
          </div>
          <div className='page-content-wrapper '>
            <div className='container'>
              <Settings {...this.props} />
            </div>
          </div>
        </div>
      )
    }
  }
}

const mapDispatchToProps = { ...actions, initApp }

const mapStateToProps = (state) => ({ ...state })

export default connect(mapStateToProps, mapDispatchToProps)(SettingsContainer)
