import SettingsContainer from './containers/SettingsContainer'

export default (store) => ({
  path: '/admin/settings',
  component : SettingsContainer
})
