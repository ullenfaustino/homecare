import ManualAssignContainer from './containers/ManualAssignContainer'

export default (store) => ({
  path: '/admin/manual-assign',
  component : ManualAssignContainer
})
