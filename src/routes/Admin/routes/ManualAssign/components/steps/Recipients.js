import React, { Component, PropTypes } from 'react'
import { Button, Icon, Divider, message, DatePicker } from 'antd'
import Select from 'react-select'
import moment from 'moment'
import * as Api from 'modules/api'
import GoogleAutcompleteInput from 'components/GoogleAutcompleteInput'
import * as GoogleApi from 'modules/googlemaps/api'

import {
  days
} from 'consts/provider'

export default class Recipients extends Component {

  constructor (props) {
    super(props)
    this.state = {
      loading: false,
      recipients: [],
      recipient: undefined,

      gender: 0,
      general_status:[],
      medical_conditions:[],
      days: [],
      start_duty: moment(),
      birthday: moment(),
      hours_per_day: 0,
      recipient_name: '',
      selectedLocation: {}
    }
  }

  componentDidMount () {
    this.fetchRecipients()
  }

  fetchRecipients = () => {
    const { seekerId } = this.props
    Api.getRecipients(seekerId)
    .then((mRecipients) => {
      this.setState({ recipients: mRecipients })
      // console.log('mRecipients', mRecipients)
    })
    .catch((err) => {
      console.log('err', err)
    })
  }

  handleOnSubmitPreferences = () => {
    const { seekerId } = this.props
    const params = {
      user: {
        id: seekerId,
        recipient_id: -1
      },
      location: {
        place_id: this.state.selectedLocation.place_id,
        google_place_id: this.state.selectedLocation.id,
        description: this.state.selectedLocation.description,
        label: this.state.selectedLocation.label,
        reference: this.state.selectedLocation.reference
      },
      gender: this.state.gender,
      birthday: this.state.birthday,
      general_status: this.state.general_status.map((item) => item.id),
      medical_conditions: this.state.medical_conditions.map((item) => item.id),
      days: this.state.days.map((item) => item.value),
      start_duty: this.state.start_duty,
      hours_per_day: this.state.hours_per_day,
      recipient_name: this.state.recipient_name
    }
    // console.log('[post-params]', params)
    Api.addRecipient(params).then((response) => {
      // console.log(response)
      this.setState({
        loading: false
      }, () => {
        message.success('Successfully saved')
        this.props.recipientId(response.id)
        this.props.moveNext()
      })
    }).catch((err) => {
      console.log(err)
      this.setState({
        loading: false
      }, () => {
        message.error('Error saving recipient preferences')
      })
    })
  }

  render () {
    const { recipients } = this.state
    const { general_status, medical_conditions } = this.props.app.settings
    const filteredRecipients = recipients.filter((recipient) => {
      if (_.has(recipient.hired, 'id')) {
        return false
      }
      return true
    })
    return (
      <div className='row'>
        {
          (filteredRecipients.length > 0) &&
          <div className='col-sm-12'>
            <div className='panel panel-primary'>
              <div className='panel-body'>
                <label className='form-label' htmlFor='field-3'>Select from existing care recipients</label>
                <span className='desc' />
                <div className='controls'>
                  <Select
                    id='id'
                    valueKey='id'
                    labelKey='name'
                    onBlurResetsInput={false}
                    onSelectResetsInput={false}
                    autoFocus
                    options={filteredRecipients}
                    clearable
                    value={this.state.recipient}
                    onChange={(value) => {
                      this.setState({ recipient: value }, () => {
                        this.props.recipientId(value.id)
                      })
                    }}
                    searchable
  	              />
                  <br />
                  {
                    (this.state.recipient) &&
                    <Button type='primary' onClick={() => this.props.moveNext()}>Next</Button>
                  }
                </div>
              </div>
            </div>
          </div>
        }
        {
          (filteredRecipients.length > 0) &&
          <div className='col-sm-12'>
            <h2>Or</h2>
          </div>
        }
        <div className='col-sm-12'>
          <div className='panel panel-primary'>
            <div className='panel-body'>
              <legend>New Care Recipient</legend>
              <form className='form-horizontal m-t-20' onSubmit={(ev) => {
                ev.preventDefault()
                this.setState({
                  loading: true
                }, () => {
                  this.handleOnSubmitPreferences()
                })
              }}>
                <div className='row'>
                  <div className='col-md-6'>
                    <label className='form-label' htmlFor='field-1'>Care recipient Name</label>
                    <span className='desc' />
                    <div className='controls'>
                      <input type='text' className='form-control' required placeholder='Recipient Name' value={this.state.recipient_name} onChange={(e) => {
                        this.setState({
                          recipient_name: e.target.value
                        })
                      }} />
                    </div>
                  </div>
                  <div className='col-md-6'>
                    <label className='form-label' htmlFor='field-3'>Location</label>
                    <span className='desc' />
                    <div className='controls'>
                      <GoogleAutcompleteInput autoComplete={(inputText) => {
                        return GoogleApi.autoComplete({
                          q: inputText
                        })
                      }} onChange={(value) => {
                        this.setState({
                          selectedLocation: value
                        })
                      }} value={this.state.selectedLocation} />
                    </div>
                  </div>
                </div>
                <div className='row'>
                  <br />
                  <div className='col-md-3'>
                    <label className='form-label' htmlFor='field-2'>Gender</label>
                    <span className='desc' />
                    <div className='controls'>
                      <select className='form-control' required value={this.state.gender} onChange={(e) => {
                        this.setState({
                          gender: parseInt(e.target.value)
                        })
                      }}>
                        <option value={-1}>- Select Gender -</option>
                        <option value={0}>Female</option>
                        <option value={1}>Male</option>
                      </select>
                    </div>
                  </div>
                  <div className='col-md-3'>
                    <label className='form-label' htmlFor='field-4'>Birthday</label>
                    <span className='desc' />
                    <div className='controls'>
                      <DatePicker value={this.state.birthday} style={{ width: '100%' }} size='large' onChange={(date, dateString) => {
                        this.setState({
                          birthday: date
                        })
                      }} />
                    </div>
                  </div>
                  <div className='col-md-3'>
                    <label className='form-label' htmlFor='field-8'>Hours per Day</label>
                    <span className='desc' />
                    <div className='controls'>
                      <input type='number' className='form-control' required placeholder='Hours per day' value={this.state.hours_per_day} onChange={(ev) => {
                        this.setState({
                          hours_per_day: parseInt(ev.target.value)
                        })
                      }} />
                    </div>
                  </div>
                  <div className='col-md-3'>
                    <label className='form-label' htmlFor='field-8'>Preferred start of duty</label>
                    <span className='desc' />
                    <div className='controls'>
                      <DatePicker value={this.state.start_duty} style={{ width: '100%' }} size='large' onChange={(date, dateString) => {
                        this.setState({
                          start_duty: date
                        })
                      }} />
                    </div>
                  </div>
                </div>
                <div className='row'>
                  <br />
                  <div className='col-md-4'>
                    <label className='form-label' htmlFor='field-6'>Medical Conditions</label>
                    <span className='desc' />
                    <div className='controls'>
                      <Select
                        multi
                        id='id'
                        valueKey='id'
                        labelKey='name'
                        onBlurResetsInput={false}
                        onSelectResetsInput={false}
                        options={medical_conditions}
                        clearable
                        value={this.state.medical_conditions}
                        onChange={(value) => {
                          this.setState({ medical_conditions: value })
                        }}
                        searchable
                       />
                    </div>
                  </div>
                  <div className='col-md-4'>
                    <label className='form-label' htmlFor='field-5'>General Status</label>
                    <span className='desc' />
                    <div className='controls'>
                      <Select
                        multi
                        id='id'
                        valueKey='id'
                        labelKey='name'
                        onBlurResetsInput={false}
                        onSelectResetsInput={false}
                        options={general_status}
                        clearable
                        value={this.state.general_status}
                        onChange={(value) => {
                          this.setState({ general_status: value })
                        }}
                        searchable
                       />
                    </div>
                  </div>
                  <div className='col-md-4'>
                    <label className='form-label' htmlFor='field-7'>Schedule Days</label>
                    <span className='desc' />
                    <div className='controls'>
                      <Select
                        multi
                        id='id'
                        valueKey='id'
                        labelKey='value'
                        onBlurResetsInput={false}
                        onSelectResetsInput={false}
                        options={days}
                        clearable
                        value={this.state.days}
                        onChange={(value) => {
                          this.setState({ days: value })
                        }}
                        searchable
                       />
                    </div>
                  </div>
                </div>
                <div className='row'>
                  <div className='col-md-12'>
                    <br />
                    <button className='btn btn-primary waves-effect waves-light' type='submit' disabled={this.state.isSaving}>
                      { this.state.isSaving ? 'Saving...' : 'Create Account' } <i className='fa fa-floppy-o' aria-hidden='true' />
                    </button>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    )
  }
}

Recipients.propTypes = {
}
