import React, { Component, PropTypes } from 'react'
import { Button, Icon, Divider, message, Spin, DatePicker } from 'antd'
import Select from 'react-select'
import moment from 'moment'
import ReactImageFallback from 'react-image-fallback'
import * as Helper from 'consts/helper'
import * as Api from 'modules/api'
import * as _Api from './../../../../modules/api'

export default class AssignProvider extends Component {

  constructor (props) {
    super(props)
    this.state = {
      loading: false,
      isSaving: false,
      isFetchingRecipient: true,
      recipient: undefined,
      value: undefined,

      first_name: undefined,
      last_name: undefined,
      middle_name: undefined,
      zip_code: undefined,
      contact_no: undefined,
      email: undefined,
      user_type: 1,
      birthday: moment(),
      gender: -1
    }
  }

  componentDidMount () {
    this.fetchRecipient()
  }

  handleOnSubmit = () => {
    if (this.state.gender == -1) {
      message.error('Please select your gender.')
      return
    }

    this.setState({
      isSaving: true
    }, this.handleOnCreateAccount())
  }

  handleOnCreateAccount = () => {
    let params = {
      first_name: this.state.first_name,
      last_name: this.state.last_name,
      middle_name: this.state.middle_name,
      zip_code: this.state.zip_code,
      contact_no: this.state.contact_no,
      email: this.state.email,
      user_type: this.state.user_type,
      birthday: this.state.birthday,
      gender: this.state.gender
    }

    const username = this.state.first_name.toLowerCase().substring(0, 1) + this.state.last_name.toLowerCase().replace(/\s/g, '')
    const password = 'ourcarenetwork'
    params = Object.assign({}, params, { username, password })

    Api.createAccount(params)
    .then((response) => {
      message.success('Account successfully created.')
      this.setState({ isSaving: false }, () => {
        this.props.providerId(response.id)
        this.props.moveNext()
      })
    })
    .catch((err) => {
      console.log('err ', err)
    })
  }

  fetchRecipient = () => {
    const { recipientId } = this.props
    Api.findRecipient(recipientId)
    .then((recipient) => {
      this.setState({ isFetchingRecipient:false, recipient })
    })
    .catch((err) => {
      this.setState({ isFetchingRecipient:false })
      console.log('err', err)
    })
  }

  fetchProviders = (e) => {
    const params = {
      search: e
    }
    return _Api.fetchProviders(params)
    .then((mProviders) => {
      const data = mProviders.map((provider) => {
        return Object.assign({}, provider, {
          label: provider.first_name + ' ' + provider.last_name
        })
      })
      return { options: data }
    })
    .catch((err) => {
      console.log('[err]', err)
    })
  }

  render () {
    const { isFetchingRecipient, recipient } = this.state
    if (isFetchingRecipient) {
      return (
        <Spin />
      )
    }

    const AsyncComponent = this.state.creatable ? Select.AsyncCreatable : Select.Async
    return (
      <div className='row'>
        <div className='col-sm-12'>
          <div className='panel panel-primary'>
            <div className='panel-body'>
              <legend>Care Recipient</legend>
              <div className='row'>
                <div className='col-sm-1 col-md-1'>
                  <ReactImageFallback
                    src={recipient.avatar}
                    fallbackImage='/user-avatar/user.svg'
                    alt={recipient.name}
                    className='img-circle'
                    style={{ width: 85, height: 85 }}
                  />
                </div>
                <div className='col-sm-11 col-md-11'>
                  <b>{recipient.name}</b>
                  <p>{Helper.getAge(recipient.birthday)} year(s) old</p>
                  <p>{recipient.location.label}</p>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className='col-sm-12'>
          <div className='panel panel-primary'>
            <div className='panel-body'>
              <legend>Care Provider</legend>
              <div className='row'>
                <div className='col-sm-12'>
                  <label className='form-label' htmlFor='field-3'>Select from Existing Care Provider</label>
                  <span className='desc' />
                  <div className='controls'>
                    <AsyncComponent
                      multi={false}
                      value={this.state.value}
                      onChange={(value) => {
                        this.setState({ value }, () => {
                          this.props.providerId(value.id)
                        })
                      }}
                      valueKey='id'
                      labelKey='label'
                      loadOptions={this.fetchProviders}
                      backspaceRemoves
                    />
                    <br />
                    {
                      (this.state.value) &&
                      <Button type='primary' onClick={() => this.props.moveNext()}>Next</Button>
                    }
                  </div>
                </div>
                <div className='col-sm-12'>
                  <h2>Or</h2>
                </div>
                <div className='col-sm-12'>
                  <div className='panel panel-primary'>
                    <div className='panel-body'>
                      <legend>New Care Provider</legend>
                      <form className='form-horizontal m-t-20' onSubmit={(ev) => {
                        this.handleOnSubmit()
                        ev.preventDefault()
                      }}>
                        <div className='row'>
                          <div className='col-md-6'>
                            <label className='form-label' htmlFor='field-1'>First Name</label>
                            <span className='desc' />
                            <div className='controls'>
                              <input type='text' className='form-control' required placeholder='First Name' value={this.state.first_name} onChange={(e) => {
                                this.setState({
                                  first_name: e.target.value
                                })
                              }} />
                            </div>
                          </div>
                          <div className='col-md-6'>
                            <label className='form-label' htmlFor='field-1'>Last Name</label>
                            <span className='desc' />
                            <div className='controls'>
                              <input type='text' className='form-control' required placeholder='Last Name' value={this.state.last_name} onChange={(e) => {
                                this.setState({
                                  last_name: e.target.value
                                })
                              }} />
                            </div>
                          </div>
                          <div className='col-md-6'>
                            <br />
                            <label className='form-label' htmlFor='field-2'>Middle Name</label>
                            <span className='desc' />
                            <div className='controls'>
                              <input type='text' className='form-control' required placeholder='Middle Name' value={this.state.middle_name} onChange={(e) => {
                                this.setState({
                                  middle_name: e.target.value
                                })
                              }} />
                            </div>
                          </div>
                          <div className='col-md-6'>
                            <br />
                            <label className='form-label' htmlFor='field-3'>Gender</label>
                            <span className='desc' />
                            <div className='controls'>
                              <select className='form-control' required value={this.state.gender} onChange={(e) => {
                                this.setState({
                                  gender: parseInt(e.target.value)
                                })
                              }}>
                                <option value={-1}>- Select Gender -</option>
                                <option value={0}>Female</option>
                                <option value={1}>Male</option>
                              </select>
                            </div>
                          </div>
                          <div className='col-md-6'>
                            <br />
                            <label className='form-label' htmlFor='field-5'>Zip Code</label>
                            <span className='desc' />
                            <div className='controls'>
                              <input type='text' className='form-control' required placeholder='Zip Code' value={this.state.zip_code} onChange={(e) => {
                                this.setState({
                                  zip_code: e.target.value
                                })
                              }} />
                            </div>
                          </div>
                          <div className='col-md-6'>
                            <br />
                            <label className='form-label' htmlFor='field-4'>Birthday</label>
                            <span className='desc' />
                            <div className='controls'>
                              <DatePicker className='form-control' style={{ width: '100%' }} value={this.state.birthday} size='large' onChange={(date, dateString) => {
                                this.setState({
                                  birthday: date
                                })
                              }} />
                            </div>
                          </div>
                          <div className='col-md-6'>
                            <br />
                            <label className='form-label' htmlFor='field-6'>Contact Number</label>
                            <span className='desc' />
                            <div className='controls'>
                              <input type='text' className='form-control' required placeholder='Contact Number' value={this.state.contact_no} onChange={(e) => {
                                this.setState({
                                  contact_no: e.target.value
                                })
                              }} />
                            </div>
                          </div>
                          <div className='col-md-6'>
                            <br />
                            <label className='form-label' htmlFor='field-7'>Email</label>
                            <span className='desc' />
                            <div className='controls'>
                              <input type='email' className='form-control' required placeholder='Email' value={this.state.email} onChange={(e) => {
                                this.setState({
                                  email: e.target.value
                                })
                              }} />
                            </div>
                          </div>
                          <div className='col-md-6'>
                            <br />
                            <button className='btn btn-primary waves-effect waves-light' type='submit' disabled={this.state.isSaving}>
                              { this.state.isSaving ? 'Saving...' : 'Create Account' } <i className='fa fa-floppy-o' aria-hidden='true' />
                            </button>
                          </div>
                        </div>
                      </form>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    )
  }
}

AssignProvider.propTypes = {
}
