import React, { Component, PropTypes } from 'react'
import { Button, Icon, Divider, message, Spin, DatePicker } from 'antd'
import Select from 'react-select'
import moment from 'moment'
import * as Helper from 'consts/helper'
import * as Api from 'modules/api'

export default class ConfirmAssign extends Component {

  constructor (props) {
    super(props)
    this.state = {
      loading: false,
      isFetchingProvider: true,
      isFetchingRecipient: true,

      recipient: undefined,
      provider: undefined
    }
  }

  componentDidMount () {
    this.fetchProvider()
    this.fetchRecipient()
  }

  fetchProvider = () => {
    const { providerId } = this.props
    Api.findUserById(providerId)
    .then((provider) => {
      this.setState({ isFetchingProvider:false, provider })
    })
    .catch((err) => {
      this.setState({ isFetchingProvider:false })
      console.log('err', err)
    })
  }

  fetchRecipient = () => {
    const { recipientId } = this.props
    Api.findRecipient(recipientId)
    .then((recipient) => {
      this.setState({ isFetchingRecipient:false, recipient })
    })
    .catch((err) => {
      this.setState({ isFetchingRecipient:false })
      console.log('err', err)
    })
  }

  render () {
    const { isFetchingProvider, isFetchingRecipient, recipient, provider } = this.state
    if (isFetchingProvider || isFetchingRecipient) {
      return (
        <Spin />
      )
    }

    return (
      <div className='row'>
        <div className='col-sm-12 col-md-12 text-center'>
          <h1>{provider.first_name + ' ' + provider.last_name}</h1>
          <span> will give care to </span>
          <h1>{recipient.name}</h1>
          <br />
          <span>Are you sure do you want to save this?</span>
        </div>
      </div>
    )
  }
}

ConfirmAssign.propTypes = {
}
