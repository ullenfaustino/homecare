import React, { Component, PropTypes } from 'react'
import { Link, browserHistory } from 'react-router'
import { Select, Button, TimePicker, Table, Icon, Divider, Spin, message, Steps } from 'antd'
import moment from 'moment'
import ReactImageFallback from 'react-image-fallback'

import * as Api from 'modules/api'
import RegisterSeeker from './steps/RegisterSeeker'
import Recipients from './steps/Recipients'
import AssignProvider from './steps/AssignProvider'
import ConfirmAssign from './steps/ConfirmAssign'

const Step = Steps.Step

export default class ManualAssign extends Component {

  constructor (props) {
    super(props)
    this.state = {
      loading: false,
      isInvalid: false,
      current: 0,

      seekerId: 0,
      recipientId: 0,
      providerId: 0
    }
  }

  next () {
    const current = this.state.current + 1
    this.setState({ current })
  }

  prev () {
    const current = this.state.current - 1
    this.setState({ current })
  }

  handleDoMatching = () => {
    const { seekerId, recipientId, providerId } = this.state
    if (seekerId != 0 && recipientId != 0 && providerId != 0) {
      const params = {
        seeker_id: seekerId,
        provider_id: providerId,
        recipient_id: recipientId
      }
      Api.hireByManualMatch(params)
      .then((hire) => {
        this.setState({ current: 0, seekerId: 0, recipientId: 0, providerId: 0 }, () => {
          message.success('You have successfully saved.')
        })
      })
      .catch((err) => {
        console.log('err', err)
      })
    } else {
      message.error('Invalid Data!')
    }
  }

  render () {
    const { current, isInvalid, isCompleted, seekerId, recipientId, providerId } = this.state
    const steps = [{
      title: 'Seeker',
      icon: <Icon type='user' />,
      content: <RegisterSeeker
        {...this.props}
        status={(status) => {
          this.setState({ isInvalid: status })
        }}
        seekerId={(id) => {
          this.setState({ seekerId: id })
        }}
        moveNext={() => {
          this.next()
        }}
      />
    }, {
      title: 'Care Recipient',
      icon: <Icon type='user' />,
      content: <Recipients
        {...this.props}
        seekerId={seekerId}
        status={(status) => {
          this.setState({ isInvalid: status })
        }}
        recipientId={(id) => {
          this.setState({ recipientId: id })
        }}
        moveNext={() => {
          this.next()
        }}
      />
    }, {
      title: 'Assign Care Provider',
      icon: <Icon type='tags-o' />,
      content: <AssignProvider
        {...this.props}
        recipientId={recipientId}
        status={(status) => {
          this.setState({ isInvalid: status })
        }}
        providerId={(id) => {
          this.setState({ providerId: id })
        }}
        moveNext={() => {
          this.next()
        }}
      />
    }, {
      title: 'Confirm Matching',
      icon: <Icon type='question-circle-o' />,
      content: <ConfirmAssign
        {...this.props}
        providerId={providerId}
        recipientId={recipientId}
        seekerId={seekerId}
      />
    }]
    return (
      <div className='row'>
        <div className='col-sm-12'>
          <div className='panel panel-primary'>
            <div className='panel-body'>
              <div>
                <Steps current={current} status={(isInvalid) ? 'error' : ''}>
                  {steps.map(item => <Step key={item.title} title={item.title} icon={item.icon} />)}
                </Steps>
                <div className='steps-content'>{steps[this.state.current].content}</div>
                <div className='steps-action'>
                  {/* {
                    current < steps.length - 1 &&
                    <Button type='primary' onClick={() => this.next()}>Next</Button>
                  } */}
                  {
                    current === steps.length - 1 &&
                    <Button type='primary' onClick={this.handleDoMatching}>Done</Button>
                  }
                  {
                    current > 0 &&
                    <Button style={{ marginLeft: 8 }} onClick={() => this.prev()}>
                      Previous
                    </Button>
                  }
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    )
  }
}

ManualAssign.propTypes = {
}
