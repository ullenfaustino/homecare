import ApplicationsContainer from './containers/ApplicationsContainer'

export default (store) => ({
  path: '/admin/applications',
  component : ApplicationsContainer
})
