import React, { Component, PropTypes } from 'react'
import { Select, Button, TimePicker, Table, Icon, Divider, Spin, message, DatePicker } from 'antd'
import moment from 'moment'
import * as _Api from './../../../modules/api'
import ApplicationDetails from './ApplicationDetails'

export default class Applications extends Component {

  constructor (props) {
    super(props)
    this.state = {
      loading: false,
      isFetchingUsersData: true,
      currentUsers : [],
      modal_visibility: false,
      selectedUser: {}
    }
  }

  componentDidMount () {
    // console.log('[componentDidMount][Props]', this.props)
    this.fetchUsers({
      user_type: 1,
      status: 1
    })
  }

  fetchUsers = (params) => {
    _Api.fetchUsers(params)
    .then((mUsers) => {
      this.setState({
        isFetchingUsersData: false,
        currentUsers: mUsers
      })
    })
    .catch((err) => {
      console.log('[err]', err)
      this.setState({
        isFetchingUsersData: false
      })
    })
  }

  renderApplicationDetailsModal = () => {
    return (
      <ApplicationDetails
        user={this.state.selectedUser}
        visibility={this.state.modal_visibility}
        setVisibility={(visibility) => {
          this.setState({ modal_visibility:visibility })
        }}
        setRefreshData={() => {
          this.fetchUsers({
            user_type: 1,
            status: 1
          })
        }}
      />
    )
  }

  render () {
    if (this.state.isFetchingUsersData) {
      return (
        <Spin />
      )
    }

    const { currentUsers } = this.state
    // console.log('[currentUsers]', currentUsers)

    const table_data = currentUsers.map((user, i) => {
      return {
        key: user.id,
        gender_name: (user.gender == 0) ? 'Female' : 'Male',
        address: user.location.label,
        ...user
      }
    })

    const columns = [{
      title: 'Username',
      dataIndex: 'username',
      key: 'username',
      render: (text, record) => <a href='#' onClick={(ev) => {
        ev.preventDefault()
        this.setState({ modal_visibility: true, selectedUser: record })
      }}>{text}</a>
    }, {
      title: 'Email',
      dataIndex: 'email',
      key: 'email'
    }, {
      title: 'First Name',
      dataIndex: 'first_name',
      key: 'first_name'
    }, {
      title: 'Middle Name',
      dataIndex: 'middle_name',
      key: 'middle_name'
    }, {
      title: 'Last Name',
      dataIndex: 'last_name',
      key: 'last_name'
    }, {
      title: 'Address',
      dataIndex: 'address',
      key: 'address'
    }, {
      title: 'Gender',
      dataIndex: 'gender_name',
      key: 'gender_name'
    }, {
      title: 'Action',
      key: 'action',
      render: (text, record) => {
        // console.log('[record]', record)
        return (
          <span>
            <a href='#' onClick={(ev) => {
              ev.preventDefault()
              this.setState({ modal_visibility: true, selectedUser: record })
            }}>Approve</a>
            <Divider type='vertical' />
            <a href='#'>Disapprove</a>
            {/* <Divider type='vertical' />
            <a href='#' className='ant-dropdown-link'>
              More actions <Icon type='down' />
            </a> */}
          </span>
        )
      }
    }]

    return (
      <div className='row'>
        <div className='col-sm-12'>
          <div className='panel panel-primary'>
            <div className='panel-body'>
              <Table columns={columns} dataSource={table_data} />

              {
                this.state.modal_visibility && this.renderApplicationDetailsModal()
              }
            </div>
          </div>
        </div>
      </div>
    )
  }
}

Applications.propTypes = {
}
