import React, { Component, PropTypes } from 'react'
import { Select, Button, TimePicker, Table, Icon, Divider, Spin, message, DatePicker } from 'antd'
import moment from 'moment'
import ReactImageFallback from 'react-image-fallback'
import * as Helper from 'consts/helper'

import * as _Api from './../../../modules/api'
import * as Api from 'modules/api'
import CareRecipientDetails from './../../../components/CareRecipientDetails'

export default class Recipients extends Component {

  constructor (props) {
    super(props)
    this.state = {
      loading: false,
      modal_visibility: false,

      recipient: {},
      recipients: []
    }
  }

  componentDidMount () {
    this.fetchAllRecipients()
  }

  fetchAllRecipients = () => {
    Api.fetchRecipients()
    .then((recipients) => {
      this.setState({ recipients })
    })
    .catch((err) => {
      console.log('err', err)
    })
  }

  renderRecipientDetailsModal = () => {
    return (
      <CareRecipientDetails
        recipient={this.state.recipient}
        visibility={this.state.modal_visibility}
        setVisibility={(visibility) => {
          this.setState({ modal_visibility:visibility })
        }}
      />
    )
  }

  render () {
    const { recipients } = this.state

    const data_recipients = recipients.map((recipient, i) => {
      return {
        key: recipient.id,
        name: recipient.name,
        age: Helper.getAge(recipient.birthday),
        gender_name: (recipient.gender == 0) ? 'Female' : 'Male',
        address: recipient.location.label,
        care_provider_name: ((_.has(recipient.hired, 'provider')) ? recipient.hired.provider.first_name : '') + ' ' + ((_.has(recipient.hired, 'provider')) ? recipient.hired.provider.last_name : ''),
        ...recipient
      }
    })

    // console.log('data_recipients', data_recipients)

    const columns = [{
      title: 'Name',
      dataIndex: 'name',
      key: 'name',
      render: (text, record) => {
        return (
          <a href='#' onClick={(ev) => {
            ev.preventDefault()
            this.setState({ modal_visibility: true, recipient: record })
          }}>
            <ReactImageFallback
              src={record.avatar}
              fallbackImage='/user-avatar/user.svg'
              alt={record.first_name}
              className='img-circle'
              style={{ width: 40, height: 40, marginRight: '10px' }}
            />
            {text}
          </a>
        )
      }
    }, {
      title: 'Care Provider',
      dataIndex: 'care_provider_name',
      key: 'care_provider_name',
      render: (text, record) => {
        return (
          <div>
            {
              (_.has(record.hired, 'provider')) &&
              <a href='#' onClick={(ev) => {
                ev.preventDefault()
              }}>
                <ReactImageFallback
                  src={(_.has(record.hired, 'provider')) ? record.hired.provider.avatar : ''}
                  fallbackImage='/user-avatar/user.svg'
                  alt={(_.has(record.hired, 'provider')) ? record.hired.provider.first_name : ''}
                  className='img-circle'
                  style={{ width: 40, height: 40, marginRight: '10px' }}
                />
                {text}
              </a>
            }
          </div>
        )
      }
    }, {
      title: 'Age',
      dataIndex: 'age',
      key: 'age'
    }, {
      title: 'Gender',
      dataIndex: 'gender_name',
      key: 'gender'
    }, {
      title: 'Address',
      dataIndex: 'address',
      key: 'address'
    }]

    return (
      <div className='row'>
        <div className='col-sm-12'>
          <div className='panel panel-primary'>
            <div className='panel-body'>
              <Table columns={columns} dataSource={data_recipients} />

              {
                this.state.modal_visibility && this.renderRecipientDetailsModal()
              }
            </div>
          </div>
        </div>
      </div>
    )
  }
}

Recipients.propTypes = {
}
