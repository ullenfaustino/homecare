import RecipientsContainer from './containers/RecipientsContainer'

export default (store) => ({
  path: '/admin/recipients',
  component : RecipientsContainer
})
