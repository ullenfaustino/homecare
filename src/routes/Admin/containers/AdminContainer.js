import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Link } from 'react-router'

import { actions } from '../modules/admin'

import Dashboard from '../components/Dashboard'

class AdminContainer extends Component {

  render () {
    let { children, params, location } = this.props

    let content
    if (children) {
      content = this.props.children
    } else {
      content = <Dashboard {...this.props} />
    }

    return content
  }
}

const mapDispatchToProps = { ...actions }

const mapStateToProps = (state) => ({
  auth: state.auth })

export default connect(mapStateToProps, mapDispatchToProps)(AdminContainer)
