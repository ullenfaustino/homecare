import { injectReducer } from '../../store/reducers'
import { injectSagas } from '../../store/sagas'

import AdminContainer from './containers/AdminContainer'

export default (store) => ({
  path : '/admin',
  /*  Async getComponent is only invoked when route matches   */
  getComponent (nextState, cb) {
    /*  Webpack - use 'require.ensure' to create a split point
        and embed an async module loader (jsonp) when bundling   */
    require.ensure([], (require) => {
      /*  Webpack - use require callback to define
          dependencies for bundling   */
      const Admin = require('./containers/AdminContainer').default
      const Breadcrumb = require('./components/Breadcrumb').default
      const reducer = require('./modules/admin').default
      // const sagas = require('./modules/category').sagas
      //
      // /*  Add the reducer to the store on key 'counter'  */
      injectReducer(store, { key: 'admin', reducer })
      // injectSagas(store, { key: 'vehicle', sagas })
      /*  Return getComponent   */
      cb(null, Admin)

    /* Webpack named bundle   */
    }, 'admin')
  },
  onEnter : (nextState, replace) => checkAuth(nextState, replace, store),
  onChange : (prevState, nextState, replace) => checkAuth(nextState, replace, store),
  getChildRoutes (location, next) {
    require.ensure([], (require) => {
      next(null, [
        // {
        //   path: ':id/view',
        //   component: VehicleViewContainer
        // },
        require('./routes/Applications').default(store),
        require('./routes/Settings').default(store),
        require('./routes/Users').default(store),
        require('./routes/ManualAssign').default(store),
        require('./routes/Recipients').default(store)
      ])
    })
  }
})

function checkAuth (nextState, replace, store) {
  let { auth, location } = store.getState()
  let { loggedIn, user } = auth

  const pName = nextState.location.pathname.split('/')
  console.log(pName[1])
  if (user.user_type == 0) {
    if (pName[1] !== 'admin') {
      replace('/admin')
    }
  } else if (user.user_type == 1) {
    if (pName[1] !== 'provider') {
      replace('/provider')
    }
  } else if (user.user_type == 2) {
    if (pName[1] !== 'seeker') {
      replace('/seeker')
    }
  }
}
