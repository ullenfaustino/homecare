import axios from 'axios'

import { URL } from 'consts/apiConsts'

function parseJSON (response) {
  return response.data.data || response.data
}

export function fetchUsers (params) {
  return axios.get(`${URL}/users`, {
    params: params
  }).then(parseJSON)
}

export function fetchRecipients (params) {
  return axios.get(`${URL}/find/recipients`, {
    params: params
  }).then(parseJSON)
}

export function fetchSeekers (params) {
  return axios.get(`${URL}/find/seekers`, {
    params: params
  }).then(parseJSON)
}

export function fetchProviders (params) {
  return axios.get(`${URL}/find/providers`, {
    params: params
  }).then(parseJSON)
}

export function updateStatusApproved (data) {
  return axios.post(`${URL}/provider/status/approved`, data).then(parseJSON)
}

export function setGenericSettings (data, params) {
  return axios.post(`${URL}/settings/generic`, data, {
    params: params
  }).then(parseJSON)
}

export function deleteGenericSettings (data) {
  return axios.post(`${URL}/settings/generic/delete`, data).then(parseJSON)
}
