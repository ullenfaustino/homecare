export const URL = '/api'
export const SOCKET_URL = '/'
export const SMARTHR_URL = 'http://dev-api-core.vibalgroup.com'

export const GENDER = [
  { id: 0, value: 'Female' },
  { id: 1, value: 'Male' }
]
