export function getAge (date) {
  const dob = new Date(date)
  const now = new Date()
  const diff = now - dob
  return Math.floor(diff / (1000 * 60 * 60 * 24 * 365))
}

export function getAverageRating (ratings) {
  let totalRatings = 0
  ratings.map((rating) => { totalRatings += rating.ratings })
  return totalRatings / ratings.length
}
