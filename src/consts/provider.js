const default_WorkRestrictions = [
  { id: 1, value: 'Work w/ moderate lifting' },
  { id: 2, value: 'Work w/ heavy lifting' },
  { id: 3, value: 'No driving' },
  { id: 4, value: 'Work w/ pets' },
  { id: 5, value: 'Work w/ smokers' }
]

const default_SchedulePreferences = [
  { id: 1, value: 'Part Time' },
  { id: 2, value: 'Full Time' },
  { id: 3, value: 'Live in' },
  { id: 4, value: 'Live out' }
]

const default_ClientPreferences = [
  { id: 1, value: 'Male or Female' },
  { id: 2, value: 'Elderlies' },
  { id: 3, value: 'Children' },
  { id: 4, value: 'W/ physical disabilities' },
  { id: 5, value: 'Ambulatory' }
]

const days = [
  { id: 1, value: 'Monday' },
  { id: 2, value: 'Tuesday' },
  { id: 3, value: 'Wednesday' },
  { id: 4, value: 'Thursday' },
  { id: 5, value: 'Friday' },
  { id: 6, value: 'Saturday' },
  { id: 7, value: 'Sunday' }
]

const availability_columns = [
  {
    title: 'Day',
    dataIndex: 'day',
    key: 'day'
  },
  {
    title: 'Start Time',
    dataIndex: 'start_time',
    key: 'start_time'
  },
  {
    title: 'End Time',
    dataIndex: 'end_time',
    key: 'end_time'
  }
]

module.exports = {
  default_WorkRestrictions,
  default_SchedulePreferences,
  default_ClientPreferences,
  days,
  availability_columns
}
