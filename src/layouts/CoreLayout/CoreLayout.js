import React, { Component, PropTypes } from 'react'
import { connect } from 'react-redux'

import HeaderContainer from '../../containers/HeaderContainer'
import SidebarContainer from '../../containers/SidebarContainer'
import { initApp, refreshUser } from 'modules/app'
import { Spin } from 'antd'

class CoreLayout extends Component {
  constructor (props) {
    super(props)
  }

  componentWillMount () {
    const { user } = this.props.auth
    console.log('[currentUser]', user)
    this.props.initApp()
  }

  render () {
    const { children, auth, isInit, main, breadcrumb, routes } = this.props

    if (!isInit) {
      return (
        <div>Initializing application... please wait
        <Spin />
        </div>
      )
    }

    if (!auth || !auth.user) {
      return (
        <div>Loading...
        <Spin />
        </div>
      )
    }

    if (auth.user.is_activated == 0) {
      return (
        <div id='wrapper'>
          {main && main}
          {children}
        </div>
      )
    }

    return (
      <div id='wrapper'>
        <HeaderContainer />
        <SidebarContainer routes={routes} />

        <div className='content-page'>
          {main && main}
          {children}
        </div>
      </div>
    )
  }
}

CoreLayout.propTypes = {
  children : React.PropTypes.element,
  main : React.PropTypes.element
}
const mapDispatchToProps = {
  initApp, refreshUser
}
const mapStateToProps = (state) => ({
  ...state,
  isInit: state.app.isInit,
  auth : state.auth
})

export default connect(mapStateToProps, mapDispatchToProps)(CoreLayout)
