import React, { Component, PropTypes } from 'react'

export default class SiteLayout extends Component {
  constructor (props) {
    super(props)
  }

  componentDidMount () {
    document.body.classList.add('gray-bg')
  }

  componentWillUnmount () {
    document.body.classList.remove('gray-bg')
  }

  render () {
    const { children } = this.props
    return (
      <div className='animated fadeInDown'>
        {children}
      </div>
    )
  }
}

SiteLayout.propTypes = {
  children : React.PropTypes.element.isRequired
}
