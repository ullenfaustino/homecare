const express = require('express')
const setupRoute = (api, authenticatePrivate) => {
  const router = express.Router()

  router.route('/add/preferences')
              .post(
                  authenticatePrivate,
                  api.seeker.addPreferences
               )

  router.route('/add/recipient')
               .post(
                   authenticatePrivate,
                   api.seeker.addRecipient
                )

  router.route('/post-job')
               .post(
                   authenticatePrivate,
                   api.seeker.postJob
                )

  router.route('/hire-applicant')
               .post(
                   authenticatePrivate,
                   api.seeker.hireApplicant
                )

  return router
}

module.exports = setupRoute
