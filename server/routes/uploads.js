const express = require('express')
const setupRoute = (api, authenticatePrivate, middleware) => {
  const router = express.Router()

  router.route('/resume')
              .post(
                  // authenticatePrivate,
                  middleware.upload.single('file'),
                  api.uploads.uploadResume
               )

  router.route('/file')
               .post(
                  //  authenticatePrivate,
                   middleware.upload.single('file'),
                   api.uploads.uploadFile
                )

  router.route('/delete/file')
               .post(
                  //  authenticatePrivate,
                   api.uploads.removeFile
                )

  router.route('/avatar')
              .post(
                  // authenticatePrivate,
                  middleware.upload.single('avatar'),
                  api.uploads.avatar
               )

  router.route('/avatar/recipient')
               .post(
                   // authenticatePrivate,
                   middleware.upload.single('avatar'),
                   api.uploads.recipientAvatar
                )

  return router
}

module.exports = setupRoute
