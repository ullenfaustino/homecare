const express = require('express')

const api = require('../api')

const apitRoutes = (middleware) => {
  const apiRouter = express.Router()

  const authenticatePrivate = [
    // middleware.api.authenticateClient,
    middleware.api.authenticateUser,
    middleware.api.requiresAuthorizedUser
  ]

  apiRouter.route('/users/createAccount').post(
    api.user.createAccount
  )

  apiRouter.route('/auth/login').post(
    api.auth.login
  )

  apiRouter.route('/activate/account').post(
    api.user.activateAccount
  )

  apiRouter.route('/user/:uid').get(
    authenticatePrivate,
    api.user.findByUid
  )

  apiRouter.route('/user/byId/:id').get(
    authenticatePrivate,
    api.user.findById
  )

  apiRouter.route('/user/:uid/recipients').get(
    authenticatePrivate,
    api.user.findUserRecipients
  )

  apiRouter.route('/find/recipients').get(
    authenticatePrivate,
    api.user.findRecipients
  )

  apiRouter.route('/find/recipient/:recipientId').get(
    authenticatePrivate,
    api.user.findRecipient
  )

  apiRouter.route('/find/seekers').get(
    authenticatePrivate,
    api.user.findSeekers
  )

  apiRouter.route('/find/providers').get(
    authenticatePrivate,
    api.user.findProviders
  )

  apiRouter.route('/users').get(
    authenticatePrivate,
    api.user.findUsers
  )

  apiRouter.route('/app/init').get(
    authenticatePrivate,
    api.app.init
  )

  // apiRouter.route('/drivers/:id').put(
  //   authenticatePrivate,
  //   middleware.upload.fields([{ name: 'license', maxCount: 10 }, { name: 'clearance', maxCount: 10 }, { name: 'photo', maxCount: 1 }]),
  //   // middleware.upload.array('files', 10),
  //   api.driver.update
  // )

  // Search API
  // apiRouter.route('/search').get(
  //   authenticatePrivate,
  //   api.search.searchByKeyword
  // )

  // Uploads
  // apiRouter.route('/uploads').post(
  //   authenticatePrivate,
  //   middleware.upload.array('files', 10),
  //   // middleware.upload.single('file'),
  //   api.upload.uploadMultipleFiles
  // )

  // apiRouter.route('/uploads/upload-single').post(
  //   authenticatePrivate,
  //   middleware.upload.single('file'),
  //   api.upload.uploadSingeFile
  // )

  // apiRouter.use('/jobs', require('./jobs')(api, authenticatePrivate, middleware.upload))
  apiRouter.use('/google-maps', require('./google-maps')(api, authenticatePrivate))
  apiRouter.use('/provider', require('./provider')(api, authenticatePrivate))
  apiRouter.use('/seeker', require('./seeker')(api, authenticatePrivate))
  apiRouter.use('/settings', require('./settings')(api, authenticatePrivate))
  apiRouter.use('/chat', require('./chat')(api, authenticatePrivate))
  apiRouter.use('/uploads', require('./uploads')(api, authenticatePrivate, middleware))

  apiRouter.use(middleware.api.errorHandler())

  return apiRouter
}

module.exports = apitRoutes
