const express = require('express')
const setupRoute = (api, authenticatePrivate) => {
  const router = express.Router()

  router.route('/sendMessage')
        .post(
            authenticatePrivate,
            api.chat.sendMessage
         )

  router.route('/getMessage')
         .post(
             authenticatePrivate,
             api.chat.getMessage
          )

  router.route('/getMessages')
         .get(
             authenticatePrivate,
             api.chat.getMessages
          )

  router.route('/markAsRead/:chatroom_id')
         .get(
             authenticatePrivate,
             api.chat.markAsRead
          )

  return router
}

module.exports = setupRoute
