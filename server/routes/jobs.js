const express = require('express')
const setupRoute = (api, authenticatePrivate, uploadMiddleware) => {
  const router = express.Router()

  router.route('/')
    .get(authenticatePrivate, api.job.find)
    .post(authenticatePrivate, api.job.create)

  router.route('/find-by-code').get(api.job.findByCode)
  router.route('/on-going').get(authenticatePrivate, api.job.findOnGoingJobs)
  router.route('/completed').get(authenticatePrivate, api.job.findCompletedJobs)
  router.route('/:id/find-by-code').get(api.job.findByCode)
  router.route('/:id/disapprove').delete(api.job.disapprove)
  router.route('/:id/return').post(authenticatePrivate, api.job.returnToSender)
  router.route('/:id/cancel').delete(api.job.cancel)

  router.route('/:id')
    .put(authenticatePrivate, api.job.update)
    .delete(authenticatePrivate, api.job.destroy)
    .get(authenticatePrivate, api.job.findById)

  router.route('/:id/set-starting-odometer').post(uploadMiddleware.array('files', 10), api.job.setStartingOdometer)
  router.route('/:id/set-ending-odometer').post(uploadMiddleware.array('files', 10), api.job.setEndingOdometer)

  router.route('/:id/update-status')
    .post(authenticatePrivate, api.job.updateStatus)

  router.route('/:id/directions')
    .get(api.job.getDirections)

  router.route('/:id/destination')
    .post(authenticatePrivate, api.job.createDestination)

  router.route('/:id/destination/add')
    .post(api.job.addDestination)

  router.route('/:id/destination/:destination_id')
    .delete(authenticatePrivate, api.job.destroyDestination)

  router.route('/:id/destination/:destination_id/cancel')
    .put(api.job.cancelRequestDestination)

  router.route('/:id/destination/:destination_id/approve')
    .put(authenticatePrivate, api.job.approveDestinationRequest)

  router.route('/:id/destination/:destination_id/reject')
    .put(authenticatePrivate, api.job.rejectDestinationRequest)

  router.route('/:id/destination/:destination_id/complete')
    .post(
      uploadMiddleware.array('files', 10),
      api.job.completeJobDestination
    )

  router.route('/:id/update-route-path')
    .post(api.job.updateJobRoutePath)

  router.route('/:id/review/route')
    .post(api.job.parseRouteReview)

  router.route('/maps/preview/all')
      .get(
        // authenticatePrivate,
        api.job.mapsPreviewAll)

  return router
}

module.exports = setupRoute
