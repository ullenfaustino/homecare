const express = require('express')
const setupRoute = (api, authenticatePrivate) => {
  const router = express.Router()

  router.route('/add/preferences')
              .post(
                  authenticatePrivate,
                  api.provider.addPreferences
               )

  router.route('/add/basicInfo')
             .post(
                 authenticatePrivate,
                 api.provider.addBasicInfo
              )

  router.route('/add/referenceExperience')
             .post(
                 authenticatePrivate,
                 api.provider.addReferencesExperience
              )

  router.route('/status/for-approval')
             .post(
                 authenticatePrivate,
                 api.provider.updateStatusForApproval
              )

  router.route('/status/approved')
             .post(
                 authenticatePrivate,
                 api.provider.updateStatusApproved
              )

  router.route('/find-available')
             .post(
                 authenticatePrivate,
                 api.provider.findAvailableProvider
              )

  router.route('/hire')
             .post(
                 authenticatePrivate,
                 api.provider.hireProvider
              )

  router.route('/hire/manual-matching')
             .post(
                 authenticatePrivate,
                 api.provider.hireProviderByMatching
              )

  router.route('/accept-job-offer')
             .post(
                 authenticatePrivate,
                 api.provider.acceptJobOffer
              )

  router.route('/find-near-job')
             .post(
                 authenticatePrivate,
                 api.provider.findNearJob
              )

  router.route('/apply-job-post')
             .post(
                 authenticatePrivate,
                 api.provider.applyOnJobPost
              )

  router.route('/finish-service')
             .post(
                 authenticatePrivate,
                 api.provider.finishService
              )

  return router
}

module.exports = setupRoute
