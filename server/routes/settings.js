const express = require('express')
const setupRoute = (api, authenticatePrivate) => {
  const router = express.Router()

  router.route('/generic')
              .post(
                  authenticatePrivate,
                  api.settings.setGenericSettings
               )

  router.route('/generic/delete')
               .post(
                   authenticatePrivate,
                   api.settings.deleteGenericSettings
                )

  return router
}

module.exports = setupRoute
