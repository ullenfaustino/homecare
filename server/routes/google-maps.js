const express = require('express')
const setupRoute = (api, authenticatePrivate) => {
  const router = express.Router()

  router.route('/autocomplete').get(api.googleMaps.autocomplete)
  router.route('/reverse/geocode').post(api.googleMaps.reverseGeocoder)
  router.route('/testautocomplete').post(api.googleMaps.testautocomplete)

  return router
}

module.exports = setupRoute
