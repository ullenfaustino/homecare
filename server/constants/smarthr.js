const GET_ALL_DEPARTMENT = 'GET_ALL_DEPARTMENT'
const GET_ALL_DEPARTMENT_CACHE_TIME = 300000 // 5mins
const GET_ALL_EMPLOYEE = 'GET_ALL_EMPLOYEE'
const GET_ALL_EMPLOYEE_CACHE_TIME = 600000 // 10mins
const USER_DEFAULT_PICKS = ['id', 'employeenumber', 'imgurl',
  'firstname', 'lastname', 'fullname',
  'mobilenumber', 'emailaddress', 'supervisorid', 'supervisorname']

module.exports = {
  GET_ALL_DEPARTMENT,
  GET_ALL_DEPARTMENT_CACHE_TIME,

  GET_ALL_EMPLOYEE,
  GET_ALL_EMPLOYEE_CACHE_TIME,
  USER_DEFAULT_PICKS
}
