const JOB_TYPE_DELIVERY = 1
const JOB_TYPE_INTERNAL = 2
const JOB_TYPE_CLIENT = 3

const JOB_STATUS_PENDING = 2
const JOB_STATUS_FOR_APPROVAL = 1
const JOB_STATUS_FOR_LOGISTICS = 3
const JOB_STATUS_FOR_REVIEW = 4
const JOB_STATUS_APPROVED = 5

const JOB_STATUS_STARTED = 6
const JOB_STATUS_FINISHED = 7

const JOB_SALES_FOR_REVIEW = 3
const JOB_SALES_STATUS_FOR_APPROVAL = 4
const JOB_SALES_STATUS_APPROVED = 5

const JOB_DEFAULT_STATUS_ID = 2

const JOB_WAREHOUSE = 1
const JOB_SALES = 2

module.exports = {
  JOB_TYPE_DELIVERY,
  JOB_TYPE_INTERNAL,
  JOB_TYPE_CLIENT,

  JOB_STATUS_PENDING,
  JOB_STATUS_FOR_APPROVAL,
  JOB_STATUS_FOR_LOGISTICS,
  JOB_STATUS_FOR_REVIEW,
  JOB_STATUS_APPROVED,

  JOB_STATUS_STARTED,
  JOB_STATUS_FINISHED,

  JOB_SALES_FOR_REVIEW,
  JOB_SALES_STATUS_FOR_APPROVAL,
  JOB_SALES_STATUS_APPROVED,

  JOB_DEFAULT_STATUS_ID,
  JOB_WAREHOUSE,
  JOB_SALES
}
