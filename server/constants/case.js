const CASE_TYPES = [
  'accident report',
  'preventive maintenance'
]
const ACCIDENT_CATEGORY = [
  'self inflict',
  'minor traffic accident',
  'major traffic accident'
]
const TRAFFIC_ACCIDENT_TYPE = [
  'fatal',
  'head on',
  'sides wiped same direction',
  'non fatal',
  'rear end',
  'damage of property',
  'angle',
  'sides wiped opposite direction'
]

module.exports = {
  CASE_TYPES,
  ACCIDENT_CATEGORY,
  TRAFFIC_ACCIDENT_TYPE
}
