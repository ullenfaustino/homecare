const path = require('path')
const isProd = process.env.SMARTHR_ENV === 'production'
const SMARTHR_URL = isProd
  ? 'http://smarthr-api.vibalgroup.com'
  : 'http://dev-api-smarthr.vibalgroup.com'
const COREHR_URL = isProd
  ? 'http://profile-api.vibalgroup.com'
  // : 'http://dev-api-core.vibalgroup.com'
  : 'http://dev-profile-api.vibalgroup.com/'

const VPN_LOGIN_URL = isProd
  ? 'http://dev-profile-api.vibalgroup.com/api/Authenticate/BusinessPartnerAccount'
  : 'http://dev-profile-api.vibalgroup.com/api/Authenticate/BusinessPartnerAccount'

module.exports = {
  PROJECT_DIR: __dirname,
  UPLOADS_DIR: path.join(__dirname, '../uploads'),
  SMARTHR_URL: SMARTHR_URL,
  COREHR_URL: COREHR_URL,
  VPN_LOGIN_URL: VPN_LOGIN_URL,
  PARTNER_URL: 'http://api-partner.vibalgroup.com/', // 'http://dev-api-partner.vibalgroup.com/'
  IMAGE_URL: 'http://cdn.virtualidad.ph/ves/smarthr/profile_image/',
  PERMISSIONS: {
    _40: [
      'contract:create',
      'contract:edit',
      'contract:view',
      'template:view'],
    _30: [
      'contract:create',
      'contract:edit',
      'contract:view',
      'template:view'],
    _20: [
      'contract:create',
      'contract:edit',
      'contract:delete',
      'contract:view',
      'template:create',
      'template:edit',
      'template:delete',
      'template:view'],
    _10: [
      'contract:create',
      'contract:edit',
      'contract:delete',
      'contract:view',
      'template:create',
      'template:edit',
      'template:delete',
      'template:view']

  },
  DEPARTMENTS: {
    PUBLISHING : 'Publishing',
    WAREHOUSE : 'Warehouse',
    SALES: 'Sales',
    ACCOUNTING: 'Accounting',
    ICT: 'Information and Communications Technology',
    ADMIN: 'Administration',
    MOTORPOOL: 'Motorpool',
    FINANCE: 'Finance',
    GOV_SALES: 'Govenment Sales',
    MULTIMEDIA: 'Multimedia',
    CREATIVES: 'Creative Service Department',
    IT: 'Information Technology',
    HR: 'HR Department',
    ACQUISITIONS: 'Acquisitions',
    GS: 'General Services',
    SECURITY: 'Security',
    SOFTWAREENG: 'Software Engineering',
    PRESS: 'Press',
    CEBUADMIN: 'Cebu - Admin',
    EXECOMMITTEE: 'Executive Committee',
    PRODUCTION: 'Production',
    MARKETING: 'Marketing',
    SALESSERVICE: 'Sales Service',
    MARKETEVENTS: 'Marketing - Events',
    BINDERY: 'Bindery',
    PRODUCTDEVELOPMENT: 'Product Development'
  },
  PROJECT_CODE: 'VES_017',
  MIMETYPE: {
    image: ['image/jpg', 'image/jpeg', 'image/png'],
    audio: ['audio/mpeg', 'audio/mp4'],
    video: ['video/mp4'],
    pdf: ['application/x-pdf', 'application/pdf']
  },
  SMARTHR_CACHE_USER_TIME: 50000,
  // GOOGLE_API_KEY: 'AIzaSyD5u1pdUPm1U1rg78yT5lQVoIr1VTX4l64'
  GOOGLE_API_KEY: 'AIzaSyBLgVYO749Y2E73IROmX33_KylGHp8JEdk'
}
