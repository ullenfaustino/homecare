const path = require('path')
const Promise = require('bluebird')
const _ = require('lodash')
const fs = require('fs')

const constants = require('../constants')

const ADMIN_URL = '/adminx'

exports.adminUrl = (url) => path.join(ADMIN_URL, url)
exports.ADMIN_URL = ADMIN_URL

exports.apiResponse = (apiData, status = 200) => {
  if (apiData && apiData.data) {
    return Object.assign({}, apiData, {
      status
    })
  } else {
    return {
      data: apiData,
      status
    }
  }
}

exports.addFilenameSuffix = (filePath, str) => {
  return filePath.replace(/(\.[\w\d_-]+)$/i, str + '$1')
}

exports.getFileType = (mimetype) => {
  if (constants.MIMETYPE.image.indexOf(mimetype) >= 0) {
    return 'image'
  }

  if (constants.MIMETYPE.audio.indexOf(mimetype) >= 0) {
    return 'audio'
  }

  if (constants.MIMETYPE.video.indexOf(mimetype) >= 0) {
    return 'video'
  }

  if (constants.MIMETYPE.pdf.indexOf(mimetype) >= 0) {
    return 'pdf'
  }
}

// Get filename from the path

function getFileNameFromPath (path) {
  var index = path.lastIndexOf('/')
  var extIndex = path.lastIndexOf('.')
  return path.substring(index, extIndex)
}
