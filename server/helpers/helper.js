const Promise = require('bluebird')
const dataProvider = require('../models')
const _ = require('lodash')
const utils = require('../api/utils')

const formatUser = (user) => {
  const adminOption = {}
  const seekerOption = utils.seekerOption
  const providerOption = utils.providerOption
  return new Promise((resolve, reject) => {
    dataProvider.User.query((qb) => {
      qb.where('id', user.id)
    }).fetch()
    .then((mUser) => {
      if (!mUser) {
        return reject('user not found')
      }

      return mUser.fetch((mUser.get('user_type') == 0 ? adminOption : ((mUser.get('user_type') == 1) ? providerOption : seekerOption)))
      .then((_mUser) => {
        resolve(_mUser)
      })
      .catch((err) => reject(err))
    })
    .catch((err) => reject(err))
  })
}

module.exports = {
  formatUser
}
