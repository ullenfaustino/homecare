const moment = require('moment')
const redis = require('socket.io-redis')

const io = require('socket.io')()
// const redisConf = require('../config/redis').__conf
const dataProvider = require('../models')

function updateJobLocation (payload) {
  const {
    id,
    lat,
    lng,
    log_at
  } = payload

  const jobLocationData = {
    job_id: id,
    log_at: moment(log_at).format('YYYY-MM-DD HH:mm:ss'),
    lat,
    lng
  }

  return dataProvider.JobLocation.forge().query((qb) => {
    // qb.where(jobLocationData)
    qb.where('job_id', id)
    qb.where('lat', lat)
    qb.where('lng', lng)
  }).fetch().then((foundJobLocation) => {
    if (!foundJobLocation) {
      return dataProvider.JobLocation.forge({
        job_id: id,
        log_at: moment(log_at).format('YYYY-MM-DD HH:mm:ss'),
        lat,
        lng
      }).save()
    }

    return foundJobLocation
  })
}

io.adapter(redis(redisConf))
.use(function (socket, next) {
  var handshakeData = socket.request
  // console.log('handshakeData', handshakeData)
  // make sure the handshake data looks good as before
  // if error do this:
    // next(new Error('not authorized'));
  // else just call next
  next()
})
.on('connection', function (socket) {
  // console.log('socket_connection_id', socket.id)
  // socket.on('job_update_destination', (payload) => {
  //   updateJobLocation(payload).then(() => {
  //     console.log('IO index job updated')
  //   })
  //   dataProvider.Job.forge({ id: payload.id })
  //   .fetch({
  //     withRelated: [
  //       'destinations',
  //       'destinations.files',
  //       'vehicleModel',
  //       'vehicleModel.attr',
  //       'vehicle',
  //       'driver',
  //       'vehicle.drivers',
  //       'type',
  //       'started_by_file',
  //       'end_by_file'
  //     ]
  //   }).then((job) => {
  //     socket.broadcast.emit('job_update_destination', payload)
  //     socket.broadcast.emit('job_vehicle_location', Object.assign({}, payload, {
  //       job,
  //       socket_id: socket.id
  //     }))
  //   })
  // })
  //
  // socket.on('disconnect', () => {
  //   console.log('client disconnected ', socket.id)
  //   socket.broadcast.emit('job_vehicle_disconnected', { socket_id: socket.id })
  // })

  // try {
  //   const user = socket.request.session.passport.user
  //   if (user) {
  //     socket.join(user.id)
  //     socket.on('disconnect', () => {
  //       socket.leave(user.id)
  //     })
  //   }
  // } catch (e) {}
}).on('disconnect', function (socket) {})

module.exports = io
