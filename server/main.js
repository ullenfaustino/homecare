const express = require('express')
const debug = require('debug')('app:server')
const webpack = require('webpack')
const webpackConfig = require('../build/webpack.config')
const config = require('../config')
const compress = require('compression')
const bodyParser = require('body-parser')
const dotenv = require('dotenv')
const exphbs = require('express-handlebars')
const Promise = require('bluebird')
const path = require('path')
const url = require('url')
const fs = require('fs')
const moment = require('moment')
const numeral = require('numeral')
const expressValidator = require('express-validator')

const api = require('./api')
// const setupSchedule = require('./schedule')
const setupQueue = require('./mailer')
const constants = require('./constants')
const helpers = require('./helpers')
const cors = require('cors')

/**
 * Load environment variables from .env file, where API keys and passwords are configured.
 */
dotenv.load({ path: __dirname + '/../.env' })

const app = express()
const paths = config.utils_paths

const hbs = exphbs.create({
  defaultLayout: 'main',
  layoutsDir: __dirname + '/views/layouts',
  helpers: {
    ifeq: function (a, b, options) {
      if (a === b) {
        return options.fn(this)
      }
      return options.inverse(this)
    },
    toJSON : function (object) {
      return JSON.stringify(object)
    },
    formatNumber: (item) => {
      return numeral(item).format('0,0.00')
    },
    moment : function (date) {
      return moment(date).format('LL')
    },
    daysLeft: function (date) {
      const now = moment(new Date())
      const mdate = moment(date)
      const left = date > now ? mdate.diff(now, 'days') : 0
      return left
    }
  }
})

app.engine('handlebars', hbs.engine)
app.set('views', __dirname + '/views')
app.set('layouts', __dirname + '/views')
app.set('view engine', 'handlebars')

// Apply gzip compression
app.use(compress())
app.use(bodyParser.json({ limit: '50mb' }))
app.use(bodyParser.urlencoded({ extended: true, limit: '50mb' }))
app.use(expressValidator())

app.use(express.static(constants.UPLOADS_DIR, { maxAge: 31557600000 }))
app.use('*', cors())

const middleware = require('./middleware')

middleware(app)
// setupSchedule(app)
setupQueue(app)

// This rewrites all routes requests to the root /index.html file
// (ignoring file requests). If you want to implement universal
// rendering, you'll want to remove this middleware.
app.use(require('connect-history-api-fallback')())

// ------------------------------------
// Apply Webpack HMR Middleware
// ------------------------------------
if (config.env === 'development') {
  const compiler = webpack(webpackConfig)

  debug('Enable webpack dev and HMR middleware')
  app.use(require('webpack-dev-middleware')(compiler, {
    publicPath  : webpackConfig.output.publicPath,
    contentBase : paths.client(),
    hot         : true,
    quiet       : config.compiler_quiet,
    noInfo      : config.compiler_quiet,
    lazy        : false,
    stats       : config.compiler_stats
  }))
  app.use(require('webpack-hot-middleware')(compiler))

  // Serve static assets from ~/src/static since Webpack is unaware of
  // these files. This middleware doesn't need to be enabled outside
  // of development since this directory will be copied into ~/dist
  // when the application is compiled.
  app.use(express.static(paths.client('static')))
} else {
  debug(
    'Server is being run outside of live development mode, meaning it will ' +
    'only serve the compiled application bundle in ~/dist. Generally you ' +
    'do not need an application server for this and can instead use a web ' +
    'server such as nginx to serve your static files. See the "deployment" ' +
    'section in the README for more information on deployment strategies.'
  )

  // Serving ~/dist by default. Ideally these files should be served by
  // the web server and not the app server, but this helps to demo the
  // server in production.
  app.use(express.static(paths.dist()))
}

module.exports = app
