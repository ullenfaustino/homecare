const Queue = require('bull')
// var globe = require('globe-connect');
const nodemailer = require('nodemailer')
const Promise = require('bluebird')
const _ = require('lodash')
const debug = require('debug')('app:queue')

const serverUtils = require('./utils')
const { ACCOUNT_CONFIRMATION } = require('./constants/queue')
const dataProvider = require('./models')
// const io = require('./io')
// const db = require('./db')
// const redisConf = require('./config/redis').__conf
const TEMP_CONTACTS = 'ullen.d.faustino@gmail.com'

const setupQueue = (app) => {
  debug('Bull Queue Started -> Setup Queue')

  // helper function for generating email template
  const generateEmailTemplate = (viewPath, data) => {
    const __data = {
      layout: 'email',
      baseUrl: process.env.URL
    }

    return new Promise(function (resolve, reject) {
      app.render(viewPath, _.merge(__data, data), (err, html) => {
        if (err) {
          reject(err)
        }
        resolve(html)
      })
    })
  }

  const accountConfirmQueuer = (data) => {
    const { user = {}, orig_password, activation_code } = data

    try {
      const account_type = (user.user_type == 1) ? 'Care Provider' : 'Care Seeker'
      return generateEmailTemplate(
        'email/account-confirmation',
        { user, orig_password, activation_code, account_type }
      ).then((html) => {
        debug('Email Sending')

        serverUtils.sendMail({
          to: getUserEmail(user.email),
          subject: '[Home Care Network] Account Verification',
          html: html
        }).then((mailResponse) => {
          debug(`mail queue process done`)
        })
      })
    } catch (e) {
      debug('Email Sending error: ${e}')
    }
  }

  module.exports.accountConfirmation_mailer = accountConfirmQueuer
}

function getUserEmail (email) {
  return process.env.NODE_ENV === 'production' ? email : TEMP_CONTACTS
  // return email
}

module.exports = setupQueue
