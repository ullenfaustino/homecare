const bookshelf = require('../config/bookshelf')

const Ratings = bookshelf.Model.extend({
  tableName: 'ratings',
  hasTimestamps: true,
  softDelete: true,

  recipient () {
    return this.hasOne('Recipients', 'id', 'recipient_id')
  }
})

module.exports = bookshelf.model('Ratings', Ratings)
