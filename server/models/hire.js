const bookshelf = require('../config/bookshelf')

const Hire = bookshelf.Model.extend({
  tableName: 'hire',
  hasTimestamps: true,
  softDelete: false,

  provider () {
    return this.hasOne('User', 'id', 'provider_id')
  }
})

module.exports = bookshelf.model('Hire', Hire)
