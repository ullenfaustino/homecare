const bookshelf = require('../config/bookshelf')

const LkupWorkSchedules = bookshelf.Model.extend({
  tableName: 'lkup_work_schedules',
  hasTimestamps: true,
  softDelete: true,

  providerSelected () {
    return this.hasMany('ProvidersWorkSchedules', 'lkup_work_schedules_id', 'id')
  }
})

module.exports = bookshelf.model('LkupWorkSchedules', LkupWorkSchedules)
