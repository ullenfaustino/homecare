const bookshelf = require('../config/bookshelf')

const RecipientsGeneralStatus = bookshelf.Model.extend({
  tableName: 'recipients_general_status',
  hasTimestamps: true,
  softDelete: false,

  value () {
    return this.hasOne('LkupGeneralStatus', 'id', 'lkup_general_status_id')
  }
})

module.exports = bookshelf.model('RecipientsGeneralStatus', RecipientsGeneralStatus)
