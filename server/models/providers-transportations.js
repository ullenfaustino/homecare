const bookshelf = require('../config/bookshelf')

const ProvidersTransportations = bookshelf.Model.extend({
  tableName: 'providers_transportations',
  hasTimestamps: true,
  softDelete: false,

  value () {
    return this.hasOne('LkupTransportations', 'id', 'lkup_transportations_id')
  }
})

module.exports = bookshelf.model('ProvidersTransportations', ProvidersTransportations)
