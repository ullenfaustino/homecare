const bookshelf = require('../config/bookshelf')

const ProvidersServicesPerform = bookshelf.Model.extend({
  tableName: 'providers_services_perform',
  hasTimestamps: true,
  softDelete: false,

  value () {
    return this.hasOne('LkupServicesPerform', 'id', 'lkup_services_perform_id')
  }
})

module.exports = bookshelf.model('ProvidersServicesPerform', ProvidersServicesPerform)
