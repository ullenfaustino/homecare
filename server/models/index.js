const _ = require('lodash')

/**
 * Expose all models
 */
// let exports = module.exports;
const models = [
  'lkup-client-preferences',
  'lkup-work-restrictions',
  'lkup-work-schedules',
  'lkup-languages',
  'lkup-transportations',
  'lkup-health-conditions',
  'lkup-services-experience',
  'lkup-services-perform',
  'lkup-type-care',
  'lkup-general-status',
  'lkup-medical-conditions',
  'providers-availability',
  'providers-location',
  'providers-client-preferences',
  'providers-work-restrictions',
  'providers-work-schedules',
  'providers-languages',
  'providers-salary-rate',
  'providers-transportations',
  'providers-character-references',
  'providers-health-conditions',
  'providers-services-perform',
  'providers-services-experience',
  'providers-files',
  'recipients-days',
  'recipients-general-status',
  'recipients-medical-conditions',
  'recipients-location',
  'recipients',
  'seekers-days',
  'seekers-type-care',
  'seekers-search-preferences',
  'file-uploads',
  'hire',
  'JobPost',
  'JobPostApplicant',
  'ratings',
  'ChatRoom',
  'Messages',
  'user'
]

function init () {
  // exports.Base = require('./base')

  models.forEach((name) => {
    module.exports[_.startCase(name).replace(/\s+/ig, '')] = require(`./${name}`)
  })
}

init()
