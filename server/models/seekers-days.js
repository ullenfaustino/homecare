const bookshelf = require('../config/bookshelf')

const SeekersDays = bookshelf.Model.extend({
  tableName: 'seekers_days',
  hasTimestamps: true,
  softDelete: false
})

module.exports = bookshelf.model('SeekersDays', SeekersDays)
