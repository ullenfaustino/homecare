const bookshelf = require('../config/bookshelf')

const ProvidersWorkSchedules = bookshelf.Model.extend({
  tableName: 'providers_work_schedules',
  hasTimestamps: true,
  softDelete: false,

  value () {
    return this.hasOne('LkupWorkSchedules', 'id', 'lkup_work_schedules_id')
  }
})

module.exports = bookshelf.model('ProvidersWorkSchedules', ProvidersWorkSchedules)
