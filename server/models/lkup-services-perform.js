const bookshelf = require('../config/bookshelf')

const LkupServicesPerform = bookshelf.Model.extend({
  tableName: 'lkup_services_perform',
  hasTimestamps: true,
  softDelete: true,

  providerSelected () {
    return this.hasMany('ProvidersServicesPerform', 'lkup_services_perform_id', 'id')
  }
})

module.exports = bookshelf.model('LkupServicesPerform', LkupServicesPerform)
