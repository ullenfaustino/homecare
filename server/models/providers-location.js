const bookshelf = require('../config/bookshelf')

const ProvidersLocation = bookshelf.Model.extend({
  tableName: 'providers_location',
  hasTimestamps: true,
  softDelete: true
})

module.exports = bookshelf.model('ProvidersLocation', ProvidersLocation)
