const bookshelf = require('../config/bookshelf')

const ProvidersFiles = bookshelf.Model.extend({
  tableName: 'providers_files',
  hasTimestamps: true,
  softDelete: false,

  file () {
    return this.hasOne('FileUploads', 'id', 'file_id')
  }
})

module.exports = bookshelf.model('ProvidersFiles', ProvidersFiles)
