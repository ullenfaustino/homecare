const bookshelf = require('../config/bookshelf')

const ProvidersServicesExperience = bookshelf.Model.extend({
  tableName: 'providers_services_experience',
  hasTimestamps: true,
  softDelete: false,

  value () {
    return this.hasOne('LkupServicesExperience', 'id', 'lkup_services_experience_id')
  }
})

module.exports = bookshelf.model('ProvidersServicesExperience', ProvidersServicesExperience)
