const bookshelf = require('../config/bookshelf')

const ProvidersSalaryRate = bookshelf.Model.extend({
  tableName: 'providers_salary_rate',
  hasTimestamps: true,
  softDelete: false
})

module.exports = bookshelf.model('ProvidersSalaryRate', ProvidersSalaryRate)
