const bookshelf = require('../config/bookshelf')

const FileUploads = bookshelf.Model.extend({
  tableName: 'file_uploads',
  hasTimestamps: true,
  softDelete: false
})

module.exports = bookshelf.model('FileUploads', FileUploads)
