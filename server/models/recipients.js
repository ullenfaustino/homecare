const bookshelf = require('../config/bookshelf')

const Recipients = bookshelf.Model.extend({
  tableName: 'recipients',
  hasTimestamps: true,
  softDelete: true,

  general_status () {
    return this.hasMany('RecipientsGeneralStatus', 'recipient_id', 'id')
  },

  medical_conditions () {
    return this.hasMany('RecipientsMedicalConditions', 'recipient_id', 'id')
  },

  days () {
    return this.hasMany('RecipientsDays', 'recipient_id', 'id')
  },

  location () {
    return this.hasOne('RecipientsLocation', 'recipient_id', 'id')
  },

  hired () {
    return this.hasOne('Hire', 'recipient_id', 'id').query((qb) => {
      qb.where('status', '<', 2)
    })
  }
})

module.exports = bookshelf.model('Recipients', Recipients)
