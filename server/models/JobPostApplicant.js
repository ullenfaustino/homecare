const bookshelf = require('../config/bookshelf')

const JobPostApplicant = bookshelf.Model.extend({
  tableName: 'job_post_applicant',
  hasTimestamps: true,
  softDelete: true,

  job_post () {
    return this.hasOne('JobPost', 'id', 'job_post_id')
  },

  provider () {
    return this.hasOne('User', 'id', 'provider_id')
  }
})

module.exports = bookshelf.model('JobPostApplicant', JobPostApplicant)
