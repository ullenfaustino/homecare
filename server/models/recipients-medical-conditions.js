const bookshelf = require('../config/bookshelf')

const RecipientsMedicalConditions = bookshelf.Model.extend({
  tableName: 'recipients_medical_conditions',
  hasTimestamps: true,
  softDelete: false,

  value () {
    return this.hasOne('LkupMedicalConditions', 'id', 'lkup_medical_conditions_id')
  }
})

module.exports = bookshelf.model('RecipientsMedicalConditions', RecipientsMedicalConditions)
