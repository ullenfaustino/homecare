const bookshelf = require('../config/bookshelf')

const ProvidersCharacterReferences = bookshelf.Model.extend({
  tableName: 'providers_character_references',
  hasTimestamps: true,
  softDelete: false
})

module.exports = bookshelf.model('ProvidersCharacterReferences', ProvidersCharacterReferences)
