const bookshelf = require('../config/bookshelf')

const LkupWorkRestrictions = bookshelf.Model.extend({
  tableName: 'lkup_work_restrictions',
  hasTimestamps: true,
  softDelete: true,

  providerSelected () {
    return this.hasMany('ProvidersWorkRestrictions', 'lkup_work_restrictions_id', 'id')
  }
})

module.exports = bookshelf.model('LkupWorkRestrictions', LkupWorkRestrictions)
