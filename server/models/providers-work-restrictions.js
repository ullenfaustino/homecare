const bookshelf = require('../config/bookshelf')

const ProvidersWorkRestrictions = bookshelf.Model.extend({
  tableName: 'providers_work_restrictions',
  hasTimestamps: true,
  softDelete: false,

  value () {
    return this.hasOne('LkupWorkRestrictions', 'id', 'lkup_work_restrictions_id')
  }
})

module.exports = bookshelf.model('ProvidersWorkRestrictions', ProvidersWorkRestrictions)
