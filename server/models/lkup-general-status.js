const bookshelf = require('../config/bookshelf')

const LkupGeneralStatus = bookshelf.Model.extend({
  tableName: 'lkup_general_status',
  hasTimestamps: true,
  softDelete: true,

  recipientSelected () {
    return this.hasMany('RecipientsGeneralStatus', 'lkup_general_status_id', 'id')
  }
})

module.exports = bookshelf.model('LkupGeneralStatus', LkupGeneralStatus)
