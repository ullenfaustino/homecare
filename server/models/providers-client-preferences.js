const bookshelf = require('../config/bookshelf')

const ProvidersClientPreferences = bookshelf.Model.extend({
  tableName: 'providers_client_preferences',
  hasTimestamps: true,
  softDelete: false,

  value () {
    return this.hasOne('LkupClientPreferences', 'id', 'lkup_client_preferences_id')
  }
})

module.exports = bookshelf.model('ProvidersClientPreferences', ProvidersClientPreferences)
