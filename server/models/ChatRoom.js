const bookshelf = require('../config/bookshelf')

const ChatRoom = bookshelf.Model.extend({
  tableName: 'chatroom',
  hasTimestamps: true,
  softDelete: true,

  messages () {
    return this.hasMany('Messages', 'chatroom_id', 'id')
  },

  recipient () {
    return this.hasOne('User', 'id', 'recipient_id')
  },

  sender () {
    return this.hasOne('User', 'id', 'sender_id')
  }
})

module.exports = bookshelf.model('ChatRoom', ChatRoom)
