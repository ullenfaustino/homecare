const bookshelf = require('../config/bookshelf')

const ProvidersAvailability = bookshelf.Model.extend({
  tableName: 'providers_availability',
  hasTimestamps: true,
  softDelete: false
})

module.exports = bookshelf.model('ProvidersAvailability', ProvidersAvailability)
