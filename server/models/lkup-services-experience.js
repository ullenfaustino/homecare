const bookshelf = require('../config/bookshelf')

const LkupServicesExperience = bookshelf.Model.extend({
  tableName: 'lkup_services_experience',
  hasTimestamps: true,
  softDelete: true,

  providerSelected () {
    return this.hasMany('ProvidersServicesExperience', 'lkup_services_experience_id', 'id')
  }
})

module.exports = bookshelf.model('LkupServicesExperience', LkupServicesExperience)
