const bookshelf = require('../config/bookshelf')

const SeekersTypeCare = bookshelf.Model.extend({
  tableName: 'seekers_type_care',
  hasTimestamps: true,
  softDelete: false,

  value () {
    return this.hasOne('LkupTypeCare', 'id', 'lkup_type_care_id')
  }
})

module.exports = bookshelf.model('SeekersTypeCare', SeekersTypeCare)
