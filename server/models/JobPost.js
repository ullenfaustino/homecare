const bookshelf = require('../config/bookshelf')

const JobPost = bookshelf.Model.extend({
  tableName: 'job_post',
  hasTimestamps: true,
  softDelete: true,

  recipient () {
    return this.hasOne('Recipients', 'id', 'recipient_id')
  },

  seeker () {
    return this.hasOne('User', 'id', 'seeker_id')
  },

  applicants () {
    return this.hasMany('JobPostApplicant', 'job_post_id', 'id')
  }
})

module.exports = bookshelf.model('JobPost', JobPost)
