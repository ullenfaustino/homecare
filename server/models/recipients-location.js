const bookshelf = require('../config/bookshelf')

const RecipientsLocation = bookshelf.Model.extend({
  tableName: 'recipients_location',
  hasTimestamps: true,
  softDelete: true
})

module.exports = bookshelf.model('RecipientsLocation', RecipientsLocation)
