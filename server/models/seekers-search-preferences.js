const bookshelf = require('../config/bookshelf')

const SeekersSearchPreferences = bookshelf.Model.extend({
  tableName: 'seekers_search_preferences',
  hasTimestamps: true,
  softDelete: false
})

module.exports = bookshelf.model('SeekersSearchPreferences', SeekersSearchPreferences)
