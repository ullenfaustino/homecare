const bookshelf = require('../config/bookshelf')

const LkupClientPreferences = bookshelf.Model.extend({
  tableName: 'lkup_client_preferences',
  hasTimestamps: true,
  softDelete: true,

  providerSelected () {
    return this.hasMany('ProvidersClientPreferences', 'lkup_client_preferences_id', 'id')
  }
})

module.exports = bookshelf.model('LkupClientPreferences', LkupClientPreferences)
