const bookshelf = require('../config/bookshelf')

const User = bookshelf.Model.extend({
  tableName: 'users',
  hasTimestamps: true,
  softDelete: true,

  toJSON (options) {
    const _options = options || {}

    let attrs = bookshelf.Model.prototype.toJSON.call(this, _options)

    // remove password hash for security reasons
    delete attrs.password
    delete attrs.passwordResetToken
    delete attrs.passwordResetExpires

    return attrs
  },

  location () {
    return this.hasOne('ProvidersLocation', 'user_id', 'id')
  },

  client_preferences () {
    return this.hasMany('ProvidersClientPreferences', 'user_id', 'id')
  },

  work_restrictions () {
    return this.hasMany('ProvidersWorkRestrictions', 'user_id', 'id')
  },

  work_schedules () {
    return this.hasMany('ProvidersWorkSchedules', 'user_id', 'id')
  },

  availability () {
    return this.hasMany('ProvidersAvailability', 'user_id', 'id')
  },

  salary_rate () {
    return this.hasOne('ProvidersSalaryRate', 'user_id', 'id')
  },

  transportations () {
    return this.hasMany('ProvidersTransportations', 'user_id', 'id')
  },

  languages () {
    return this.hasMany('ProvidersLanguages', 'user_id', 'id')
  },

  character_references () {
    return this.hasOne('ProvidersCharacterReferences', 'user_id', 'id')
  },

  health_conditions () {
    return this.hasMany('ProvidersHealthConditions', 'user_id', 'id')
  },

  services_perform () {
    return this.hasMany('ProvidersServicesPerform', 'user_id', 'id')
  },

  services_experience () {
    return this.hasMany('ProvidersServicesExperience', 'user_id', 'id')
  },

  files () {
    return this.hasMany('ProvidersFiles', 'user_id', 'id')
  },

  seekers_search_preferences () {
    return this.hasOne('SeekersSearchPreferences', 'user_id', 'id')
  },

  seekers_days_pref () {
    return this.hasMany('SeekersDays', 'user_id', 'id')
  },

  seekers_type_care () {
    return this.hasMany('SeekersTypeCare', 'user_id', 'id')
  },

  recipients () {
    return this.hasMany('Recipients', 'user_id', 'id')
  },

  hired () {
    return this.hasMany('User').through('Hire', 'id', 'seeker_id', 'provider_id').query((qb) => {
      qb.select(['users.*', 'hire.status as hiring_status', 'hire.id as hiring_id', 'hire.job_offer_date', 'hire.hired_date', 'hire.finished_date'])
    })
  },

  recipient () {
    return this.hasOne('Recipients').through('Hire', 'id', 'provider_id', 'recipient_id')
  },

  client () {
    return this.hasMany('Recipients').through('Hire', 'id', 'provider_id', 'recipient_id').query((qb) => {
      qb.select(['recipients.*', 'hire.status as hiring_status', 'hire.id as hiring_id', 'hire.job_offer_date', 'hire.hired_date', 'hire.finished_date'])
    })
  },

  postedJobs () {
    return this.hasMany('JobPost', 'seeker_id', 'id')
  },

  job_applications () {
    return this.hasMany('JobPostApplicant', 'provider_id', 'id')
  },

  ratings () {
    return this.hasMany('Ratings', 'provider_id', 'id')
  }

})

module.exports = bookshelf.model('User', User)
