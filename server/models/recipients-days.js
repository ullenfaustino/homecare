const bookshelf = require('../config/bookshelf')

const RecipientsDays = bookshelf.Model.extend({
  tableName: 'recipient_days',
  hasTimestamps: true,
  softDelete: false
})

module.exports = bookshelf.model('RecipientsDays', RecipientsDays)
