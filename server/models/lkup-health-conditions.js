const bookshelf = require('../config/bookshelf')

const LkupHealthConditions = bookshelf.Model.extend({
  tableName: 'lkup_health_conditions',
  hasTimestamps: true,
  softDelete: true,

  providerSelected () {
    return this.hasMany('ProvidersHealthConditions', 'lkup_health_conditions_id', 'id')
  }
})

module.exports = bookshelf.model('LkupHealthConditions', LkupHealthConditions)
