const bookshelf = require('../config/bookshelf')

const LkupLanguages = bookshelf.Model.extend({
  tableName: 'lkup_languages',
  hasTimestamps: true,
  softDelete: true
})

module.exports = bookshelf.model('LkupLanguages', LkupLanguages)
