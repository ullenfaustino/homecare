const bookshelf = require('../config/bookshelf')

const LkupTransportations = bookshelf.Model.extend({
  tableName: 'lkup_transportations',
  hasTimestamps: true,
  softDelete: true,

  providerSelected () {
    return this.hasMany('ProvidersTransportations', 'lkup_transportations_id', 'id')
  }
})

module.exports = bookshelf.model('LkupTransportations', LkupTransportations)
