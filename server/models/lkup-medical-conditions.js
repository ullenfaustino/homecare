const bookshelf = require('../config/bookshelf')

const LkupMedicalConditions = bookshelf.Model.extend({
  tableName: 'lkup_medical_conditions',
  hasTimestamps: true,
  softDelete: true,

  recipientSelected () {
    return this.hasMany('RecipientsMedicalConditions', 'lkup_medical_conditions_id', 'id')
  }
})

module.exports = bookshelf.model('LkupMedicalConditions', LkupMedicalConditions)
