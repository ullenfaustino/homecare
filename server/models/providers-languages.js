const bookshelf = require('../config/bookshelf')

const ProvidersLanguages = bookshelf.Model.extend({
  tableName: 'providers_languages',
  hasTimestamps: true,
  softDelete: false,

  value () {
    return this.hasOne('LkupLanguages', 'id', 'lkup_languages_id')
  }
})

module.exports = bookshelf.model('ProvidersLanguages', ProvidersLanguages)
