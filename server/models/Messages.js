const bookshelf = require('../config/bookshelf')

const Messages = bookshelf.Model.extend({
  tableName: 'messages',
  hasTimestamps: true,
  softDelete: true,

  sender () {
    return this.hasOne('User', 'id', 'sender_id')
  }
})

module.exports = bookshelf.model('Messages', Messages)
