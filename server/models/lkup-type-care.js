const bookshelf = require('../config/bookshelf')

const LkupTypeCare = bookshelf.Model.extend({
  tableName: 'lkup_type_care',
  hasTimestamps: true,
  softDelete: true,

  providerSelected () {
    return this.hasMany('SeekersTypeCare', 'lkup_type_care_id', 'id')
  }
})

module.exports = bookshelf.model('LkupTypeCare', LkupTypeCare)
