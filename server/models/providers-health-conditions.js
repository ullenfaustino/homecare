const bookshelf = require('../config/bookshelf')

const ProvidersHealthConditions = bookshelf.Model.extend({
  tableName: 'providers_health_conditions',
  hasTimestamps: true,
  softDelete: false,

  value () {
    return this.hasOne('LkupHealthConditions', 'id', 'lkup_health_conditions_id')
  }
})

module.exports = bookshelf.model('ProvidersHealthConditions', ProvidersHealthConditions)
