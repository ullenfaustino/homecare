const dotenv = require('dotenv')

dotenv.load({ path: __dirname + '/../.env' })

module.exports = {
  client: 'mysql',
  connection: process.env.DATABASE_URL || {
    host: process.env.DB_HOST,
    user: process.env.DB_USER,
    password: process.env.DB_PASSWORD,
    database: process.env.DB_NAME,
    port: process.env.DB_PORT || 3306
  }
}
