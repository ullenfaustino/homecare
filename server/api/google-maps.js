const createError = require('http-errors')
const Promise = require('bluebird')
const _ = require('lodash')

const constants = require('../constants')
const dataProvider = require('../models')
const ApiUtils = require('./utils')
const googleMapsClient = require('../config/google-maps')

const geocoderOptions = {
  provider: 'google',
  httpAdapter: 'https',
  apiKey: constants.GOOGLE_API_KEY,
  formatter: null
}
const geocoder = require('node-geocoder')(geocoderOptions)

exports.autocomplete = (req, res, next) => {
  const { q = '' } = req.query

  if (!q) {
    return res.send(ApiUtils.apiResponse(
      []
    ))
  }

  googleMapsClient.placesAutoComplete({
    input: req.query.q,
    components: { country: process.env.COUNTRY || 'ph' }
  }, function (err, response) {
    if (err) {
      return next(err)
    }

    res.send(ApiUtils.apiResponse(
      response.json.predictions
    ))
  })
}

exports.testautocomplete = (req, res, next) => {
  const { search = '' } = req.body

  if (!search) {
    return res.send(ApiUtils.apiResponse(
      []
    ))
  }

  googleMapsClient.placesAutoComplete({
    input: search,
    components: { country: process.env.COUNTRY || 'ph' }
  }, function (err, response) {
    if (err) {
      return next(err)
    }

    res.send(ApiUtils.apiResponse(
      response.json.predictions
    ))
  })
}

exports.reverseGeocoder = (req, res, next) => {
  const { lat, lng } = req.body
  let latlng = { lat: lat, lon: lng }

  geocoder.reverse(latlng, function (err, response) {
    if (err) {
      return next(err)
    }
    res.send(ApiUtils.apiResponse(
      response
    ))
  })
}
