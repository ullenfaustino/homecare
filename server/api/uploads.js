const request = require('request')
const createError = require('http-errors')
const rp = require('request-promise')
const Promise = require('bluebird')
const dataProvider = require('../models')
const ApiUtils = require('./utils')
const moment = require('moment')
const path = require('path')
const constants = require('../constants')
const fs = require('fs')

const UPLOAD_SUB_DIR = 'files'
const DESTINATION_DIR = path.join(constants.UPLOADS_DIR, UPLOAD_SUB_DIR)

const UPLOAD_AVATAR_SUB_DIR = 'avatar'
const DESTINATION_AVATAR_DIR = path.join(constants.UPLOADS_DIR, UPLOAD_AVATAR_SUB_DIR)

exports.removeFile = (req, res, next) => {
  const { name, user } = req.body

  dataProvider.FileUploads.query((qb) => {
    qb.where('name', name)
  }).fetchAll()
  .then((mFiles) => {
    let _mFiles = mFiles.toJSON()
    return Promise.mapSeries(_mFiles, (mFile) => {
      return dataProvider.ProvidersFiles.query((qb2) => {
        qb2.where('user_id', user.id)
        qb2.where('file_id', mFile.id)
      }).fetch()
      .then((mPFile) => {
        if (mPFile) {
          mPFile.destroy()
          return dataProvider.FileUploads.query((qb3) => {
            qb3.where('name', name)
            qb3.where('id', mFile.id)
          }).destroy()
          .then(() => {
            const currPath = DESTINATION_DIR + '/' + mFile.filename
            fs.unlink(currPath, function (err) {
              if (err) return console.log(err)
              console.log('file deleted successfully')
            })
            return res.send({ data: 'file successfully deleted' })
          })
        }

        return res.send({ data: 'file successfully deleted' })
      })
    })
  })
  .catch((err) => next(err))
}

exports.uploadResume = (req, res, next) => {
  const { user_id } = req.headers
  const reqFile = req.file

  ApiUtils.file.processFile(reqFile, { subDir: UPLOAD_SUB_DIR, destinationDir: DESTINATION_DIR })
  .then((fileItem) => {
    const fileParams = Object.assign({}, fileItem, {
      file_type: 'cv_resume'
    })
    return dataProvider.FileUploads.forge(fileParams).save()
      .then((mFile) => {
        return dataProvider.ProvidersFiles.forge({
          user_id: user_id,
          file_id: mFile.get('id')
        }).save()
        .then((mPFiles) => {
          return res.send(fileItem)
        })
        .catch((err) => next(err))
      })
      .catch((err) => next(err))
  })
  .catch((err) => next(err))
}

exports.uploadFile = (req, res, next) => {
  const reqFile = req.file
  const file_type = req.query.fileType
  const user_id = req.query.userId

  ApiUtils.file.processFile(reqFile, { subDir: UPLOAD_SUB_DIR, destinationDir: DESTINATION_DIR })
  .then((fileItem) => {
    const fileParams = Object.assign({}, fileItem, {
      file_type: file_type
    })
    return dataProvider.FileUploads.forge(fileParams).save()
      .then((mFile) => {
        return dataProvider.ProvidersFiles.forge({
          user_id: user_id,
          file_id: mFile.get('id')
        }).save()
        .then((mPFiles) => {
          return res.send(fileItem)
        })
        .catch((err) => next(err))
      })
      .catch((err) => next(err))
  })
  .catch((err) => next(err))
}

exports.avatar = (req, res, next) => {
  const reqFile = req.file
  const { userId } = req.query

  dataProvider.User.query((qb) => {
    qb.where('id', userId)
  }).fetch()
  .then((mUser) => {
    if (!mUser) {
      return next(createError(500, 'User not found.'))
    }

    return new Promise((resolve, reject) => {
      fs.exists(constants.UPLOADS_DIR + mUser.get('avatar'), function (exists) {
        if (exists) {
          fs.unlink(constants.UPLOADS_DIR + mUser.get('avatar'), function (err) {
            if (err) return reject(err)
            resolve(mUser)
          })
        } else {
          resolve(mUser)
        }
      })
    }).then((response) => {
      return ApiUtils.file.processFile(reqFile, { subDir: UPLOAD_AVATAR_SUB_DIR, destinationDir: DESTINATION_AVATAR_DIR })
      .then((fileItem) => {
        return mUser.save({
          avatar: fileItem.url
        }).then((svUser) => {
          return res.send(fileItem)
        }).catch((err) => next(err))
      }).catch((err) => next(err))
    }).catch((err) => err)
  }).catch((err) => next(err))
}

exports.recipientAvatar = (req, res, next) => {
  const reqFile = req.file
  const { recipientId } = req.query

  dataProvider.Recipients.query((qb) => {
    qb.where('id', recipientId)
  }).fetch()
  .then((mRecipient) => {
    if (!mRecipient) {
      return next(createError(500, 'Recipient not found.'))
    }

    return new Promise((resolve, reject) => {
      fs.exists(constants.UPLOADS_DIR + mRecipient.get('avatar'), function (exists) {
        if (exists) {
          fs.unlink(constants.UPLOADS_DIR + mRecipient.get('avatar'), function (err) {
            if (err) return reject(err)
            resolve(mRecipient)
          })
        } else {
          resolve(mRecipient)
        }
      })
    }).then((response) => {
      return ApiUtils.file.processFile(reqFile, { subDir: UPLOAD_AVATAR_SUB_DIR, destinationDir: DESTINATION_AVATAR_DIR })
      .then((fileItem) => {
        return mRecipient.save({
          avatar: fileItem.url
        }).then((svRecipient) => {
          return res.send(fileItem)
        }).catch((err) => next(err))
      }).catch((err) => next(err))
    }).catch((err) => err)
  }).catch((err) => next(err))
}
