const Promise = require('bluebird')
const _ = require('lodash')
const moment = require('moment')

const dataProvider = require('../models')

exports.init = (req, res, next) => {
  Promise.all([
    dataProvider.LkupClientPreferences.forge().fetchAll({ withRelated: ['providerSelected'] }),
    dataProvider.LkupWorkSchedules.forge().fetchAll({ withRelated: ['providerSelected'] }),
    dataProvider.LkupWorkRestrictions.forge().fetchAll({ withRelated: ['providerSelected'] }),
    dataProvider.LkupLanguages.forge().fetchAll(),
    dataProvider.LkupTransportations.forge().fetchAll({ withRelated: ['providerSelected'] }),
    dataProvider.LkupHealthConditions.forge().fetchAll({ withRelated: ['providerSelected'] }),
    dataProvider.LkupServicesPerform.forge().fetchAll({ withRelated: ['providerSelected'] }),
    dataProvider.LkupServicesExperience.forge().fetchAll({ withRelated: ['providerSelected'] }),
    dataProvider.LkupTypeCare.forge().fetchAll({ withRelated: ['providerSelected'] }),
    dataProvider.LkupGeneralStatus.forge().fetchAll({ withRelated: ['recipientSelected'] }),
    dataProvider.LkupMedicalConditions.forge().fetchAll({ withRelated: ['recipientSelected'] })
  ])
  .then((results) => {
    res.send({
      data: {
        account: {
          isInit: true
        },
        client_preferences: results[0],
        work_schedules: results[1],
        work_restrictions: results[2],
        languages: results[3],
        transportations: results[4],
        health_conditions: results[5],
        services_perform: results[6],
        services_experience: results[7],
        type_care: results[8],
        general_status: results[9],
        medical_conditions: results[10],
        serverDateTime: moment().format('YYYY-MM-DD HH:mm:ss')
      }
    })
  }).catch((err) => next(err))
}
