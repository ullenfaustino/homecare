const request = require('request')
const createError = require('http-errors')
const rp = require('request-promise')
const Promise = require('bluebird')
const dataProvider = require('../models')
const uuidv4 = require('uuid/v4')
const ApiUtils = require('./utils')
const moment = require('moment')
const constants = require('./../constants')

const distance = require('google-distance')
distance.apiKey = constants.GOOGLE_API_KEY
const googleMapsClient = require('../config/google-maps')
const getPlaceById = require('../config/google-maps').getPlaceById

exports.addPreferences = (req, res, next) => {
  const { user, location,
          client_preferences = [],
          work_restrictions = [],
          work_schedules = [],
          availability = [] } = req.body

  dataProvider.User.query((qb) => {
    qb.where('id', user.id)
    qb.where('uid', user.uid)
  }).fetch()
  .then((mUser) => {
    if (!mUser) {
      return next(createError(400, 'Invalid User.'))
    }

    return Promise.all([
      // save providers location
      new Promise((resolve, reject) => {
        getPlaceById(location.place_id)
          .then((placeResult) => resolve(placeResult))
          .catch((err) => reject(err))
      }).then((place) => {
        return dataProvider.ProvidersLocation.query((qb) => {
          qb.where('user_id', user.id)
        }).fetch()
        .then((mProvidersLocation) => {
          if (!mProvidersLocation) { // create new
            return dataProvider.ProvidersLocation
            .forge(Object.assign({}, location, {
              user_id: user.id,
              latitude: place.geometry.location.lat,
              longitude: place.geometry.location.lng
            })).save()
            .then((mProvidersLocation) => {
              return mProvidersLocation
            }).catch((err) => next(err))
          } else { // update existing
            return mProvidersLocation.save(Object.assign({}, location, {
              latitude: place.geometry.location.lat,
              longitude: place.geometry.location.lng
            }), { patch: true })
            .then((svProvidersLocation) => {
              return svProvidersLocation
            }).catch((err) => next(err))
          }
        }).catch((err) => next(err))
      }),

      // save providers client preferences
      dataProvider.ProvidersClientPreferences.query((qb) => {
        qb.where('user_id', user.id)
      }).destroy()
      .then(() => {
        return Promise.mapSeries(client_preferences, (client_preference) => {
          return dataProvider.ProvidersClientPreferences
          .forge({
            user_id: user.id,
            lkup_client_preferences_id: client_preference
          }).save()
          .then((mProvidersClientPreferences) => {
            return mProvidersClientPreferences
          }).catch((err) => next(err))
        })
      }).catch((err) => next(err)),

      // save providers work Restrictions
      dataProvider.ProvidersWorkRestrictions.query((qb) => {
        qb.where('user_id', user.id)
      }).destroy()
      .then(() => {
        return Promise.mapSeries(work_restrictions, (work_restriction) => {
          return dataProvider.ProvidersWorkRestrictions
          .forge({
            user_id: user.id,
            lkup_work_restrictions_id: work_restriction
          }).save()
          .then((mProvidersWorkRestrictions) => {
            return mProvidersWorkRestrictions
          }).catch((err) => next(err))
        })
      }).catch((err) => next(err)),

      // save providers work schedules
      dataProvider.ProvidersWorkSchedules.query((qb) => {
        qb.where('user_id', user.id)
      }).destroy()
      .then(() => {
        return Promise.mapSeries(work_schedules, (work_schedule) => {
          return dataProvider.ProvidersWorkSchedules
          .forge({
            user_id: user.id,
            lkup_work_schedules_id: work_schedule
          }).save()
          .then((mProvidersWorkSchedules) => {
            return mProvidersWorkSchedules
          }).catch((err) => next(err))
        })
      }).catch((err) => next(err)),

      // save providers availability
      dataProvider.ProvidersAvailability.query((qb) => {
        qb.where('user_id', user.id)
      }).destroy()
      .then(() => {
        return Promise.mapSeries(availability, (_availability) => {
          return dataProvider.ProvidersAvailability
          .forge(Object.assign({}, _availability, {
            user_id: user.id
          })).save()
          .then((mAvailability) => {
            return mAvailability
          }).catch((err) => next(err))
        })
      }).catch((err) => next(err))
    ])
    .then((results) => {
      return res.send(results)
    }).catch((err) => next(err))
  })
  .catch((err) => next(err))
}

exports.addBasicInfo = (req, res, next) => {
  const {
    user,
    transportations,
    salary_rate,
    authorized_to_work,
    registered_homecare,
    registry_id,
    languages
  } = req.body

  dataProvider.User.query((qb) => {
    qb.where('id', user.id)
    qb.where('uid', user.uid)
  }).fetch()
  .then((mUser) => {
    if (!mUser) {
      return next(createError(400, 'Invalid User.'))
    }

    return Promise.all([
      // update user details
      mUser.save({
        is_authorized_to_work: authorized_to_work,
        is_registered_homecare: registered_homecare,
        registry_id: registry_id
      }, { patch: true })
      .then((svUser) => {
        return svUser
      })
      .catch((err) => next(err)),

      // save providers salary rate
      dataProvider.ProvidersSalaryRate.query((qb) => {
        qb.where('user_id', user.id)
      }).fetch()
      .then((mProvidersSalaryRate) => {
        if (!mProvidersSalaryRate) { // create new
          return dataProvider.ProvidersSalaryRate
          .forge(Object.assign({}, salary_rate, {
            user_id: user.id
          })).save()
          .then((mProvidersSalaryRate) => {
            return mProvidersSalaryRate
          }).catch((err) => next(err))
        } else { // update existing
          return mProvidersSalaryRate.save(salary_rate, { patch: true })
          .then((svProvidersSalaryRate) => {
            return svProvidersSalaryRate
          }).catch((err) => next(err))
        }
      }).catch((err) => next(err)),

      // save providers transportations
      dataProvider.ProvidersTransportations.query((qb) => {
        qb.where('user_id', user.id)
      }).destroy()
      .then(() => {
        return Promise.mapSeries(transportations, (transportation) => {
          return dataProvider.ProvidersTransportations
          .forge({
            user_id: user.id,
            lkup_transportations_id: transportation
          }).save()
          .then((mProvidersTransportations) => {
            return mProvidersTransportations
          }).catch((err) => next(err))
        })
      }).catch((err) => next(err)),

      // save providers languages
      dataProvider.ProvidersLanguages.query((qb) => {
        qb.where('user_id', user.id)
      }).destroy()
      .then(() => {
        return Promise.mapSeries(languages, (language) => {
          return dataProvider.ProvidersLanguages
          .forge({
            user_id: user.id,
            lkup_languages_id: language
          }).save()
          .then((mProvidersLanguages) => {
            return mProvidersLanguages
          }).catch((err) => next(err))
        })
      }).catch((err) => next(err))
    ])
    .then((results) => {
      return res.send(results)
    }).catch((err) => next(err))
  })
  .catch((err) => next(err))
}

exports.addReferencesExperience = (req, res, next) => {
  const {
    user,
    character_references,
    services_perform,
    services_experience,
    health_conditions
  } = req.body

  dataProvider.User.query((qb) => {
    qb.where('id', user.id)
    qb.where('uid', user.uid)
  }).fetch()
  .then((mUser) => {
    if (!mUser) {
      return next(createError(400, 'Invalid User.'))
    }

    return Promise.all([
      // save character references
      dataProvider.ProvidersCharacterReferences.query((qb) => {
        qb.where('user_id', user.id)
      }).fetch()
      .then((mProvidersCharacterReferences) => {
        if (!mProvidersCharacterReferences) { // create new
          return dataProvider.ProvidersCharacterReferences
          .forge(Object.assign({}, character_references, {
            user_id: user.id
          })).save()
          .then((mProvidersCharacterReferences) => {
            return mProvidersCharacterReferences
          }).catch((err) => next(err))
        } else { // update existing
          return mProvidersCharacterReferences
          .save(character_references, { patch: true })
          .then((svProvidersCharacterReferences) => {
            return svProvidersCharacterReferences
          }).catch((err) => next(err))
        }
      }).catch((err) => next(err)),

      // save services perform
      dataProvider.ProvidersServicesPerform.query((qb) => {
        qb.where('user_id', user.id)
      }).destroy()
      .then(() => {
        return Promise.mapSeries(services_perform, (_services_perform) => {
          return dataProvider.ProvidersServicesPerform
          .forge({
            user_id: user.id,
            lkup_services_perform_id: _services_perform
          }).save()
          .then((mProvidersServicesPerform) => {
            return mProvidersServicesPerform
          }).catch((err) => next(err))
        })
      }).catch((err) => next(err)),

      // save services experience
      dataProvider.ProvidersServicesExperience.query((qb) => {
        qb.where('user_id', user.id)
      }).destroy()
      .then(() => {
        return Promise.mapSeries(services_experience, (_services_experience) => {
          return dataProvider.ProvidersServicesExperience
          .forge({
            user_id: user.id,
            lkup_services_experience_id: _services_experience
          }).save()
          .then((mProvidersServicesExperience) => {
            return mProvidersServicesExperience
          }).catch((err) => next(err))
        })
      }).catch((err) => next(err)),

      // save services experience
      dataProvider.ProvidersHealthConditions.query((qb) => {
        qb.where('user_id', user.id)
      }).destroy()
      .then(() => {
        return Promise.mapSeries(health_conditions, (_health_conditions) => {
          return dataProvider.ProvidersHealthConditions
          .forge({
            user_id: user.id,
            lkup_health_conditions_id: _health_conditions
          }).save()
          .then((mProvidersHealthConditions) => {
            return mProvidersHealthConditions
          }).catch((err) => next(err))
        })
      }).catch((err) => next(err))
    ])
    .then((results) => {
      return res.send(results)
    }).catch((err) => next(err))
  })
  .catch((err) => next(err))
}

exports.updateStatusForApproval = (req, res, next) => {
  const {
    user
  } = req.body

  dataProvider.User.query((qb) => {
    qb.where('id', user.id)
    qb.where('uid', user.uid)
  }).fetch()
  .then((mUser) => {
    if (!mUser) {
      return next(createError(400, 'Invalid User.'))
    }

    return mUser.save({
      application_status: 1
    }, { patch: true })
    .then((svUser) => {
      return res.send(svUser)
    })
    .catch((err) => next(err))
  })
  .catch((err) => next(err))
}

exports.updateStatusApproved = (req, res, next) => {
  const {
    user_id
  } = req.body

  dataProvider.User.query((qb) => {
    qb.where('id', user_id)
  }).fetch()
  .then((mUser) => {
    if (!mUser) {
      return next(createError(400, 'Invalid User.'))
    }

    return mUser.save({
      application_status: 2
    }, { patch: true })
    .then((svUser) => {
      return res.send(svUser)
    })
    .catch((err) => next(err))
  })
  .catch((err) => next(err))
}

exports.hireProvider = (req, res, next) => {
  const { seeker_id, provider_id, recipient_id } = req.body
  dataProvider.Hire.forge({
    seeker_id,
    provider_id,
    recipient_id,
    status: 0,
    job_offer_date: moment().format('YYYY-MM-DD hh:mm:ss')
  }).save()
  .then((mHire) => {
    res.send(mHire.toJSON())
  }).catch((err) => next(err))
}

exports.hireProviderByMatching = (req, res, next) => {
  const { seeker_id, provider_id, recipient_id } = req.body
  dataProvider.Hire.forge({
    seeker_id,
    provider_id,
    recipient_id,
    status: 1,
    job_offer_date: moment().format('YYYY-MM-DD hh:mm:ss'),
    hired_date: moment().format('YYYY-MM-DD hh:mm:ss')
  }).save()
  .then((mHire) => {
    res.send(mHire.toJSON())
  }).catch((err) => next(err))
}

exports.acceptJobOffer = (req, res, next) => {
  const { hiring_id } = req.body
  dataProvider.Hire.forge({ id: hiring_id })
  .fetch().then((mHire) => {
    return mHire.save({
      status: 1,
      hired_date: moment().format('YYYY-MM-DD hh:mm:ss')
    }, { patch: true }).then((svHire) => {
      res.send(svHire.toJSON())
    }).catch((err) => next(err))
  }).catch((err) => next(err))
}

exports.findAvailableProvider = (req, res, next) => {
  const { recipient, searchParams } = req.body
  const { health_conditions, work_restrictions, work_schedules, client_preferences } = searchParams

  const fromLatLng = { lat: recipient.location.latitude, lng: recipient.location.longitude }
  dataProvider.ProvidersLocation.query((qb) => {
    qb.leftJoin('users', 'users.id', 'providers_location.user_id')
    qb.where('users.is_activated', 1)
    qb.where('users.application_status', 2)

    if (health_conditions.length > 0) {
      // qb.orWhere(function where () {
      qb.leftJoin('providers_health_conditions', 'providers_health_conditions.user_id', 'users.id')
      qb.leftJoin('lkup_health_conditions', 'lkup_health_conditions.id', 'providers_health_conditions.lkup_health_conditions_id')
      qb.whereIn('lkup_health_conditions.id', health_conditions)
      // })
    }

    if (work_restrictions.length > 0) {
      // qb.orWhere(function where () {
      qb.leftJoin('providers_work_restrictions', 'providers_work_restrictions.user_id', 'users.id')
      qb.leftJoin('lkup_work_restrictions', 'lkup_work_restrictions.id', 'providers_work_restrictions.lkup_work_restrictions_id')
      qb.whereIn('lkup_work_restrictions.id', work_restrictions)
      // })
    }

    if (work_schedules.length > 0) {
      // qb.orWhere(function where () {
      qb.leftJoin('providers_work_schedules', 'providers_work_schedules.user_id', 'users.id')
      qb.leftJoin('lkup_work_schedules', 'lkup_work_schedules.id', 'providers_work_schedules.lkup_work_schedules_id')
      qb.whereIn('lkup_work_schedules.id', work_schedules)
      // })
    }

    if (client_preferences.length > 0) {
      // qb.orWhere(function where () {
      qb.leftJoin('providers_client_preferences', 'providers_client_preferences.user_id', 'users.id')
      qb.leftJoin('lkup_client_preferences', 'lkup_client_preferences.id', 'providers_client_preferences.lkup_client_preferences_id')
      qb.whereIn('lkup_client_preferences.id', client_preferences)
      // })
    }

    qb.groupBy('providers_location.user_id')
    qb.select('providers_location.*')
  })
  .fetchAll()
  .then((providersLocations) => {
    return Promise.map(providersLocations.toJSON(), (location) => {
      const toLatLng = { lat: location.latitude, lng: location.longitude }
      return getDistanceRange(fromLatLng, toLatLng)
      .then((range) => {
        console.log('range', range)
        if ((range.distanceValue / 1000) <= recipient.distanceRange) {
          return location.user_id
        }
        return undefined
      })
      .catch((err) => undefined)
    }).then((nearestProviders) => {
      nearestProviders_ids = nearestProviders.filter((nearestProvider) => (nearestProvider !== undefined))
      return getProviders(nearestProviders_ids).then((availableProviders) => {
        res.send(availableProviders)
      })
    })
  })
}

exports.findNearJob = (req, res, next) => {
  const { distanceRange, fromLatLng } = req.body

  dataProvider.JobPost.query((qb) => {
    qb.leftJoin('recipients', 'recipients.id', 'job_post.recipient_id')
    qb.leftJoin('recipients_location', 'recipients_location.recipient_id', 'job_post.recipient_id')

    qb.where('job_post.status', 0)

    qb.select('*')
    qb.groupBy('recipients.id')
  }).fetchAll()
  .then((mRecipients) => {
    return Promise.map(mRecipients.toJSON(), (recipient) => {
      const toLatLng = { lat: recipient.latitude, lng: recipient.longitude }
      return getDistanceRange(fromLatLng, toLatLng)
      .then((range) => {
        console.log('range', range)
        if ((range.distanceValue / 1000) <= distanceRange) {
          return recipient.id
        }
        return undefined
      })
      .catch((err) => undefined)
    }).then((nearestRecipients) => {
      nearestRecipients_ids = nearestRecipients.filter((nearestRecipient) => (nearestRecipient !== undefined))
      return getRecipients(nearestRecipients_ids).then((recipients) => {
        res.send(recipients)
      })
    })
  })
  .catch((err) => next(err))
}

exports.applyOnJobPost = (req, res, next) => {
  const { recipient_id, seeker_id, provider_id } = req.body

  dataProvider.JobPost.query((qb) => {
    qb.where('seeker_id', seeker_id)
    qb.where('recipient_id', recipient_id)
    qb.where('status', 0)
  })
  .fetch()
  .then((mPost) => {
    if (!mPost) {
      return next(createError(400, 'Job Post not found.'))
    }

    return dataProvider.JobPostApplicant.query((qb) => {
      qb.where('job_post_id', mPost.get('id'))
      qb.where('provider_id', provider_id)
      qb.where('status', 0)
    })
    .fetch()
    .then((mApplicant) => {
      if (mApplicant) {
        return next(createError(400, 'Already applied in job.'))
      }

      return dataProvider.JobPostApplicant.forge({
        job_post_id: mPost.get('id'),
        provider_id: provider_id,
        status: 0,
        application_date: moment().format('YYYY-MM-DD hh:mm:ss')
      }).save()
      .then((svApplicant) => {
        return res.send(svApplicant)
      })
      .catch((err) => next(err))
    })
    .catch((err) => next(err))
  })
  .catch((err) => next(err))
}

exports.finishService = (req, res, next) => {
  const { hiring_id, ratings, comment } = req.body

  dataProvider.Hire.forge({ id: hiring_id })
  .fetch().then((mHire) => {
    if (!mHire) {
      return next(createError(400, 'Invalid hiring id'))
    }

    return mHire.save({
      status: 2,
      finished_date: moment().format('YYYY-MM-DD hh:mm:ss')
    }, { patch: true })
    .tap((svHire) => {
      return dataProvider.JobPost.forge({
        seeker_id: svHire.get('seeker_id'),
        recipient_id: svHire.get('recipient_id'),
        status: 1
      }).fetch().then((mJobPost) => {
        if (!mJobPost) {
          return svHire
        }

        return mJobPost.save({ status: 2 }, { patch: true }).then((svJobPost) => {
          return dataProvider.JobPostApplicant.forge({ job_post_id: svJobPost.get('id'), provider_id: svHire.get('provider_id') })
          .fetch().then((mApplicant) => {
            if (!mApplicant) {
              return svHire
            }

            return mApplicant.save({ status: 2 }, { patch: true }).then((svApplicant) => {
              return svHire
            })
            .catch((err) => svHire)
          })
          .catch((err) => svHire)
        })
        .catch((err) => svHire)
      })
      .catch((err) => svHire)
    })
    .then((svHire) => {
      return dataProvider.Ratings.forge({
        seeker_id: svHire.get('seeker_id'),
        recipient_id: svHire.get('recipient_id'),
        provider_id: svHire.get('provider_id'),
        ratings,
        comment
      }).save()
      .then((mRatings) => {
        return res.send(mRatings)
      })
      .catch((err) => next(err))
    })
    .catch((err) => next(err))
  })
  .catch((err) => next(err))
}

const getDistanceRange = (fromLatLng, toLatLng) => {
  console.log('[getDistanceRange]', fromLatLng, toLatLng)
  return new Promise((resolve, reject) => {
    distance.get(
      {
        index: 1,
        origin: `${fromLatLng.lat},${fromLatLng.lng}`,
        destination: `${toLatLng.lat},${toLatLng.lng}`
      },
    function (err, data) {
      if (err) {
        console.log('[getDistanceRange][err]', err)
        return reject(err)
      }
      resolve(data)
    })
  })
}

const getProviders = (provider_ids) => {
  const providerOption = ApiUtils.providerOption
  return new Promise((resolve, reject) => {
    return Promise.map(provider_ids, (user_id) => {
      return dataProvider.User.forge({ id: user_id })
      .fetch(providerOption)
      .then((user) => {
        if (!user) return undefined
        return user.toJSON()
      }).catch(() => undefined)
    }, { concurrency: 1000000 })
    .then((response) => {
      response = response.filter((item) => (item !== undefined))
      resolve(response)
    })
  })
}

const getRecipients = (recipient_ids) => {
  const recipientOption = ApiUtils.recipientOption
  return new Promise((resolve, reject) => {
    return Promise.map(recipient_ids, (id) => {
      return dataProvider.Recipients.forge({ id })
      .fetch(recipientOption)
      .then((recipient) => {
        if (!recipient) return undefined
        return recipient.toJSON()
      }).catch(() => undefined)
    }, { concurrency: 1000000 })
    .then((response) => {
      response = response.filter((item) => (item !== undefined))
      resolve(response)
    })
  })
}
