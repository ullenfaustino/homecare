const request = require('request')
const createError = require('http-errors')
const rp = require('request-promise')
const Promise = require('bluebird')
const dataProvider = require('../models')
const uuidv4 = require('uuid/v4')
const ApiUtils = require('./utils')
const moment = require('moment')

exports.setGenericSettings = (req, res, next) => {
  const { name, model } = req.body
  const { id } = req.query

  dataProvider[model].query((qb) => {
    qb.where('id', id)
  }).fetch()
  .then((mItem) => {
    if (mItem) { // update
      return mItem.save({ name })
      .then((svItem) => {
        return res.send(svItem)
      })
      .catch((err) => next(err))
    } else {
      return dataProvider[model].forge({ name }).save()
      .then((svItem) => {
        return res.send(svItem)
      })
      .catch((err) => next(err))
    }
  })
  .catch((err) => next(err))
}

exports.deleteGenericSettings = (req, res, next) => {
  const { id, model } = req.body

  dataProvider[model].query((qb) => {
    qb.where('id', id)
  }).fetch()
  .then((mItem) => {
    if (!mItem) {
      return next(createError(500, 'Item Not Found.'))
    }
    return mItem.destroy().then(() => {
      res.send({
        data: 'deleted'
      })
    })
  })
  .catch((err) => next(err))
}
