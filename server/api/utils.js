const path = require('path')
const fs = require('fs')
const url = require('url')
const _ = require('lodash')
const Promise = require('bluebird')
const fse = require('fs-extra')

const constants = require('../constants')
const helpers = require('../helpers')

const formatPagination = (pagination) => {
  return Object.assign(pagination, {
    total: pagination.rowCount
  })
}

const apiResponse = (apiData, status = 200) => {
  if (apiData && apiData.data) {
    return Object.assign({}, apiData, {
      status
    })
  } else {
    return {
      data: apiData,
      status
    }
  }
}

const seekerOption = {
  withRelated: [
    'seekers_search_preferences',
    'seekers_days_pref',
    'seekers_type_care.value',
    'recipients',
    'recipients.general_status.value',
    'recipients.medical_conditions.value',
    'recipients.days',
    'recipients.location',

    'hired.location',
    'hired.client_preferences.value',
    'hired.work_restrictions.value',
    'hired.work_schedules.value',
    'hired.availability',
    'hired.salary_rate',
    'hired.transportations.value',
    'hired.languages.value',
    'hired.character_references',
    'hired.health_conditions.value',
    'hired.services_perform.value',
    'hired.services_experience.value',
    'hired.files.file',
    'hired.ratings.recipient',

    'hired.client.general_status.value',
    'hired.client.medical_conditions.value',
    'hired.client.days',
    'hired.client.location',

    'hired.recipient.general_status.value',
    'hired.recipient.medical_conditions.value',
    'hired.recipient.days',
    'hired.recipient.location',

    'postedJobs.recipient.general_status.value',
    'postedJobs.recipient.medical_conditions.value',
    'postedJobs.recipient.days',
    'postedJobs.recipient.location',

    'postedJobs.applicants.provider.location',
    'postedJobs.applicants.provider.client_preferences.value',
    'postedJobs.applicants.provider.work_restrictions.value',
    'postedJobs.applicants.provider.work_schedules.value',
    'postedJobs.applicants.provider.availability',
    'postedJobs.applicants.provider.salary_rate',
    'postedJobs.applicants.provider.transportations.value',
    'postedJobs.applicants.provider.languages.value',
    'postedJobs.applicants.provider.character_references',
    'postedJobs.applicants.provider.health_conditions.value',
    'postedJobs.applicants.provider.services_perform.value',
    'postedJobs.applicants.provider.services_experience.value',
    'postedJobs.applicants.provider.files.file'
  ]
}
const providerOption = {
  withRelated: [
    'location',
    'client_preferences.value',
    'work_restrictions.value',
    'work_schedules.value',
    'availability',
    'salary_rate',
    'transportations.value',
    'languages.value',
    'character_references',
    'health_conditions.value',
    'services_perform.value',
    'services_experience.value',
    'files.file',
    'ratings.recipient',

    'client.general_status.value',
    'client.medical_conditions.value',
    'client.days',
    'client.location',

    'job_applications.job_post.recipient.general_status.value',
    'job_applications.job_post.recipient.medical_conditions.value',
    'job_applications.job_post.recipient.days',
    'job_applications.job_post.recipient.location',

    'job_applications.job_post.seeker.seekers_search_preferences',
    'job_applications.job_post.seeker.seekers_days_pref',
    'job_applications.job_post.seeker.seekers_type_care.value',
    'job_applications.job_post.seeker.recipients',
    'job_applications.job_post.seeker.recipients.general_status.value',
    'job_applications.job_post.seeker.recipients.medical_conditions.value',
    'job_applications.job_post.seeker.recipients.days',
    'job_applications.job_post.seeker.recipients.location'
  ]
}
const recipientOption = {
  withRelated: [
    'general_status.value',
    'medical_conditions.value',
    'days',
    'location',

    'hired.provider.location',
    'hired.provider.client_preferences.value',
    'hired.provider.work_restrictions.value',
    'hired.provider.work_schedules.value',
    'hired.provider.availability',
    'hired.provider.salary_rate',
    'hired.provider.transportations.value',
    'hired.provider.languages.value',
    'hired.provider.character_references',
    'hired.provider.health_conditions.value',
    'hired.provider.services_perform.value',
    'hired.provider.services_experience.value',
    'hired.provider.files.file',
    'hired.provider.ratings.recipient'
  ]
}

module.exports = {
  parseBrowseQuery: (query) => {
    const {
      page = 1,
      pageSize = 10,

      sortName,
      sortOrder = 'asc',
      searchText = '',
      filterStatus = ''
    } = query
    return {
      page,
      pageSize,
      sortName,
      sortOrder,
      searchText,
      filterStatus
    }
  },

  formatPagination: formatPagination,
  apiResponse: apiResponse,
  apiResponseFetchPage: (result) => {
    return apiResponse({
      data: result.toJSON(),
      pagination: formatPagination(result.pagination)
    })
  },

  file: {
    processFiles: (items, opts = {}) => {
      const { destinationDir, subDir } = opts
      const attrs = [
        'originalname',
        'mimetype',
        'filename',
        'size'
      ]

      return new Promise.mapSeries(items, (item) => {
        const imageUrl = url.resolve('/', [subDir, item.filename].join('/'))
        const fileType = helpers.getFileType(item.mimetype)
        const destinationPath = path.join(destinationDir, item.filename)
        const filePath = item.path

        return new Promise((resolve, reject) => {
          fse.copy(filePath, destinationPath, (err) => {
            if (err) {
              reject(err)
            }

            const __item = Object.assign(
              {},
              _.pick(item, attrs),
              {
                name: item.originalname,
                url: imageUrl,
                // thumbnail_url: helpers.addFilenameSuffix(imageUrl, '_thumb'),
                type: fileType,
                mimetype: item.mimetype
              }
            )

            resolve(__item)
          })
        }).finally(() => {
          // Remove uploaded file from tmp location
          return Promise.promisify(fs.unlink)(item.path)
        })
      })
    },
    processFile: (item, opts = {}) => {
      const { destinationDir, subDir } = opts
      const attrs = [
        'originalname',
        'mimetype',
        'filename',
        'size'
      ]

      const imageUrl = url.resolve('/', [subDir, item.filename].join('/'))
      const fileType = helpers.getFileType(item.mimetype)
      const destinationPath = path.join(destinationDir, item.filename)
      const filePath = item.path

      return new Promise((resolve, reject) => {
        fse.copy(filePath, destinationPath, (err) => {
          if (err) {
            reject(err)
          }

          const __item = Object.assign(
            {},
            _.pick(item, attrs),
            {
              name: item.originalname,
              url: imageUrl,
              // thumbnail_url: helpers.addFilenameSuffix(imageUrl, '_thumb'),
              type: fileType,
              mimetype: item.mimetype
            }
          )

          resolve(__item)
        })
      }).finally(() => {
        // Remove uploaded file from tmp location
        return Promise.promisify(fs.unlink)(item.path)
      })
    }
  },
  api: {
    response: (apiData, status = 200) => {
      if (apiData && apiData.data) {
        return Object.assign({}, apiData, {
          status
        })
      }
      return {
        data: apiData,
        status
      }
    }
  },
  seekerOption,
  providerOption,
  recipientOption
}
