const createError = require('http-errors')
const Promise = require('bluebird')
const bcrypt = require('bcryptjs')
const jwt = require('jsonwebtoken')
const request = require('request')
const _ = require('lodash')

const helpers = require('../helpers')
const constants = require('../constants')
const dataProvider = require('../models')
const helper = require('./../helpers/helper')
// const smarthr = require('./smarthr')

let bcryptGenSalt = Promise.promisify(bcrypt.genSalt),
  bcryptHash = Promise.promisify(bcrypt.hash),
  bcryptCompare = Promise.promisify(bcrypt.compare)

const validatePasswordLength = (password) => {
  return validator.isLength(password, 8)
}

const generatePasswordHash = (password) => {
  // Generate a new salt
  return bcryptGenSalt().then((salt) => {
    // Hash the provided password with bcrypt
    return bcryptHash(password, salt)
  })
}

exports.login = (req, res, next) => {
  const {
    username,
    password
  } = req.body

  // smarthr.getUserProfile(username, password)
  //   .then((profile) => {
  //     if (!profile) {
  //       return next(createError(400, 'Username or password not match.'))
  //     }
  //
  //     const permissions = permitted_modules[profile.user.role]
  //     const sidebars = _.pick(sidebar, permissions)
  //     const user = Object.assign({}, profile.user, { sidebars })
  //     profile = Object.assign({}, profile, { user })
  //
  //     res.json({
  //       data: profile
  //     })
  //   }).catch((err) => {
  //     smarthr.authenticateVPN(username, password)
  //     .then((response) => {
  //       let userObj = response.toJSON()
  //       const { _operator = {} } = userObj
  //
  //       userObj = Object.assign({}, userObj, {
  //         role: 'operator_manager',
  //         name: _operator.name
  //       })
  //       const token = jwt.sign(userObj, 'itssecret_', {
  //         expiresIn: 60 * 60
  //       })
  //
  //       userObj = Object.assign({}, userObj, {
  //         token
  //       })
  //
  //       let profile = {
  //         token,
  //         user: userObj
  //       }
  //
  //       const permissions = permitted_modules['operator_manager']
  //       const sidebars = _.pick(sidebar, permissions)
  //       const usah = Object.assign({}, profile.user, { sidebars, validModules: permissions })
  //
  //       profile = Object.assign({}, profile, { user: usah })
  //
  //       return res.json(helpers.apiResponse({ data: profile }))
  //     }).catch((err) => {
  //       if (err.message === 'User not found') {
  //         return next(createError(400, 'Username or password not match.'))
  //       }
  //
  //       if (err.message === 'Access Denied') {
  //         return next(createError(400, 'User access denied.'))
  //       }
  //       return next(err)
  //     })
  //   })

  dataProvider.User.forge({
    username: username
  }).fetch().then((user) => {
    if (!user) {
      return next(createError(400, 'User not found.'))
    }

    return bcryptCompare(password, user.get('password')).then((matched) => {
      if (!matched) {
        return next(createError(400, 'Authentication failed. Wrong password.'))
      }

      // if user is found and password is right
      // create a token
      var token = jwt.sign(user, 'itssecret_', {
        expiresIn: 60 * 60 * 60
      })

      return helper.formatUser(user.toJSON()).then((formattedUser) => {
        // console.log('USER', formattedUser.toJSON())
        res.json({
          data: {
            token: token,
            user: formattedUser
          }
        })
      })
    })
  }).catch(error => next(error))
}
