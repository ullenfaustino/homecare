const request = require('request')
const createError = require('http-errors')
const rp = require('request-promise')
const Promise = require('bluebird')
const dataProvider = require('../models')
const uuidv4 = require('uuid/v4')
const ApiUtils = require('./utils')
const moment = require('moment')

exports.sendMessage = (req, res, next) => {
  const { sender_id, recipient_id, chatroom_id, message } = req.body

  dataProvider.ChatRoom.forge({ id: chatroom_id })
  .fetch()
  .then((mChatRoom) => {
    if (!mChatRoom) { // create new chatroom
      return dataProvider.ChatRoom.forge({
        ref_id: uuidv4(),
        sender_id,
        recipient_id,
        last_sender_id: sender_id,
        is_read: 1
      }).save()
    } else { // update existing chat
      return mChatRoom.save({
        last_sender_id: sender_id,
        is_read: 1
      }, { patch: true })
    }
  })
  .then((svChatRoom) => {
    console.log('svChatRoom', svChatRoom)
    return dataProvider.Messages.forge({
      chatroom_id: svChatRoom.get('id'),
      sender_id,
      message,
      is_read: 1
    }).save().then((svMessage) => {
      return res.send(ApiUtils.apiResponse(svMessage))
    })
    .catch((err) => next(err))
  })
  .catch((err) => next(err))
}

exports.getMessage = (req, res, next) => {
  const { sender_id, recipient_id } = req.body

  dataProvider.ChatRoom.query((qb) => {
    qb.orWhere(function where () {
      this.where('sender_id', sender_id)
      this.where('recipient_id', recipient_id)
    })
    qb.orWhere(function where () {
      this.where('sender_id', recipient_id)
      this.where('recipient_id', sender_id)
    })
    // console.log('query', qb.toString())
  })
  .fetch({
    withRelated: ['messages.sender', 'sender.location', 'recipient.location']
  })
  .then((mMessages) => {
    if (!mMessages) {
      return next(createError(400, 'No Message Found'))
    }
    res.send(ApiUtils.apiResponse(mMessages))
  })
  .catch((err) => next(err))
}

exports.getMessages = (req, res, next) => {
  const {
    page,
    pageSize,
    sortName,
    sortOrder,
    searchText
  } = ApiUtils.parseBrowseQuery(req.query)
  const { user_id } = req.query

  let options = { page, pageSize, withRelated: ['messages.sender', 'sender.location', 'recipient.location'] }

  dataProvider.ChatRoom.query((qb) => {
    qb.orWhere('sender_id', user_id)
    qb.orWhere('recipient_id', user_id)

    qb.orderBy('updated_at', 'desc')
  })
  .fetchPage(options)
  .then((mMessages) => {
    res.send(ApiUtils.apiResponseFetchPage(mMessages))
  })
  .catch((err) => next(err))
}

exports.markAsRead = (req, res, next) => {
  const { chatroom_id } = req.params

  dataProvider.ChatRoom.forge({ id: chatroom_id })
  .fetch()
  .then((mChatRoom) => {
    if (!mChatRoom) {
      return next(createError(400, 'Invalid ChatRoom'))
    }

    return mChatRoom.save({
      is_read: 0
    }).then((svChatRoom) => {
      return dataProvider.Messages.where({ chatroom_id: svChatRoom.get('id') })
      .save({ is_read: 0 }, { method: 'update', patch: true })
      .then((svMessage) => {
        return res.send(ApiUtils.apiResponse(svMessage))
      })
      .catch((err) => next(err))
    })
    .catch((err) => next(err))
  })
  .catch((err) => next(err))
}
