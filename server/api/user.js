const request = require('request')
const createError = require('http-errors')
const rp = require('request-promise')
const Promise = require('bluebird')
const bcrypt = require('bcryptjs')
const dataProvider = require('../models')
const uuidv4 = require('uuid/v4')
const randtoken = require('rand-token')
const ApiUtils = require('./utils')
const mailer = require('../mailer')
const moment = require('moment')

let bcryptGenSalt = Promise.promisify(bcrypt.genSalt),
  bcryptHash = Promise.promisify(bcrypt.hash),
  bcryptCompare = Promise.promisify(bcrypt.compare)

const generatePasswordHash = (password) => {
  // Generate a new salt
  return bcryptGenSalt().then((salt) => {
    return bcryptHash(password, salt)
  })
}

exports.createAccount = (req, res, next) => {
  const {
    email,
    username,
    password
  } = req.body

  generatePasswordHash(password)
  .then((hashPassword) => {
    dataProvider.User.query((qb) => {
      qb.where('email', email)
      qb.where('username', username)
    })
    .fetch()
    .then((mUser) => {
      if (mUser) {
        return next(createError(500, 'User already exits.'))
      }

      let formattedParams = Object.assign({}, req.body, {
        uid: uuidv4(),
        password: hashPassword,
        birthday: moment(req.body.birthday).format('YYYY-MM-DD')
      })

      return dataProvider.User.forge(formattedParams)
      .save()
      .then((svUser) => {
        // update activation_code in user details
        let activation_code = randtoken.generate(6)
        return svUser.save({ activation_code })
        .then((usvUser) => {
          // send mail verification
          mailer.accountConfirmation_mailer({ user: svUser.toJSON(), orig_password: password, activation_code })
          return res.send(svUser.toJSON())
        })
      })
      .catch((err) => next(err))
    })
    .catch((err) => next(err))
  })
  .catch((err) => next(err))
}

exports.findByUid = (req, res, next) => {
  const { uid } = req.params

  const adminOption = {}
  const seekerOption = ApiUtils.seekerOption
  const providerOption = ApiUtils.providerOption

  dataProvider.User.query((qb) => {
    qb.where('uid', uid)
  }).fetch()
  .then((mUser) => {
    if (!mUser) {
      return next(createError(400, 'Invalid User.'))
    }

    return mUser.fetch((mUser.get('user_type') == 0 ? adminOption : ((mUser.get('user_type') == 1) ? providerOption : seekerOption)))
    .then((_mUser) => {
      res.send(_mUser)
    })
    .catch((err) => next(err))
  })
  .catch((err) => next(err))
}

exports.findById = (req, res, next) => {
  const { id } = req.params

  const adminOption = {}
  const seekerOption = ApiUtils.seekerOption
  const providerOption = ApiUtils.providerOption

  dataProvider.User.query((qb) => {
    qb.where('id', id)
  }).fetch()
  .then((mUser) => {
    if (!mUser) {
      return next(createError(400, 'Invalid User.'))
    }

    return mUser.fetch((mUser.get('user_type') == 0 ? adminOption : ((mUser.get('user_type') == 1) ? providerOption : seekerOption)))
    .then((_mUser) => {
      res.send(_mUser)
    })
    .catch((err) => next(err))
  })
  .catch((err) => next(err))
}

exports.findUsers = (req, res, next) => {
  // const { uid } = req.params
  const { user_type, status } = req.query

  const options = {
    withRelated: [
      'location',
      'client_preferences.value',
      'work_restrictions.value',
      'work_schedules.value',
      'availability',
      'salary_rate',
      'transportations.value',
      'languages.value',
      'character_references',
      'health_conditions.value',
      'services_perform.value',
      'services_experience.value',
      'files.file',
      'seekers_search_preferences',
      'seekers_days_pref',
      'seekers_type_care.value',
      'recipients',
      'recipients.general_status.value',
      'recipients.medical_conditions.value',
      'recipients.days',
      'recipients.location'
    ]
  }

  dataProvider.User.query((qb) => {
    // qb.where('uid', uid)
    if (user_type) {
      if (user_type === 'users') {
        qb.where('user_type', 1).orWhere('user_type', 2)
      } else {
        qb.where('user_type', user_type)
      }
    }

    if (status) {
      qb.where('application_status', status)
    }
  }).fetchAll(options)
  .then((mUser) => {
    if (!mUser) {
      return next(createError(400, 'Invalid User.'))
    }

    res.send(mUser)
  })
  .catch((err) => next(err))
}

exports.activateAccount = (req, res, next) => {
  const { uid, activation_code } = req.body

  dataProvider.User.query((qb) => {
    qb.where('uid', uid)
  }).fetch()
  .then((mUser) => {
    if (!mUser) {
      return next(createError(400, 'Invalid User.'))
    }

    if (mUser.get('activation_code') == activation_code) {
      mUser.save({
        is_activated: 1,
        activation_code: null,
        activated_at: moment().format('YYYY-MM-DD HH:mm:ss')
      }).then((svUser) => {
        return res.send(svUser.toJSON())
      }).catch((err) => next(err))
    } else {
      return next(createError(400, 'Invalid Activation Code.'))
    }
  }).catch((err) => next(err))
}

exports.findUserRecipients = (req, res, next) => {
  const { uid } = req.params
  const options = ApiUtils.recipientOption
  dataProvider.Recipients.query((qb) => {
    qb.where('user_id', uid)
  }).fetchAll(options)
  .then((mRecipients) => {
    res.send(mRecipients)
  }).catch((err) => next(err))
}

exports.findRecipients = (req, res, next) => {
  const { search } = req.query
  const options = ApiUtils.recipientOption
  dataProvider.Recipients.query((qb) => {
    if (search && search.length > 0) {
      qb.where(function where () {
        this.orWhere('name', 'like', `%${search}%`)
      })
    }
  }).fetchAll(options)
  .then((mRecipients) => {
    res.send(mRecipients)
  }).catch((err) => next(err))
}

exports.findRecipient = (req, res, next) => {
  const { recipientId } = req.params
  const options = ApiUtils.recipientOption
  dataProvider.Recipients.query((qb) => {
    qb.where('id', recipientId)
  }).fetch(options)
  .then((mRecipient) => {
    res.send(mRecipient)
  }).catch((err) => next(err))
}

exports.findSeekers = (req, res, next) => {
  const { search } = req.query
  const seekerOption = ApiUtils.seekerOption

  dataProvider.User.query((qb) => {
    qb.where('user_type', 2)

    qb.where(function where () {
      this.orWhere('first_name', 'like', `%${search}%`)
      this.orWhere('last_name', 'like', `%${search}%`)
    })
  }).fetchAll(seekerOption)
  .then((mUser) => {
    if (!mUser) {
      return next(createError(400, 'Invalid User.'))
    }

    res.send(mUser)
  })
  .catch((err) => next(err))
}

exports.findProviders = (req, res, next) => {
  const { search } = req.query
  const providerOption = ApiUtils.providerOption

  dataProvider.User.query((qb) => {
    qb.where('user_type', 1)

    qb.where(function where () {
      this.orWhere('first_name', 'like', `%${search}%`)
      this.orWhere('last_name', 'like', `%${search}%`)
    })
  }).fetchAll(providerOption)
  .then((mUser) => {
    if (!mUser) {
      return next(createError(400, 'Invalid User.'))
    }

    res.send(mUser)
  })
  .catch((err) => next(err))
}
