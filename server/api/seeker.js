const request = require('request')
const createError = require('http-errors')
const rp = require('request-promise')
const Promise = require('bluebird')
const dataProvider = require('../models')
const uuidv4 = require('uuid/v4')
const ApiUtils = require('./utils')
const moment = require('moment')

const googleMapsClient = require('../config/google-maps')
const getPlaceById = require('../config/google-maps').getPlaceById

exports.addPreferences = (req, res, next) => {
  const { user,
          gender,
          require_car,
          start_duty,
          rate,
          rate_amount,
          hours_per_day,
          days = [],
          type_of_care = []
         } = req.body

  dataProvider.User.query((qb) => {
    qb.where('id', user.id)
    qb.where('uid', user.uid)
  }).fetch()
  .then((mUser) => {
    if (!mUser) {
      return next(createError(400, 'Invalid User.'))
    }

    return Promise.all([
      // update user details
      mUser.save({
        is_require_car: require_car,
        gender: gender
      }),

      // update search preferences
      dataProvider.SeekersSearchPreferences.query((qb) => {
        qb.where('user_id', user.id)
      }).fetch()
      .then((mSeeker) => {
        if (mSeeker) {
          return mSeeker.save({
            hours_per_day,
            rate,
            rate_amount,
            start_duty: moment(start_duty).format('YYYY-MM-DD')
          }, { patch: true })
        } else {
          dataProvider.SeekersSearchPreferences.forge({
            user_id: user.id,
            hours_per_day,
            rate,
            rate_amount,
            start_duty: moment(start_duty).format('YYYY-MM-DD')
          }).save()
          .then((newPref) => {
            return newPref
          })
        }
      }),

      // update days preferences
      dataProvider.SeekersDays.query((qb) => {
        qb.where('user_id', user.id)
      }).destroy()
      .then(() => {
        return Promise.mapSeries(days, (day) => {
          return dataProvider.SeekersDays
          .forge({
            user_id: user.id,
            day
          }).save()
          .then((mSeekersDays) => {
            return mSeekersDays
          })
        })
      }),

      // update type care
      dataProvider.SeekersTypeCare.query((qb) => {
        qb.where('user_id', user.id)
      }).destroy()
      .then(() => {
        return Promise.mapSeries(type_of_care, (_type_of_care) => {
          return dataProvider.SeekersTypeCare
          .forge({
            user_id: user.id,
            lkup_type_care_id: _type_of_care
          }).save()
          .then((mSeekersTypeCare) => {
            return mSeekersTypeCare
          })
        })
      })
    ])
    .then((results) => {
      return res.send(results)
    })
    .catch((err) => next(err))
  })
  .catch((err) => next(err))
}

exports.addRecipient = (req, res, next) => {
  const { user,
          recipient_name,
          gender,
          birthday,
          start_duty,
          hours_per_day,
          location,
          days = [],
          medical_conditions = [],
          general_status = []
         } = req.body

  dataProvider.User.query((qb) => {
    qb.where('id', user.id)
    // qb.where('uid', user.uid)
  }).fetch()
  .then((mUser) => {
    if (!mUser) {
      return next(createError(400, 'Invalid User.'))
    }

    return new Promise((Resolve, Reject) => {
      dataProvider.Recipients.query((qb) => {
        qb.where('id', user.recipient_id)
      }).fetch().then((recipient) => {
        if (!recipient) {
          dataProvider.Recipients.forge({
            user_id: user.id,
            name: recipient_name,
            gender,
            hours_per_day,
            birthday: moment(birthday).format('YYYY-MM-DD'),
            start_duty: moment(start_duty).format('YYYY-MM-DD')
          }).save()
          .then((svRecipient) => Resolve(svRecipient))
          .catch((err) => Reject(err))
        } else {
          recipient.save({
            name: recipient_name,
            gender,
            hours_per_day,
            birthday: moment(birthday).format('YYYY-MM-DD'),
            start_duty: moment(start_duty).format('YYYY-MM-DD')
          }).then((svRecipient) => Resolve(svRecipient))
          .catch((err) => Reject(err))
        }
      })
    }).then((mRecipient) => {
      return Promise.all([
        // save seekers location
        new Promise((resolve, reject) => {
          getPlaceById(location.place_id)
            .then((placeResult) => resolve(placeResult))
            .catch((err) => reject(err))
        }).then((place) => {
          return dataProvider.RecipientsLocation.query((qb) => {
            qb.where('recipient_id', mRecipient.get('id'))
          }).fetch()
          .then((mRecipientsLocation) => {
            if (!mRecipientsLocation) { // create new
              return dataProvider.RecipientsLocation
              .forge(Object.assign({}, location, {
                recipient_id: mRecipient.get('id'),
                latitude: place.geometry.location.lat,
                longitude: place.geometry.location.lng
              })).save()
              .then((mRecipientsLocation) => {
                return mRecipientsLocation
              }).catch((err) => next(err))
            } else { // update existing
              return mRecipientsLocation.save(Object.assign({}, location, {
                latitude: place.geometry.location.lat,
                longitude: place.geometry.location.lng
              }), { patch: true })
              .then((svRecipientsLocation) => {
                return svRecipientsLocation
              }).catch((err) => next(err))
            }
          }).catch((err) => next(err))
        }),

        // update Recipient General Status
        dataProvider.RecipientsGeneralStatus.query((qb) => {
          qb.where('recipient_id', mRecipient.get('id'))
        }).destroy()
        .then(() => {
          return Promise.mapSeries(general_status, (_general_status) => {
            return dataProvider.RecipientsGeneralStatus
            .forge({
              recipient_id: mRecipient.get('id'),
              lkup_general_status_id: _general_status
            }).save()
            .then((mRecipientGeneralStatus) => {
              return mRecipientGeneralStatus
            })
          })
        }),

        // update days preferences
        dataProvider.RecipientsDays.query((qb) => {
          qb.where('recipient_id', mRecipient.get('id'))
        }).destroy()
        .then(() => {
          return Promise.mapSeries(days, (day) => {
            return dataProvider.RecipientsDays
            .forge({
              recipient_id: mRecipient.get('id'),
              day
            }).save()
            .then((mRecipientDays) => {
              return mRecipientDays
            })
          })
        }),

        // update Recipient Medical Conditions
        dataProvider.RecipientsMedicalConditions.query((qb) => {
          qb.where('recipient_id', mRecipient.get('id'))
        }).destroy()
        .then(() => {
          return Promise.mapSeries(medical_conditions, (medical_condition) => {
            return dataProvider.RecipientsMedicalConditions
            .forge({
              recipient_id: mRecipient.get('id'),
              lkup_medical_conditions_id: medical_condition
            }).save()
            .then((mRecipientMedicalConditions) => {
              return mRecipientMedicalConditions
            })
          })
        })
      ]).then((response) => {
        return res.send(mRecipient)
      })
      .catch((err) => next(err))
    })
    .catch((err) => next(err))
  })
  .catch((err) => next(err))
}

exports.postJob = (req, res, next) => {
  const { seeker_id, recipient_id } = req.body

  dataProvider.JobPost.query((qb) => {
    qb.where('seeker_id', seeker_id)
    qb.where('recipient_id', recipient_id)
    qb.where('status', 0)
  }).fetch().then((mPost) => {
    if (mPost) {
      return next(createError(400, 'Already Exists.'))
    }

    return dataProvider.JobPost.forge({
      seeker_id,
      recipient_id,
      status: 0,
      post_date: moment().format('YYYY-MM-DD hh:mm:ss')
    })
    .save()
    .then((svPost) => {
      res.send(svPost)
    })
    .catch((err) => next(err))
  })
  .catch((err) => next(err))
}

exports.hireApplicant = (req, res, next) => {
  const { provider_id, seeker_id, recipient_id, job_post_id, application_id } = req.body
  dataProvider.JobPost.forge({ id: job_post_id, status: 0 }).fetch()
  .then((post) => {
    if (!post) {
      return next(createError(400, 'Posted Job not found.'))
    }

    return dataProvider.Hire.forge({
      provider_id,
      seeker_id,
      recipient_id,
      status: 1,
      job_offer_date: moment().format('YYYY-MM-DD hh:mm:ss'),
      hired_date: moment().format('YYYY-MM-DD hh:mm:ss')
    }).save()
    .then((mHire) => {
      return post.save({
        status: 1,
        hired_date: moment().format('YYYY-MM-DD hh:mm:ss')
      }, { patch: true })
      .then((svPost) => {
        return dataProvider.JobPostApplicant.forge({ id: application_id }).fetch()
        .then((application) => {
          if (!application) {
            return next(createError(400, 'Applicant not found.'))
          }

          return application.save({
            status: 1,
            hired_date: moment().format('YYYY-MM-DD hh:mm:ss')
          }, { patch: true })
          .then((svApplication) => {
            res.send(mHire)
          })
          .catch((err) => next(err))
        })
        .catch((err) => next(err))
      })
      .catch((err) => next(err))
    })
    .catch((err) => next(err))
  })
  .catch((err) => next(err))
}
