const path = require('path')
const exec = require('child_process').exec
const fs = require('fs')
const cheerio = require('cheerio')
const Promise = require('bluebird')
const uuid = require('uuid')
const google = require('googleapis')

const helpers = require('../helpers')
const constants = require('../constants')

const SUB_DIR = 'files'
const DESTINATION_DIR = path.join(constants.UPLOADS_DIR, SUB_DIR)

const htmlToODT = (targetPath, outdir, filename) => {
  return new Promise((resolve, reject) => {
    exec(`soffice --headless --convert-to odt --outdir ${outdir} ${targetPath}`, function (err, stdout, stderr) {
      if (err) {
        reject(err)
      }

      resolve(path.join(outdir, `${filename}.odt`))
    })
  })
}

const odtToPDF = (odtPath, outdir, filename) => {
  return new Promise((resolve, reject) => {
    exec(`soffice --headless --convert-to pdf:writer_web_pdf_Export --outdir ${outdir} ${odtPath}`, function (err, stdout, stderr) {
      if (err) {
        reject(err)
      }

      resolve(path.join(outdir, `${filename}.pdf`))
    })
  })
}

const htmlToPDF = (htmlPath, outdir, filename) => {
  const htmlOutputPath = path.join(outdir, `${filename}.pdf`)
  return new Promise((resolve, reject) => {
    exec(`prince ${htmlPath} -o ${htmlOutputPath}`, function (err, stdout, stderr) {
      if (err) {
        reject(err)
      }

      resolve(htmlOutputPath)
    })
  })
  // return htmlToODT(htmlPath, outdir, filename).then((odtPath) => {
  //   return odtToPDF(odtPath, outdir, filename)
  // })
}

const htmlStrToPDF = (options = {}) => {
  const {
    htmlStr = '',
    headerStr = '',
    footerStr = '',
    filename = 'filename'
  } = options

  const folderName = uuid.v4()
  return new Promise((resolve, reject) => {
    const htmlPath = path.join(DESTINATION_DIR, `${folderName}.html`)
    const writeStream = fs.createWriteStream(htmlPath, {
      encoding: 'UTF-8'
    })
    writeStream.on('finish', function () {
      resolve(htmlPath)
    })
    writeStream.on('error', (err) => {
      reject(err)
    })

    let $ = cheerio.load(htmlStr)
    // let $header = cheerio.load(headerStr)
    // let $footer = cheerio.load(footerStr)

    if (headerStr != '') {
      $(`<div title="header">
          <p>Yey</p>
      </div>`).prependTo('body')
      // $('div[title="header"]').html('<p>Yey</p>')
      // let $header = cheerio.load(headerStr)
      // const headerBody = $header.html()
    }

    if (footerStr != '') {
      // $main('title="header"').html(footerStr)
      // let $header = cheerio.load(headerStr)
      // const headerBody = $header.html()

      $(`<div title="footer">
          <p>Footer</p>
      </div>`).appendTo('body')
    }
    writeStream.write(htmlStr)
    writeStream.end()
  }).then((htmlPath) => {
    return htmlToPDF(htmlPath, DESTINATION_DIR, folderName)
  })
}

// const htmlToPDF = (targetPath, outdir) => {
//   return new Promise((resolve, reject) => {
//     exec(`soffice --headless --convert-to pdf --outdir ${outdir} ${targetPath}`, function (err, stdout, stderr) {
//       if (err) {
//         reject(err)
//       }
//
//       var filename = importedFile.filename.replace(path.extname(importedFile.filename), '.html')
//
//       fs.readFile(path.join(DESTINATION_DIR, filename), 'utf8', function (err, htmlStr) {
//         if (err) {
//           reject(err)
//         }
//
//         resolve(htmlStr)
//       })
//     })
//   })
// }

module.exports = {
  htmlToPDF,
  htmlStrToPDF
}
