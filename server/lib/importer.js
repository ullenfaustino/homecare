const path = require('path')
const exec = require('child_process').exec
const fs = require('fs')
const cheerio = require('cheerio')
const Promise = require('bluebird')
const uuid = require('uuid')
const google = require('googleapis')

const helpers = require('../helpers')
const constants = require('../constants')

const SUB_DIR = 'files'
const DESTINATION_DIR = path.join(constants.UPLOADS_DIR, SUB_DIR)

const googleDrive = (importedFile) => {
  const key = require(__dirname + '/../google-cf52fb3c6e7b.json')
  const jwtClient = new google.auth.JWT(
    key.client_email,
    null,
    key.private_key,
    [
      'https://www.googleapis.com/auth/drive'
    ],
    null
  )

  return new Promise(function (resolve, reject) {
    if (!importedFile) {
      resolve('')
    }

    jwtClient.authorize(function (err, tokens) {
      if (err) {
        reject(err)
      }

      const drive = google.drive({ version: 'v3', auth: jwtClient })

      drive.files.create({
        resource: {
          name: `Contract Management Template - ${importedFile.originalname}`,
          mimeType: 'application/vnd.google-apps.document'
        },
        media: {
          mimeType: importedFile.mimetype,
          body: fs.createReadStream(importedFile.path) // read streams are awesome!
        }
      }, (err, file) => {
        if (err) {
          reject(err)
        }

        if (!file) {
          resolve('')
        }

        const generatedUUID = uuid.v4()
        const destPath = __dirname + '/../' + generatedUUID + '.html'
        const destWriteStream = fs.createWriteStream(destPath)

        drive.files.export({
          fileId: file.id,
          mimeType: 'text/html'
        })
        .on('end', function () {
          destWriteStream.end()
          fs.readFile(destPath, 'utf8', function (err, htmlStr) {
            resolve(htmlStr)
          })
        })
        .on('error', function (err) {
          if (err) {
            reject(err)
          }
        })
        .pipe(destWriteStream)
      })
    })
  })
}

const sofficeImport = (importedFile) => {
  return new Promise((resolve, reject) => {
    exec(`soffice --headless --convert-to "html:XHTML Writer File:UTF8" --outdir ${DESTINATION_DIR} ${importedFile.path}`, function (err, stdout, stderr) {
    // exec(`soffice --headless --convert-to "html" --outdir ${DESTINATION_DIR} ${importedFile.path}`, (err, stdout, stderr) => {
      if (err) {
        reject(err)
      }

      var filename = importedFile.filename.replace(path.extname(importedFile.filename), '.html')

      fs.readFile(path.join(DESTINATION_DIR, filename), 'utf8', function (err, htmlStr) {
        if (err) {
          reject(err)
        }

        resolve(htmlStr)
      })
    })
  })
}

const wordToHtml = (uploadedFile, type = 'google') => {
  if (type === 'soffice') {
    return sofficeImport(uploadedFile)
  } else {
    return googleDrive(uploadedFile)
  }
}
module.exports = {
  wordToHtml
}
