const nodemailer = require('nodemailer')
const Promise = require('bluebird')
const Handlebars = require('handlebars')
const _ = require('lodash')
const fs = require('fs')
const path = require('path')
const debug = require('debug')('app:mailer')
const request = require('request')

const transporter = nodemailer.createTransport({
  host: process.env.SMTP_HOST || 'smtp.zoho.com',
  // port: parseInt(process.env.SMTP_PORT, 10),
  port: process.env.SMTP_PORT || 587,
  // secure: true,
  auth: {
    user: process.env.SMTP_USER || 'support@ourcarenetwork.com',
    pass: process.env.SMTP_PASS || 'Carenetwork2017'
  }
})

const defaultOptions = {
  from: '"Care Network System" <support@ourcarenetwork.com>',
  to: 'ullen.d.faustino@gmail.com'
}

// const generateHtml = (type, data) => {
//   const views = {
//     bounce: '../views/email/bounce.handlebars'
//   }
//   const source = fs.readFileSync(path.join(__dirname, views[type]), 'utf-8')
//   const template = Handlebars.compile(source)
//   return template(data)
// }
//
// const mailer = (type = 'bounce', data = {}, __options = {}) => {
//   let options = Object.assign({}, defaultOptions, __options)
//   options.html = generateHtml(type, data)
//
//   transporter.sendMail(options, (err, info) => {
//     if (err) {
//       console.log('err', err)
//     }
//     console.log('info', info)
//   })
// }

const sendMail = (__options = {}) => {
  const options = Object.assign({}, defaultOptions, __options)
  return new Promise(function (resolve, reject) {
    transporter.sendMail(options, (error, info) => {
      if (error) {
        console.log('error', error)
      }
      if (info) {
        debug(`Email sent to ${info.accepted}`)
      }
      resolve(info)
    })
  })
}

module.exports = {
  // mailer,
  sendMail
}
