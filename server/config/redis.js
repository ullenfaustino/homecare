const redis = require('redis')

const __conf = {
  host: process.env.REDIS_DB_HOST,
  port: process.env.REDIS_DB_PORT,
  prefix: process.env.REDIS_DB_PREFIX
}
const client = redis.createClient(__conf)

module.exports = client
module.exports.__conf = __conf
