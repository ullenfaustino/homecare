const Promise = require('bluebird')
const constants = require('../constants')
const googleMapsClient = require('@google/maps').createClient({
  // key: 'AIzaSyD5u1pdUPm1U1rg78yT5lQVoIr1VTX4l64'
  key: constants.GOOGLE_API_KEY
})

module.exports = googleMapsClient

function getPlaceById (placeid) {
  return new Promise(function (resolve, reject) {
    googleMapsClient.place({
      placeid
    }, function (err, response) {
      if (err) {
        reject(err)
      }

      resolve(response.json.result)
    })
  })
}
module.exports.getPlaceById = getPlaceById
