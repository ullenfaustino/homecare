exports.up = function (knex, Promise) {
  return new Promise.all([
    knex.schema.createTable('job_post_applicant', (t) => {
      t.increments('id')
      t.integer('job_post_id').unsigned().defaultsTo(0)
      t.integer('provider_id').unsigned().defaultsTo(0)
      t.integer('status')// 0 pending, 1 accepted, 2 completed, 3 rejected

      t.dateTime('application_date')
      t.dateTime('hired_date')

      t.timestamps()
      t.string('created_by', 150)
      t.string('updated_by', 150)

      // soft delete
      t.specificType('is_deleted', 'tinyint').unsigned().defaultsTo(0)
      t.dateTime('deleted_at')
      t.integer('deleted_by').unsigned().defaultsTo(0)
    })
  ])
}

exports.down = function (knex, Promise) {
  return new Promise.all([
    knex.schema.dropTable('job_post_applicant')
  ])
}
