exports.up = function (knex, Promise) {
  return new Promise.all([
    knex.schema.createTable('recipients', (t) => {
      t.increments('id')
      t.integer('user_id').unsigned().defaultsTo(0)
      t.string('name', 250)
      t.specificType('gender', 'tinyint').unsigned().defaultsTo(0)
      t.dateTime('birthday')
      t.integer('hours_per_day')
      t.dateTime('start_duty')
      t.string('avatar', 250)

      t.timestamps()
      t.string('created_by', 150)
      t.string('updated_by', 150)

      // soft delete
      t.specificType('is_deleted', 'tinyint').unsigned().defaultsTo(0)
      t.dateTime('deleted_at')
      t.integer('deleted_by').unsigned().defaultsTo(0)
    })
  ])
}

exports.down = function (knex, Promise) {
  return new Promise.all([
    knex.schema.dropTable('recipients')
  ])
}
