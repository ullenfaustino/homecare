exports.up = function (knex, Promise) {
  return new Promise.all([
    knex.schema.createTable('hire', (t) => {
      t.increments('id')
      t.integer('provider_id').unsigned().defaultsTo(0)
      t.integer('seeker_id').unsigned().defaultsTo(0)
      t.integer('recipient_id').unsigned().defaultsTo(0)
      t.integer('status')// 1 pending, 2 accepted, 3 completed, 4 rejected

      t.dateTime('job_offer_date')
      t.dateTime('hired_date')
      t.dateTime('finished_date')

      t.timestamps()
      t.string('created_by', 150)
      t.string('updated_by', 150)

      // soft delete
      t.specificType('is_deleted', 'tinyint').unsigned().defaultsTo(0)
      t.dateTime('deleted_at')
      t.integer('deleted_by').unsigned().defaultsTo(0)
    })
  ])
}

exports.down = function (knex, Promise) {
  return new Promise.all([
    knex.schema.dropTable('hire')
  ])
}
