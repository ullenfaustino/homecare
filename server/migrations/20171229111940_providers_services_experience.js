exports.up = function (knex, Promise) {
  return new Promise.all([
    knex.schema.createTable('providers_services_experience', (t) => {
      t.increments('id')
      t.integer('user_id').unsigned().defaultsTo(0)
      t.integer('lkup_services_experience_id').unsigned().defaultsTo(0)

      t.timestamps()
      t.string('created_by', 150)
      t.string('updated_by', 150)

      // soft delete
      t.specificType('is_deleted', 'tinyint').unsigned().defaultsTo(0)
      t.dateTime('deleted_at')
      t.integer('deleted_by').unsigned().defaultsTo(0)
    })
  ])
}

exports.down = function (knex, Promise) {
  return new Promise.all([
    knex.schema.dropTable('providers_services_experience')
  ])
}
