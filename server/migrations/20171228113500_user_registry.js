exports.up = function (knex, Promise) {
  return Promise.all([
    knex.schema.table('users', function (t) {
      t.specificType('is_authorized_to_work', 'tinyint').unsigned().defaultsTo(0)
      t.specificType('is_registered_homecare', 'tinyint').unsigned().defaultsTo(0)
      t.string('registry_id', 250)
    })
  ])
}

exports.down = function (knex, Promise) {
  return Promise.all([
    knex.schema.table('users', function (t) {
      t.dropColumn('is_authorized_to_work')
      t.dropColumn('is_registered_homecare')
      t.dropColumn('registry_id')
    })
  ])
}
