exports.up = function (knex, Promise) {
  return new Promise.all([
    knex.schema.createTable('chatroom', (t) => {
      t.increments('id')
      t.string('ref_id')
      t.integer('sender_id').unsigned().defaultsTo(0)
      t.integer('recipient_id').unsigned().defaultsTo(0)
      t.integer('last_sender_id').unsigned().defaultsTo(0)
      t.specificType('is_read', 'tinyint').unsigned().defaultsTo(0)

      t.timestamps()
      t.string('created_by', 150)
      t.string('updated_by', 150)

      // soft delete
      t.specificType('is_deleted', 'tinyint').unsigned().defaultsTo(0)
      t.dateTime('deleted_at')
      t.integer('deleted_by').unsigned().defaultsTo(0)
    })
  ])
}

exports.down = function (knex, Promise) {
  return new Promise.all([
    knex.schema.dropTable('chatroom')
  ])
}
