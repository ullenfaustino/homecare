exports.up = function (knex, Promise) {
  return new Promise.all([
    knex.schema.createTable('lkup_transportations', (t) => {
      t.increments('id')
      t.string('name', 500)

      t.timestamps()
      t.string('created_by', 150)
      t.string('updated_by', 150)

      // soft delete
      t.specificType('is_deleted', 'tinyint').unsigned().defaultsTo(0)
      t.dateTime('deleted_at')
      t.integer('deleted_by').unsigned().defaultsTo(0)
    }).then(() => {
      const defaults = [
        { name: 'Walking' },
        { name: 'Cycling' }
      ]

      return knex('lkup_transportations').insert(defaults)
    })
  ])
}

exports.down = function (knex, Promise) {
  return new Promise.all([
    knex.schema.dropTable('lkup_transportations')
  ])
}
