exports.up = function (knex, Promise) {
  return new Promise.all([
    knex.schema.createTable('job_post', (t) => {
      t.increments('id')
      t.integer('seeker_id').unsigned().defaultsTo(0)
      t.integer('recipient_id').unsigned().defaultsTo(0)
      t.integer('status')// 0 pending, 1 accepted, 2 completed, 3 rejected

      t.dateTime('post_date')
      t.dateTime('application_date')
      t.dateTime('hired_date')

      t.timestamps()
      t.string('created_by', 150)
      t.string('updated_by', 150)

      // soft delete
      t.specificType('is_deleted', 'tinyint').unsigned().defaultsTo(0)
      t.dateTime('deleted_at')
      t.integer('deleted_by').unsigned().defaultsTo(0)
    })
  ])
}

exports.down = function (knex, Promise) {
  return new Promise.all([
    knex.schema.dropTable('job_post')
  ])
}
