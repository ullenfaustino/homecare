exports.up = function (knex, Promise) {
  return new Promise.all([
    knex.schema.createTable('lkup_services_experience', (t) => {
      t.increments('id')
      t.string('name', 500)

      t.timestamps()
      t.string('created_by', 150)
      t.string('updated_by', 150)

      // soft delete
      t.specificType('is_deleted', 'tinyint').unsigned().defaultsTo(0)
      t.dateTime('deleted_at')
      t.integer('deleted_by').unsigned().defaultsTo(0)
    }).then(() => {
      const defaults = [
        { name: 'Sample 1 services experience from database' },
        { name: 'Sample 2 services experience from database' },
        { name: 'Sample 3 services experience from database' }
      ]

      return knex('lkup_services_experience').insert(defaults)
    })
  ])
}

exports.down = function (knex, Promise) {
  return new Promise.all([
    knex.schema.dropTable('lkup_services_experience')
  ])
}
