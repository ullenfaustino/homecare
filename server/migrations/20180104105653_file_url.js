exports.up = function (knex, Promise) {
  return Promise.all([
    knex.schema.table('file_uploads', function (t) {
      t.string('url', 500)
    })
  ])
}

exports.down = function (knex, Promise) {
  return Promise.all([
    knex.schema.table('file_uploads', function (t) {
      t.dropColumn('url')
    })
  ])
}
