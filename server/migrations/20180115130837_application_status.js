exports.up = function (knex, Promise) {
  return Promise.all([
    knex.schema.table('users', function (t) {
      t.specificType('application_status', 'tinyint').unsigned().defaultsTo(0) // 0 pending: 1 for verification: 2 approved: 3 denied
    })
  ])
}

exports.down = function (knex, Promise) {
  return Promise.all([
    knex.schema.table('users', function (t) {
      t.dropColumn('application_status')
    })
  ])
}
