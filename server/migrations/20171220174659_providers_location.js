exports.up = function (knex, Promise) {
  return new Promise.all([
    knex.schema.createTable('providers_location', (t) => {
      t.increments('id')
      t.integer('user_id').unsigned().defaultsTo(0)
      t.string('place_id', 250)
      t.string('google_place_id', 250)
      t.string('description', 500)
      t.string('label', 500)
      t.string('reference', 500)
      t.string('latitude', 50)
      t.string('longitude', 50)

      t.timestamps()
      t.string('created_by', 150)
      t.string('updated_by', 150)

      // soft delete
      t.specificType('is_deleted', 'tinyint').unsigned().defaultsTo(0)
      t.dateTime('deleted_at')
      t.integer('deleted_by').unsigned().defaultsTo(0)
    })
  ])
}

exports.down = function (knex, Promise) {
  return new Promise.all([
    knex.schema.dropTable('providers_location')
  ])
}
