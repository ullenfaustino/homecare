
exports.up = function (knex, Promise) {
  return new Promise.all([
    knex.schema.createTable('users', (t) => {
      t.increments('id')
      t.string('uid', 150)
      t.string('email', 150)
      t.string('username', 150)
      t.string('password', 150)
      t.string('token', 150)
      t.specificType('user_type', 'tinyint').unsigned().defaultsTo(1) // 1 care Provider, 2 care Seeker
      t.string('first_name', 250)
      t.string('middle_name', 250)
      t.string('last_name', 250)
      t.specificType('gender', 'tinyint').unsigned().defaultsTo(0) // 0 is female, 1 is male
      t.string('avatar', 250)
      t.string('zip_code', 50)
      t.string('contact_no', 150)
      t.dateTime('birthday')

      t.timestamps()
      t.string('created_by', 150)
      t.string('updated_by', 150)

      t.specificType('is_activated', 'tinyint').unsigned().defaultsTo(0)
      t.string('activation_code', 250)
      t.dateTime('activated_at')

      // soft delete
      t.specificType('is_deleted', 'tinyint').unsigned().defaultsTo(0)
      t.dateTime('deleted_at')
      t.integer('deleted_by').unsigned().defaultsTo(0)
    }).then(() => {
      const defaults = [
        {
          uid: '7af1f4d8-b617-4984-9a4c-2053c40f62c3',
          email: 'ullen.d.faustino@gmail.com',
          username: 'admin',
          password: '$2a$10$jnklpLIx.YtUl7MAPvZ5Huae5aYcPab0hRJmaJfrSkEuuiIxOnnB6',
          user_type: 0,
          first_name: 'Admin',
          middle_name: 'Admin',
          last_name: 'Admin',
          zip_code: 1234,
          contact_no: '123456789',
          created_at: '2017-12-19 14:28:43',
          is_activated: 1,
          activated_at: '2017-12-19 14:30:02'
        },
        {
          uid: '7af1f4d8-b617-4984-9a4c-1053c40f62c2',
          email: 'ullen.d.faustino@gmail.com',
          username: 'provider',
          password: '$2a$10$jnklpLIx.YtUl7MAPvZ5Huae5aYcPab0hRJmaJfrSkEuuiIxOnnB6',
          user_type: 1,
          first_name: 'Sample',
          middle_name: 'Care',
          last_name: 'Provider',
          zip_code: 2100,
          contact_no: '123456789',
          created_at: '2017-12-19 14:28:43',
          is_activated: 1,
          activated_at: '2017-12-19 14:30:02'
        },
        {
          uid: '7af1f4d8-b617-4984-9a4c-1053c40f62c4',
          email: 'ullen.d.faustino@gmail.com',
          username: 'seeker',
          password: '$2a$10$jnklpLIx.YtUl7MAPvZ5Huae5aYcPab0hRJmaJfrSkEuuiIxOnnB6',
          user_type: 2,
          first_name: 'Sample',
          middle_name: 'Care',
          last_name: 'Seeker',
          zip_code: 2100,
          contact_no: '123456789',
          created_at: '2017-12-19 14:28:43',
          is_activated: 1,
          activated_at: '2017-12-19 14:30:02'
        }
      ]

      return knex('users').insert(defaults)
    })
  ])
}

exports.down = function (knex, Promise) {
  return new Promise.all([
    knex.schema.dropTable('vehicles')
  ])
}
