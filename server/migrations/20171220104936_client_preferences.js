exports.up = function (knex, Promise) {
  return new Promise.all([
    knex.schema.createTable('lkup_client_preferences', (t) => {
      t.increments('id')
      t.string('name', 500)

      t.timestamps()
      t.string('created_by', 150)
      t.string('updated_by', 150)

      // soft delete
      t.specificType('is_deleted', 'tinyint').unsigned().defaultsTo(0)
      t.dateTime('deleted_at')
      t.integer('deleted_by').unsigned().defaultsTo(0)
    }).then(() => {
      const defaults = [
        { name: 'Male or Female' },
        { name: 'Elderlies' },
        { name: 'Children' },
        { name: 'W/ physical disabilities' },
        { name: 'Ambulatory' }
      ]

      return knex('lkup_client_preferences').insert(defaults)
    })
  ])
}

exports.down = function (knex, Promise) {
  return new Promise.all([
    knex.schema.dropTable('lkup_client_preferences')
  ])
}
