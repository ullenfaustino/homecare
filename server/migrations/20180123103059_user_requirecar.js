exports.up = function (knex, Promise) {
  return Promise.all([
    knex.schema.table('users', function (t) {
      t.specificType('is_require_car', 'tinyint').unsigned().defaultsTo(0)
    })
  ])
}

exports.down = function (knex, Promise) {
  return Promise.all([
    knex.schema.table('users', function (t) {
      t.dropColumn('is_require_car')
    })
  ])
}
