exports.up = function (knex, Promise) {
  return new Promise.all([
    knex.schema.createTable('lkup_work_restrictions', (t) => {
      t.increments('id')
      t.string('name', 500)

      t.timestamps()
      t.string('created_by', 150)
      t.string('updated_by', 150)

      // soft delete
      t.specificType('is_deleted', 'tinyint').unsigned().defaultsTo(0)
      t.dateTime('deleted_at')
      t.integer('deleted_by').unsigned().defaultsTo(0)
    }).then(() => {
      const defaults = [
        { name: 'Work w/ moderate lifting' },
        { name: 'Work w/ heavy lifting' },
        { name: 'No driving' },
        { name: 'Work w/ pets' },
        { name: 'Work w/ smokers' }
      ]

      return knex('lkup_work_restrictions').insert(defaults)
    })
  ])
}

exports.down = function (knex, Promise) {
  return new Promise.all([
    knex.schema.dropTable('lkup_work_restrictions')
  ])
}
