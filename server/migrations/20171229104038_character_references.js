exports.up = function (knex, Promise) {
  return new Promise.all([
    knex.schema.createTable('providers_character_references', (t) => {
      t.increments('id')
      t.integer('user_id').unsigned().defaultsTo(0)

      t.string('reference_name_1', 250)
      t.string('reference_contact_1', 250)

      t.string('reference_name_2', 250)
      t.string('reference_contact_2', 250)

      t.string('reference_name_3', 250)
      t.string('reference_contact_3', 250)

      t.timestamps()
      t.string('created_by', 150)
      t.string('updated_by', 150)

      // soft delete
      t.specificType('is_deleted', 'tinyint').unsigned().defaultsTo(0)
      t.dateTime('deleted_at')
      t.integer('deleted_by').unsigned().defaultsTo(0)
    })
  ])
}

exports.down = function (knex, Promise) {
  return new Promise.all([
    knex.schema.dropTable('providers_character_references')
  ])
}
