exports.up = function (knex, Promise) {
  return new Promise.all([
    knex.schema.createTable('file_uploads', (t) => {
      t.increments('id')

      t.string('name')
      t.string('file_type')
      t.string('originalname')
      t.string('filename')
      t.string('size')

      t.string('mimetype', 50)
      t.string('type', 50)

      t.timestamps()
      t.string('created_by', 150)
      t.string('updated_by', 150)

      // soft delete
      t.specificType('is_deleted', 'tinyint').unsigned().defaultsTo(0)
      t.dateTime('deleted_at')
      t.integer('deleted_by').unsigned().defaultsTo(0)
    })
  ])
}

exports.down = function (knex, Promise) {
  return new Promise.all([
    knex.schema.dropTable('file_uploads')
  ])
}
