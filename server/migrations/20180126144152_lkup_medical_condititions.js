exports.up = function (knex, Promise) {
  return new Promise.all([
    knex.schema.createTable('lkup_medical_conditions', (t) => {
      t.increments('id')
      t.string('name', 500)

      t.timestamps()
      t.string('created_by', 150)
      t.string('updated_by', 150)

      // soft delete
      t.specificType('is_deleted', 'tinyint').unsigned().defaultsTo(0)
      t.dateTime('deleted_at')
      t.integer('deleted_by').unsigned().defaultsTo(0)
    }).then(() => {
      const defaults = [
        { name: 'Sample Medical Condition 1' },
        { name: 'Sample Medical Condition 2' },
        { name: 'Sample Medical Condition 3' },
        { name: 'Sample Medical Condition 4' },
        { name: 'Sample Medical Condition 5' }
      ]

      return knex('lkup_medical_conditions').insert(defaults)
    })
  ])
}

exports.down = function (knex, Promise) {
  return new Promise.all([
    knex.schema.dropTable('lkup_medical_conditions')
  ])
}
