exports.up = function (knex, Promise) {
  return new Promise.all([
    knex.schema.createTable('lkup_type_care', (t) => {
      t.increments('id')
      t.string('name', 500)

      t.timestamps()
      t.string('created_by', 150)
      t.string('updated_by', 150)

      // soft delete
      t.specificType('is_deleted', 'tinyint').unsigned().defaultsTo(0)
      t.dateTime('deleted_at')
      t.integer('deleted_by').unsigned().defaultsTo(0)
    }).then(() => {
      const defaults = [
        { name: 'Sample Type care 1' },
        { name: 'Sample Type care 2' },
        { name: 'Sample Type care 3' }
      ]

      return knex('lkup_type_care').insert(defaults)
    })
  ])
}

exports.down = function (knex, Promise) {
  return new Promise.all([
    knex.schema.dropTable('lkup_type_care')
  ])
}
